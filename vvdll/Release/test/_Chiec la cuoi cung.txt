Hẳn nhiều saganor biết câu chuyện cảm động về bức vẽ tranh cuối cùng, chiếc lá cuối cùng thể hiện tinh thần vượt lên trên mọi nghiệt ngã của cuộc sống, làm việc quên mình mang lại niềm vui và hạnh phúc cho người khác. Xin chia sẻ truyện ngắn "Chiếc lá cuối cùng" của O. Henry, bản dịch của Nguyễn Cao Nguyên. 

Tại một quận nhỏ nằm về hướng tây công trường Washington, đường sá chạy rối loạn, quanh co, tròng tréo nhau tạo thành nhiều khu nhỏ, thường được gọi là những "cái ổ". Những "cái ổ" này cong lượn, hình thù thật kỳ dị. Một con đường cứ tự cắt mình ra một hoặc hai lần. Có chàng nghệ sĩ phát ngôn rằng đường sá như vậy cũng có cái... lợi của nó. Giả sử như có một tên đòi nợ đến thu tiền cọ tiền sơn các thân chủ họa sĩ ở đây, lòng vòng qua những con đường như vậy cuối cùng chỉ để tìm thấy chính... mình, với hai bàn tay không. 

Vì vậy mà các nghệ sĩ tìm đến, lang thang trong cái làng cổ Greenwich để tìm thuê những căn nhà có những chiếc cổng xây theo kiểu Hà Lan từ thế kỷ thứ mười tám, cửa sổ quay về hướng bắc với giá thuê thật hạ Rồi họ mang vào những chiếc thùng bằng thiết, vài cái dĩa mòn lỉn kiếm được ở Đại Lộ Thứ Sáu và cứ như vậy cái làng cổ mặc nhiên trở thành một "thuộc địa" của đám nghệ sĩ. 

Sue và Johnsy có một xưỡng vẽ ở lầu trên cùng của một căn nhà gạch ba từng thấp lè tè. Johnsy là tên gọi ở nhà của Joanna. Một người từ Main, một người từ California, họ gặp nhau tại quán cơm bình dân trên đường Số Tám, cùng yêu thích hội họa, cùng mê món rau díp trộn, tương đắc đến nỗi cột tay áo mướn chung với nhau một xưởng vẽ. 

Đó là hồi tháng Năm. Vào tháng mười một thì trời phái một ông khách lạnh lùng mà các ngài bác sĩ thường gọi là Bệnh Viêm Phổi đến khủng bố khu "thuộc địa", thò cái bàn tay lạnh ngắt, sờ chỗ này một chút, vỗ chỗ kia một phát. Khu vực phía đông bị ảnh hưởng nặng nề với khá nhiều người bệnh, nhưng bước chân của ông khách chậm dần khi bước vào cái mê cung chật hẹp rêu phong của khu "thuộc địa" 

Ông khách Viêm Phổi không phải là hạng người mà bạn thường gọi là ông bạn già hào hiệp. Cô Johnsy gốc Cali nhỏ bé, liễu yếu đó thật ra không phải là đối thủ xứng tay của lão già tay đỏ điếm đàng, nhưng lão vẫn đập cô ta tận tình. Vì vậy mà cô gái nằm xụi lơ, bất động trên chiếc giường sắt, mắt nhìn vào bức tường gạch của căn nhà kế cận xuyên qua khung cửa sổ nhỏ. 

Một bữa sáng nọ, ông bác sĩ mời Sue ra ngoài hành lang, nhướng đôi mày rậm: 

"Cô ta chỉ có chừng một phần..., để coi, phần mười cơ hội sống sót". Vừa nói ông vừa rảy rảy cái nhiệt độ kế. "Đó là vì cô ta không có chí cầu sinh. Cái cảnh bệnh nhân sắp hàng chờ chết chớ không chịu uống thuốc chắc phải làm cho các thầy chế thuốc xấu hổ. Cô bạn nhỏ của cô đã hạ quyết tâm không chịu bình phục rồi. Cô ta có mơ ước gì trong đầu không?" 

Sue đáp: 

"Cô ấy... muốn một ngày nào đó sẽ vẽ Vịnh Naples." 

"Vẽ? Chậc! Cô ta có một cái gì đáng giá hơn trong đầu để nghĩ tới không, một người đàn ông chẳng hạn?" 

"Đàn ông?" Sue lập lại với một giọng mũi khịt khịt. "Đàn ông mà đáng giá thật..., à... không, thưa bác sĩ, không có những chuyện như vậy." 

"Chà, như vậy thì tệ thật!". Bác sĩ nói. "Tôi sẽ rán hết sức làm những gì mà khoa học có thể làm được. Nhưng hễ khi nào bệnh nhân của tôi bắt đầu nghĩ tới chiếc xe chở quan tài và đám đô tùy thì tôi trừ đi một nửa cái hiệu quả của thuốc men. Nếu cô có thể làm cho cô ấy thắc mắc một chút gì về mùa đông năm tới thì tôi có thể hứa với cô là cái cơ hội sống sót là một trên năm, còn không thì một trên mười." 

Sau khi ông bác sĩ bỏ đi, Sue trở vào phòng làm việc và khóc đến nhão chiếc khăn giấy Nhật Bản. Sau đó cô vác cái giá vẽ vào phòng Johnsy, miệng ba hoa những câu bông đùa chọc vui. 

Johnsy nằm im lìm bất động dưới tấm chăn phủ giường, mặt hướng về phía cửa sổ. Ngỡ là bạn đang ngủ, Sue ngưng những lời đùa giỡn. 

Cô ta sắp xếp giá vẽ và bắt đầu minh họa cho một truyện đăng báo. Những họa sĩ trẻ dọn đường đến với nghệ thuật bằng cách minh họa cho các truyện trên báo, cũng như các nhà văn trẻ dọn đường đến với văn chương bằng ngòi bút. 

Lúc Sue đang mặc áo quần, mang mắt kiếng cho một nhân vật anh hùng, một chàng kỵ sĩ miền Idaho, thì cô nghe một thứ âm thanh rầm rì, lập đi lập lại nhiều lần. Cô ta chạy vội lại bên giường người bệnh. 

Johnsy đang mở tròn mắt, nhìn ra ngoài cửa sổ và đếm. Đếm ngược. 

"Mười hai." một lát sau "Mười một", "Mười", rồi "Chín"; rồi "Tám" và "Bảy" gần như liền nhau. 

Sue nhìn ra ngoài cửa sổ với vẻ bồn chồn lo lắng. Có gì ở ngoài đó để đếm đâu? Chỉ có một khoảng sân trống trơn buồn thảm và cái tường gạch của căn nhà cách đó hai mươi thước. Một giây nho già, thật già, gân guốc và cằn cỗi bò lên gần nửa bức tường. Những cơn gió lạnh mùa thu vừa qua làm lá rụng gần hết chỉ còn trơ lại mấy nhánh bám lủng lẳng vào bức tường gạch đổ nát. 

Sue hỏi: 

"Đếm gì vậy, nhỏ?" 

"Sáu." Johnsy nói, gần như thì thầm. "Nó bắt đầu rụng nhanh rồi. Ba ngày trước còn cả trăm, làm mình đếm đến nhức đầu. Nhưng bây giờ thì dễ rồi. Lại một chiếc nữa rụng kìa. Chỉ còn có năm chiếc." 

"Năm chiếc gì hở nhỏ? Nói cho Sudie biết với." 

"Lá! Lá trên giây nho. Khi chiếc lá nho cuối cùng rơi xuống thì mình cũng sẽ đi luôn. Mình nghiệm ra điều đó ba hôm rồi. Bác sĩ không nói cho bạn biết như vậy sao?" 

"Ồ, mình chưa bao giờ nghe được những chuyện vô lý như vậy." Sue gắt bạn, giọng pha một chút giễu cợt. "Cái giây nho già đó thì có liên quan gì đến sức khỏe của bạn? Bạn thường vẫn thích cái giây nho đó lắm mà, cái cô hư này. Đừng có suy nghĩ dại dột như vậy. Ồ, hồi sáng này bác sĩ nói là bạn có nhiều cơ hội bình phục lắm, để nhớ chính xác coi ổng nói sao, ổng nói, mười ăn một. Như vậy thì chắc ăn như bắp rồi, chắc ăn như mình chạy xe trên đường phố hay đi bộ qua các cao ốc ở New York vậy. Thôi, rán ăn chút súp nhá, rồi để cho con nhỏ Sudie này tiếp tục vẽ tranh bán kiếm chút tiền còm mua rượu cho con đang bệnh, mua thịt heo xay về ăn cho đỡ đói nhá." 

"Bạn không cần phải mua thêm rượu nho." Johnsy nói mà mắt vẫn dán chặt ra ngoài cửa sổ. "Một chiếc nữa rụng. Không, mình không muốn ăn súp đâu. Vậy là chỉ còn lại bốn chiếc. Mình muốn nhìn thấy chiếc lá cuối cùng rơi trước khi trời tối. Rồi mình sẽ đi theo..." 

Sue cúi xuống bên mình Johnsy: 

"Johnsy, nhắm mắt lại và hứa là sẽ không nhìn ra ngoài cửa sổ nữa cho đến khi nào mình vẽ xong. Mình phải nộp mấy bản vẽ này vào ngày mai. Mình cần chút ánh sáng, nếu không thì bức tranh sẽ tối hù." 

Johnsy hỏi, vẻ lạnh lùng: 

"Bạn không vẽ ở phòng khác được à?" 

"Mình cần phải ở đây với bạn." Sue đáp. "Ngoài ra, mình không muốn bạn cứ nhìn vào mấy cái lá nho chết tiệt đó." 

"Vẽ xong thì nhớ cho biết liền." Johnsy nói và nhắm mắt lại, nằm bất động như một bức tượng bị đổ. "Tại mình muốn nhìn thấy chiếc lá cuối cùng rơi xuống. Mình chờ mệt quá rồi. Mình suy nghĩ mệt quá rồi. Mình muốn buông xuôi tất cả, để cho nó cứ tự do rơi xuống, rơi xuống như những chiếc lá tội nghiệp kia." 

"Rán ngủ đi." Sue nói. "Mình phải kêu ông Behrman lên ngồi làm mẫu một người thợ mỏ nghèo. Mình sẽ đi chừng một phút. Đừng cử động gì cả cho đến khi mình trở lại, nhé." 

Lão Behrman là một họa sĩ ở ngay dưới tầng trệt. Lão đã quá sáu mươi tuổi, có một bộ râu chảy dài từ một cái đầu to tướng xuống một thân hình nhỏ thó giống như bộ râu dài của thánh Moses trong tranh của Michael Angelo. Behrman là một nghệ sĩ thất bại. Lão cầm cọ đã bốn mươi năm rồi mà chẳng vẽ được một bức tranh nào cho ra hồn. Lúc nào lão cũng làm như sắp sửa thực hiện một tác phẩm lớn, nhưng thực sự chưa bao giờ bắt tay vào việc. Suốt mấy năm liền lão chẳng vẽ vời gì ngoài một vài bức tranh quảng cáo lem nhem. Lão kiếm sống bằng nghề làm người mẫu cho các họa sĩ trẻ trong khu thuộc địa vì các họa sĩ trẻ này không đủ tiền mướn các người mẫu chuyên nghiệp. Lão uống rượu như hũ chìm, và nói hoài về cái tác phẩm lớn sắp hoàn thành của mình. Đối với nhiều người thì lão là một lão già nhỏ con mà gân lắm, thường mạt sát thậm tệ cái tánh ủy mị yếu đuối của người khác và tự coi mình như một người dẫn dắt, bảo vệ hai cô họa sĩ trẻ trong cái xưỡng vẽ trên lầu. 

Sue tìm thấy lão Behrman trong căn phòng nhỏ tối hù của lão ở lầu dưới, người nồng nặc mùi rượu dâu. Trong một góc phòng, tấm vải trên khung vẽ vẫn còn trống trơn nằm chờ hơn hai mươi năm rồi đường cọ đầu tiên của cái tác phẩm lớn của lão. Cô kể cho lão nghe về sự tưởng tượng lạ lùng của Johnsy, về nỗi lo là cô ta vốn đã mong manh, yếu đuối, khi nghị lực của cô cạn đi rồi thì cô sẽ trôi tuột đi như chiếc lá lìa cành. 

Lão Behrman, cặp mắt đỏ ngầu chiếu thẳng ra trước, la lên với vẻ chế nhạo, khinh bỉ cái óc tưởng tượng ngu ngốc đó. 

"Dà." Lão khóc. "Thế dan có người ngu nào nghĩ như con nhỏ đó là mình sẽ chít theo những chiếc lá nho khi nó lìa cành không? Lão chưa từng nghe chuyện đó bao giờ. Không, Lão không làm cái người mẫu khổ hạnh ngu ngốc đó cho cô đâu, hiểu chưa. Sao cô lại để cho những chiện ngu xuẩn như dậy chạy dô đầu con nhỏ đó như dậy hả? Ờ, cái con nhỏ Johnsy tội nghiệp." 

"Cô ta bệnh nặng và yếu lắm." Sue đáp. "Cơn sốt làm cho tâm trí cô ta trở nên hổn loạn, tưởng tượng ra những việc hoang đường. Được rồi. Ông Mehrman, nếu ông không muốn làm người mẫu cho tôi thì cũng không sao. Tôi biết ông chỉ là một lão già khó tánh, ba hoa, ngồi lê đôi mách." 

"Cô đúng là cái giống đờn bà!" Lão Behrman la lên. "Ai nói là lão không chịu ngồi mẫu? Đi đi. Lão sẽ lên gặp cô. Suốt nửa tiếng đồng hồ vừa rồi lão đã giải thích là lão sẳn sàng ngồi mẫu mà. Dà, đây đâu phải là chỗ cho những người tốt như cô Johnsy nằm bịnh chớ. Một ngày nào đó lão sẽ dẽ xong tác phẩm lớn, rồi chúng ta sẽ rời bỏ chỗ này. Dà, đúng dậy." 

Lúc hai người lên đến nơi thì Johnsy đang ngủ. Sue kéo tấm màn cửa sổ cho kín lại và ra dấu cho lão Behrman sang phòng kế. Cả hai cùng lo lắng nhìn mấy cọng giây nho bên ngoài cửa sổ. Rồi cả hai cùng nhìn nhau một lúc lâu không nói lời nào. Trời mưa dai dẳng và có chút tuyết rơi. Lão Behrman trong chiếc áo xanh cũ kỷ, bắt đầu ngồi lên trên cái ấm nước đặt lộn ngược giả làm tảng đá để làm mẫu một người thợ mỏ nghèo khó. 

Sáng hôm sau, khi Sue thức dậy sau chừng một giờ ngủ mê, cô bắt gặp Johnsy với cặp mắt mở lớn, đờ đẫn đang nhìn chăm chăm vào một cái bóng màu xanh. 

"Kéo cửa sổ lên. Mình muốn nhìn một chút." Cô thì thầm ra lệnh. 

Sue làm theo một cách uể oải. 

Nhưng, ô kìa, sau một cơn mưa dữ dội và những cơn gió vật vả suốt đêm dài, một chiếc lá nho vẫn còn hiện rõ mồn một trên bức tường gạch. Đó là chiếc lá nho cuối cùng còn sót lại. Cuống lá vẫn còn cái màu xanh đậm nhưng ven rìa lá răng cưa đã nhuốm màu vàng úa, treo lơ lững như thách đố trên một giây nho cách mặt đất chừng hai chục thước. 

"Đó là chiếc lá cuối cùng." Johnsy nói. "Mình cứ tưởng nó chắc phải rụng hồi đêm rồi. Mình nghe gió dữ lắm. Chắc nó sẽ rụng trong ngày hôm nay và mình cũng sẽ rụng theo." 

"Nhỏ, nhỏ ơi." Sue vừa nói vừa dúi khuôn mặt mệt mõi của mình vào chiếc gối. "Nếu bạn không muốn nghĩ đến bản thân mình thì xin hãy nghĩ tới Sue đây. Sue phải làm sao đây?" 

Nhưng Johnsy không trả lời. Cái sinh vật cô đơn khủng khiếp nhất trên thế gian này phải là cái linh hồn khi đã sẳn sàng đi vào một cuộc hành trình miên viễn, đầy huyền bí. Sự tưởng tượng hoang đường đó dường như càng ám ảnh cô gái mạnh mẽ hơn khi mối liên hệ với bạn bè và với trái đất này của cô cứ lỏng lẽo dần. 

Ngày tàn dần, và dù trời nhá nhem tranh tối tranh sáng, họ vẫn có thể nhìn thấy chiếc lá nho duy nhất còn bám vào cuống giây nổi bật trên bức tường. Rồi đêm đến và những cơn gió bắc lụn dần, trong khi cơn mưa vẫn còn đập vào khung cửa sổ, và rơi lộp bộp dưới mái hiên. 

Khi trời vừa hừng sáng, Johnsy thẩn thờ đòi kéo tấm màn che lên. 

Chiếc lá vẫn còn đó. 

Johnsy nằm yên lặng nhìn một lúc khá lâu. Rồi cô cất tiếng gọi Sue đang khuấy nồi súp gà trên cái lò gas. 

"Mình thật là hư quá, Sudie ạ." Johnsy nói. "Một cái gì đó đã làm cho chiếc lá cuối cùng kia không chịu rụng làm mình cảm thấy xấu hổ quá, mình thật là tệ quá. Muốn chết đi thì quả là một điều ngu xuẩn. Bạn mang cho mình một chút súp đi, một ít sữa có pha chút rượu nho nữa; nhưng khoan, mang cho mình một cái gương nhỏ trước đã, rồi kê giùm mình mấy chiếc gối để mình có thể ngồi dậy nhìn bạn làm bếp." 

Một giờ sau cô ta bảo: 

"Sudie này, mình hy vọng có ngày sẽ vẽ Vịnh Naples." 

Chiều hôm đó bác sĩ đến thăm. Sue cáo lỗi với bạn để đi ra hành lang gặp bác sĩ khi ông ta ra về. 

"Có triển vọng tốt." Bác sĩ vừa nói vừa cầm lấy bàn tay gầy guộc và run rẩy của Sue. "Rán chăm sóc thì cô ấy sẽ khỏi. Bây giờ tôi phải đi thăm một bệnh nhân khác dưới lầu, tên là Behrman, nghe nói là một nghệ sĩ gì đó. Cũng bị viêm phổi. Ông ta già, yếu thành ra bị nặng lắm. Không có hy vọng gì; nhưng hôm nay sẽ đưa ông ta vào bệnh viện để được tiện nghi hơn." 

Hôm sau bác sĩ bảo Sue: 

"Cô ấy qua cơn nguy rồi. Cô đã thắng. Giờ chỉ cần ăn uống và tịnh dưỡng là đủ." 

Chiều hôm đó, Sue đến bên Johnsy đang nằm trên giường bệnh, thắt lại chiếc khăn len quàng cổ màu thật xanh, quàng tay ôm lấy bạn, rồi ôm chiếc gối, tần mần tẩn mẩn... 

"Mình có chuyện này muốn nói với bạn, con chuột bạch à." Cô ta nói. "Cụ Behrman đã mất trong bệnh viện ngày hôm nay vì bị viêm phổi. Cụ mắc bệnh chỉ có hai ngày. Hôm đầu tiên ông quản gia tìm thấy cụ nằm trong phòng có vẻ đau đớn lắm. Giày dép, quần áo cụ ướt sũng và lạnh như đá. Họ không tưởng tượng ra được cụ ấy đi đâu, làm gì trong một đêm gió mưa khủng khiếp như vậy. Sau đó họ tìm thấy chiếc đèn lồng vẫn còn cháy sáng, một chiếc thang được kéo lê ra từ trong kho, mấy chiếc cọ còn dính sơn và một hộp sơn pha trộn hai thứ màu xanh và vàng. Và bạn thử nhìn ra ngoài cửa sổ kia, nhìn vào chiếc lá nho cuối cùng trên bức tường đó. Bạn không cảm thấy ngạc nhiên là tại sao nó chẳng lung lay động đậy gì trong cơn gió à? Nhỏ ơi, nó chính là cái tác phẩm lớn của cụ Behrman đó. Cụ đã vẽ nó vào cái đêm chiếc lá nho cuối cùng rơi xuống."

O. Henry, bản dịch của Nguyễn Cao Nguyên
