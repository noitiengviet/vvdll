#include "concatenator.h"
#include "directsound.h"
#include "silenceadder.h" // VT
/**
* Constructor
*/
VietVoice::WavePlayer::WavePlayer ()
{}

/**
* Play a wave represented by the given data.
*
* Parameters:
*
* vector<unsigned char> spokenData:  the wave data
*/
void VietVoice::WavePlayer::Play (std::vector<unsigned char> & spokenData)
{
			
	WAVEFORMATEX format;

	format.wFormatTag = WAVE_FORMAT_PCM;
	format.nChannels = 1; 
	format.nSamplesPerSec = 44100; 
	format.nAvgBytesPerSec = 88200; 
	format.nBlockAlign = 2; 
	format.wBitsPerSample =16; 
	format.cbSize = sizeof (format); 

	waveOutOpen( &_hWave, WAVE_MAPPER, &format, NULL, NULL, CALLBACK_NULL); 
	WAVEHDR wavehdr;
	wavehdr.lpData = (LPSTR)&spokenData[0];
	wavehdr.dwBufferLength = spokenData.size ();
	wavehdr.dwBytesRecorded = 0;
	wavehdr.dwUser = 0;
	wavehdr.dwFlags = 0;
	wavehdr.dwLoops = 0;

	waveOutPrepareHeader (_hWave, &wavehdr, sizeof (wavehdr));

	waveOutWrite  (_hWave, &wavehdr, sizeof (wavehdr));

	//while ((wavehdr.dwFlags & WHDR_DONE) == 0);
	while (waveOutUnprepareHeader (_hWave, &wavehdr, sizeof (wavehdr)) == WAVERR_STILLPLAYING)
	{
		::Sleep (100); // Viet xoa
	}

	waveOutClose (_hWave);
		
}

/**
* Stop playing the current wave
*/
void VietVoice::WavePlayer::Stop ()
{
	::waveOutReset (_hWave);
}

/**
* Constructor
*/
VietVoice::Concatenator::Concatenator()
{
	// Put in init method
	/*sound.Create (hwnd);
	sound.SetCoopPriority (hwnd);
			
	primaryBuffer.Create (sound);*/
//v_break_silence=0; // VT
}

/**
* Destructor
*/
VietVoice::Concatenator::~Concatenator ()
{}

/**
* Concategnate the sound data and play it
*
* @param toSpoke Data representing the words to be spoken
* @return The toSpoke data after the treatement
*/
void VietVoice::Concatenator::Treat (ToBeSynthetise & toBeSynthetise) 
{ 	
	std::list <ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
	std::list <ToBeSynthetiseData> newListData ;
	int size = toBeSynthetise.GetSizeWords();
	bool firstWord = true;
	std::wstring v_lightWords = L"với/và/mà/là/thì/đã/sẽ";
	std::wstring v_heavyWords = L"nhưng/vẫn/chỉ/đâu/đó/tuy/đây/nầy/này/nếu/phải";
	int v_punc_break = 0;
	vector<long> v_word ;
	// ==================================================================================
	if (size != 0) // if 1 - Make sure there is sound data to be spoken (prevent a bug when user enter nothing) 
	{			
		//unsigned char * spokenData = new unsigned char[toSpoke.GetSizeWords()]; // Will contain all the sound data
		vector<unsigned char> spokenData;
		//spokenData.resize ();
		// if 2
		// ------------------------------------------------------------------------------
		if (size > 0) // Makes sure there is some sound data // if 2
		{ 
//		int k = 0; // VX

		// For every word to be spoken, get the data and concatenate it
			std::list<ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
			std::list<Token> listToken;
			std::list<ToBeSynthetiseData> newListData;

			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
			{
				ToBeSynthetiseData data = *iter;
				Word * word = data.GetWordData();
				
				if (data.GetWord().length() == 0) continue;
				if (data.GetPostPunctuation()==L",") v_punc_break = v_break_silence1;
				else 
				if ((data.GetPostPunctuation()==L".") || (data.GetPrePunctuation()==L")"))
					v_punc_break = v_break_silence2 ;
				else v_punc_break = 0;
				// unsigned char * tmp = word->Get; // Get the sound data
				unsigned long sizeTmp = word->GetNbSamples () * word->GetBlockAlign ();
				unsigned long wordCut = word->GetNbSamples () * data.GetWordCut ();
//::MessageBox (NULL, StringHelper::ToString (wordCut).c_str (), L"Find cycles", MB_OK) ;

				wordCut = wordCut * word->GetBlockAlign();
				sizeTmp -= wordCut;
// Concategnate the sound data the number of time necessary
//for (int repeat = 0; repeat < data.GetNbRepeats(); repeat++) 
//				{
//::MessageBox(NULL, StringHelper::ToString(::ceilf(sizeTmp * v_speed /100)).c_str(), L"Concatenator", MB_OK);
// ====================================================================
// Xu ly compress words
// ====================================================================
//			sizeTmp = sizeTmp * v_speed /100; // VT
//			sizeTmp = sizeTmp /2 * 2; // VT
				unsigned mBegin = sizeTmp * (100 - v_speed ) / 200; // VT
				mBegin = mBegin / 2 * 2; // VT
				unsigned int j = mBegin; unsigned char mB1; unsigned char mB2;
				long mTmp; long mTmp1; long mTmp2 ; long mTmp_1; long mTmp_2;
				if (v_speed < 100) 
				{
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ; // VT
					bool mStop = false;
					while (mStop==false) 
					{
						j += 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= 0) 
						{
							mStop = true; 
							mBegin = j;
							if (abs(mTmp_1) < abs(mTmp_2)) mBegin -= 2;
						}
						else 
							mTmp_1 = mTmp_2;
					} // End while
				}
				unsigned mEnd = sizeTmp - 1 ; // VT
				mEnd = mEnd / 2 * 2; // VT
				if (v_speed < 100)
				{
					j = mEnd;
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ;
					bool mStop = false;
					while (mStop==false)
					{
						j -= 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= 0) 
						{
							mStop = true ; 
							mEnd = j;
							if (abs(mTmp_1) < abs(mTmp_2)) 
								mEnd += 2 ;
						}
						else 
							mTmp_1 = mTmp_2 ;
					} // End while
				}
//===============================================================
/*std::vector <int> mData;
for (unsigned int j = 0; j < sizeTmp; j++) 
			{
			  if (j % 2==0)
			  {
			    mTmp = word->GetByte(j) -5;
			  }
			  else
			  {
				mTmp = mTmp + (word->GetByte(j) -5) * 256;
				if (mTmp <= 32767) mData.push_back (mTmp);
				else mData.push_back (mTmp - 65536);
				mB1 = mTmp % 256;
				mB2 = (mTmp - mB1) / 256;
			  }
			}
int mKC = 0;
int mjOld = 0;
for (unsigned j=0; j< mData.size () - 1; j++)
{
  if ((mData[j]*mData[j+1]<0) && (j - mjOld > 10))
	{
mjOld = j;
//::MessageBox (NULL, StringHelper::ToString (j).c_str (), L"Concatenator",MB_OK);
//::MessageBox (NULL, StringHelper::ToString (mData[j]).c_str (), L"Data 1",MB_OK);
//::MessageBox (NULL, StringHelper::ToString (mData[j+1]).c_str (), L"Data 2",MB_OK);

  }
}
*/
//===============================================================
//			for (unsigned int j = 0; j < sizeTmp; j++) // VX
					// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
				v_word.clear() ;
				for (unsigned int j = mBegin; j < mEnd; j++) // VT
//for (unsigned int j = 0; j < (int) ::ceilf(sizeTmp * v_speed /100); j++) 
				{
// **************************************************************************************
					// spokenData.push_back ((unsigned char)word->GetByte (j) - 5);//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3. // VX
					if (j % 2==0)
					{	mTmp = word->GetByte(j) -5;
					}
					else
					{	mTmp = mTmp + (word->GetByte(j) -5) * 256;
						v_word.push_back (ByteToWord(mTmp)) ;
					}
// **************************************************************************************




//
//
				/* if (firstWord == true)
					{
						j = 0;
						firstWord = false;
					} */
					//::MessageBox (NULL, StringHelper::ToString (word->GetByte (j)).c_str (), L"aaa", MB_OK);
//					if ((v_lightWords.find(data.GetOriginalWord(),0) != string::npos) || (v_heavyWords.find(data.GetOriginalWord(),0) != string::npos))
//					{
//						long mTmp ; unsigned char mB1; unsigned char mB2;
//						if (j % 2==0)
//							mTmp = word->GetByte(j) -5;
//						else
//						{
//							mTmp = mTmp + (word->GetByte(j) -5) * 256;
//							if (v_lightWords.find(data.GetOriginalWord(),0) != string::npos) 
//							{
//								mTmp = ChangeVolume(mTmp , v_volume) ;
//							}
//							else
//							{
//								mTmp = ChangeVolume(mTmp , 200 - v_volume) ;
//							}
//							mB1 = mTmp % 256;
//							mB2 = (mTmp - mB1) / 256;
//							spokenData.push_back ((unsigned char)mB1);
//							spokenData.push_back ((unsigned char)mB2);
//						}
						/* if (word->GetByte (j)<127)
						{
							spokenData.push_back (( (unsigned char)word->GetByte (j) - 5) * v_volume /100 );//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
							//::MessageBox (NULL, StringHelper::ToString (v_temp).c_str (), L"Concatenator",MB_OK);
						}
						else
						{
							spokenData.push_back ((((unsigned char)word->GetByte (j) - 5 - 255) * v_volume /100)+255) ;//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
							//::MessageBox (NULL, StringHelper::ToString (word->GetByte (j) - 5).c_str (), L"Concatenator",MB_OK);
						}
*/
//					}
//					else
//						spokenData.push_back ((unsigned char)word->GetByte (j) - 5);//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
					//spokenData [k] -= 5;
				//k++;
				} // End for j
// ::MessageBox (NULL, StringHelper::ToString (v_word.size ()).c_str (), L"aaa", MB_OK);
				Compress_Word(v_word);	
// ::MessageBox (NULL, StringHelper::ToString (v_word.size ()).c_str (), L"aaa", MB_OK);
				// spokenData.clear() ; // VX
				for (int j = 0; j < v_word.size(); j++) {
					mB1 = v_word[j] % 256;
					mB2 = v_word[j] / 256;
					spokenData.push_back(mB1) ;
					spokenData.push_back(mB2) ;
				}
					// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// End Viet them
//::MessageBox (NULL, StringHelper::ToString (data.GetSilenceLength ()).c_str (), L"aaa", MB_OK);
//			for (unsigned int j = 0; j < data.GetSilenceLength (); j++) // VX
				int mSilence = data.GetSilenceLength () * v_speed / 100; // VT
				mSilence = mSilence / 2 * 2; // VT
				for (unsigned int j = 0; j < mSilence  ; j++) // VT
					spokenData.push_back (0);
			} // End for iter
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//			} 
			float mBreak ; 
			if (spokenData.size () > 400000) mBreak = 0 ;
			else mBreak= float (400000 - spokenData.size ()) / 400000 ;
			::Sleep (v_punc_break * mBreak) ;
//::MessageBox (NULL, StringHelper::ToString (v_punc_break * mBreak).c_str (), L"aaa", MB_OK);
//			for (unsigned int j = 0; j < (v_punc_break * mBreak * 88.2f) / 2 * 2 ; j++) // VT
//				spokenData.push_back (0);
			Word final (spokenData);
			if (toBeSynthetise.GetMustPlay ())
			{
				//::MessageBox (NULL, StringHelper::ToString (spokenData.size ()).c_str (), L"aaa", MB_OK);
				if (spokenData.size () > 0)
				{	//_//soundPlayer.Create (_sound, spokenData);
					//_//soundPlayer.Play ();
					_wavePlayer.Play (spokenData);
					//_mp3Player.Play(L"database\\tmp.wav");
				}
			}
			else
			{
				Word final (spokenData);
				final.Save (toBeSynthetise.GetMp3Name ());
			}
			// delete [] spokenData;
		} // End if 2
		// ------------------------------------------------------------------------------
	} // End if 1
	// ==================================================================================
} // End void Treat

/**
* Save a byte array containning the sound data in a mp3 file
*
* return value: True if success, elsse false
*
* unsigned char * spokenData spokenData: The sound data contained in an array
* int size:  Size of the array.
*/
bool VietVoice::Concatenator::SaveSpokenData (std::wstring filename, unsigned char * spokenData, int size )
{
	try
	{
		//File::Saver saver (filename);		 
		//saver.SaveMP3 (spokenData, size);
	}
	catch (File::Exception ex)
	{
		throw VietnameseTTException(L" Error, could not save temporary file");
	}

	return true;
}

/**
* Stop reading the current text
*/
void VietVoice::Concatenator::Stop ()
{	
	_wavePlayer.Stop ();
}
void VietVoice::Concatenator::Get_BS1 (long v_bs)
{	
	v_break_silence1 = v_bs;
}
void VietVoice::Concatenator::Get_BS2 (long v_bs)
{	
	v_break_silence2 = v_bs;
}
void VietVoice::Concatenator::Get_VL (long v_vl)
{	
	v_volume = v_vl;
}
void VietVoice::Concatenator::Get_SP (long v_sp)
{	
	v_speed = v_sp;
}
long VietVoice::Concatenator::ByteToWord (long v_byte)
{	
	if (v_byte <= 32767) return v_byte;
	else return v_byte - 65536;
//	if (v_byte <= 32767) return 32767 - v_byte;
//	else return 32767 + 65536 - v_byte;
	return v_byte ;
}
long VietVoice::Concatenator::ChangeVolume (long v_byte, long v_volume)
{	
	if (v_byte <= 32767) return v_byte * v_volume /100; 
	else
	return ((v_byte - 65536) * v_volume / 100) + 65536;
}
void VietVoice::Concatenator::Compress_Word (vector<long> & tmp_word)
{	
	int mi = 0 ;
	int v_num_of_breaks = 0;
	int v_num_of_cycles = 0;
	int v_cycle_max = 4 ;

	vector<unsigned long> v_sum ;
	vector<unsigned int> v_avg ;
	vector<unsigned int> v_loc ;
	vector<unsigned int> v_seg ;
	vector<bool> v_cycle ;

	v_sum.push_back (0) ;
	v_avg.push_back (0) ;
	v_loc.push_back (0) ;
	v_seg.push_back (0) ;
	v_cycle.push_back (false) ;
	for (int i = 0; i < (tmp_word.size() - 1); i++)
	{
		if ((tmp_word[i] <= 0) && (tmp_word[i+1] >= 0) && (i - mi  >= 20))
//		if ((tmp_word[i] * tmp_word[i+1] <= 0) && (i - mi  >= 15))
		{
			v_sum.push_back (0) ;
			v_avg.push_back (0) ;
			v_loc.push_back (0) ;
			v_seg.push_back (0) ;
			v_cycle.push_back (false) ;
			v_num_of_breaks ++;
			mi = i ;
		}
		else
		{
			v_sum[v_num_of_breaks] += abs(tmp_word[i]) ;
			v_loc[v_num_of_breaks] = i ;
		}
	}
	v_num_of_breaks -- ;
	for (int i = 0; i < v_num_of_breaks - 1; i++) v_avg[i] = v_sum[i] / (v_loc[i+1] - v_loc[i]) ;
//	::MessageBox (NULL, StringHelper::ToString (v_num_of_breaks).c_str (), L"Ti le", MB_OK) ;
	if (v_num_of_breaks == 0) return;

	int k = 0 ;
	int j = 0 ;
	float v_ratio = 0.1f ;
	float m_ratio_sum ;
	float m_ratio_loc ;

	int v_percent = 10 ;
	int v_cut_cycle = 0 ;
//	for (int i = 0; i < v_num_of_breaks - v_cycle_max * 2 + 1; i++) {
	int vi = 1 ;
	do {
		k = 2 ; // 2
		do {
			bool v_found = true ;
			j = 0 ;
			do {
				m_ratio_sum = float (v_avg[vi + k]) / v_avg[vi] ;
				m_ratio_sum = abs(1 - m_ratio_sum) ;
				m_ratio_loc = float (v_loc[vi + k]) / v_loc[vi] ;
				m_ratio_loc = abs(1 - m_ratio_loc) ;
//				::MessageBox (NULL, StringHelper::ToString (v_sum[vi + k]).c_str (), StringHelper::ToString (v_sum[vi]).c_str (), MB_OK) ;
//				::MessageBox (NULL, StringHelper::ToString (vi).c_str (), L"Ti le", MB_OK) ;
				if ((m_ratio_sum > v_ratio) || (m_ratio_loc > v_ratio))
					v_found = false ;
				j++ ;
			} while (j <= k-1) ;
			if (v_found) {
				v_cycle[vi] = true ;
				v_seg[vi] = k ;
				v_num_of_cycles ++ ;
				vi += k - 1 ;
				k = v_cycle_max;
			}
			k++ ; 
		} while (k <= v_cycle_max) ;
		vi ++ ;
	} while (vi < v_num_of_breaks - v_cycle_max * 2 + 1) ;

//::MessageBox (NULL, StringHelper::ToString (v_cycle.size()).c_str (), L"v_cycle", MB_OK) ;

	for (int i = 0; i < v_num_of_breaks; i++) {
//::MessageBox (NULL, StringHelper::ToString (i).c_str (), L"Diem thu", MB_OK) ;
//::MessageBox (NULL, StringHelper::ToString (v_cycle[i]).c_str (), StringHelper::ToString (v_seg[i]).c_str (), MB_OK) ;
		if (v_cycle[i] == true && v_seg[i] > 0) {
			// for (int j = 0; j < v_seg[i] - 1; j++)
			int j = 0;
			do {		
				if (i + j + 1 < v_cycle.size ()) {
//					int mtmp = i + j + 1 ;
//::MessageBox (NULL, StringHelper::ToString (v_cycle[i + j + 1]).c_str (), StringHelper::ToString (i + j + 1).c_str (), MB_OK) ;
					v_cycle[i + j + 1] = true ;
				}
				j ++ ;
			} while ((j < v_seg[i] - 1) && (i + j + 1 < v_cycle.size ())) ;
		}
	}
	v_cut_cycle = v_num_of_breaks * v_percent / 100 ;
	int v_count = 0 ;
	if (v_num_of_cycles < v_cut_cycle) return ;
	for (int i = 0; i < v_num_of_breaks; i++)
		if (v_cycle[i] == true && v_seg[i] > 0) {
			v_count ++;
			if (v_count % (v_num_of_cycles / v_cut_cycle) != 1) {
				v_cycle[i] = false ;
				for (int j = 0; j < v_seg[i] - 1; j++)
					v_cycle[i + j + 1] = false ;
			}
		}
	vector<long> v_tmp ;
	for (int i = 0; i < v_loc[0]; i++) // Lay doan dau
		v_tmp.push_back(tmp_word[i]) ;
	int i = 0 ;
	do {
		if (v_cycle[i] == false) {
			for (int j = v_loc[i]; j < v_loc[i + 1]; j++) {// Lay doan thu i
				v_tmp.push_back(tmp_word[j]);
		}
			}
		else {
//			i += v_seg[i] ;
//			for (int j = v_loc[i]; j < v_loc[i + 1]; i++) // Lay doan thu i
//				v_tmp.push_back(v_word[j]);
		}
		i++ ;
	} while (i < v_num_of_breaks - 1) ;

	for (int i = v_loc[v_num_of_breaks - 1]; i < tmp_word.size(); i++) // Lay doan cuoi
		v_tmp.push_back(tmp_word[i]) ;
	tmp_word.clear() ;
	tmp_word = v_tmp ;

	v_tmp.clear () ;
	v_sum.clear () ;
	v_avg.clear () ;
	v_loc.clear () ;
	v_seg.clear () ;
	v_cycle.clear () ;
//				::MessageBox (NULL, StringHelper::ToString (v_loc[i]).c_str (), L"Find cycles", MB_OK) ;
}
