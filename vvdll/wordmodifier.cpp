#include "WordModifier.h"

VietVoice::WordModifier::WordModifier ()
{}

void VietVoice::WordModifier::Treat (ToBeSynthetise & toSynthetise)
{

	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData ();
	std::list<ToBeSynthetiseData> newListData;

	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		ToBeSynthetiseData data = *iter;
		std::wstring wordVal  = data.GetWord ();

		
			
		if (data.GetAccent () != -1) // Words with a negative accent are extra words, must not be modified
			ProcessConsonnant (wordVal);

		data.SetWord (wordVal) ;

		newListData.push_back (data);
	}
	
	toSynthetise.SetListData (newListData);
}

VietVoice::WordModifier::~WordModifier ()
{}

VietVoice::WordModifier::WordModifier  (const WordModifier & wm)
{

}

void VietVoice::WordModifier::operator = (const WordModifier & wm)
{

}

/**
*
* Handle the case where gy musts become either gi or gii
* @param tokenVal String representing the token
*/
void VietVoice::WordModifier::ProcessGy (std::wstring & tokenVal)
{
	bool m_has_ = false ;
	if (tokenVal[tokenVal.length() -1] == L'_') {
		m_has_ = true ;
		tokenVal  = tokenVal.substr(0, tokenVal.length() - 1) ;
	}
	if (tokenVal.compare(L"gya") == 0 || tokenVal.compare(L"gyu") == 0)
	{
		StringHelper::ReplaceFirst (tokenVal, L"gy", L"gii") ;
	}
	else
	{

		StringHelper::ReplaceFirst (tokenVal, L"gy", L"gi") ;
	}
	if (m_has_ == true) tokenVal.append(L"_") ;
}

/**
* Takes care of things like converting gi to gii, gy to gi, y to i, qu to c or co, etc.
* @param tokenVal The word
* @return The modified word
*/

void VietVoice::WordModifier::ProcessConsonnant (std::wstring & tokenVal)
{
	if (tokenVal.compare (0, 2,L"gi") == 0 || tokenVal.compare (0, 2, L"g�") == 0)
	{
		ProcessGi (tokenVal);
	}
	else if (tokenVal.compare (0, 2,L"gy") == 0)
	{
		ProcessGy (tokenVal);
	}
	else if (tokenVal.compare (0, 2,L"qu") == 0)
	{
		ProcessQu (tokenVal);
	}
	else if (tokenVal.compare(L"y") == 0 || tokenVal.compare(L"my") == 0 || tokenVal.compare(L"vy") == 0 || tokenVal.compare(L"ky") == 0 || tokenVal.compare(L"hy") == 0 || tokenVal.compare(L"ny") == 0 || tokenVal.compare(L"ty") == 0 || tokenVal.compare(L"thy") == 0 || tokenVal.compare(L"xy") == 0 || tokenVal.compare(L"sy") == 0 || tokenVal.compare(L"ly") == 0 || tokenVal.compare(L"by") == 0 || tokenVal.compare(L"cy") == 0 || tokenVal.compare(L"chy") == 0 || tokenVal.compare(L"dy") == 0 || tokenVal.compare(L"d-y") == 0 || tokenVal.compare(L"gy") == 0 || tokenVal.compare(L"giy") == 0 || tokenVal.compare(L"khy") == 0 || tokenVal.compare(L"ngy") == 0 || tokenVal.compare(L"nghy") == 0 || tokenVal.compare(L"nhy") == 0 || tokenVal.compare(L"phy") == 0 || tokenVal.compare(L"ry") == 0 || tokenVal.compare(L"try") == 0)
	{
//::MessageBox (NULL, StringHelper::ToString (tokenVal).c_str (), L"Modify", MB_OK) ;

		StringHelper::ReplaceFirst (tokenVal, L"y", L"i") ;
		//tokenVal = L"mi";
	}
	else if (tokenVal.compare(L"y_") == 0 || tokenVal.compare(L"my_") == 0 || tokenVal.compare(L"vy_") == 0 || tokenVal.compare(L"ky_") == 0 || tokenVal.compare(L"hy_") == 0 || tokenVal.compare(L"ny_") == 0 || tokenVal.compare(L"ty_") == 0 || tokenVal.compare(L"thy_") == 0 || tokenVal.compare(L"xy_") == 0 || tokenVal.compare(L"sy_") == 0 || tokenVal.compare(L"ly_") == 0 || tokenVal.compare(L"by_") == 0 || tokenVal.compare(L"cy_") == 0 || tokenVal.compare(L"chy_") == 0 || tokenVal.compare(L"dy_") == 0 || tokenVal.compare(L"d-y_") == 0 || tokenVal.compare(L"gy_") == 0 || tokenVal.compare(L"giy_") == 0 || tokenVal.compare(L"khy_") == 0 || tokenVal.compare(L"ngy_") == 0 || tokenVal.compare(L"nghy_") == 0 || tokenVal.compare(L"nhy_") == 0 || tokenVal.compare(L"phy_") == 0 || tokenVal.compare(L"ry_") == 0 || tokenVal.compare(L"try_") == 0)
	{
		StringHelper::ReplaceFirst (tokenVal, L"y", L"i") ;
	}
	else if (tokenVal.compare(L"q\x01B0\x01A1u") == 0) // Pas sure... || qu7o7u -> cu7o7u
	{
		tokenVal = L"c\x01B0\x01A1u" ;
	}
	else if (tokenVal.compare(L"q\x01B0\x01A1u_") == 0) // Pas sure... || qu7o7u -> cu7o7u
	{
		tokenVal = L"c\x01B0\x01A1u_" ;
	}
	else if (tokenVal.compare(L"q\x1EE5yt") == 0) // qu5yt -> cu5yt
	{
		tokenVal = L"c\x1EE5yt";
	}
	else if (tokenVal.compare(L"q\x1EE5yt_") == 0) // qu5yt -> cu5yt
	{
		tokenVal = L"c\x1EE5yt_";
	}
}

/**
 * Replace the qu part of the word by c or co depending on the word
 * @param tokenVal String representing the word
 */
void VietVoice::WordModifier::ProcessQu (std::wstring & tokenVal)
{
	bool m_has_ = false ;
	if (tokenVal[tokenVal.length() -1] == L'_') {
		m_has_ = true ;
		tokenVal  = tokenVal.substr(0, tokenVal.length() - 1) ;
	}
	if (tokenVal.compare (L"qu�t") == 0)
	{
	tokenVal = L"cu�t";
	}
	else if (tokenVal.compare(L"qui") == 0)
	{
		tokenVal = L"cuy";
	}

	else if (tokenVal.compare(L"quy") == 0 || tokenVal.compare(L"qu\x01A1") == 0 || tokenVal.compare(L"qu�") == 0 || tokenVal.compare(L"qu�n") == 0|| tokenVal.compare(L"quy�n") == 0 || tokenVal.compare (L"qu\x1EA5t") == 0 || tokenVal.compare(L"qu\x1EADt") == 0 || tokenVal.compare (L"qu�t") == 0 || tokenVal.compare(L"qu\x1EF5t") == 0 || tokenVal.compare(L"quy\x1EBFt") == 0 || tokenVal.compare(L"quy\x1EC7t") == 0)
	{
		StringHelper::ReplaceFirst (tokenVal, L"q", L"c");
	}
	else //if (tokenVal.compare("que") || tokenVal.compare ("qua") || tokenVal.compare("quai") || tokenVal.compare ("quay") || tokenVal.compare("quen") || tokenVal.compare("quang") || tokenVal.compare("quanh") || tokenVal.compare("qu�t") || tokenVal.compare("qu\u1EB9t") || tokenVal.compare("qu�c") || tokenVal.compare("qu\u1EA1c") || tokenVal.compare("qu�t") || tokenVal.compare("qu\u1EA1t"))
	{
		StringHelper::ReplaceFirst (tokenVal, L"qu", L"co");
	}
	if (m_has_ == true) tokenVal.append(L"_") ;
}

/**
 *
 * For the appropriate words, replace gi by gii so the algorithme finding the position of the word in the index works properly
 * @param tokenVal String representing the token
 */
void  VietVoice::WordModifier::ProcessGi (std::wstring & tokenVal)
{
	bool m_has_ = false ;
	if (tokenVal[tokenVal.length() -1] == L'_') {
		m_has_ = true ;
		tokenVal  = tokenVal.substr(0, tokenVal.length() - 1) ;
	}
	std::wstring a;
	a = L"gi\x1EBF"; //L"gi\x1EBFc"
	a += L"c";

	std::wstring b;
	b = L"gi\x1EC7";
	b += L"c";

	if (tokenVal.compare (L"gi") == 0 || tokenVal.compare (L"gi�u") == 0 || tokenVal.compare (L"gim") == 0  ||tokenVal.compare(L"gin") == 0 ||  tokenVal.compare(L"ginh") == 0 || tokenVal.compare (L"gi�m") == 0 || tokenVal.compare(L"gi�ng") == 0 || tokenVal.compare (a) == 0 ||  tokenVal.compare(b) == 0 || tokenVal.compare(L"gi�n") == 0 || tokenVal.compare(L"gi\x1EBFt") == 0 || tokenVal.compare(L"gi\x1EC7t") == 0)       
		StringHelper::ReplaceFirst(tokenVal, L"gi", L"gii") ;

	else if (tokenVal.compare (L"g�p") == 0 || tokenVal.compare(L"g�ch") == 0)
	{
		StringHelper::ReplaceFirst(tokenVal, L"g�", L"gi�");
	}
	if (m_has_ == true) tokenVal.append(L"_") ;
}

