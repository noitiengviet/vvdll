#if !defined (TOKENS_TO_WORDS_H)

#define TOKENS_TO_WORDS_H
#pragma warning( disable : 4786 ) 

#include <fstream>
#include <ctime>
#include <map>
#include <windows.h>
#include "treatment.h"
#include "fileio.h"
#include "vietnameseTTSException.h"
#include "stringhelper.h"
#pragma warning( disable : 4786 ) 

namespace VietVoice
{
	/**
	 * Class Name:         TokenToWords.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class used to convert the tokens into words
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	/**
	  * CODE MODIFICATIONS AND BUG CORRECTIONS
	  * --------------------------------------
	  * January 28 2005 -> Modified so space between number like 1 000 are ignored
	  *
	  * February 1 2005 -> Modified so alpha-numeric charater (ex:  B17) are spelled
	  *
	  *
	  * Mars 3 2005 -> Modified word used for april
	  *
	  * september 21 2005:  Started convertion to C++
	  *
	  * september 28 2005:  Added arrays for number/date conversioin and isTokenNumber method.
	  * september 29 2005:  Added the loadAbreviationFile method, far for completed!
	  * october 8 2005:  Replaced the text files to binary file to solve reading problems, finnally completed LoadAbreviations function, removed method that was added to read lines of text from files
	  * october 9 2005:  Implemented LoadNoSilence isTokenNumberOrSymbol, addZeros, convertNumber, convertDigit, convertHundred, convertTen, processDecimal completely
	  * octer 11 2005:   Implemented the data class (except the tokenizeDate method).
	  * october 15:  Implemented the tokenizeData method in the Date class.  Also implemented the isDate, processNumber, convertMonth and convertYear
	  * october 16:  Implemented the convertDay, processDate, processSymbol, processNormalWord and isTokenAlphaNumeric methods.  Also modified the loadAbreviationFile method to use the new StringTokenizer class.
	  * october 17:  Implemented preprocess number and processSlashBetweenNumber
	  * october 18:  Implemented processAlphaNumeric, completed treat
	  * october25:  Corrected the ConvertDay method so it find the day of the week (sunday, monday, ect), must be chanded before 2038!!!!!.
	  * November 29 2005 -> Added destructor in case
	  * novembre 29 2005:  Implemented the copy constructor and the = operator
	  * January 10 2005:  Corrected some mistake conserning substr linked to the processing of / between numbers and - better words
	  * January 12 2005:  Corrected the functions treating alpha numeric entries (for instance 1a3b) so they work properly
	  * February 4 2006:  Modified the class to use unicode file names.
	  * February 7 2006:  Change the loadAbbreviationFile a little bit to allow proper reloading of the file
	  * March 6 2006:  Seperated implementation in source and header
	  * June 10 2006: Change the methods name so they start by a capital letter.
	  */

	  /**
	   * Objet contenant les informations au sujet d'une date (jour, mois, ann�e)
	   *
	   * Object containing the informations about a date (day month, year)
	   */

	  class Date
	  {
	  public:
		/**
		 * Constructor
		 * @param date String util� pour innitialis� l'objet / String used to innitialize the object
		 */
		Date (std::wstring date);

		/**
		* Copy constructor
		*/
		Date  (const Date & date);

		void operator = (const Date & date);

		/**
		 * Obtient le jours de la date
		 * @return Le jours sous forme de String
		 *
		 * Gets the day of the date
		 * @return the day as a String
		 */
		std::wstring GetDayString ();

		/**
		 * Obtient le mois de la date
		 * @return Le mois sous forme de String
		 *
		 * Gets the month of the date
		 * @return the month as a String
		 */
		std::wstring GetMonthString ();

		/**
		 * Obtient l'ann�e de la date
		 * @return L'ann�e sous forme de String
		 *
		 * Gets the year of the date
		 * @return the year as a String
		 */
		std::wstring GetYearString ();

		/**
		 * Obtient le jours de la date
		 * @return Le jours sous forme de int
		 *
		 * Gets the day of the date
		 * @return the day as a int
		 */
		int GetDayInt ();

		/**
		 * Obtient le mois de la date
		 * @return Le mois sous forme de int
		 *
		 * Gets the month of the date
		 * @return the month as a int
		 */
		int GetMonthInt ();

		/**
		 * Obtient l'ann�e de la date
		 * @return L'ann�e sous forme de int
		 *
		 * Gets the year of the date
		 * @return the year as a int
		 */
		int GetYearInt ();

	  private:
		/**
		 * Innitialise l'objet
		 * @param date Un objet String contenant les info sur la date
		 *
		 * Innitialize the object
		 * @param date A String object containing the informations about the date
		 */
		void TokenizeDate (std::wstring date);

		private:

		std::wstring _month ;
		std::wstring _day ;
		std::wstring _year ;
	  } ;

	class TokensToWords : public Treatement
	{

	public:

		/**
		* Constructor
		*/
		TokensToWords (std::wstring fileAbb, std::wstring fileNoSilence);

		// Thuan add 11-02-2020
		TokensToWords (std::wstring fileAbb, std::wstring fileNoSilence,u_int nbOfWordInPhrase,std::wstring pathPhrase, std::wstring pathVeryShortWord);

		/**
		* Copy Constructor
		*/
		TokensToWords  (const TokensToWords & tokensToWords);

		void operator = (const TokensToWords & tokensToWords);

		virtual ~TokensToWords ();

		void Treat (ToBeSynthetise & toSynthetise);

		/*
		* Convert a number to text if it is a number
		* @param tokenVal String representing the token
		* @param tokenItem Object representing the token
		* return true if the conversion was possible else false
		*/
		
		// Type : ( 1: normal, 2: not end phrase, 3: end phrase, 4: veryshort)
		bool PreprocessNumbers (std::wstring tokenVal, Token & token, u_int type);



		/**
		 * Treats the number by removing slashes between them if there are some and then pass each number to another method for further processing
		 * @param tokenVal The name of the token
		 * @param token The token object
		 * @return True if the token was processed as a number, else false
		 */
			// Type : ( 1: normal, 2: not end phrase, 3: end phrase, 4: veryshort)
		 bool ProcessSlashBetweenNumbers (std::wstring tokenVal, Token token, u_int type);

		/**
		 * Determines if the token is a date.
		 * @param tokenVal String representing the token
		 * @return true if the token is a date, else false
		 */
		 bool IsDate (std::wstring tokenVal);
		 bool IsTime (std::wstring tokenVal);

	public:
		/**
		 *
		 * Loads a file containing the abbreviations and how to prononce them.
		 * @param filename Name and path of the file to be loaded
		 */
		void LoadAbbreviationFile (std::wstring filename);
	
		/**
		 * Loads a file containing a list of words after witch there must be no silence
		 * @param filename Name of the file to be loaded
		 */
		void LoadNoSilenceWords (std::wstring filename);

	private:

		/**
		 * Determines if the token is a number.
		 * @param tokenVal String representing the token
		 * @return true if the token is a number, else false
		 */
		  bool IsTokenNumber (std::wstring tokenVal);
		  


		 /*
		 * Determines if the token is a number or a '.', ',', '-' or '/'.
		 * @param tokenVal String representing the token
		 * @return true if the token is a number or a '.', ',', '-' or '/', else false
		 */
		 bool IsTokenNumberOrSymbol (std::wstring tokenVal);

		/**
		 * Adds 0 to the begining of a string of digit until the total number of digits is a multiple of
		 * 3
		 * @param tokenVal String of digit representing the token
		 * @return The string of digit with some 0 added at the beginning
		 */
		 void AddZeros (std::wstring & tokenVal);

		/**
		 * Convert the number to text if it is a number
		 * @param tokenVal String representing the token
		 * return true if the conversion was possible else false
		 */
		// Type : ( 1: normal, 2: not end phrase, 3: end phrase, 4: veryshort)
		 bool ProcessNumbers (std::wstring tokenVal,u_int type,std::wstring posPun);

		 /**
		 * Convert the date to text if it is a date
		 * @param tokenVal String representing the token
		 * return true if the conversion was possible else false
		 */
		 bool ProcessDate (std::wstring tokenVal, Token & token,u_int type,std::wstring posPun);
		 bool ProcessTime (std::wstring tokenVal, Token & token,u_int type);

		/**
		 * Convert the day of the date to texte
		 * @param date Object representing the date
		 * @return true if the convertion was succesful, else false
		 */
		 bool ConvertDay (Date & date,u_int type);
		 void ConvertHour (Date & date, std::wstring wa,u_int type);
		 void ConvertMinute (Date & date, std::wstring wa,u_int type);
		 void ConvertSecond (Date & date, std::wstring wa,u_int type);

		 /**
		 * Convert the month of the date to texte
		 * @param date Object representing the date
		 * @return true if the convertion was succesful, else false
		 */
		 bool ConvertMonth (Date & date,u_int type);

		 /**
		 * Convert the year of the date to texte
		 * @param date Object representing the date
		 * @return true if the convertion was succesful, else false
		 */
		 void ConvertYear (Date & date,u_int type,std::wstring posPun);

		 /**
		 *
		 * Take a string of digits and convert it into a string of words
		 * @param tokenVal The string of digit
		 */
		void ConvertNumber (std::wstring tokenVal,u_int type,std::wstring posPun);

		/**
		 * Convert the hundred digit
		 * @param hundredDigit The digit to convert
		 * @return true if hundred digit is not 0, else false;
		 */
		bool ConvertHundred (wchar_t hundredDigit,u_int type,std::wstring posPun);

		 /**
		 * Convert the hundred digit
		 * @param tensDigit The digit to convert
		 * @return true if tens digit is not 0, else false;
		 */
		 bool ConvertTen (wchar_t tensDigit,u_int type,std::wstring posPun);

		/**
		 * Convert the "unit" digit
		 * @param tensDigit The digit to convert
		 * @return true if "unit" digit is not 0, else false;
		 */
		 bool ConvertDigit (wchar_t unitDigit, wchar_t tensDigit, wchar_t hundredDigit, int nbGroupsOfThree, bool addLe, u_int type,std::wstring posPun);

		/**
		 * Process the fractionnal part of a number
		 * @param decimal The fractionnal part of a number
		 */
		 void ProcessDecimal (std::wstring decimal,u_int type);

		/**
		 * Process normal words
		 *
		 * @param tokenVal String representing the word
		 * @param tokenItem Item object representing the word
		 */
		 void ProcessNormalWord (std::wstring tokenVal, Token & token, ToBeSynthetiseData::WORD_TYPE,u_int type);

		 /**
		 * Converts the symbols %, $ and $US to text
		 * @param tokenVal The symbol to convert
		 * @return true if tokenVal could be convert, else false
		 */
		 bool ProcessSymbols (std::wstring tokenVal, Token & token,u_int type);

		/**
		 * Used to find out if the token is alpha-numeric
		 * @param str the string that is verified
		 * @return true if the token is alpha-numeric else false
		 */
		 bool IsTokenAlphaNumeric (std::wstring str);

		/*
		 * Process an alpha-numeric token
		 * @param tokenVal String representing the token
		 * @param tokenItem Object representing the token
		 * return true if the conversion was possible else false
		 */
		bool ProcessAlphaNumeric (std::wstring tokenVal, Token & token,u_int type);
		bool ProcessRoman (std::wstring tokenVal, Token & token,u_int type); // VT
		bool ProcessUnit (std::wstring tokenVal, Token & token,u_int type); // VT
		void ProcessWord (std::wstring tokenVal,u_int type); // VT
		bool IsAbbreviation (std::wstring tokenVal); // VT
		bool IsName (std::wstring tokenVal); // VT
		bool VietVoice::TokensToWords::IsUpper (WCHAR letter);
		bool VietVoice::TokensToWords::IsLower (WCHAR letter);
		std::vector<std::wstring> LoadPhrase(std::wstring tu);


private:

  std::wstring _threeDigits [4];//  Allow to convert number to text
  std::wstring _oneDigit [10];//  Allow to convert number to text;'
  std::wstring _dayOfWeek [7];// Allow to convert dates to text
  std::wstring _wordBefore;
  std::wstring _wordAfter; // VT
  std::wstring v_wordAfter; // VT
  std::wstring v_tokenVal ; // VT
  std::wstring v_oldToken ; // VT
  u_int _nbOfWordInPhrase; // number of word in phrase
  std::map< std::wstring, std::list<std::wstring> > _abreviations ;
  std::map< std::wstring, bool> _noSilenceAfter ; 
  std::wstring _phrasePath;
  std::vector<std::wstring> _phraseVeryShort; // list of Phrase very short  
  std::map <std::wstring, std::vector<std::wstring>> _phraseNotVeryShort; // list of not Phrase very short
  std::map <std::wstring, int> _veryShortWord;

  ToBeSynthetise * _toSynthetise ;
  _locale_t _vietnameseLocal;
  } ;


}

#endif