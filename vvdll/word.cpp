
#include "word.h"

const int VietVoice::Word::MAGIC = 0xFECADA;

/**
* Construct an empty wave
*/
VietVoice::Word::Word ()
	: formatTag (0),
	  nbChannels (0),
	  samplesPerSecond (0),
	  averageBytesPerSecond (0),
	  blockAlign (0),
	  bitsPerSample (0),
	  nbSamples (0),
	  nbSamplePU1(0),
	  TuGhep(false)
{SetDefault();}

/**
* Creates a new wave from separate data members
* @param data The byte data of the wave
* @param formatTag The format tag of the wave
* @param nbChannels The number of channel of the wave
* @param samplesPerSecond The sample rate of the wave
* @param averageBytesPerSecond The average bytes per second for the wave
* @param blockAlign The block align for the wave
* @param bitsPerSample The bitsPerSample for the wave
* @param size The size of the wave in bytes
* @param nbSamples The size of the wave in samples
*/
VietVoice::Word::Word (vector<unsigned char> data, short formatTag, long nbChannels, unsigned long samplesPerSecond, unsigned long averageBytesPerSecond, long blockAlign, long bitsPerSample, long size, long nbSamples)
{
	this->data = data ;
	this->formatTag = formatTag;
	this->nbChannels = nbChannels;
	this->samplesPerSecond = samplesPerSecond;
	this->averageBytesPerSecond = averageBytesPerSecond;
	this->blockAlign = blockAlign;
	this->bitsPerSample = bitsPerSample;
	this->nbSamples = nbSamples;
	this->nbSamplePU1=nbSamples;
}

/**
* Creates a new wave by combining parts of 2 other waves.
* @param w1  The first wave used to create the new wave.
* @param from1  Starting point in the first wave.
* @param nbSamples1  Number of samples used from the first wave to create the new wave.
* @param w2  The second wave used to create the new wave.
* @param from2 Starting point in the second wave.
* @param nbSamples2 Number of samples used from the second wave to create the new wave.
*/
VietVoice::Word::Word (Word &w1, float percent1, Word & w2, float percent2)//(Wave &w1, int from1, int nbSamples1, Wave & w2, int from2, int nbSamples2)
{	
	percent2=100 - percent2;
	int from1 = 0;
	int nbSamples1 = static_cast<int> ((percent1/100.0) * w1.GetNbSamples ());
	int from2 = static_cast<int> ((percent2/100.0) * w2.GetNbSamples ());
	int nbSamples2 = w2.GetNbSamples () - from2;
	// TO DO:  Check to make sure the 2 waves have the same header info.
	// The two wave must have the same header info.  Use the header info of w1 to innitialize the header info
	// of the new wave.
	this->formatTag = w1.GetFormatTag();
	this->nbChannels = w1.GetNbChannels();
	this->samplesPerSecond = w1.GetSampleRate();
	this->averageBytesPerSecond = w1.GetBytesPerSecond();
	this->blockAlign = w1.GetBlockAlign();
	this->bitsPerSample = w1.GetBitsPerSample();
	//this->data = new byte [(nbSamples1 + nbSamples2) * (((int) Math.ceil(bitsPerSample / 8) * getNbChannels()))]; // Size of byte array
	if(percent1<100)
	{
		this->nbSamples = nbSamples1 + nbSamples2;
		this->size = (nbSamples1 + nbSamples2) * (((int) ::ceilf (static_cast <float> (bitsPerSample / 8)) * GetNbChannels()));
	}
	else
	{
		this->nbSamples = nbSamples1 ;
		this->size = (nbSamples1) * (((int) ::ceilf (static_cast <float> (bitsPerSample / 8)) * GetNbChannels()));
	}
	this->nbSamplePU1=nbSamples1;
	if(percent1<100)
	{
		this->TuGhep=true;
	}

	// Converts the parameters from sample position and number to byte position and number
	int from   = from1 * GetBlockAlign();
	int nbByte = nbSamples1 * GetBlockAlign();

	// Copy the desired bytes (samples) from the first wave.
	int j = from;
	int i ;

	for (i = 0; i < nbByte; i++,j++)
	{	
		data.push_back (w1.data[j]);
	}

	if (percent2 != 99) // B -> 0 // V -> 99
	{
		// Converts the parameters from sample position and number to byte position and number
		from   = from2 * GetBlockAlign() ;
		nbByte += nbSamples2 * GetBlockAlign() ;

		// Copy the desired bytes from the second wave.
		j = from ;
		for(; i < nbByte; i++, j++)
		{
			data.push_back (w2.data[j]) ;
		}
	}	
}


VietVoice::Word::Word (Word &w1, float percent1, Word & w2, float percent2,float percentVolumePU1, float percentVolumePU2,int ViTri, short loaiIndex,int positionPU1,int positionPU2)//(Wave &w1, int from1, int nbSamples1, Wave & w2, int from2, int nbSamples2)
{	
	percent2=100 - percent2;
	int from1 = 0;
	int nbSamples1 = static_cast<int> ((percent1/100.0) * w1.GetNbSamples ());
	int from2 = static_cast<int> ((percent2/100.0) * w2.GetNbSamples ());
	int nbSamples2 = w2.GetNbSamples () - from2;
	// TO DO:  Check to make sure the 2 waves have the same header info.
	// The two wave must have the same header info.  Use the header info of w1 to innitialize the header info
	// of the new wave.
	this->formatTag = w1.GetFormatTag();
	this->nbChannels = w1.GetNbChannels();
	this->samplesPerSecond = w1.GetSampleRate();
	this->averageBytesPerSecond = w1.GetBytesPerSecond();
	this->blockAlign = w1.GetBlockAlign();
	this->bitsPerSample = w1.GetBitsPerSample();
	this->percentVolumePU1=percentVolumePU1;
	this->percentVolumePU2=percentVolumePU2;
	this->percentPU1=percent1;
	this->percentPU2=100-percent2;
	this->viTri=ViTri;
	this->loaiIndex=loaiIndex;
	this->positionPU1=positionPU1;
	this->positionPU2=positionPU2;

	//this->data = new byte [(nbSamples1 + nbSamples2) * (((int) Math.ceil(bitsPerSample / 8) * getNbChannels()))]; // Size of byte array
	if(percent1<100)
	{
		this->nbSamples = nbSamples1 + nbSamples2;
		this->size = (nbSamples1 + nbSamples2) * (((int) ::ceilf (static_cast <float> (bitsPerSample / 8)) * GetNbChannels()));
	}
	else
	{
		this->nbSamples = nbSamples1 ;
		this->size = (nbSamples1) * (((int) ::ceilf (static_cast <float> (bitsPerSample / 8)) * GetNbChannels()));
	}
	this->nbSamplePU1=nbSamples1;
	if(percent1<100)
	{
		this->TuGhep=true;
	}

	// Converts the parameters from sample position and number to byte position and number
	int from   = from1 * GetBlockAlign();
	int nbByte = nbSamples1 * GetBlockAlign();

	// Copy the desired bytes (samples) from the first wave.
	int j = from;
	int i ;

	for (i = 0; i < nbByte; i++,j++)
	{	
		data.push_back (w1.data[j]);
	}

	if (percent2 != 99) // B -> 0 // V -> 99
	{
		// Converts the parameters from sample position and number to byte position and number
		from   = from2 * GetBlockAlign() ;
		nbByte += nbSamples2 * GetBlockAlign() ;

		// Copy the desired bytes from the second wave.
		j = from ;
		for(; i < nbByte; i++, j++)
		{
			data.push_back (w2.data[j]) ;
		}
	}	
}

VietVoice::Word::Word (int lengthSilence)
{
	SetDefault ();

	int size = GetBytesPerSecond() * lengthSilence;
	for (int i = 0; i < size; i++)
		data.push_back (0);

}

VietVoice::Word::Word (vector <unsigned char> theData)
{
	SetDefault (); 
	data = theData;
	size = data.size ();
	nbSamples = size / blockAlign;
	nbSamplePU1 = size / blockAlign;
}

/**
* Obtains the format tag of the wave.
*
* @return The format tag.
*/
short VietVoice::Word::GetFormatTag ()
{
	return formatTag;
}

/**
* Obtains the number of channels of the wave.
*
* @return The number of channels.
*/

long VietVoice::Word::GetNbChannels ()
{
	return nbChannels;
}

/**
* Obtains the sample rate of the wave.
*
* @return The sample rate.
*/
unsigned long VietVoice::Word::GetSampleRate ()
{
	return samplesPerSecond;
}

/**
* Obtains the byte rate per second of the wave.
*
* @return The byte rate.
*/
unsigned long VietVoice::Word::GetBytesPerSecond ()
{
	return averageBytesPerSecond;
}

/**
* Obtains the block alignement of the wave.
*
* @return The block alignement.
*/
long VietVoice::Word::GetBlockAlign ()
{
	return blockAlign;
}


bool VietVoice::Word::GetTuGhep ()
{
	return TuGhep;
}


float VietVoice::Word::GetPercentPU1 ()
{
	return percentPU1;
}

float VietVoice::Word::GetPercentPU2 ()
{
	return percentPU2;
}

float VietVoice::Word::GetPercentVolumePU1 ()
{
	return percentVolumePU1;
}

float VietVoice::Word::GetPercentVolumePU2 ()
{
	return percentVolumePU2;
}


int VietVoice::Word::GetPositionPU1 ()
{
	return positionPU1;
}

int VietVoice::Word::GetPositionPU2()
{
	return positionPU2;
}


int VietVoice::Word::GetViTri ()
{
	return viTri;
}

short VietVoice::Word::GetLoaiIndex ()
{
	return loaiIndex;
}

/**
* Obtains the number of bits forming a sample for the wave.
*
* @return The number of bits forming a sample.
*/
long VietVoice::Word::GetBitsPerSample ()
{
	return bitsPerSample;
}

/**
* Obtains the size in bytes of the sample data .
*
* @return The size in bytes of the sample data.
*/

int VietVoice::Word::GetSize ()
{
	return data.size();
}

/**
* Obtains the number of samples of the wave.
*
* @return The number of samples.
*/

int VietVoice::Word::GetNbSamples ()
{
	return nbSamples;
}


int VietVoice::Word::GetNbSamplePU1 ()
{
	return nbSamplePU1;
}


void VietVoice::Word::Load (std::wstring fileName)
{
	RiffReader reader (fileName);


	if (reader.ReadChunkName().compare("RIFF") != 0) //Make sure the file is in the RIFF format.
	{
		throw File::Exception (TEXT ("The file is not a wave file"));
	}

	reader.ReadLong (); //Skip the size of the whole file.

	if (reader.ReadChunkName().compare("WAVE") != 0) //Make sur it is a wave file
	{
		throw File::Exception (TEXT ("The file is not a wave file"));
	}

	reader.FindChunk("fmt "); //Find the format chunk

	reader.ReadLong(); // Skip the size of the format chunk, alway 16 bytes

	//Read the format data of the wave (format tab, sample rate, etc)
	formatTag = reader.ReadShort();
	nbChannels = reader.ReadShort();
	samplesPerSecond = static_cast<unsigned long>(reader.ReadLong ());
	averageBytesPerSecond = static_cast<unsigned long> (reader.ReadLong());
	blockAlign = reader.ReadShort() ;
	bitsPerSample = reader.ReadShort() ;

	reader.FindChunk("data"); // Find the data chunk.

	size = reader.ReadLong () ; // Read the size of the chunk
	nbSamples = size / blockAlign; //Determine the number of sample

	data.resize (size);
	reader.ReadUnsignedCharArrayJava (&data[0], data.size ()); // Read the sample data in a byte array.


	if (bitsPerSample == 8) //8-bit is unsigned, convert to signed.
	{
	  for (int i = 0; i < size; i++)
		data [i] -= 128;
	}
}
/**
* Saves a wave to a file. Only a subset of the wave can be saved.
*
* @param fileName The name of the file.
* @param firstSample the first sample to be saved
* @param len The number of samples to be saved
* @throws FileNotFoundException
* @throws IOException
*/
void VietVoice::Word::Save (std::wstring const fileName, int firstSample, int len)
{
RiffWriter writter (fileName) ;
unsigned int v_speed = 1; // VT

// ::MessageBox (NULL, StringHelper::ToString (fileName).c_str (), L"Concatenator",MB_OK);

long firstByte = firstSample * blockAlign; // Find the first byte to be saved
// 1-4-2013 || long byteLen = len * blockAlign; //Find the number of byte to be saved
long byteLen = len * blockAlign / v_speed; //Find the number of byte to be saved


writter.SaveChunkName("RIFF"); // Save the riff ID
writter.SaveLong (byteLen + 36 + (byteLen % 2)); //Save the size of the file
writter.SaveChunkName("WAVE"); //Is a wave file
writter.SaveChunkName("fmt "); //Save the format ID

//Save the various format data
writter.SaveLong(16);
writter.SaveShort(formatTag);
writter.SaveShort((short)nbChannels);
// 1-4-2013 || writter.SaveLong( samplesPerSecond);
writter.SaveLong( samplesPerSecond / v_speed);
// 1-4-2013 || writter.SaveLong( averageBytesPerSecond);
writter.SaveLong( averageBytesPerSecond  / v_speed);
writter.SaveShort((short)blockAlign);
writter.SaveShort((short)bitsPerSample);

writter.SaveChunkName("data"); //Save the data ID
writter.SaveLong(byteLen); //Save the size of the data chunk



if (v_speed != 1) {
	vector <unsigned char> v_data; // VT

	for (unsigned int i = 0; i < data.size(); i ++) // VT
		if (i % (v_speed * 2) < 2) v_data.push_back(data[i]); // VT
	data = v_data; // VT
}


if (bitsPerSample == 8) //8-bit, so reconvert to unsigned
{
	for (int i = 0; i < size; i++)
		data [i] += 128;
}

writter.SaveByteArray(&data[0], byteLen - firstByte); // Save the bytes

if(byteLen % 2 != 0) //Add padding if number of bytes is odd.
  writter.SaveUnsignedChar ((unsigned char)0) ;

}

void VietVoice::Word::Save (std::wstring fileName)
{
	Save (fileName, 0, nbSamples) ;
}

unsigned char VietVoice::Word::GetByte (int index)
{
return data[index] ;
}

void VietVoice::Word::SetDefault ()
{
	formatTag = 1; // A tag indicating the format of the wave (PCM = 1)
	nbChannels = 1; // Number of channel (1 = Mono, 2 = Stereo) 
	samplesPerSecond = 4000; //The sampling rate of the wave (44100, 11000, etc) // 44100
	averageBytesPerSecond = 8000; //Number of bytes being "played" in a second // 88200
	blockAlign = 2; // Size of the sample frame in term of bytes // 2
	bitsPerSample = 16;  //Bits used to represents the sample (8 or 16 bits) // 16
	percentVolumePU1=100;
	percentVolumePU2=100;
	loaiIndex=0;
	percentPU1=100;
	percentPU2=0;
	positionPU1=0;
	positionPU2=0;
	viTri=0;
	//// data.empty(); //// [[[]]]
}

void VietVoice::Word::Load (File::Reader & reader)
{
	try
	{
		SetDefault ();
		int numBytes;

		int magic = reader.ReadLongJava();

		if ( magic != MAGIC) 
		{ //  Check the magic number
			
			throw VietVoice::VietnameseTTException (L"Error, wrong magic number");
		}

		
	   //  Ignore the duration
		reader.ReadFloatJava ();

		numBytes = reader.ReadLongJava ();
		data.resize (numBytes);
		size = numBytes;
		nbSamples = size / blockAlign;
		
	
		//  Read the data block (samples) of the word
		reader.ReadUnsignedCharArrayJava(&data[0], data.size ());

		
		
	}
	catch (File::Exception ex)
	{
		throw VietVoice::VietnameseTTException(ex.GetMsg ().c_str ());//L"Error, could not load a word");
	}

	
	return;
}

