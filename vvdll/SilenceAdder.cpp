#include "SilenceAdder.h"
#include <fstream>
#include <cstdlib>
#include <algorithm>
using namespace std;
VietVoice::SilenceAdder::SilenceAdder (std::wstring adjectiveName, std::wstring lightName, std::wstring prosodyName, std::wstring silenceDictionnaryName2, std::wstring silenceDictionnaryName3, std::wstring silenceDictionnaryName4, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, std::wstring linkingFile, std::wstring quantityFile, std::wstring nounFile, std::wstring verbFile, WordSelector & selector)
: _selector (&selector)
{
	//LoadSilenceDictionnary (silenceDictionnaryName);
	//LoadSilencesSize(silencesSizeFileName);
	//LoadWordsSize(wordsSizeFileName);
	std::vector< std::wstring > combinedWords2;
	File::Reader::ReadTextFileLines( combinedWords2, silenceDictionnaryName2, L"\t\r\n" );

	for(unsigned int i = 0; i < combinedWords2.size(); ++i)
	{
		StringHelper::RemoveSpace(combinedWords2[i]);
		std::pair<std::wstring, std::wstring> p (combinedWords2[i], combinedWords2[i]);
		_silenceDictionnary2.insert (p);
	}
// Viet them
	std::vector< std::wstring > combinedWords3;
	File::Reader::ReadTextFileLines( combinedWords3, silenceDictionnaryName3, L"\t\r\n" );

	for(unsigned int i = 0; i < combinedWords3.size(); ++i)
	{
		StringHelper::RemoveSpace(combinedWords3[i]);
		std::pair<std::wstring, std::wstring> p (combinedWords3[i], combinedWords3[i]);
		_silenceDictionnary3.insert (p);
	}
// End Viet them
// Viet them
	std::vector< std::wstring > combinedWords4;
	File::Reader::ReadTextFileLines( combinedWords4, silenceDictionnaryName4, L"\t\r\n" );

	for(unsigned int i = 0; i < combinedWords4.size(); ++i)
	{
		StringHelper::RemoveSpace(combinedWords4[i]);
		std::pair<std::wstring, std::wstring> p (combinedWords4[i], combinedWords4[i]);
		_silenceDictionnary4.insert (p);
	}
// End Viet them
	File::Reader::ReadTextFileLines( m_articlesDictionary, articlesFile);
	File::Reader::ReadTextFileLines( m_linkingDictionary, linkingFile);
	File::Reader::ReadTextFileLines( m_quantityDictionary, quantityFile);
	File::Reader::ReadTextFileLines( SilenceAdder::m_nounDictionary, nounFile);
	File::Reader::ReadTextFileLines( m_verbDictionary, verbFile);

// Viet them ==========================================================
	std::vector<std::wstring> v_prosody;
	File::Reader::ReadTextFileLines( v_prosody, prosodyName);
	this->v_volume = StringHelper::ToInt(v_prosody[0]);
	this->v_speed = StringHelper::ToInt(v_prosody[1]);
	this->v_pitch = StringHelper::ToInt(v_prosody[2]);
	this->v_compress = StringHelper::ToInt(v_prosody[3]); // speed = 47%
	this->v_main_vol = StringHelper::ToInt(v_prosody[4]); // Main volume = 100%
	this->v_soft_vol1 = StringHelper::ToInt(v_prosody[5]); // Soft volume 1= 100%
	this->v_soft_vol21 = StringHelper::ToInt(v_prosody[6]); // Soft volume 2= 100%
	this->v_soft_vol22 = StringHelper::ToInt(v_prosody[7]); // Soft volume 3 = 100%
	this->v_soft_vol3 = StringHelper::ToInt(v_prosody[8]); // Soft volume 4 = 100%
	this->v_dots = StringHelper::ToInt(v_prosody[9]); // Average = 5 dots
	this->v_fade = StringHelper::ToInt(v_prosody[10]); // xx% fade 
	this->v_samples = StringHelper::ToInt(v_prosody[11]); // xx sample(s)
//	::MessageBox(NULL, StringHelper::ToString(this->v_samples).c_str(), NULL, MB_OK);
// New
	this->v_per_pitch = StringHelper::ToInt(v_prosody[12]); // xx% change pitch
	this->v_pitch0 = StringHelper::ToInt(v_prosody[13]); // change pitch xx% for 0 
	this->v_pitch1 = StringHelper::ToInt(v_prosody[14]); // change pitch xx% for 1 
	this->v_pitch2 = StringHelper::ToInt(v_prosody[15]); // change pitch xx% for 2 
	this->v_pitch3 = StringHelper::ToInt(v_prosody[16]); // change pitch xx% for 3 
	this->v_pitch4 = StringHelper::ToInt(v_prosody[17]); // change pitch xx% for 4 
	this->v_pitch5 = StringHelper::ToInt(v_prosody[18]); // change pitch xx% for 5 

	this->v_compress0 = StringHelper::ToInt(v_prosody[19]); // compress xx% for 0
	this->v_compress1 = StringHelper::ToInt(v_prosody[20]); // compress xx% for 0
	this->v_compress2 = StringHelper::ToInt(v_prosody[21]); // compress xx% for 0
	this->v_compress3 = StringHelper::ToInt(v_prosody[22]); // compress xx% for 0
	this->v_compress4 = StringHelper::ToInt(v_prosody[23]); // compress xx% for 0
	this->v_compress5 = StringHelper::ToInt(v_prosody[24]); // compress xx% for 0
	this->v_not_echo = StringHelper::ToInt(v_prosody[25]); // bo qua khong echo xxx% dau tu
	this->v_cut_begin = StringHelper::ToInt(v_prosody[26]); // xx sample(s)
	this->v_cut_end = StringHelper::ToInt(v_prosody[27]); // xx sample(s)
	this->v_sc_comp = StringHelper::ToInt(v_prosody[28]); // nen sc %
// End Viet them ==========================================================

	std::vector<std::wstring> silenceSize;
	File::Reader::ReadTextFileLines( silenceSize, silencesSizeFileName);
	this->m_silenceSize = StringHelper::ToInt(silenceSize[0])*(100 - this->v_compress)/100;
	this->m_silenceSize2 = StringHelper::ToInt(silenceSize[1])*(100 - this->v_compress)/100;
	this->tooSmall = StringHelper::ToInt(silenceSize[2])*(100 - this->v_compress)/100;
	this->v_break_silence1 = StringHelper::ToInt(silenceSize[3])*(100 - this->v_compress)/100; // VT
	this->v_break_silence1 = int(this->v_break_silence1 / 2) * 2;
	this->v_break_silence2 = StringHelper::ToInt(silenceSize[4])*(100 - this->v_compress)/100; // VT
	this->v_break_silence2 = int(this->v_break_silence2 / 2) * 2;
	this->v_break_compound = float (StringHelper::ToInt(silenceSize[5])*(100 - this->v_compress)/100); // VT
	this->v_break_compound = this->v_break_compound / 2 * 2;
	this->v_compound_min = StringHelper::ToInt(silenceSize[6])*(100 - this->v_compress)/100; // VT
	this->v_compound_min = this->v_compound_min / 2 * 2; 
	this->v_stop_silence = StringHelper::ToInt(silenceSize[7])*(100 - this->v_compress)/100; // VT
	this->v_stop_silence = this->v_stop_silence / 2 * 2; 
	this->v_adjectives_sl = StringHelper::ToInt(silenceSize[8])*(100 - this->v_compress)/100; // VT
	this->v_adjectives_sl = this->v_adjectives_sl / 2 * 2;
	this->v_linking_sl = StringHelper::ToInt(silenceSize[9])*(100 - this->v_compress)/100; // VT
	this->v_linking_sl = this->v_linking_sl / 2 * 2;
	this->v_number_sl = StringHelper::ToInt(silenceSize[10])*(100 - this->v_compress)/100; // VT
	this->v_number_sl = this->v_number_sl / 2 * 2; 

	std::vector<std::wstring> silenceSize2;
	File::Reader::ReadTextFileLines( silenceSize2, wordsSizeFileName);
	this->mediumSilence = StringHelper::ToInt(silenceSize2[0])*(100 - this->v_compress)/100;
	this->mediumSilence = this->mediumSilence / 2 * 2; 
	this->mediumSilence2 = StringHelper::ToInt(silenceSize2[1])*(100 - this->v_compress)/100;
	this->mediumSilence2 = this->mediumSilence2 / 2 * 2; 
/*
0::20 (nén xx % của từ kép thứ nhất) 17 - giá trị nầy phải < 50
1::-15 (tăng xx % từ thứ hai) -10 - giá trị nầy phải > -50
2::22000 (word size nhỏ hơn giá trị nầy / 2 thì không compress) 7000
3::300 (ms sau dấu phẩy) 300
4::400 (ms sau dấu chấm) 400
5::20 (thêm silence = xx % của tổng độ dài hai từ của từ kép vào sau từ thứ hai) 20
6::5000 (nếu giá trị thêm vào ở thông số trên < xxxx thì thêm xxxx) 5000
7::4000 (thêm silence = xxxx vào sau các stop consonant) 3000
8::7000 (thêm xxxx / 2 sau các Adjectives) 12000
9::2000 (thêm xxxx / 2 sau các number hay date-time) 2000
*/
	std::vector<std::wstring> v_light;
	File::Reader::ReadTextFileLines(v_light, lightName);
	this->v_light_Words = StringHelper::ToString(v_light[0]);

	std::vector<std::wstring> v_adjective;
	File::Reader::ReadTextFileLines(v_adjective, adjectiveName);
	this->v_adjective_Words = StringHelper::ToString(v_adjective[0]);
// End VT =============================================================
}

VietVoice::SilenceAdder::SilenceAdder (const SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary2 = silenceAdder._silenceDictionnary2;
		_selector = silenceAdder._selector;
	}
}

VietVoice::SilenceAdder::~SilenceAdder ()
{
	// delete this; // Them va xoa 7-12-2012 khong ep phe
}

void VietVoice::SilenceAdder::operator = (const VietVoice::SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary2 = silenceAdder._silenceDictionnary2;
		_selector = silenceAdder._selector;
	}
}

void VietVoice::SilenceAdder::Treat (VietVoice::ToBeSynthetise & toSynthetise) 
{
	u_int type=1;
	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData();
	std::list<ToBeSynthetiseData> newListData;
	bool v_check = true ;
	int v_wordSize ;
	int v_wordSize1; 
	int v_wordSize2; 
	int accent;
	std::wstring wordName;
	std::wstring expression2 = L"";
	std::wstring expression3 = L"";
	std::wstring expression4 = L"";
	std::wstring expAfter1_1w = L"";
	std::wstring expAfter1_2w = L"";
	std::wstring expAfter1_3w = L"";
	std::wstring expAfter2_1w = L"";
	std::wstring expAfter2_2w = L"";
	std::wstring expAfter2_3w = L"";
	int nbSamples;
	int v_wordLength;
	std::wstring v_after;
	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		Word * word = NULL; // Doi len 7-12-2012
		accent = iter->GetAccent(); // int
		// Obains the accent for the word
		wordName = iter->GetWord (); // std::wstring
//::MessageBox (NULL, StringHelper::ToString (wordName).c_str (), StringHelper::ToString (L"1").c_str (), MB_OK) ;

				delete word; // 10-1-2013 << Leak here>> [[]] // hay hang up
				if (word != NULL) word = NULL;

		if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
		{
			word = _selector->FindWord(wordName, accent,type);
		}
		else if (wordName != L"silence") // An extra word, get it in database unless silence
		{
			word = _selector->GetExtraWord(wordName,type);
		}
//		if( word == NULL ) // VX
		if( word == NULL || v_check == false) // VS
		{
			newListData.push_back (*iter);
			delete word; // Leak [[]]
			if (word != NULL) word = NULL;
			v_check = true ;
			continue;
		}

//		std::list<ToBeSynthetiseData>::iterator iterBefore = iter;
//		if (iterBefore != listData.begin())
//			iterBefore--;

		std::list<ToBeSynthetiseData>::iterator iterAfter = iter;
		iterAfter++;



		ToBeSynthetiseData data = *iter;
		
		expression2 = L""; // 		std::wstring
		expAfter1_1w = L""; // 		std::wstring
		expAfter2_1w = L""; // 		std::wstring
		if (iterAfter != listData.end())
		{
			expression2 = data.GetOriginalWord(); //expression += L" "; // VLL
			expression2 += iterAfter->GetOriginalWord(); //StringHelper::RemoveSpace(expression);
//::MessageBox (NULL, StringHelper::ToString (expression2).c_str (), StringHelper::ToString (data.GetOriginalWord()).c_str (), MB_OK) ;
			expAfter1_1w = iterAfter->GetOriginalWord();
		}
// VT
		expression3 = L"";
		expAfter1_2w = L""; // 		std::wstring
		expression4 = L"";
		expAfter1_3w = L""; // 		std::wstring
		if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"")
		{
			if (iterAfter->GetPostPunctuation() == L"")	{
				iterAfter ++ ;
				if (iterAfter != listData.end()) {
					expression3 = expression2 + iterAfter->GetOriginalWord(); //StringHelper::RemoveSpace(expression);
					expAfter1_2w = expAfter1_1w + L" " + iterAfter->GetOriginalWord();
					expAfter2_1w = iterAfter->GetOriginalWord();
				}
				if (iterAfter != listData.end()) {
					if (iterAfter->GetPostPunctuation() == L"")	{
						iterAfter ++ ;
						if (iterAfter != listData.end()) {
							expression4 = expression3 + iterAfter->GetOriginalWord(); //StringHelper::RemoveSpace(expression);
							expAfter1_3w = expAfter1_2w + L" " + iterAfter->GetOriginalWord();
							expAfter2_2w = expAfter2_1w + L" " + iterAfter->GetOriginalWord();
						}



						if (iterAfter != listData.end()) {
							if (iterAfter->GetPostPunctuation() == L"")	{
								iterAfter ++ ;
								if (iterAfter != listData.end()) {
									expAfter2_3w = expAfter2_2w + L" " + iterAfter->GetOriginalWord();
								}




								iterAfter--;
							}
						}



						iterAfter--;
					}
				}
				iterAfter -- ;
			}
		}
// End VT

		if (wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
		{

			nbSamples = word->GetNbSamples(); // int
			if (nbSamples < this->mediumSilence / 2)
			{
				if(data.GetSilenceLength() == 0) {
				}
			}
			if (v_isStopConsonant(data.GetOriginalWord(),data.GetAccent())) {
				if (data.GetSilenceLength() == 0)
					data.SetSilenceLength(v_stop_silence / 2 * word->GetBlockAlign());
			}
			v_wordLength = data.GetSilenceLength() ; // int
// ****************************************************************************************************************
// ****************************************************************************************************************
			std::map<wstring,wstring>::iterator it;
			it = _silenceDictionnary4.find(expression4) ;

///////		if (_silenceDictionnary4.find(expression4) != _silenceDictionnary4.end())
			if (it != _silenceDictionnary4.end())
			if (it->first.length() == expression4.length())
			{
//// Từ thứ nhất
//::MessageBox (NULL, StringHelper::ToString (this->m_silenceSize).c_str (), StringHelper::ToString (expression4).c_str (), MB_OK) ;
				v_check = false ; // VT
				v_wordSize1 = 0 ; 
				unsigned int v_silence_4 = 5 ;
				if(nbSamples > this->tooSmall / 2)
				{
					data.m_percentageAdjust = float (this->m_silenceSize + v_silence_4 + 50) ;
					v_wordSize1 = word->GetNbSamples() ; // VT
				}
//// Từ thứ hai // khong ngung duoc
				wordName = iterAfter->GetWord (); // std::wstring 
//::MessageBox (NULL, StringHelper::ToString (L"[" + data.GetOriginalWord() + L"]").c_str (), StringHelper::ToString (expression4).c_str (), MB_OK) ;
				delete word; // 10-1-2013 << Leak here>> [[]]
				if (word != NULL) word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent,type);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName,type);
				}
				if(word != NULL) 
				{
					v_wordSize2 = word->GetNbSamples() ; // VT // int
					v_after = L"" ; // std::wstring
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
						if(word->GetNbSamples() > this->tooSmall / 2) {
							iterAfter->m_percentageAdjust = float (this->m_silenceSize + v_silence_4 + 50) ;
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength(0) ;
						}
					}
				}

				
//// Từ thứ ba
////			if (iterAfter != listData.end()) { // ---------------------------------------------------------------------------------------------------------------------------
				iterAfter ++ ; ////////
				wordName = iterAfter->GetWord (); // std::wstring 
//::MessageBox (NULL, StringHelper::ToString (wordName).c_str (), StringHelper::ToString (L"3").c_str (), MB_OK) ;
				delete word; // 10-1-2013 << Leak here>> [[]]
				if (word != NULL) word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent,type);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName,type);
				}
				if(word != NULL) 
				{
					v_wordSize2 = word->GetNbSamples() ; // VT // int
					v_after = L"" ; // std::wstring
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
						if(word->GetNbSamples() > this->tooSmall / 2) {
							iterAfter->m_percentageAdjust = float (this->m_silenceSize + v_silence_4 + 50) ;
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength(0) ;
						}
					}
				}
////			} //----------------------------------------------------------------------------------------------------------------------------------------------------------------
////// Từ thứ tư
				if (iterAfter != listData.end()) { //////// iterAfter->GetPostPunctuation() == L"")
				iterAfter ++ ; ////////
					/////////iterAfter ++ ;
					wordName = iterAfter->GetWord (); // std::wstring 
					delete word; // 10-1-2013 << Leak here>> [[]]
					if (word != NULL) word = NULL;

					if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
					{
						word = _selector->FindWord(wordName, accent,type);
					}
					else if (wordName != L"silence") // An extra word, get it in database unless silence
					{
						word = _selector->GetExtraWord(wordName,type);
					}

					if (word != NULL) 
					{
						if (iterAfter->GetSilenceLength() == 0)
							iterAfter->SetSilenceLength(this->tooSmall / 1) ; // [/2]
						// iterAfter -- ;
						if (iterAfter->GetPostPunctuation() == L"")	{
							if (iterAfter != listData.end()) {
								//////// iterAfter ++;
								if (iterAfter != listData.end()) { // UBND - iterAfter = "dân"
									v_after = iterAfter->GetOriginalWord() ;
									iterAfter->m_percentageAdjust = this->m_silenceSize2 * 1.45f - 50 ; // * 1








									iterAfter->SetSilenceLength(unsigned long (this->v_adjectives_sl * 1.1f / 1)) ; // New new new new // [/2.7f/
								}
							}
							// iterAfter --;
						}
					}
				}
				// iterAfter->m_percentageAdjust = this->m_silenceSize;
			}
//// ****************************************************************************************************************
//// ****************************************************************************************************************
			unsigned int v_silence_3 = 3 ;
			if (_silenceDictionnary3.find (expression3) != _silenceDictionnary3.end())
			{
//// Từ thứ nhất
				v_check = false ; // VT
				v_wordSize1 = 0 ; 
				if(nbSamples > this->tooSmall / 2)
				{
					data.m_percentageAdjust = float (this->m_silenceSize + v_silence_3 + 50) ;
					v_wordSize1 = word->GetNbSamples() ; // VT
				}
//// Từ thứ hai
				wordName = iterAfter->GetWord (); // std::wstring 
				delete word ; // 10-1-2013 << Leak here>> [[]]
				if (word != NULL) word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent,type);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName,type);
				}
				if(word != NULL) 
				{
					v_wordSize2 = word->GetNbSamples() ; // VT // int
					v_after = L"" ; // std::wstring
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
						if(word->GetNbSamples() > this->tooSmall / 2) {
							iterAfter->m_percentageAdjust = float (this->m_silenceSize + v_silence_3 + 50) ;
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength(0) ;
						}
					}
				}
//// Từ thứ ba
				iterAfter ++ ;
				wordName = iterAfter->GetWord (); // std::wstring 
				delete word; // 10-1-2013 << Leak here>> [[]]
				if (word != NULL) word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent,type);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName,type);
				}
				if(word != NULL) 
				{
					if (iterAfter->GetSilenceLength() == 0)
						iterAfter->SetSilenceLength(this->tooSmall / 2) ;
					iterAfter -- ;
					if (iterAfter->GetPostPunctuation() == L"")	{
						iterAfter ++;
						if (iterAfter != listData.end()) {
							v_after = iterAfter->GetOriginalWord() ;
							iterAfter->m_percentageAdjust = this->m_silenceSize2 * 1.25f - 50 ; // * 1
							iterAfter->SetSilenceLength(this->v_adjectives_sl / 2) ; // New new new new // [/3]
//::MessageBox (NULL, StringHelper::ToString (iterAfter->GetSilenceLength()).c_str (), StringHelper::ToString (iterAfter->GetOriginalWord()).c_str (), MB_OK) ;
						}
						iterAfter --;
					}
				}
				//// iterAfter->m_percentageAdjust = this->m_silenceSize; //// [Lay lai]
			}
// *********************************************************************************************
// *********************************************************************************************
			else if (_silenceDictionnary2.find (expression2) != _silenceDictionnary2.end())
			{
				v_check = false ; // VT
				v_wordSize1 = 0 ; // int
				if (nbSamples > this->tooSmall / 2)
				{
					data.m_percentageAdjust = float (this->m_silenceSize + 50) ; // nén từ thứ nhất
				}
				else
				{
					data.m_percentageAdjust = 1 + 50 ; // nén từ thứ nhất
				}

				v_wordSize1 = word->GetNbSamples() ; // VT
				wordName = iterAfter->GetWord (); // std::wstring 
				// Word * word = NULL; // Sua 7-12-2012
				delete word; // 10-1-2013 << Leak here>> [[]]
				if (word != NULL) word = NULL;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent,type);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName,type);
				}
				
				if (word != NULL) 
				{
					v_wordSize2 = word->GetNbSamples() ; // VT // int
					v_wordSize = int ((v_wordSize1 + v_wordSize2) * v_break_compound / 100) ;
					if (v_wordSize < v_compound_min) v_wordSize = v_compound_min ; // VT
// Viet sua adjective ---------------------------------------------------------------
					v_after = L"" ; // std::wstring 
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
						if (iterAfter->GetPostPunctuation() == L"")	{
							iterAfter ++;
							if (iterAfter != listData.end()) {
								v_after = L"/ " ;
								v_after.append(iterAfter->GetOriginalWord()) ;
								v_after.append(L"/") ;
							}
							iterAfter --;
						}
					}
					if (v_adjective_Words.find(v_after, 0) == string::npos) { // từ sau từ thứ hai không là adjective
////::MessageBox (NULL, StringHelper::ToString (v_after).c_str (), StringHelper::ToString (L"Non Adj").c_str (), MB_OK) ;
						if (word->GetNbSamples() > this->tooSmall / 2 && iterAfter->GetSilenceLength() == 0) {
							iterAfter->SetSilenceLength(v_wordSize) ; // Xoa 9-9-2012 :VT // 27-8-2012 : v_wordSize -> v_breakcompound
						}
						if (v_isStopConsonant(iterAfter->GetOriginalWord(),iterAfter->GetAccent())==false) { // khong stretch SC
							iterAfter->m_percentageAdjust = float (this->m_silenceSize2 / 2 - 50) ; // VT : giản từ thứ 2
							if (iterAfter->GetSilenceLength() == 0) 
								iterAfter->SetSilenceLength(this->tooSmall / 2) ; // VT
						} else { // 
							if (iterAfter->GetSilenceLength() == 0) 
								iterAfter->SetSilenceLength(this->tooSmall / 1) ; // VT
						}

					} else { // từ sau từ thứ hai là adjective
////::MessageBox (NULL, StringHelper::ToString (v_after).c_str (), StringHelper::ToString (L"Adj").c_str (), MB_OK) ;
						if (v_after != L"") { // v_after là từ thứ ba, iterAfter là từ thừ nhì của từ kép
							// Tu thu ba khong la SC
							if (v_isStopConsonant(iterAfter->GetOriginalWord(), iterAfter->GetAccent()) == false) { // V moi them
								iterAfter->m_percentageAdjust = float (this->m_silenceSize + 50) ; // VT không là SC
//---------------------------------------------------------------------------------
								iterAfter++;
//*								iterAfter->SetSilenceLength(this->mediumSilence);
								if (iterAfter->GetSilenceLength() == 0) {
									iterAfter->SetSilenceLength(this->v_adjectives_sl); // them silence vao sau tu thu ba
									iterAfter->m_percentageAdjust = float (m_silenceSize2 - 50) ; ////////////////// new 2012
								}
								iterAfter--;
//---------------------------------------------------------------------------------
							} //Tu thu ba la SC
							else { // Viet moi them - Them silence vao sau stop consonant
// -------------------------
//::MessageBox (NULL, StringHelper::ToString (v_after).c_str (), StringHelper::ToString (L"Non Adj").c_str (), MB_OK) ;
								iterAfter++;
								iterAfter->m_percentageAdjust = float (this->m_silenceSize + 50) ; // VT : nén từ thứ 2
								if (iterAfter->GetSilenceLength() == 0)
									iterAfter->SetSilenceLength(this->mediumSilence);
								iterAfter--;
							}
//-----------------------
						}
						else { // tu thu ba la ""
							if (v_isStopConsonant(iterAfter->GetOriginalWord(),iterAfter->GetAccent())==false) // khong stretch SC
								iterAfter->m_percentageAdjust = float (this->m_silenceSize2 / 2 - 50) ; // VT : giản từ thứ hai
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength(v_wordSize) ; // Xoa 9-9-2012: 27-8-2012 v_wordSize -> v_breakcompound
						}
					}
				}
			}
// End Viet sua adjective -----------------------------------------------------------
// **********************************************************************************
			else if( data.m_wordType == ToBeSynthetiseData::ABBR_S) // VT 31-8-2012
			{
				if (iterAfter->GetOriginalWord() == L"silence" || iterAfter->GetOriginalWord() == L"_silence_") {
					data.m_percentageAdjust = float (this->m_silenceSize * 0.32 + 50) ; // Sua 18-8-2012 * 3 / 2 -50 //  * 0.32 // 0.39
				}
				else {
					data.m_percentageAdjust = this->m_silenceSize * 1.3f + 50; // Nén number nhiều hơn // 1.4
				}
			}
// **********************************************************************************
			else if( data.m_wordType == ToBeSynthetiseData::ABBR_L) // VT 10-9-2012
			{
				if (iterAfter->GetOriginalWord() == L"silence" || iterAfter->GetOriginalWord() == L"_silence_") {
					data.m_percentageAdjust = float (m_silenceSize * 0.0 + 0) ; // Sua 18-8-2012 * 3 / 2 -50 //  * 0.32 // 0.39 + 50
				}
				else {
					data.m_percentageAdjust = float (this->m_silenceSize * 1.05 + 50) ; // Nén number nhiều hơn // * 3 / 2 // * 1.43 / 1.45
				}
			}
// **********************************************************************************
			else if( data.m_wordType == ToBeSynthetiseData::DATE || data.m_wordType == ToBeSynthetiseData::NUMBER || data.m_wordType == ToBeSynthetiseData::SYMBOL)
			{
				if (iterAfter != listData.end()) { // if ------------------------------------ 11-9-2012
					if (iterAfter->GetOriginalWord() == L"silence" || iterAfter->GetOriginalWord() == L"_silence_") {
//::MessageBox(NULL, StringHelper::ToString(data.GetOriginalWord()).c_str(), L"Adder", MB_OK);
						data.m_percentageAdjust = m_silenceSize * 0.34f + 50 ; // 0.39
					}
					else {


						////if (data.GetOriginalWord() != L"") {
						////	if (data.GetOriginalWord() == L"mươi" || data.GetOriginalWord() == L"mười" 
						////		|| data.GetOriginalWord() == L"trăm" || data.GetOriginalWord() == L"chục" 
						////		|| data.GetOriginalWord() == L"ngàn" || data.GetOriginalWord() == L"triệu" 
						////		|| data.GetOriginalWord() == L"tĩ" || data.GetOriginalWord() == L"tỷ" 
						////		|| data.GetOriginalWord() == L"đồng" || data.GetOriginalWord() == L"phẩy") {
						////		data.m_percentageAdjust = this->m_silenceSize * 1.84f + 50; // Nén number nhiều hơn // * 3 / 2 // * 1.43 // * 1.69 // 1.79
						////	}
						////	else { // ngày/ giờ
						////		data.m_percentageAdjust = this->m_silenceSize * 1.60f + 50; // Nén ít hơn // * 3 / 2 // * 1.43 / 1.45 // 1.55
						////	}
						////}



					}
				}
			} // end if ----------------------------- 11-9-2012




////			else if ( std::find( this->m_articlesDictionary.begin(), this->m_articlesDictionary.end(), iter->GetOriginalWord ()) != this->m_articlesDictionary.end() ) // VS
////			{
////				if( data.GetPostPunctuation() != L"," &&
////					data.GetPostPunctuation() != L";" &&
////					data.GetPostPunctuation() != L"." &&
////					data.GetPostPunctuation() != L"?" &&
////					data.GetPostPunctuation() != L"!" &&
////					data.GetPostPunctuation() != L":" ) {
////// new ----------------
////					if (iterAfter != listData.end() && std::find( this->m_linkingDictionary.begin(), this->m_linkingDictionary.end(), iterAfter->GetOriginalWord ()) != this->m_linkingDictionary.end() ) // VS
////					{
////					//* if (iter->GetSilenceLength() == 0)
////						iterAfter->m_percentageAdjust = float (this->m_silenceSize2) ; // New new new
////						data.SetSilenceLength(v_linking_sl / 2) ; // v_adjectives_sl
////					//-- data.m_percentageAdjust=m_silenceSize2 / 2 ; // New new new new
////					}
////// new ----------------
////					v_check = true ; // VT
////					//if(nbSamples > (this->tooSmall * (100 - v_compress) / 100) / 2) // VLL
////					if (nbSamples > this->tooSmall / 2) // VLL
////					{ // VLL
////						data.m_percentageAdjust = float (this->m_silenceSize + 50) ; // VLL
////						if (data.GetSilenceLength() == 0)
////							data.SetSilenceLength(0) ;
////					} // VLL
////				} // chu thich: medium silence = word silence sau moi tu
////				else
////					if (data.GetSilenceLength() == 0)
////						data.SetSilenceLength(this->v_adjectives_sl /2 * word->GetBlockAlign()) ; // VT các mạo từ có silence length = word length
////			}



////// Viet them linking words -----------------------------------------------------------
////			else if (iterAfter != listData.end()) // them else -> else if
////				if (std::find( this->m_linkingDictionary.begin(), this->m_linkingDictionary.end(), iterAfter->GetOriginalWord ()) != this->m_linkingDictionary.end() ) // VS
////				{
////					v_check = true ; ///////////////////////////////////////////// 2012
////					//* if (iter->GetSilenceLength() == 0)
////					/////////iterAfter->m_percentageAdjust=this->m_silenceSize2 -50; // New new new
////
////
////
////
////
////
////					if (data.GetSilenceLength()==0) { // [Moi them If]
////						data.m_percentageAdjust = float (m_silenceSize2 * 5 / 4  - 50) ; // 3 / 2
////						data.SetSilenceLength(v_linking_sl / 2) ; // v_adjectives_sl
////					} // [Moi them]
////
////					//-- data.m_percentageAdjust=m_silenceSize2 / 2 ; // New new new new
////				}
////// End Viet them linking words -----------------------------------------------------------

				
				
// không ép phê, cắt bỏ -> co ep phe
			// Neu tu la adjective, truoc khong la compound words


////			else if (v_adjective_Words.find(data.GetOriginalWord(),0) != string::npos) { // Adjective words
////				if (iter != listData.begin()) {
////					iter -- ;
////					iter->m_percentageAdjust = float (m_silenceSize) ; // New new new new
////					iter ++ ;
////					iter->m_percentageAdjust = float (m_silenceSize2) ; // New 14-7-2012
////					iter->SetSilenceLength(v_adjectives_sl * 10) ; // New 14-7-2012
////				}
////			}
////// không ép phê, cắt bỏ -> co ep phe
////// ****************************************************************************************************************
////// Viet sua adjective ---------------------------------------------------------------
////			else 
////			{
////				v_after = L"" ;
////				if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
////				{
////					if (iter->GetPostPunctuation() == L"")	{
////						if (iterAfter != listData.end()) 
////							v_after = iterAfter->GetOriginalWord() ;
////						if (v_adjective_Words.find(v_after,0) != string::npos) {
////							if (v_isStopConsonant(data.GetOriginalWord(), data.GetAccent()) == false)
////								data.m_percentageAdjust = float (this->m_silenceSize + 50) ; // VT
////							if (iterAfter->GetSilenceLength() == 0)
////								iterAfter->SetSilenceLength(unsigned long ((iter->GetWord().size() + iterAfter->GetWord().size()) * v_break_compound / 100)) ; // VT
////						}
////					}
////				}
////			}



// ****************************************************************************************************************
			////// article
			////if (iter != listData.end())
			////	if (std::find( this->m_articlesDictionary.begin(), this->m_articlesDictionary.end(), iter->GetOriginalWord ()) != this->m_articlesDictionary.end()) // VS
			////	{					iter ++ ;
			////		if (iter != listData.end()) 
			////		{
			////			if (iter->GetOriginalWord() != L"" && std::find( this->m_articlesDictionary.begin(), this->m_articlesDictionary.end(), iter->GetOriginalWord ()) != this->m_articlesDictionary.end())  // Adjective words
			////				{
			////				iter->m_percentageAdjust = float (m_silenceSize2 - 50) ; // gian adjective 
			////				//////////iter->SetSilenceLength(v_adjectives_sl / 2) ;
			////				}
			////		}
			////		iter -- ;
			////		iter->m_percentageAdjust = float (m_silenceSize + 50) ; // New new new new
			////		iter->SetSilenceLength(mediumSilence) ;
			////	} // New 20-6-2012 : đều đều, xanh xanh


			////if (iterAfter != listData.end()) 
			////	if (iter->GetOriginalWord() == iterAfter->GetOriginalWord() && iter->GetOriginalWord() != L"năm")  // eception: nam nam rong
			////	{
			////		data.m_percentageAdjust = float (m_silenceSize + 50) ;
			////		iterAfter->m_percentageAdjust = m_silenceSize2 * 1.65f - 50 ; // Sua ngay 19-8-2012 / * 1 // 1.4 // 1.50 / 1.55
			////		iterAfter->SetSilenceLength(unsigned long (word->GetNbSamples() * 0.69f)) ; // 0.29 // 0.49 
			////	}



		}


// ****************************************************************************************************************




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// quá nhiều + Nound			
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
//// ** Tìm biểu thức Quantity --------------------------------------------------------------------------------------
//			bool v_find1 = false;
//			bool v_find2 = false;
//			int v_rate = 2.0f;
//			std::wstring quantityExp = L"";
//			if (iter != listData.end()) { // Chỉ còn từ đầu của Từ kép 2/ 3/ 4
//				quantityExp = data.GetOriginalWord(); // Tìm 2 từ chỉ số lượng (nhiều/ rất nhiều...)
//				if (iterAfter != listData.end())
//				{
//					quantityExp += L" " + iterAfter->GetOriginalWord();
//					if (std::find( this->m_quantityDictionary.begin(), this->m_quantityDictionary.end(), quantityExp) != this->m_quantityDictionary.end()) 
//					{
//						v_find2 = true; // Tìm thấy (rất nhiều)
//					}
//					else if (std::find( this->m_quantityDictionary.begin(), this->m_quantityDictionary.end(), data.GetOriginalWord()) != this->m_quantityDictionary.end()) // Tìm không thấy (rất nhiều) => Tìm (rất) 
//					{
//						v_find1 = true; // Tìm thấy (rất)					
//					}
//				} // Đang ở từ đầu (rất)
//				if (std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_3w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_2w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_1w) != this->m_nounDictionary.end())
//				{
//					if (v_find1 == true) {
//						iter->m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 1 (ít hơn)
//						if (iter != listData.begin()) {
//							iter --;
////							iter->SetSilenceLength(v_adjectives_sl * 5);
//							iter->SetSilenceLength(99999);
//							iter ++;
//						}
//
////::MessageBox(NULL, StringHelper::ToString(iter->GetOriginalWord()).c_str(), L"11", MB_OK);
//					}
//				}
//
//				if (std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_3w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_2w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_1w) != this->m_nounDictionary.end())
//				{
//					if (v_find2 == true) {
//						if (iterAfter != listData.end()) { // Từ thứ 1, chú ý là [data.] mới ép phê chứ không phải [iter->]
//							data.m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 1 (ít hơn)
//							iterAfter->m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 2 (nhiều hơn)
//							if (iter != listData.begin()) {
//								iter --;
////::MessageBox(NULL, StringHelper::ToString(iter->GetOriginalWord()).c_str(), L"Silence", MB_OK);
//								iter->SetSilenceLength(v_adjectives_sl * 2);
////::MessageBox(NULL, StringHelper::ToString(iter->GetSilenceLength()).c_str(), L"Silence", MB_OK);
//								iter ++;
//							}
//							v_check = false;
//						}
//					}
//				}
//			}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
//// Verb + Noun (ăn hủ tíu)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
//			v_find1 = false;
//			v_find2 = false;
//			std::wstring verbExp = L"";
//			if (iter != listData.end()) { // Chỉ còn từ đầu của Từ kép 2/ 3/ 4
//				verbExp = data.GetOriginalWord(); // Tìm 2 từ chỉ số lượng (nhiều/ rất nhiều...)
//				if (iterAfter != listData.end())
//				{
//					verbExp += L" " + iterAfter->GetOriginalWord();
//					if (std::find( this->m_verbDictionary.begin(), this->m_verbDictionary.end(), verbExp) != this->m_verbDictionary.end()) {
//						v_find2 = true; // Tìm thấy (rất nhiều)
//					}
//					else if (std::find( this->m_verbDictionary.begin(), this->m_verbDictionary.end(), data.GetOriginalWord()) != this->m_verbDictionary.end()) { // Tìm không thấy (rất nhiều) => Tìm (rất) 
//						v_find1 = true; // Tìm thấy (rất)					
//					}
//				} // Đang ở từ đầu (ăn)
//				if (std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_3w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_2w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter1_1w) != this->m_nounDictionary.end())
//				{
//					if (v_find1 == true) {
//						iter->m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 1 (ít hơn)
////::MessageBox(NULL, StringHelper::ToString(iter->GetOriginalWord()).c_str(), L"11", MB_OK);
//						if (iter != listData.begin()) {
//							iter --;
////::MessageBox(NULL, StringHelper::ToString(iter->GetOriginalWord()).c_str(), L"Silence", MB_OK);
//							iter->SetSilenceLength(v_adjectives_sl * 5);
////::MessageBox(NULL, StringHelper::ToString(iter->GetSilenceLength()).c_str(), L"Silence", MB_OK);
//							iter ++;
//						}
//					}
//				}
//
//				if (std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_3w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_2w) != this->m_nounDictionary.end()
//				  ||std::find(this->m_nounDictionary.begin(), this->m_nounDictionary.end(), expAfter2_1w) != this->m_nounDictionary.end())
//				{
//					if (v_find2 == true) {
//						if (iterAfter != listData.end()) { // Từ thứ 1, chú ý là [data.] mới ép phê chứ không phải [iter->]
//							data.m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 1 (ít hơn)
//							iterAfter->m_percentageAdjust = float (m_silenceSize2 * v_rate + 50) ; // Nén từ thứ 2 (nhiều hơn)
//							if (iter != listData.begin()) {
//								iter --;
////::MessageBox(NULL, StringHelper::ToString(iter->GetOriginalWord()).c_str(), L"Silence", MB_OK);
//								iter->SetSilenceLength(v_adjectives_sl * 2);
////::MessageBox(NULL, StringHelper::ToString(iter->GetSilenceLength()).c_str(), L"Silence", MB_OK);
//								iter ++;
//							}
//							v_check = false;
//						}
//					}
//				}
//			}
// ****************************************************************************************************************


		newListData.push_back( data );
		delete word; // 7-12-2012 << Leak memory>> [[]]
		if (word != NULL) word = NULL; // 7-12-2012
		// data.~ToBeSynthetiseData(); // Them va xoa 7-12-2012 khong ep phe
	} // End for
	toSynthetise.SetListData (newListData);
}

void VietVoice::SilenceAdder::_LoadSilencesSize(std::wstring filename) 
{
	File::Reader reader (filename);
reader.ReadShort();
	
	smallSilence = ReadIntFromFile(reader);
	mediumSilence = ReadIntFromFile(reader);
	longSilence = ReadIntFromFile(reader);
}

void VietVoice::SilenceAdder::_LoadWordsSize(std::wstring filename)
{
	File::Reader reader (filename);

	reader.ReadShort();
	smallWord  = ReadIntFromFile(reader);
	mediumWord  = ReadIntFromFile(reader);
	longWord  = ReadIntFromFile(reader);
}

int VietVoice::SilenceAdder::ReadIntFromFile(File::Reader & reader)
{
	wchar_t car;
	std::wstring line;
	do 
	{
		car = reader.ReadShort();
		if(car != L'\n')
		{
			line.push_back(car);
		}
	} while (car != L'\n');
	std::wistringstream stream( line );
	int value = 0;
	stream >> value;
	return value;
}
long VietVoice::SilenceAdder::v_getBreakSilence1() // VT
{
	return v_break_silence1;
}
long VietVoice::SilenceAdder::v_getBreakSilence2() // VT
{
	return v_break_silence2;
}
long VietVoice::SilenceAdder::v_getVolume() // VT
{
	return v_volume;
}
long VietVoice::SilenceAdder::v_getSpeed() // VT
{
	return v_speed;
}
long VietVoice::SilenceAdder::v_getPitch() ////// VT
{
	return v_pitch;
}
void VietVoice::SilenceAdder::v_getLight(std::wstring &v_lws) // VT
{
	v_lws = v_light_Words;
}
void VietVoice::SilenceAdder::v_getAdjective(std::wstring &v_ads) // VT
{
	v_ads = v_adjective_Words;
}
bool VietVoice::SilenceAdder::v_isStopConsonant(std::wstring v_word, int v_accent) // VT
{
	if (v_word.size() < 2) return false ;
	if ((v_word.substr(v_word.size()-2,2) == L"ch" || v_word.substr(v_word.size()-1,1) == L"p" || v_word.substr(v_word.size()-1,1) == L"t" || v_word.substr(v_word.size()-1,1) == L"c") && (v_accent == 1 || v_accent != 5))
		return true ;
	//if (v_word.substr(v_word.size()-2,2) != L"nh" || v_word.substr(v_word.size()-2,2) != L"ng")
		//return true ;
	// if (v_accent != 1 && v_accent != 5) return false ;
	return false ;
}
long VietVoice::SilenceAdder::v_getCompress() // VT
{
	return v_compress;
}
long VietVoice::SilenceAdder::v_getWordSize() // VT
{
	return mediumSilence;
}
long VietVoice::SilenceAdder::v_getWordSize2() // VT
{
	return mediumSilence2;
}
long VietVoice::SilenceAdder::v_get_main_vol() // VT
{
	return v_main_vol;
}
long VietVoice::SilenceAdder::v_get_soft_vol1() // VT
{
	return v_soft_vol1;
}
long VietVoice::SilenceAdder::v_get_soft_vol21() // VT
{
	return v_soft_vol21;
}
long VietVoice::SilenceAdder::v_get_soft_vol22() // VT
{
	return v_soft_vol22;
}
long VietVoice::SilenceAdder::v_get_soft_vol3() // VT
{
	return v_soft_vol3;
}
long VietVoice::SilenceAdder::v_getCW1() // VT
{
	return m_silenceSize;
}
long VietVoice::SilenceAdder::v_getCW2() // VT
{
	return m_silenceSize2;
}
long VietVoice::SilenceAdder::v_getStopSilence() // VT
{
	return v_stop_silence;
}
long VietVoice::SilenceAdder::v_getDots() // VT
{
	return v_dots;
}
long VietVoice::SilenceAdder::v_getNotEcho() // VT
{
	return v_not_echo;
}
long VietVoice::SilenceAdder::v_Fade() // VT
{
	return v_fade;
}
long VietVoice::SilenceAdder::v_Samples() // VT
{
	return v_samples;
}
long VietVoice::SilenceAdder::v_get_per_pitch() // VT
{
	return v_per_pitch;
}
long VietVoice::SilenceAdder::v_get_pitch0() // VT
{
	return v_pitch0;
}
long VietVoice::SilenceAdder::v_get_pitch1() // VT
{
	return v_pitch1;
}
long VietVoice::SilenceAdder::v_get_pitch2() // VT
{
	return v_pitch2;
}
long VietVoice::SilenceAdder::v_get_pitch3() // VT
{
	return v_pitch3;
}
long VietVoice::SilenceAdder::v_get_pitch4() // VT
{
	return v_pitch4;
}
long VietVoice::SilenceAdder::v_get_pitch5() // VT
{
	return v_pitch5;
}
long VietVoice::SilenceAdder::v_get_com0() // VT
{
	return v_compress0;
}
long VietVoice::SilenceAdder::v_get_com1() // VT
{
	return v_compress1;
}
long VietVoice::SilenceAdder::v_get_com2() // VT
{
	return v_compress2;
}
long VietVoice::SilenceAdder::v_get_com3() // VT
{
	return v_compress3;
}
long VietVoice::SilenceAdder::v_get_com4() // VT
{
	return v_compress4;
}
long VietVoice::SilenceAdder::v_get_com5() // VT
{
	return v_compress5;
}
long VietVoice::SilenceAdder::v_get_c_begin() // VT
{
	return v_cut_begin ;
}
long VietVoice::SilenceAdder::v_get_c_end() // VT
{
	return v_cut_end ;
}
long VietVoice::SilenceAdder::v_get_sc_comp() // VT
{
	return v_sc_comp ;
}

/*
- những chữ như:  du khách, mát máy, Bãi Cháy, ngày nay...  có vẻ bị cắt hơi lố
- những chữ loại "nhỏ nhẹ" : ở, và, ... còn khá dài.
- chấm xuống hàng không có silence hay quá ít.

Đọc ngày 19/2 là 19 trên 2, có lẽ giải thuật cho vụ này còn thiếu sót vì quá nhiều case phải xử lý.
*/
int VietVoice::SilenceAdder::v_count_words(std::wstring v_exp) // VT
{
	if (v_exp == L"") return 0;
	int wordCount = 0;
	for (int i=0; i < v_exp.length(); i++)
		if (v_exp[i] == L' ') wordCount +=1;
	return wordCount + 1;
}

//bool VietVoice::SilenceAdder::v_find_content(vector <wstring> v_dic, wstring v_exp) // VT
//{
//	if (find(v_dic.begin(), v_dic.end(), v_exp) != v_dic.end())
//		return true;
//	return false;
//}
//bool VietVoice::SilenceAdder::v_find_Noun(std::wstring v_exp) // VT
//{
////std::vector<SilenceAdder*> SilenceAdder::m_nounDictionary;
//	if (VietVoice::SilenceAdder::v_find_noun(v_exp))
//		return true;
//	return false;
//}
long VietVoice::SilenceAdder::v_getLinkingSilence() // VT
{
	return v_linking_sl;
//	this->v_adjectives_sl = this->v_adjectives_sl / 2 * 2;
//	this->v_linking_sl = StringHelper::ToInt(silenceSize[9])*(100 - this->v_compress)/100; // VT
}
