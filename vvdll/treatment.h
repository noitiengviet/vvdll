#if !defined (TREATEMENT_H)

#define TREATEMENT_H

#include "ToBeSpoken.h"


#pragma warning( disable : 4786 ) 

namespace VietVoice
{
	/**
	 * Class Name:         Interface
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Abastract data type.  Every object that treats the input
	 *                     text to be spoken (like TokenToWords and WordSelector
	 *                     for instance) must implement this interface
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * september 17 2005:  Converted from java to C++
	 * November 29 2005 -> Added destructor in case
	 * March 6 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 */

	class Treatement
	{
	public:

		virtual ~Treatement ();

		/**
		* Treat method that each treatement inheriters implements
		* in order to perform the various task nesscessary to perform 
		* TTS
		*
		* Parameters:
		*
		* ToBeSpoken & toSpoke:  Various data related tothe TTS
		*/
		virtual void Treat (ToBeSynthetise & toSpoke) = 0;
	};
}

#endif
