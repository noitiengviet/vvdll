#if !defined (VIETNAMESETTSEXCEPTION_H)

#define VIETNAMESETTSEXCEPTION_H

#include <string>


namespace VietVoice
{
	/**
	 * Class Name:         VietnameseTTSException
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class thrown as an exception when an error occurs
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * october 8 2005:  Created this file and coded the class.
	 * Noveber 29 2005 -> Added destructor in case
	 * novembre 29 2005:  Implemented the copy constructor and the = operator
	 * March 6 2006:  Seperated implementation in source and header
	 * June 10 2006: Change the methods name so they start by a capital letter.
	 */
	class VietnameseTTException
	{
	public:

		VietnameseTTException (std::wstring const & msg);

		~VietnameseTTException ();

		VietnameseTTException  (const VietnameseTTException & ex);

		void operator = (const VietnameseTTException & ex);

		/**
		* Obtain the error message linked to the exception
		*
		* Return value:  The error message
		*/
		std::wstring GetMsg() const;

	private:

		std::wstring _msg;
	};
}

#endif
