#if !defined (TOKEN_H)

#define TOKEN_H

#include <string>
#include <windows.h>

#pragma warning( disable : 4786 ) 

using namespace std;

namespace VietVoice
{

	/**
	 * Class Name:         Token
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 13 2004
	 *
	 * Description:        Class used to the data of token
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * september 17 2005:  Converted from java to C++
	 * september 21 2005:  Implemented the isEmpty and hasPostPunc methods
	 * novembre 29 2005:  Implemented the copy constructor and the = operator	 
	 * March 4 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 17 2006:  Improved the comments of the .h and .cpp files as well as the spacing
	 */

	class Token
	{
	public:



		/**
		* Constructor
		*/
		Token ();

		/**
		* Destructor
		*/
		~Token ();

		/**
		* Copy constructor.
		*
		* Parameters:
		*
		* const Token & token:  Object that is bein copyied
		*/
		Token  (const Token & token);

		/**
		* Assignement operator used to avoid a memory leak
		*/
		void operator = (const Token & token);

		/**
		 * Changes the token name
		 *
		 * Parameters:
		 * 
		 * std::wstring token The new token name
		 */
		void SetToken (std::wstring token);

		/**
		 * Obtains the token name
		 *
		 * return value: The token name
		 */
		wstring GetToken ();

		/**
		 * Sets the token's pre-punctuation
		 *
		 * Parameters
		 *
		 * wstring punc The new pre-punctuation
		 */
		void SetPrePunctuation (wstring punc);

		/**
		 * Obtains the token prePunctuation
		 *
		 * return value: The token prePunctuation
		 */
		wstring GetPrePunctuation ();

		/**
		 * Obtains the token postPunctuation
		 *
		 * return value: The token postPunctuation
		 */
		void SetPostPunctuation (wstring punc);

		/**
		 * Obtains the token postPunctuation
		 *
		 * return value The token postPunctuation
		 */
		wstring GetPostPunctuation ();

		/**
		 * Determine if the object does not contain a token
		 *
		 * return value: true if the object is empty, else false
		 **/
		bool IsEmpty ();

		/**
		 * Determine if the current token is followed by a
		 * punctuation mark
		 *
		 * return value; true if the object is followed by a punctuation mark, else false
		 **/
		bool HasPostPunc ();

	private:

		wstring _token; // Value of the token
		wstring _prepunc; // Punctuation before the token
		wstring _postpunc; // Punctuation after the token
	};
}

#endif