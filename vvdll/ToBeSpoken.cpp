
#include "ToBeSpoken.h"
// #include "StringHelper.h" // VT

/**
* Copy constructor
*
* Parameters:
* 
* const ToBeSynthetise & toBeSynthetise:  The objct being copied
*/
VietVoice::ToBeSynthetise::ToBeSynthetise  (const ToBeSynthetise & toBeSynthetise)
{
	if (this != &toBeSynthetise)  // Make sure we are not copying the object into itself
	{
		  _listToken = toBeSynthetise._listToken;
		  _listData = toBeSynthetise._listData; 
		  _avgLengthSilence = toBeSynthetise._avgLengthSilence;
		  _speed = toBeSynthetise._speed;
		  _volume = toBeSynthetise._volume;
		  _pitch = toBeSynthetise._pitch;
		  _wsola = toBeSynthetise._wsola;
		  _vbs = toBeSynthetise._vbs;
		  _mp3Name = toBeSynthetise._mp3Name;
		  _mustPlay = toBeSynthetise._mustPlay;
		  _curVoice = toBeSynthetise._curVoice;
	}
}

/**
* Assignement operator used to avoid memory leaks
*/
void VietVoice::ToBeSynthetise::operator = (const ToBeSynthetise & toBeSynthetise)
{
	if (this != &toBeSynthetise) // Make sure we aren't copying the object into itself.
	{
		  _listToken = toBeSynthetise._listToken;
		  _listData = toBeSynthetise._listData; 
		  _avgLengthSilence = toBeSynthetise._avgLengthSilence;	
		  _speed = toBeSynthetise._speed;
		  _volume = toBeSynthetise._volume;
		  _pitch = toBeSynthetise._pitch;
		  _wsola = toBeSynthetise._wsola;
		  _vbs = toBeSynthetise._vbs;
		  _mp3Name = toBeSynthetise._mp3Name;
		  _mustPlay = toBeSynthetise._mustPlay;
		  _curVoice = toBeSynthetise._curVoice;
	}		
}

/**
* Constructor
*/
VietVoice::ToBeSynthetise::ToBeSynthetise ()
{
	_avgLengthSilence = 0;
	_curVoice = MALE; 
}

/**
* Retreive a token from the list of tokens
*
* return value: The desired token
*
* Parameters:
*
* unsigned int index Index of the desired token
* 
*/
VietVoice::Token * VietVoice::ToBeSynthetise::GetToken (unsigned int index)
{
	list<Token>::iterator iter = _listToken.begin ();

	if (index < 0 || index >= _listToken.size ()) // Check for invalid index
		return NULL;

	for (unsigned int i = 0 ; i < index; i++, iter++); // find the desired token

	return &(*iter);
}

/**
* Appends a token at the end of the list of token
*
* Parameters:
*
* Token & token The token to be appended.
*/
void VietVoice::ToBeSynthetise::AppendToken (Token & token)
{
	_listToken.push_back (token);
}


/**
* Obtains the list of tokens
*
* return value: The list of tokens
*/
list<VietVoice::Token> & VietVoice::ToBeSynthetise::GetListToken ()
{
	return _listToken;
}

/**
* Obtains a specific ToBeSpokenData object from the list
* 
* return value: The desired ToBeSpokenData object
*
* Parameters:
*
* unsigned int: index The index of the ToBeSpokenData object
* 
*/
VietVoice::ToBeSynthetiseData * VietVoice::ToBeSynthetise::GetData (unsigned int index)
{
	list<ToBeSynthetiseData>::iterator iter = _listData.begin ();

	if (index < 0 || index > _listData.size ()) // Check for invalid index
		return NULL;

	for (unsigned int i = 0 ; i < index; i++, iter++); // find the desired token

	return &(*iter);
}

/**
* Appends a token at the end of the list of token
*
* return value:
*
* ToBeSpokenData & token: The token to be appended.
*/
void VietVoice::ToBeSynthetise::AppendData (VietVoice::ToBeSynthetiseData & data)
{
	_listData.push_back (data);
}

/**
* Obtains the list of tokens
*
* return value: The list of tokens
*/
list<VietVoice::ToBeSynthetiseData> & VietVoice::ToBeSynthetise::GetListData ()
{
	return _listData;
}

/**
* Changes the avgLengthSilence member
*
* Parameters:
* 
* float dur The new value for avgLengthSilence
*/
void VietVoice::ToBeSynthetise::SetDurMoyenSilence (float dur)
{
	_avgLengthSilence = dur;
}
void VietVoice::ToBeSynthetise::SetSpeed (float speed)
{
	_speed = speed;
}
void VietVoice::ToBeSynthetise::SetVolume (float volume)
{
	_volume = volume;
}
void VietVoice::ToBeSynthetise::SetPitch (float pitch)
{
	_pitch = pitch;
}
void VietVoice::ToBeSynthetise::SetWsola (bool wsola)
{
	_wsola = wsola;
}
void VietVoice::ToBeSynthetise::Set_BS (long vbs)
{
	_vbs = vbs;
}

/**
* Obtains the avgLengthSilence member
*
* return value: The value of avgLengthSilence
*/
float VietVoice::ToBeSynthetise::GetDurMoyenSilence ()
{
	return _avgLengthSilence;
}
float VietVoice::ToBeSynthetise::GetSpeed ()
{
	return _speed;
}
float VietVoice::ToBeSynthetise::GetVolume ()
{
	return _volume;
}
float VietVoice::ToBeSynthetise::GetPitch ()
{
	return _pitch;
}
bool VietVoice::ToBeSynthetise::GetWsola ()
{
	return _wsola;
}
long VietVoice::ToBeSynthetise::Get_BS ()
{
	return _vbs;
}
long VietVoice::ToBeSynthetise::Get_Voice ()
{
	return _voice;
}

/**
* Empty all data contained in the ToBeSpoken object
*/
void VietVoice::ToBeSynthetise::Empty ()
{
	_listToken.clear ();
	_listData.clear ();

	///// delete this ; //////// [[Doc mot cau roi ngung luon]]
// Cho nay de lay thong so ============================================
//	::MessageBox(NULL, StringHelper::ToString (_vbs).c_str () , L"To be spoken", MB_OK);
// Cho nay de lay thong so ============================================
}

/**
* Changes the list of ToBeSpokenData objects
*
* Parameters:
*
* std::list<ToBeSpokenData> & listData: listData The new list
*/
void VietVoice::ToBeSynthetise::SetListData (std::list<ToBeSynthetiseData> & listData)
{
	_listData = listData;	
}

/**
* Calculate the size in bytes of the sound data to be played
*
* return value: The size in bytes of the sound data to be played
*/
int VietVoice::ToBeSynthetise::GetSizeWords ()
{
	int size = 0;

	for (std::list<ToBeSynthetiseData>::iterator iter = _listData.begin (); iter != _listData.end (); iter++) // For each words
	{	
		ToBeSynthetiseData data = *iter;

		if (data.GetWordData () != NULL) 
		{
			size += data.GetWordData()->GetSize ();// * data.GetNbRepeats (); // Multiply the size of the data by the number of time it must be repeaded
		}
	}
	return size;
}

/**
* Set the _mustPlay flag
*
* bool mustPlay:  The new value of the mustPlay flag
*/
void VietVoice::ToBeSynthetise::SetMustPlay (bool mustPlay)
{
	_mustPlay = mustPlay;
}

/**
* Set the file name in which the mp3 data is saved
*
* std::wstring mp3Name:  The new file name
*/
void VietVoice::ToBeSynthetise::SetMp3Name (std::wstring mp3Name)
{
	_mp3Name = mp3Name;	
}

std::wstring VietVoice::ToBeSynthetise::GetMp3Name ()
{
	return _mp3Name;
}
bool VietVoice::ToBeSynthetise::GetMustPlay ()
{
	return _mustPlay;
}

void VietVoice::ToBeSynthetise::SetVoice (VOICESEL voice)
{
	_curVoice = voice;
	_voice = 12;
}

VietVoice::ToBeSynthetise::VOICESEL VietVoice::ToBeSynthetise::GetVoice () const
{
	return _curVoice;
}