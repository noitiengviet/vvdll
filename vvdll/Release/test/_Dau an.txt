Dấu ấn 100 ngày hoạt động của Chính phủ mới

Sau khi nhậm chức, Thủ tướng cùng các thành viên Chính phủ đã bắt tay ngay vào các vấn đề cấp bách như mô hình tăng trưởng, cấu trúc kinh tế, các vấn nạn xã hội. Chính phủ cũng nhận thức rõ hơn về thách thức phải vượt qua để giải quyết bài toán tăng trưởng đi đôi với ổn định vĩ mô, phát triển bền vững. 

"Đấy là những dấu hiệu tích cực, cho thấy quyết tâm mạnh mẽ, sự xông xáo và tinh thần trách nhiệm của bộ máy mới", chuyên gia kinh tế Trần Du Lịch nói.

3 tháng qua, chưa một quyết sách hoàn toàn mới nào được đưa ra. Những việc Chính phủ đang làm chỉ là tiếp nối chủ trương từ nhiệm kỳ trước, như thực hiện Nghị quyết 11 về ổn định kinh tế vĩ mô và đảm bảo an sinh xã hội, hoàn thiện kế hoạch 5 năm đến 2015, tiệm cận gần hơn với nhiệm vụ tái cơ cấu nền kinh tế.                                           