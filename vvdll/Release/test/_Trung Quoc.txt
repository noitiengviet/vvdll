Thứ Hai, 09/01/2012, 14:58 (GMT+7) 
Trung Quốc truy lùng kẻ giết người hàng loạt

TTO - Hơn 10.000 công an ở miền đông Trung Quốc đang được huy động để lùng bắt một kẻ giết người hàng loạt được cho là một cựu chiến sĩ công an có vũ trang và đã lẩn trốn ít nhất 8 năm qua.
 
13.000 công an Trung Quốc đang truy lùng một kẻ giết người hàng loạt ở miền đông nước này - Ảnh: AFP 

Nhật báo Global Times ngày 9-1 đưa tin hai trực thăng cùng 13.000 chiến sĩ công an đang tham gia tìm kiếm Zeng Kaigui, 42 tuổi, từng phục vụ trong lực lượng công an vũ trang và rất giỏi trong việc lẩn trốn.

Zeng bị tình nghi bắn chết một người đàn ông hôm thứ sáu 6-1 và cướp 200.000 nhân dân tệ (31.700 USD) mà ông này vừa rút ra từ một ngân hàng ở thành phố Nam Kinh. 

Trong một thông cáo ra sau đó, công an thành phố Nam Kinh cho biết họ sẽ thưởng 100.000 nhân dân tệ cho ai cung cấp thông tin giúp bắt giữ Zeng.

Zeng cũng bị tình nghi đã thực hiện ít nhất 6 vụ cướp có vũ khí khác ở hai thành phố của Trung Quốc trong thời gian từ tháng 4-2004 đến tháng 6-2011, giết chết 6 người, làm bị thương 2 người và cướp 280.000 nhân dân tệ.

Global Times dẫn nguồn tin công an Nam Kinh cho biết hiện công an đang tuần tra tại các trạm xe lửa và xe buýt, các bến đò, quán cà phê Internet và khách sạn ở thành phố. Họ cũng lập nhiều chốt chặn trên các con đường chính để kiểm tra những chiếc xe khả nghi. 

Những hành khách là nam giới bắt các chuyến xe công cộng trên quãng đường dài cũng bị buộc trình chứng minh thư trước khi lên xe.

Một người phát ngôn chính quyền Nam Kinh xác nhận với AFP rằng vụ nổ súng ngày 6-1 và 6 vụ cướp có vũ trang khác có liên quan với nhau, song từ chối cung cấp thêm thông tin. Phía cảnh sát cũng từ chối đưa ra bình luận.

MINH ANH
