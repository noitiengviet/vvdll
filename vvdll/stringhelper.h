#if !defined (STRINGHELPER_H)

#define STRINGHELPER_H

#include <vector>
#include <sstream>

namespace VietVoice
{
	/**
	 * Class Name:         StringHelper
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class thrown as an exception when an error occurs
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	  * CODE MODIFICATIONS AND BUG CORRECTIONS
	  * --------------------------------------
	  * october 11 2005:  Created this file and coded the replaceFirst and endWith methods
	  * october 15 2005:  Added the replaceAll and nbChar methods.
	  * october 16 2005:  Added the StringTokenizer class.
	  * November 5 2005:  Added one method:  OneOfCharPresent
	  * March 4 2006:  Seperated implementation in source and header
  	  * June 9 2006: Change the methods name so they start by a capital letter.
	  * June 15 2006:  Improved comments to the stringhelper.h file.
	  * June 17 2006:  Added even more comments to stringhelper.h and stringhelper.cpp
	  * February 26 2007:  Added Tokenize and ToString methods
	  */

	class StringHelper
	{
	public:

		/**
		* Replace the first offcurrence of oldString in toModify with newStr
		*
		* Parameters:
		* 
		* std::wstring & toModify:  The string that will be modified.
		* std::wstring oldStr:  The substring that must be replace.
		* std::wstring newStr:  The substring that will replace oldStr.
		*/
		static void ReplaceFirst (std::wstring & toModify, std::wstring oldStr, std::wstring newStr);


		//Thuan add 29-05-2020///////////////////////////////
			static void ReplaceString (std::wstring & toModify, std::wstring oldStr, std::wstring newStr);
			static std::wstring ChuyenTu(std::wstring tu);
		//////////////////////////////////////////////////////



		/**
		* Determine if one string end with another string.
		*
		* Return value:  True if str end with endWith, else false
		*
		* Parameters:
		*
		* std::wstring str:  String that is tested
		* std::wstring endWith:  str must end with this
		*/
		static bool EndWith (std::wstring str, std::wstring endWith);

		/**
		* Replace all charater of a certain type from a string to another character
		*
		* Parameters:
		* 
		* std::wstringtoModify:  The string that will be modified
		* wchar_t replaceThis:  the character to be replaced
		* wchar_t withThis:  Character that is used as replacement
		*/
		static void ReplaceAll (std::wstring & toModify, wchar_t replaceThis, wchar_t withThis);

		/**
		* Determine the number of time a specific character is found in a string
		*
		* Return:  True if str end with endWith, else false
		*
		* Parameters: 
		* 
		* std::wstring str:  String that is tested
		* wchar_t c:  str must end with this
		*
		*/
		static long NbChar (std::wstring str, wchar_t c);


		/**
		* Remove every space from a specified string
		*
		* Parameters:
		*
		* std::wstring & str:  The string from wich all spaces are removed.
		*/
		static void RemoveSpace (std::wstring & str);

		/**
		* Determine if one of the character of chars is present in checkStr.
		*
		* Return value:  True if one of the character of chars is present in checkStr
		*                else false.
		*
		* Parameters: 
		*
		* std::wstring checkStr:  The string that is verified for presence of characters.
		* std::wstring chars:  Contains the characaters that are looked for.
		*
		*/
		static bool OneOfCharPresent (std::wstring checkStr, std::wstring chars);

		/**
		* Divide a string into tokens devided by specified delimiters
		*
		* Parameters:
		*
		* std::vector<std::wstring> & tokens:  Vector that will contain the tokens
		* const std::wstring & str:  The string to be tokenized
		* const std::wstring delimiters:  List of character that comes between the tokens
		*/
		static void Tokenize(std::vector<std::wstring> & tokens, const std::wstring & str, const std::wstring& delimiters = L" ");

		/**
		* Convert a numerical value to a string
		*
		* Return value:  The string formed from the numerical value
		*
		* Parameters:
		*
		* T value:  The numerical value that needs to be converted to a string
		**/
		template <class T>
		static std::wstring ToString (T value)
		{
			std::wstringstream stream;
			stream << value;
			return stream.str ();
		}

		static long ToInt(std::wstring str)
		{
			std::wistringstream stream( str );
			int value = 0;
			stream >> value;
			return value;
		}
	};
}

#endif 