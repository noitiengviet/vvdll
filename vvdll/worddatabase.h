#if !defined (WORD_DATABASE_H)

#define WORD_DATABASE_H

#include <vector>
#include <map>
#include "fileio.h"
#include "vietnameseTTSException.h"
#include "word.h"
#include "stringhelper.h"

#pragma warning( disable : 4786 ) 

namespace VietVoice
{
	/**
	 * Class Name:         WordDatabase.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 15 2004
	 *
	 * Description:        Represents the 3 database where all the sound data is stored.  Maintain the indexes to
	 *                     those database, allow access to the sound data
	 *
	 *
	 *
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	struct IndexElements
	{
		int _posFirstWord;
		int _posSecondWord;
		float _percent1; // char
		float _percent2; // char
		float _percentVolume1; // char
		float _percentVolume2; // char
	};

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 *   novembre 29 2005:  Implemented the copy constructor and the = operator
	 * February 4 2006:  Modified the class to use unicode file names.
	 * March 6 2006:  Seperated implementation in source and header
	 * June 10 2006: Change the methods name so they start by a capital letter.
	 * September 28 2006:  Added the SetDatabase method to allow realtime switching of voice
	 * End of september 2007:  modified for new database
	 */
	class WordDatabase
	{

	public:

		/**
		 * Constructor
		 * @param index6Accents Path to the index for the database for words with 6 accent
		 * @param index2AccentsStr Pah to the index for the database for words with 2 accent
		 * @param indexExtra Pah to the index for the database with extra (special) words
		 * @param database6Accents Path to the index to the database for words with 6 accent
		 * @param database2Accents Path to  the database for words with 2 accent
		 * @param databaseExtra Path to the database for words with 6 accent
		 * @param extra  Path to a file listing word presents in the extra database
		 */
		WordDatabase();

		WordDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring extra);


		WordDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra,std::wstring silence, 
			std::wstring index6Accents2, std::wstring index2Accents2, std::wstring indexExtra2, std::wstring databaseMain2, std::wstring databaseExtra2,std::wstring silence2,
			std::wstring extra,
			std::wstring indexVeryShortWord, std::wstring databaseVeryShortWord,std::wstring veryShortWord,std::wstring silenceVeryShortWord);


		~WordDatabase ();

		WordDatabase  (const WordDatabase & wb);

		void operator = (const WordDatabase & wb);

		float m_percentageAdjust;

		/**
		 * Obrains the size of the index for the base with 6 accents
		 * @return The size of the index
		 */
		int GetIndex6AccentsSize ();

		/**
		 * Obtains a word from the database of words with 6 accents.
		 * @param i The index of the word
		 * @return The data of the desired word encapsulated in a Word object
		 */
		Word * GetWord6Accents(int i,u_int type);

		/**
		 * Obtains a word from the database of words with 2 accents.
		 * @param i The index of the word
		 * @return The data of the desired word encapsulated in a Word object
		 */
		Word * GetWord2Accents(int i,u_int type);

		/**
		 * Obtains a word from the database for extra words.
		 * @param name the name of the word
		 * @return The data of the desired word encapsulated in a Word object
		 */
		Word * GetExtraWord (std::wstring name,u_int type);

		Word * GetVeryShortWord (std::wstring name);// Thuan add 12-02-2020

		/**
		* Change the database (voice) that is used.
		*
		* Parameters:
		*
		* std::wstring index6Accents:  Path to the 6 accents index
		* std::wstring index2Accents:  Path to the 2 accents index
		* std::wstring indexExtra:  Path to the extra index
		* std::wstring database6Accents:  Path to the 6 accents database
		* std::wstring database2Accents:  Path to the 2 accents database
		* std::wstring databaseExtra:  Path to the extra database
		* std::wstring extra:  Path to the file listing all extrawords
		*/
		void SetDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring extra);
		
		// Thuan add 12-02-2020 Type=1: Normal database, type=2: short database
		void SetDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring extra,std::wstring silence,u_int type);

		void SetDatabaseVeryShortWord(std::wstring index,std::wstring database,std::wstring pathFileVeryShortWord,std::wstring silenceVeryShort);

		/**
		 * Loads the binary index for the database with 6 accents.
		 * @param  the location  of the binary index file
		 */
		void LoadIndex6Accents(std::wstring filename);

		void LoadIndex6Accents(std::wstring filename,u_int type); //Thuan add 12-02-2020

		/**
		 * Loads the binary index for the database with 2 accents.
		 * @param  the location  of the binary index file
		 */
		void LoadIndex2Accents(std::wstring filename);

		void LoadIndex2Accents(std::wstring filename,u_int type);//Thuan add 12-02-2020

		/**
		 * Loads the binary index for the database with extra words.
		 * @param  the location  of the binary index file
		 */
		void LoadIndexExtra(std::wstring filename);
		void LoadIndexExtra(std::wstring filename,u_int type);//Thuan add 12-02-2020

		void LoadIndexVeryShortWord(std::wstring filename);//Thuan add 12-02-2020

		std::vector<long> GetSilence(std::wstring word,u_int type);// Thuan add 12-03-2020





		/**
		 * Read all extra words from a file
		 * @param filename The name of the file
		 */
		 void PrepareExtraWords (std::wstring filename);
			
	private:

		const static int MAGIC;
		const static int VERSION;
		const static int SIZE_HEADER; //
		const int INDEX;

		int _nbChannels;
		std::vector <IndexElements> _index6Accents;
		std::vector <IndexElements> _index2Accents;
		std::vector <int> _indexExtra;
		std::map <std::wstring, int> _extraWords;
		std::map <std::wstring, int> _listSilence;

		std::wstring _databaseMain;
		std::wstring _databaseExtra;

		/////////// Thuan add 12-02-2020//////////////////
		std::vector <IndexElements> _index6Accents2;
		std::vector <IndexElements> _index2Accents2;
		std::vector <int> _indexExtra2;	
		std::wstring _databaseMain2;
		std::wstring _databaseExtra2;
		std::map <std::wstring, int> _listSilence2;


		std::vector <int> _indexVeryShortWord;
		std::map <std::wstring, int> _veryShortWord;
		std::wstring _databaseVeryShortWord;
		std::map <std::wstring, int> _listSilenceVeryShort;

		///// End Thuan add/////////////////////////
		std::wstring _pathSilenceNomalWord;
		std::wstring _pathSilenceShortWord;
		std::wstring _pathSilenceVeryShortWord;

		//// Thuan add 27-05-2020////////////////

		//////////end Thuan add//////////////////
	};

/*	const int VietVoice::WordDatabase::MAGIC = 0xCAFEDA;
	const unsigned int VietVoice::WordDatabase::INDEX_MAGIC = 0xCADAFE;
	const int VietVoice::WordDatabase::VERSION = 1;
	const int VietVoice::WordDatabase::SIZE_HEADER = 107;*/
}

#endif