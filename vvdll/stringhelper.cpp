#include "stringhelper.h"

/**
* Replace the first offcurrence of oldString in toModify with newStr
*
* Parameters:
* 
* std::wstring & toModify:  The string that will be modified.
* std::wstring oldStr:  The substring that must be replace.
* std::wstring newStr:  The substring that will replace oldStr.
*/
void VietVoice::StringHelper::ReplaceFirst (std::wstring & toModify, std::wstring oldStr, std::wstring newStr)
{
	unsigned int pos=toModify.find_first_of (oldStr);
	//toModify = toModify +  L'\0';
	//oldStr += L'\0';
	//const wchar_t * pos = wcsstr (toModify.c_str (), oldStr.c_str ()); // Find where first occurence of oldStr is present in toModify
	std::wstring tmp;

	if (pos == std::string::npos) // No occurence
		return ;
			
	// Copy begining of toModify (before oldStr first occurance)
	unsigned int i = 0;
	for (; i < pos ; i++)
	{
		tmp += toModify [i];
	}

	// Put newStr insteand of oldStr
	tmp += newStr;
	i += (unsigned int) oldStr.length ();

	// Copy rest of the string
	for (; i < toModify.length (); i++)
	{
		tmp += toModify [i];
	}

	toModify = tmp;		
}


void VietVoice::StringHelper::ReplaceString (std::wstring & toModify, std::wstring oldStr, std::wstring newStr)
{
	unsigned int pos=toModify.find (oldStr);
	//toModify = toModify +  L'\0';
	//oldStr += L'\0';
	//const wchar_t * pos = wcsstr (toModify.c_str (), oldStr.c_str ()); // Find where first occurence of oldStr is present in toModify
	std::wstring tmp;

	if (pos == std::string::npos) // No occurence
		return ;
			
	// Copy begining of toModify (before oldStr first occurance)
	unsigned int i = 0;
	for (; i < pos ; i++)
	{
		tmp += toModify [i];
	}

	// Put newStr insteand of oldStr
	tmp += newStr;
	i += (unsigned int) oldStr.length ();

	// Copy rest of the string
	for (; i < toModify.length (); i++)
	{
		tmp += toModify [i];
	}

	toModify = tmp;		
}


std::wstring VietVoice::StringHelper::ChuyenTu(std::wstring tu)
{
		std::wstring str=tu;
		std::wstring  dau = L"0";
        if (str.find(L"ó")!=std::string::npos || str.find(L"á")!=std::string::npos || str.find(L"ắ")!=std::string::npos || str.find(L"ấ")!=std::string::npos || str.find(L"é")!=std::string::npos || str.find(L"ế")!=std::string::npos || str.find(L"ố")!=std::string::npos || str.find(L"ớ")!=std::string::npos || str.find(L"ú")!=std::string::npos || str.find(L"ứ")!=std::string::npos || str.find(L"í")!=std::string::npos || str.find(L"ý")!=std::string::npos)
		{
            dau = L"1";
		}


        if (str.find(L"ò")!=std::string::npos || str.find(L"à")!=std::string::npos || str.find(L"ằ")!=std::string::npos || str.find(L"ầ")!=std::string::npos || str.find(L"è")!=std::string::npos || str.find(L"ề")!=std::string::npos || str.find(L"ồ")!=std::string::npos || str.find(L"ờ")!=std::string::npos || str.find(L"ù")!=std::string::npos || str.find(L"ừ")!=std::string::npos || str.find(L"ì")!=std::string::npos || str.find(L"ỳ")!=std::string::npos)
		{
            dau = L"2";
		}

        if (str.find(L"ỏ")!=std::string::npos || str.find(L"ả")!=std::string::npos || str.find(L"ẳ")!=std::string::npos || str.find(L"ẩ")!=std::string::npos || str.find(L"ẻ")!=std::string::npos || str.find(L"ể")!=std::string::npos || str.find(L"ổ")!=std::string::npos || str.find(L"ở")!=std::string::npos || str.find(L"ủ")!=std::string::npos || str.find(L"ử")!=std::string::npos || str.find(L"ỉ")!=std::string::npos || str.find(L"ỷ")!=std::string::npos)
		{
            dau = L"3";
		}

        if (str.find(L"õ")!=std::string::npos || str.find(L"ã")!=std::string::npos || str.find(L"ẵ")!=std::string::npos || str.find(L"ẫ")!=std::string::npos || str.find(L"ẽ")!=std::string::npos || str.find(L"ễ")!=std::string::npos || str.find(L"ỗ")!=std::string::npos || str.find(L"ỡ")!=std::string::npos || str.find(L"ũ")!=std::string::npos || str.find(L"ữ")!=std::string::npos || str.find(L"ĩ")!=std::string::npos || str.find(L"ỹ")!=std::string::npos)
		{
            dau = L"4";
		}

         if (str.find(L"ọ")!=std::string::npos || str.find(L"ạ")!=std::string::npos || str.find(L"ặ")!=std::string::npos || str.find(L"ậ")!=std::string::npos || str.find(L"ẹ")!=std::string::npos || str.find(L"ệ")!=std::string::npos || str.find(L"ộ")!=std::string::npos || str.find(L"ợ")!=std::string::npos || str.find(L"ụ")!=std::string::npos || str.find(L"ự") !=std::string::npos|| str.find(L"ị")!=std::string::npos || str.find(L"ỵ")!=std::string::npos)
		{
            dau = L"5";
		}


		// ::MessageBox(NULL, StringHelper::ToString(str.find(L"ọ")).c_str(), L"Pre-Processing save file", MB_OK); // VT 

       StringHelper::ReplaceFirst(str,L"đ", L"d-");
       StringHelper::ReplaceFirst(str,L"â", L"a+");
       StringHelper::ReplaceFirst(str,L"ă", L"aa");
       StringHelper::ReplaceFirst(str,L"ê", L"e+");
       StringHelper::ReplaceFirst(str,L"ô", L"o+");
       StringHelper::ReplaceFirst(str,L"ơ", L"oo");
       StringHelper::ReplaceFirst(str,L"ư", L"uu");

       StringHelper::ReplaceFirst(str,L"á", L"a");
       StringHelper::ReplaceFirst(str,L"é", L"e");
       StringHelper::ReplaceFirst(str,L"ó", L"o");
       StringHelper::ReplaceFirst(str,L"ú", L"u");

       StringHelper::ReplaceFirst(str,L"ấ", L"a+");
       StringHelper::ReplaceFirst(str,L"ắ", L"aa");
       StringHelper::ReplaceFirst(str,L"ế", L"e+");
       StringHelper::ReplaceFirst(str,L"ố", L"o+");
       StringHelper::ReplaceFirst(str,L"ớ", L"oo");
       StringHelper::ReplaceFirst(str,L"ứ", L"uu");
       StringHelper::ReplaceFirst(str,L"í", L"i");
       StringHelper::ReplaceFirst(str,L"ý", L"y");



       StringHelper::ReplaceFirst(str,L"à", L"a");
       StringHelper::ReplaceFirst(str,L"è", L"e");
       StringHelper::ReplaceFirst(str,L"ò", L"o");
       StringHelper::ReplaceFirst(str,L"ù", L"u");

       StringHelper::ReplaceFirst(str,L"ầ", L"a+");
       StringHelper::ReplaceFirst(str,L"ằ", L"aa");
       StringHelper::ReplaceFirst(str,L"ề", L"e+");
       StringHelper::ReplaceFirst(str,L"ồ", L"o+");
       StringHelper::ReplaceFirst(str,L"ờ", L"oo");
       StringHelper::ReplaceFirst(str,L"ừ", L"uu");
       StringHelper::ReplaceFirst(str,L"ì", L"i");
       StringHelper::ReplaceFirst(str,L"ỳ", L"y");

       StringHelper::ReplaceFirst(str,L"ả", L"a");
       StringHelper::ReplaceFirst(str,L"ẻ", L"e");
       StringHelper::ReplaceFirst(str,L"ỏ", L"o");
       StringHelper::ReplaceFirst(str,L"ủ", L"u");

       StringHelper::ReplaceFirst(str,L"ẩ", L"a+");
       StringHelper::ReplaceFirst(str,L"ẳ", L"aa");
       StringHelper::ReplaceFirst(str,L"ể", L"e+");
       StringHelper::ReplaceFirst(str,L"ổ", L"o+");
       StringHelper::ReplaceFirst(str,L"ở", L"oo");
       StringHelper::ReplaceFirst(str,L"ử", L"uu");
       StringHelper::ReplaceFirst(str,L"ỉ", L"i");
       StringHelper::ReplaceFirst(str,L"ỷ", L"y");

       StringHelper::ReplaceFirst(str,L"ã", L"a");
       StringHelper::ReplaceFirst(str,L"ẽ", L"e");
       StringHelper::ReplaceFirst(str,L"õ", L"o");
       StringHelper::ReplaceFirst(str,L"ũ", L"u");

       StringHelper::ReplaceFirst(str,L"ẫ", L"a+");
       StringHelper::ReplaceFirst(str,L"ẵ", L"aa");
       StringHelper::ReplaceFirst(str,L"ễ", L"e+");
       StringHelper::ReplaceFirst(str,L"ỗ", L"o+");
       StringHelper::ReplaceFirst(str,L"ỡ", L"oo");
       StringHelper::ReplaceFirst(str,L"ữ", L"uu");
       StringHelper::ReplaceFirst(str,L"ĩ", L"i");
       StringHelper::ReplaceFirst(str,L"ỹ", L"y");

       StringHelper::ReplaceFirst(str,L"ạ", L"a");
       StringHelper::ReplaceFirst(str,L"ẹ", L"e");
       StringHelper::ReplaceFirst(str,L"ọ", L"o");
       StringHelper::ReplaceFirst(str,L"ụ", L"u");

       StringHelper::ReplaceFirst(str,L"ậ", L"a+");
       StringHelper::ReplaceFirst(str,L"ặ", L"aa");
       StringHelper::ReplaceFirst(str,L"ệ", L"e+");
       StringHelper::ReplaceFirst(str,L"ộ", L"o+");
       StringHelper::ReplaceFirst(str,L"ợ", L"oo");
       StringHelper::ReplaceFirst(str,L"ự", L"uu");
       StringHelper::ReplaceFirst(str,L"ị", L"i");
       StringHelper::ReplaceFirst(str,L"ỵ", L"y");
	
	   StringHelper::ReplaceString(str,L"com", L"com-");

		if (str.find (L"ay") == std::string::npos && str.find (L"a+y") == std::string::npos && str.find (L"uy") == std::string::npos) // No occurence
		{
			StringHelper::ReplaceFirst(str,L"y", L"i");
		}

		
        str = str + dau;

		
		return str;
}



/**
* Determine if one string end with another string.
*
* Return value:  True if str end with endWith, else false
*
* Parameters:
*
* std::wstring str:  String that is tested
* std::wstring endWith:  str must end with this
*/
bool VietVoice::StringHelper::EndWith (std::wstring str, std::wstring endWith)
{
	if (str.length () < endWith.length ()) // str too short, connot end with endWith
		return false;

	for (unsigned int i = 0; i < endWith.length (); i++) // Compare the end of str with endWith
	{
		if (str [str.length () - i - 1] != endWith [endWith.length () - i - 1])
			return false;
	}

	return true;
}


/**
* Replace all charater of a certain type from a string to another character
*
* Parameters:
* 
* std::wstringtoModify:  The string that will be modified
* wchar_t replaceThis:  the character to be replaced
* wchar_t withThis:  Character that is used as replacement
*/
void VietVoice::StringHelper::ReplaceAll (std::wstring & toModify, wchar_t replaceThis, wchar_t withThis)
{
	for (unsigned int i = 0; i < toModify.length (); i++) // Check every characters
	{
		if (toModify [i] == replaceThis) // Check if character is the one to replace, if so replace it
			toModify [i] = withThis;
	}
}

/**
* Determine the number of time a specific character is found in a string
*
* Return:  True if str end with endWith, else false
*
* Parameters: 
* 
* std::wstring str:  String that is tested
* wchar_t c:  str must end with this
*
*/
long VietVoice::StringHelper::NbChar (std::wstring str, wchar_t c)
{
	long count = 0;

	for (unsigned int i = 0; i < str.length (); i++) // Check every character
	{
		if (str [i] == c) // Check if current character is the one we are looking for
			++count;
	}

	return count;
}

/**
* Remove every space from a specified string
*
* Parameters:
*
* std::wstring & str:  The string from wich all spaces are removed.
*/
void VietVoice::StringHelper::RemoveSpace (std::wstring & str)
{
	std::wstring tmpStr;

	for (unsigned int i = 0; i < str.length (); i++) // Check every character
	{
		if (str [i] != L' ' && str [i] != L'\t' && str [i] != 10) // If character is a space/tab, remove it
			tmpStr += str [i];
	}

	str = tmpStr;
}

/**
* Determine if one of the character of chars is present in checkStr.
*
* Return value:  True if one of the character of chars is present in checkStr
*                else false.
*
* Parameters: 
*
* std::wstring checkStr:  The string that is verified for presence of characters.
* std::wstring chars:  Contains the characaters that are looked for.
*
*/
bool VietVoice::StringHelper::OneOfCharPresent (std::wstring checkStr, std::wstring chars)
{
	for (unsigned int i = 0; i < chars.length (); i++) // Check every caracter in checStr
	{
		for (unsigned int j = 0; j < checkStr.length (); j++) // Used to see if the current checkStr caracter is one of chars
		{
			if (checkStr [j] == chars [i])
				return true;
		}
	}

	return false;
}

/**
* Divide a string into tokens devided by specified delimiters
*
* Parameters:
*
* std::vector<std::wstring> & tokens:  Vector that will contain the tokens
* const std::wstring & str:  The string to be tokenized
* const std::wstring delimiters:  List of character that comes between the tokens
*/
void VietVoice::StringHelper::Tokenize(std::vector<std::wstring> & tokens, const std::wstring & str, const std::wstring& delimiters)
{
	// Find the begining and end of the next token
	int beginToken = str.find_first_not_of(delimiters, 0);
	int endToken     = str.find_first_of(delimiters, beginToken);

	// Go on until all tokens are found
	while (endToken != std::wstring::npos  || beginToken != std::wstring::npos)
	{
		// Add newly found token to vector
		tokens.push_back(str.substr(beginToken, endToken - beginToken));

		// Find the begining and end of the next token
		beginToken = str.find_first_not_of(delimiters, endToken);
		endToken = str.find_first_of(delimiters, beginToken);
	}
}
/**
* constructor.
*
* Parameters:
*
* std::wstring str:  String to be tokenized
* std::wstring param:  Characters indicating the end of a token
*
*/
/*VietVoice::StringTokenizer::StringTokenizer (std::wstring str, std::wstring deliminator)
{
	_deliminator = deliminator;
	_str = new wchar_t [str.length () + 1];

	::wcsncpy (_str, str.c_str (), str.length ());
	::wcsncpy (_str, _wcslwr (_str), str.length ());
	_str[str.length ()] = 0;

	_token = ::wcstok (_str, _deliminator.c_str ());
}

/**
* Destructor, perform clean up
*/
/*VietVoice::StringTokenizer::~StringTokenizer ()
{
	delete [] _str;
}

/**
* Determine wheter or not there are more tokens in the string
*
* return value:  True if there are more tokens, else false
*/
/*bool VietVoice::StringTokenizer::HasMoreTokens ()
{
	return _token != NULL;
}

/**
* Obtain the next token
*
*return value:  The next token
*/
/*std::wstring * VietVoice::StringTokenizer::NextToken ()
{
	if (_token == NULL)
		return NULL;

	_tmpToken = _token;

	_token = ::wcstok (NULL, _deliminator.c_str ());

	return &_tmpToken;
}*/