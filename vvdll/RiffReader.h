#if !defined RIFFREADER_H

#define RIFFREADER_H

#include "fileio.h"

/**
 * Class Name:         RiffReader.java
 *
 * Author(s):          Benoit Lanteigne
 *
 * Description:       RiffReader is class used to help load RIFF (Resource Interchange File Format) file.
 *                    RIFF file are file organized in chunks.  A program reads the chunk it is
 *                    interrested in an ignore the others.  Wave file are a type
 *                    of RIFF file.</p>

 *
 * Copyright:          All rights reserved Moncton University
 *                     Tang-Ho L� Ph. D. - Computer Science Department
 *
 */

/**
  * CODE MODIFICATIONS AND BUG CORRECTIONS
  * --------------------------------------
  * created on september 8 2007
  */
class RiffReader :  public File::Reader
{
public:

	RiffReader (std::wstring const & filename);
	/**
	* Each chunks start with a 4 bytes name used as a ID.  readChunkName
	* is used to read such a name from a file.
	*
	* return value:  A String containing the name of the chunk.
	*/
	std::string ReadChunkName ();

	/**
	* Skip a chunk in the file.  The name (id) of the chunk must have been read
	* before calling that method, but not the size.
	*/
	void SkipChunk ();

	/**
	* Finds a particular chunck in the file.  The chunks situated before the desired
	* chunk are skipped.
	*
	* parameters:
	*
	* std::string name The name of the desired chunk.
	*/
	void FindChunk (std::string name);
};

#endif