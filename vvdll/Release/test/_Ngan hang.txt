Thứ ba, 10/9/2013 22:10 GMT+7    
‘Ngân hàng đang ở gần đáy khủng hoảng’
Theo bà Nguyễn Thùy Dương - Phó tổng giám đốc dịch vụ tài chính ngân hàng E&Y Việt Nam, cho vay dưới chuẩn là căn nguyên chung của các cuộc khủng hoảng tài chính thế giới cũng như khó khăn của ngân hàng trong nước hiện nay.

- Thế giới đã đi qua 5 năm của cuộc khủng hoảng tài chính thế giới, trong khi đó, với các ngân hàng Việt Nam thời điểm này vẫn là “tâm bão”. Theo bà tại sao lại có độ trễ như vậy? 

- Từ năm 2009, khó khăn bắt đầu nổ ra trên toàn thế giới nhưng sự ảnh hưởng tại Việt Nam chưa nhiều và không trực tiếp. Nếu có tác động ngay, thông thường chỉ ở lĩnh vực xuất khẩu. Lúc đó, chúng ta có 4 - 5 thị trường xuất khẩu chính như Mỹ, Trung Quốc, châu Âu, Nhật Bản.

Bà Nguyễn Thùy Dương là Thạc sỹ Quản trị kinh doanh, Đại học Gloucestershire, Vương quốc Anh. Hiện bà là Phó tổng giám đốc Dịch vụ tài chính ngân hàng của E&Y Việt Nam (Ernst & Young), chuyên trách về lĩnh vực kiểm toán, tư vấn cho các ngân hàng và các tổ chức tài chính tại Việt Nam, Lào. 
 
Bà Dương có kinh nghiệm chuyên sâu về thị trường tài chính Việt Nam, các vấn đề sáp nhập và các giải pháp thực tế cũng như quản trị rủi ro.
Còn trong lĩnh vực tài chính ngân hàng, mức độ hội nhập khi đó chưa cao. Quan trọng hơn, các ngân hàng lúc ấy mới bước vào giai đoạn tăng trưởng "nóng". 2009 là thời điểm cực "thịnh" của họ. Điều này khác hẳn với Mỹ và thế giới khi ấy. 

Đến nay khi thế giới đi qua khủng hoảng được 5 năm thì hệ thống ngân hàng Việt Nam lại gặp rất nhiều khó khăn và có thể nói là gần chạm đáy. Khó khăn thực ra đã bắt đầu lộ dần từ năm 2010, 2011, ngân hàng vẫn tăng trưởng "nóng" nhưng nhiều vấn đề khác đã xấu đi, nợ dưới chuẩn cũng tăng lên. Được cái các ngân hàng khi đó vẫn gò được tỷ lệ nợ xấu dưới 3%. 

- Một trong những lý do mà người ta nói nhiều nhất về sự sụp đổ của các ngân hàng trong cuộc khủng hoảng 2008 là vấn đề cho vay dưới chuẩn. Còn với các ngân hàng Việt Nam, nguồn cơn của những khó khăn là gì? 

- Cho vay nhanh - vội cộng với không quản lý tốt rủi ro dẫn đến chất lượng tài sản đi xuống, đó là điểm giống nhau. Ở Mỹ, khi đó họ cho vay rất nhiều, rất nhanh và không thực sự có trách nhiệm với danh mục cho vay của mình. 

Còn ở Việt Nam, một phần là do ảnh hưởng nền kinh tế vĩ mô cộng với rủi ro về đạo đức không được kiểm soát chặt chẽ. Các ngân hàng quản lý rủi ro rất kém và đến gần đây, Ngân hàng Nhà nước mới chuẩn bị ra thông tư siết chặt hơn. 

Nguồn tiền và cơ chế là hai điểm mà bà Nguyễn Thùy Dương cho rằng cần thay đổi để giải quyết những vấn đề hiện tại của hệ thống ngân hàng. Ảnh: EY.
- Tại cuộc khủng hoảng 2008, hàng loạt ngân hàng, tên tuổi lớn trên thế giới đã phá sản. Còn ở Việt Nam tình trạng này khó xảy ra, theo bà tại sao?

- Thực ra không nhất thiết cứ phải để một ngân hàng chết đi rồi lập một ngân hàng mới trong khi chi phí lại khá lớn. Khả năng là vẫn nên khuyến khích các nguồn lực khác tham gia. Đây có thể là một quan điểm mà tôi nghĩ Ngân hàng Nhà nước cũng có lý của họ. Ví dụ như để các ngân hàng yếu kém tự chủ, tự tìm một ông chủ mới nào đó để sáp nhập, tái cơ cấu như TienPhong Bank, TrustBank, Habubank... đã làm.

Hơn nữa, nay Ngân hàng Nhà nước hạn chế mở phòng giao dịch, ngừng cấp phép ngân hàng, rõ ràng việc bỏ tiền mua một nhà băng yếu kém để làm nó tốt hơn sẽ kinh tế hơn rất nhiều. 

- Để giải quyết khó khăn của hệ thống ngân hàng Việt Nam hiện nay, theo bà nên bắt đầu từ đâu?

- Hai điều chúng ta cần phải tạo ra, một là cơ chế, hai là tiền. Đầu tiên là phải có sự ổn định tương đối trong điều hành kinh tế vĩ mô. Nhà điều hành đôi khi vẫn chủ quan trong việc đưa ra quyết định và có quá nhiều mệnh lệnh hành chính trong đó. 

Để giải quyết vấn đề thứ hai, một gợi ý có thể là nên tạo sân chơi bình đẳng cho các ngân hàng trong nước và nhà đầu tư nước ngoài. Như với nợ xấu cũng vậy, để xử lý cần phải có một luồng tiền thực sự chảy vào. Nếu mở cửa cho nước ngoài tham gia, họ có thể mang được tài sản là tiền, là trí tuệ, sự chuyên nghiệp hóa, giúp tài sản có thể sinh lời và sau đó bán lại cho chúng ta. Như vậy không phải quá tệ. Còn nếu chỉ phụ thuộc vào các nguồn vốn đi vay thì sẽ phải đối mặt nhiều điều kiện cũng như những rủi ro về mặt chính sách. 

Nếu làm vậy thì e bản thân Công ty Quản lý và Khai thác tài sản Việt Nam (VAMC) lẫn các ngân hàng chẳng có động lực nào để mua bán nợ. Để mô hình VAMC hiệu quả, nên tạo hành lang pháp lý để cho phép các giao dịch mua bán dễ dàng được thực hiện. Ngoài ra, cần có chế độ ưu đãi về thuế nhất định để các bên thấy có động lực tham gia. Theo tôi, để các bên mua nhanh, bán nhanh mà hiệu quả thì phải đảm bảo lợi ích cho cả hai phía. Nếu những khoản nợ sau khi xử lý họ không được gì thì cũng chẳng có động lực nào cho họ cố gắng cả.

Thanh Thanh Lan