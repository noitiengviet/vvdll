
#include "wordselector.h"
#include <vector>
/**
* Constructor.
*
* @param String datatabase Path leading to the database
* @param String index Path leading to the index of the database
* @param String syl Path leading to a file containing all vietnamese _syllables
* @param String cons1 Path leading to a file containing vietnamese consonant formed from one letter (d, m, b, ect)
* @param String cons2 Path leading to a file containing vietnamese consonant formed from two letter th, tr, ph, etc)
*/
VietVoice::WordSelector::WordSelector(std::wstring databaseMain, std::wstring index6Accents, std::wstring index2Accents, std::wstring databaseExtra, std::wstring indexExtra, std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2, std::wstring extra)
	: _wordDatabase (index6Accents, index2Accents, indexExtra, databaseMain, databaseExtra, extra)
{
		
	_nbSilencesAfter2Acc = 4 ; // Modify this in order to change the lenght of the silence add after words with 2 accents

	// Makes sure each path is not null
	if (index6Accents == L"")
	{
		throw VietnameseTTException (L"Can't load unit database index ");
	}

	if (databaseMain == L"")
	{
		throw VietnameseTTException(L"Can't load unit database");
	}

	if (syl == L"")
	{
		throw VietnameseTTException(L"Can't load the map of _syllable");
	}

	if (cons1 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 1 letter");
	}

	if (cons2 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 2 letter");
	}

	_durMoyen = 0.28f;
	LoadFromFile (syl, _syllable, 85); //  Loads the _syllables in an array
		
	LoadFromFile (syl2, _syllable2, 95);
	LoadFromFile (cons1, _consonne1, 23); //  Loads the one letter consonnant in an array
	LoadFromFile (cons2, _consonne2, 24); //Loads the two letters consonnant in an array

}

////////////Thuan add 13-02-2020///////////////////////
VietVoice::WordSelector::WordSelector(std::wstring databaseMain, std::wstring index6Accents, std::wstring index2Accents, std::wstring databaseExtra, std::wstring indexExtra, std::wstring silence,
			std::wstring databaseMain2, std::wstring index6Accents2, std::wstring index2Accents2, std::wstring databaseExtra2, std::wstring indexExtra2,std::wstring silence2, 
			std::wstring databaseVeryShortWord, std::wstring indexVeryShortWord,std::wstring silenceVeryShort,
			std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2, 
			std::wstring extra,std::wstring veryShortWord)
			:_wordDatabase (index6Accents, index2Accents, indexExtra, databaseMain, databaseExtra,silence,
			index6Accents2, index2Accents2, indexExtra2, databaseMain2,databaseExtra2,silence2,
			extra,
			indexVeryShortWord, databaseVeryShortWord,veryShortWord,silenceVeryShort)
{
		
	_nbSilencesAfter2Acc = 4 ; // Modify this in order to change the lenght of the silence add after words with 2 accents

	// Makes sure each path is not null
	if (index6Accents == L"")
	{
		throw VietnameseTTException (L"Can't load unit database index ");
	}

	if (index6Accents2 == L"")
	{
		throw VietnameseTTException (L"Can't load unit database 2 index ");
	}

	if (databaseMain == L"")
	{
		throw VietnameseTTException(L"Can't load unit database");
	}

	if (databaseMain2 == L"")
	{
		throw VietnameseTTException(L"Can't load unit database 2");
	}


	if (syl == L"")
	{
		throw VietnameseTTException(L"Can't load the map of _syllable");
	}

	if (cons1 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 1 letter");
	}

	if (cons2 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 2 letter");
	}

	_durMoyen = 0.28f;
	LoadFromFile (syl, _syllable, 85); //  Loads the _syllables in an array
		
	LoadFromFile (syl2, _syllable2, 95);
	LoadFromFile (cons1, _consonne1, 23); //  Loads the one letter consonnant in an array
	LoadFromFile (cons2, _consonne2, 24); //Loads the two letters consonnant in an array

}


////////////Thuan add 26-05-2020///////////////////////
VietVoice::WordSelector::WordSelector(std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2)
			:_wordDatabase ()
{
		
	_nbSilencesAfter2Acc = 4 ; // Modify this in order to change the lenght of the silence add after words with 2 accents

	// Makes sure each path is not null


	if (syl == L"")
	{
		throw VietnameseTTException(L"Can't load the map of _syllable");
	}

	if (cons1 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 1 letter");
	}

	if (cons2 == L"")
	{
		throw VietnameseTTException(L"Can't load the map on consonant with 2 letter");
	}

	_durMoyen = 0.28f;
	LoadFromFile (syl, _syllable, 85); //  Loads the _syllables in an array
		
	LoadFromFile (syl2, _syllable2, 95);
	LoadFromFile (cons1, _consonne1, 23); //  Loads the one letter consonnant in an array
	LoadFromFile (cons2, _consonne2, 24); //Loads the two letters consonnant in an array

}


VietVoice::WordSelector::~WordSelector ()
{}

VietVoice::WordSelector::WordSelector  (const WordSelector & ws)
	: _wordDatabase (ws._wordDatabase)
{
	if (this != &ws)
	{
		int i = 0;
		for (; i < 85; i++)
			_syllable [i] = ws._syllable[i];

		for (; i < 95; i++)
			_syllable2 [i] = ws._syllable2[i];

		for (; i < 23; i++)
			_consonne1 [i] = ws._consonne1 [i];

		for (; i < 23; i++)
			_consonne2 [i] = ws._consonne2 [i];
		
		_durMoyen = ws._durMoyen;
		_nbSilencesAfter2Acc = ws._nbSilencesAfter2Acc;
	}
}

void VietVoice::WordSelector::operator = (const WordSelector & ws)
{
	if (this != &ws)
	{
		int i = 0;
		for (; i < 85; i++)
			_syllable [i] = ws._syllable[i];

		for (; i < 95; i++)
			_syllable2 [i] = ws._syllable2[i];

		for (; i < 23; i++)
			_consonne1 [i] = ws._consonne1 [i];
				
		for (; i < 23; i++)
			_consonne2 [i] = ws._consonne2 [i];
		
		_durMoyen = ws._durMoyen;
		_nbSilencesAfter2Acc = ws._nbSilencesAfter2Acc;
	}		
}

void VietVoice::WordSelector::SetDatabase (std::wstring accents6idx, std::wstring accents2idx, std::wstring specialidx, std::wstring accentsMain, std::wstring specialbin, std::wstring extra,std::wstring silence,u_int type)
{
	_wordDatabase.SetDatabase (accents6idx, accents2idx, specialidx, accentsMain, specialbin, extra,silence,type);
}
////////////////Thuan add 13-02-2020////////////////////
void VietVoice::WordSelector::SetDatabaseVeryShortWord(std::wstring index,std::wstring database,std::wstring pathFileVeryShortWord,std::wstring silence)
{
	_wordDatabase.SetDatabaseVeryShortWord(index,database,pathFileVeryShortWord,silence);
}
/////////////////End Thuan add/////////////////////////////////////////
void VietVoice::WordSelector::LoadParam (std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2)
{
	LoadFromFile (syl, _syllable, 85); //  Loads the _syllables in an array	
	LoadFromFile (syl2, _syllable2, 95);
	LoadFromFile (cons1, _consonne1, 23); //  Loads the one letter consonnant in an array
	LoadFromFile (cons2, _consonne2, 24); //Loads the two letters consonnant in an array
}

/**
* Obtains the sound data from the database for the words that must be spokened.
*
* @param ToBeSpoken toSpoke
*/
void VietVoice::WordSelector::Treat(ToBeSynthetise & toSynthetise)
{	
	std::wstring wordName;
	std::wstring punc;
	double dur = 0; // Duration of the word
	double end = 0;  // Time at witch the word end
	double extraDurPunc = 0; //  waiting time after a punctuation
	int accent = 0;
	double durBefore = 0 ; //  Duration of the previous word, use dto find the duration of the silence added after each word
	bool beforeWas2accents = false;
	double silenceLength = 0;
	std::wstring v_symbol = L"*/ \xAB/ \xBB"; // VT
	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData ();
	std::list<Token> listToken;
	std::list<ToBeSynthetiseData> newListData;
	u_int type=1;
	float durationSilence = 0.025f; //word->GetDuration ();
	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		ToBeSynthetiseData data = *iter;
		if(data.m_wordType==ToBeSynthetiseData::WORD||data.m_wordType==ToBeSynthetiseData::SYMBOL 
			||data.m_wordType==ToBeSynthetiseData::ABBR_S||data.m_wordType==ToBeSynthetiseData::ABBR_L)
		{
			type=1;
		}
		else
		{
			if(data.m_wordType==ToBeSynthetiseData::DATE_G || data.m_wordType==ToBeSynthetiseData::NUMBER_G
			||data.m_wordType==ToBeSynthetiseData::WORD_G||data.m_wordType==ToBeSynthetiseData::SYMBOL_G 
			||data.m_wordType==ToBeSynthetiseData::ABBR_S_G||data.m_wordType==ToBeSynthetiseData::ABBR_L_G)
			{
				type=2;
			}
			else
			{
				if(data.m_wordType==ToBeSynthetiseData::DATE_GE || data.m_wordType==ToBeSynthetiseData::NUMBER_GE
				||data.m_wordType==ToBeSynthetiseData::WORD_GE||data.m_wordType==ToBeSynthetiseData::SYMBOL_GE 
				||data.m_wordType==ToBeSynthetiseData::ABBR_S_GE||data.m_wordType==ToBeSynthetiseData::ABBR_L_GE)
				{
					type=1;
				}
				else
				{
					if(data.m_wordType==ToBeSynthetiseData::VSWORD)
						type=3;
					else
					{
						if(data.m_wordType==ToBeSynthetiseData::DATE || data.m_wordType==ToBeSynthetiseData::NUMBER)
						{
							if(data.GetOriginalWord().compare(L"tháng")==0 || data.GetOriginalWord().compare(L"năm")==0 || data.GetOriginalWord().compare(L",")==0)
								type=1;
							else
								type=2;
						}
					}
				}
			}
		}
		

		std::vector<long> _thongSo=_wordDatabase.GetSilence(data.GetOriginalWord(),type);

		
		

		wstring prePunc = data.GetPostPunctuation ();
		data.SetSilenceLength(_thongSo[0]);
		data.SetTyLeTangVolumeTu(_thongSo[1]);
	
		// Obains the accent for the word
		wordName = data.GetWord ();
		
			
		// Thuan add 14-02-2020//////////////
		if(wordName.length()>1)
		{
			if(wordName.substr(wordName.length()-1).compare(L"_")==0)
			{
				wordName=wordName.substr(0,wordName.length()-1);
			}
		}
		///////// End Thuan add//////////////////
		

		accent = data.GetAccent ();
		this->_wordDatabase.m_percentageAdjust = data.m_percentageAdjust;
		Word * word = NULL;
// Viet sua
		if (wordName == L"_silence") { 
			type=1;
			word = _wordDatabase.GetExtraWord(L"silence",type);
		}
		else if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
		{		
			if(type!=3)
				word = FindWord(wordName, accent,type); 
			else
				word=_wordDatabase.GetVeryShortWord(data.GetOriginalWord());
		}
		else if (wordName != L"silence") // An extra word, get it in database unless silence
		{			
			if(type!=3)
			{
				word = _wordDatabase.GetExtraWord(wordName,type);
			}
			else
				word=_wordDatabase.GetVeryShortWord(data.GetOriginalWord ());
		}

		if (word != NULL)
		{
			
			data.SetWordData(word);
			newListData.push_back(data);				
		}
		else //if //(word == NULL) //Word not in database, play "silence" by playing silence sound in database a certian number of time
		{	
		
			std::list<ToBeSynthetiseData>::iterator nextIter = iter;
			nextIter++;
			
			//Unknow word so adds a noise
			if (wordName.compare (L"") != 0 && wordName.compare (L"silence") != 0 && wordName.compare (L"silenceSpecial") != 0 &&  StringHelper::OneOfCharPresent (wordName, L"\t\r\"'``\"'`.,:;~!?(){}[]\n\"'()<>[]`{}«»‘’\x201B“”„\x2032\x2033‹›\xFD3E\xFD3F" )== false)
			{
				type=1;
				if(wordName[0] == L'b')
				{
					
					word = FindWord (L"bê", 0,type);
				
					data.SetWordData(word);
					newListData.push_back(data);

				}
				else if(wordName[0] == L'c')
				{
					word = FindWord (L"xê", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'd')
				{
					word = FindWord (L"dê", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'đ')
				{
					word = FindWord (L"đê", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);

				}
				else if(wordName[0] == L'g')
				{
					word = FindWord (L"ghê", 0,type); // giê
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'z')
				{
					word = FindWord (L"dét", 1,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'h')
				{
					word = FindWord (L"hát", 1,type);
					data.SetWordData(word);
					newListData.push_back(data);

				}
				else if(wordName[0] == L'k')
				{
					word = FindWord (L"ca", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'l')
				{
					word = FindWord (L"en", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
//// word = NULL ; //// []

					word = FindWord (L"lơ", 2,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'm')
				{
					word = FindWord (L"em", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'n')
				{
					word = FindWord (L"en", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'p')
				{
					// word = FindWord (L"bê", 0); // VX
					word = GetExtraWord (L"pê",type); // VS
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'r')
				{
					word = FindWord (L"e", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
//// word = NULL ; //// []

					word = FindWord (L"rơ", 2,type); // VX
					data.SetWordData(word); // VX
					newListData.push_back(data); // VX
				}
				else if(wordName[0] == L's')
				{
					word = FindWord (L"ét", 1,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L't')
				{
					word = FindWord (L"tê", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'v')
				{
					word = FindWord (L"vê", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'x')
				{
					
					word = FindWord (L"ích", 1,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'q')
				{
					word = FindWord (L"cuy", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'f')
				{
					word = FindWord (L"ép", 1,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'j')
				{
					word = FindWord (L"gii", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'w')
				{
				
					word = FindWord (L"vê", 0,type); // VS
					data.SetWordData(word); // VS
					newListData.push_back(data); // VS
//// word = NULL ; //// []

					word = FindWord (L"kép", 1,type); // VS
					data.SetWordData(word); // VS
					newListData.push_back(data); // VS
				}
				else if(wordName[0] == L'y')
				{
					word = FindWord (L"i", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'a')
				{
					word = FindWord (L"a", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'e')
				{
					word = FindWord (L"e", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'i')
				{
					word = FindWord (L"i", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'o')
				{
					word = FindWord (L"o", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else if(wordName[0] == L'u')
				{
					word = FindWord (L"u", 0,type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
				else

					if ((v_symbol.find(wordName,0) == string::npos)) // VT
					{}
					else

				{
					word = _wordDatabase.GetExtraWord(L"beep",type);
					data.SetWordData(word);
					newListData.push_back(data);
				}
			}
	
		}
		
		
		
		//// delete word ; //// [hang up]
	}
	toSynthetise.SetListData (newListData) ;


}

/**
* When a word is unknown, the first letter is spoken
*
* Parameters:
*
* std::wstring:  the unknwon word
* std::list<VietVoice::ToBeSpokenData> & listData:  Contain the vocal (mp3) data
*/
void VietVoice::WordSelector::SpellFirstLetter (std::wstring wordName, std::list<VietVoice::ToBeSynthetiseData> & listData,u_int type)
{		

	Word * word = NULL;
	ToBeSynthetiseData data(ToBeSynthetiseData::WORD);

	if(wordName[0] == L'b')
	{	
		word = FindWord (L"bê", 0,type);	
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'c')
	{
		word = FindWord (L"xê", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'd')
	{
		word = FindWord (L"dê", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'đ')
	{
		word = FindWord (L"đê", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'g')
	{
		word = FindWord (L"ghê", 0,type); // giê
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'z')
	{
		word = FindWord (L"dét", 1,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'h')
	{
		word = FindWord (L"hát", 1,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'k')
	{
		word = FindWord (L"ca", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'l')
	{
		word = FindWord (L"en", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'm')
	{
		word = FindWord (L"em", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'n')
	{
		word = FindWord (L"en", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'p')
	{
		// word = FindWord (L"bê", 0); // VX
		word = GetExtraWord (L"pê",type); // VS
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'r')
	{
		word = FindWord (L"e", 0,type);
		data.SetWordData(word);
		listData.push_back(data);

		word = FindWord (L"rơ", 2,type); // VX
		data.SetWordData(word); // VX
		listData.push_back(data); // VX
	}
	else if(wordName[0] == L's')
	{
		word = FindWord (L"ét", 1,type);
		data.SetWordData(word);
		listData.push_back(data);

	}
	else if(wordName[0] == L't')
	{
		word = FindWord (L"tê", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'v')
	{
		word = FindWord (L"vê", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'x')
	{
		
		word = FindWord (L"ích", 1,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'q')
	{
		word = FindWord (L"cuy", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'f')
	{
		word = FindWord (L"ép", 1,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'j')
	{
		word = FindWord (L"gii", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'w')
	{
		word = FindWord (L"vê", 0,type); // VS
		data.SetWordData(word); // VS 
		listData.push_back(data); // VS

		word = FindWord (L"kép", 1,type); // VS
		data.SetWordData(word); // VS
		listData.push_back(data); // VS
	}
	else if(wordName[0] == L'y')
	{
		word = FindWord (L"i", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'a')
	{
		word = FindWord (L"a", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'e')
	{
		word = FindWord (L"e", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'i')
	{
		word = FindWord (L"i", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'o')
	{
		word = FindWord (L"o", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else if(wordName[0] == L'u')
	{
		word = FindWord (L"u", 0,type);
		data.SetWordData(word);
		listData.push_back(data);
	}
	else
	{
		word = _wordDatabase.GetExtraWord(L"beep",type);
		data.SetWordData(word);
		listData.push_back(data);
	}

}

/**
* Finds a word in the database
* @param wordName Name of the word
* @param accent Accent of the word
* @return The data of the word
*/
VietVoice::Word * VietVoice::WordSelector::FindWord (std::wstring wordName, int accent,u_int type)
{		
	Word * word = NULL;
	if (StringHelper::EndWith (wordName, L"p") || StringHelper::EndWith (wordName, L"c") || StringHelper::EndWith (wordName, L"t") || StringHelper::EndWith (wordName, L"ch"))
	{
		word = FindWord2Accents (wordName,type) ;
	}
	else
	{
		word = FindWord6Accents (wordName, accent,type) ; // Check in the base for words with 6 accents
	}

	return word;
}

/**
* Finds a word in the database for words with 6 accents
* @param wordName Name of the word
* @param accent Accent of the word
* @return The data of the word
*/
VietVoice::Word * VietVoice::WordSelector::FindWord6Accents (std::wstring wordName, int accent,u_int type) 
{
	std::wstring three = L"none";
	std::wstring two   = L"none";
	std::wstring first = L"none";
	int partie1 = 0;
	int partie2 = 0;
	int  len1 = 0;

	if (wordName == L"nghiêng" ) //nghiêngest is a special case, it's position is 1252
	{
		return _wordDatabase.GetWord6Accents (496 + accent * 1955,type) ;
	}
	else if (wordName.length() > 6) // Other then nghiêng, no word has more then 6 letter
	{
		return NULL;
	}

	if (wordName.length () > 3) // Take the first 3 letter of a word
	{
		three = wordName.substr (0, 3);
	}

	if (wordName.length () > 2) //  Take the first 2 letter of a word
	{
		two = wordName.substr (0, 2);
	}

	if (wordName.length () >= 1) // Take the first letter of a word
	{
		first = wordName.substr (0,1);

		if (first.compare(L"y") == 0) //  If the first letter is y, replace y by i
		{
			StringHelper::ReplaceFirst(wordName, L"y", L"i") ;

		}
	}

	for (int i = 0; i < 85; i++) //For each _syllable
	{
		partie1 = 0;
		partie2 = 0;
		len1 = 0;
		if (wordName.compare (_syllable[i]) == 0)
		{ //  If the word is the _syllable
			return _wordDatabase.GetWord6Accents(i + accent * 1955,type); //  Find the _syllable in the database
		}

		if (first != wordName)
		{
			if (three.compare (L"ngh") == 0)
			{ // ngh = ng
				partie1 = 6;
				len1 = 3;
			}
			else 
			{
				for (int j = 1; j < 23; j++)
				{ // Find the consonnant of the word
					if (two.compare(_consonne2[j]) == 0)
					{ // Determines if the consonnant is a consonnant with 2 letters
						partie1 = j + 1;
						len1 = 2;
						break;
					}
					else if (first.compare(L"k") == 0)
					{ 
						partie1 = 7;
						len1 = 1;
					}
					else
					{
						for (int k = 1; k < 23; k++) 
						{ // Determine if the consonnant is a consonnant with one letter
							if (first.compare(_consonne1[k]) == 0)
							{
								partie1 = k + 1;
								len1 = 1;
								break;
							}
						}
					}
				}

				if (partie1 != 0 && len1 != 0)
					break;
			}
		}
	}

	std::wstring second = wordName.substr(len1);

	for (int k = 0; k < 85; k++)
	{ //Find the _syllable after the consonnant
		if (second.compare(_syllable[k]) == 0)
		{
			partie2 = k + 1;
			return _wordDatabase.GetWord6Accents( (partie1 - 1) * 85 +
														(partie2 - 1) + accent * 1955,type); // Trouve le mot dans la base / Finds the word in the database
		}
	}
	return NULL  ; // Word was not found
}

/**
* Finds a word in the database for words with 2 accents
* @param wordName Name of the word
* @param accent Accent of the word
* @return The data of the word
*/
VietVoice::Word * VietVoice::WordSelector::FindWord2Accents (std::wstring wordName,u_int type)
{
	std::wstring three = L"";
	std::wstring two = L"";
	std::wstring first = L"";
	int partie1 = 0;
	int partie2 = 0;
	int len1 = 0;
	
	
	if (wordName.length () <= 1)
		return NULL;
	if (wordName.length () > 6)
	{ //  Other then nghiêng, no word has more then 6 letter
		return NULL;
	}

	if (wordName.length () > 3)
	{ //Take the first 3 letter of a word
		three = wordName.substr(0, 3);
	}



	if (wordName.length () > 2)
	{ //Take the first 2 letter of a word
		two = wordName.substr(0, 2);
	}

	if (wordName.length () > 1)
	{ // Take the first letter of a word
		first = wordName.substr(0, 1);

		// if (wordName.compare (L"y") == 0) // Viet sua ngay 30-8-2012
		if (first.compare (L"y") == 0) // Viet sua ngay 30-8-2012
		{ // If the first letter is y, replace y by i
//::MessageBox (NULL, StringHelper::ToString (wordName).c_str (), L"Word selector", MB_OK) ;
			StringHelper::ReplaceFirst(wordName, L"y", L"i");
//::MessageBox (NULL, StringHelper::ToString (wordName).c_str (), L"Word replaced", MB_OK) ;
		}
	}

	for (int i = 0; i <  95; i++) 
	{ // For each _syllable
		partie1 = 0;
		partie2 = 0;
		len1 = 0;

		if (wordName.compare (_syllable2[i]) == 0)
		{ // If the word is the _syllable
			return _wordDatabase.GetWord2Accents(i,type); // Trouve la _syllable dans la base / Find the _syllable in the database
		}
	}

	for (int i = 0; i <  95; i++) 
	{ //  For each _syllable
		partie1 = 0;
		partie2 = 0;
		len1 = 0;

		if (wordName.compare (_syllable2[i]) == 0)
		{ // If the word is the _syllable
			return _wordDatabase.GetWord2Accents(i,type); // Trouve la _syllable dans la base / Find the _syllable in the database
		}

		if (three.compare (L"ngh") == 0)
		{ 
			partie1 = 6;
			len1 = 3;
		}
		else 
		{
			for (int j = 1; j < 23; j++) 
			{ //Find the consonnant of the word
				if (two.compare (_consonne2[j]) == 0)
				{ // Determines if the consonnant is a consonnant with 2 letters
					partie1 = j + 1;
					len1 = 2;
					break;
				}
				else if (first.compare (L"k") == 0)
				{
					partie1 = 7;
					len1 = 1;
				}
				else
				{
					for (int k = 1; k < 23; k++)
					{ //  Determine if the consonnant is a consonnant with one letter
						if (first.compare (_consonne1[k]) == 0)
						{
							partie1 = k + 1;
							len1 = 1;
							break;
						}
					}
				}
			}

			if (partie1 != 0 && len1 != 0)
				break;
		}
	}
	std::wstring second = wordName.substr(len1);
	for (int k = 0; k < 95; k++) 
	{ // Find the _syllable after the consonnant
		if (second.compare (_syllable2[k]) == 0)
		{
			partie2 = k + 1;

			if (partie1 == 0)
				partie1++;
			
			
			//::MessageBox(NULL, StringHelper::ToString((partie1 - 1) * 95 + (partie2 - 1)).c_str(), L"Volume | Volume too high...", MB_OK);
			return _wordDatabase.GetWord2Accents( (partie1 - 1) * 95 + (partie2 - 1),type); // Finds the word in the database
		}
	}
	return NULL; // Word was not found
}

VietVoice::Word * VietVoice::WordSelector::GetExtraWord (std::wstring wordName,u_int type)
{
	return _wordDatabase.GetExtraWord (wordName,type);
}

//Thuan add 12-02-2020//////////

VietVoice::Word * VietVoice::WordSelector::GetVeryShortWord (std::wstring wordName)
{
	return _wordDatabase.GetVeryShortWord (wordName);
}

//// End Thuan add////////////


/**
* Method used to read the files containing the _syllable and consonnant
* @param filename Name of the file
* @param array Array containing the data of the file
*/
void VietVoice::WordSelector::LoadFromFile (std::wstring filename, std::wstring array [] , int size) 
{
	int i = 0 ;
	try
	{
		std::wstring line;

		File::Reader reader (filename);
		long count = reader.ReadLong (); // Read the number of lines in the file

		for (int i = 0; i < count; i++)  // For each line
		{
			if (i >= size)
			{
				throw VietnameseTTException (L"Invalid file in WordSelector object.");
			}

			line = reader.ReadWString ();  // Read the next line
			StringHelper::RemoveSpace (line);
////::MessageBox (NULL, line.c_str(), L"Line", MB_OK);
			if (line != L"")
			{
				//StringTokenizer tokenizer (line, L" \t\n");
				std::vector <std::wstring> tokens;
				StringHelper::Tokenize (tokens, line, L" \t\n");

				for (std::vector<std::wstring>::iterator iter = tokens.begin (); iter != tokens.end(); iter++)
				{
					array[i] = *iter;
				}
			}
		}
	}
	catch (File::Exception ex) 
	{
	throw VietnameseTTException (L"Something went wrong while reading in WordUnitSelector");
	}
}
