#include "vvinterfaces.h"
#include "VietnamienVoice.h"

EXPORT_DLL IUnknown * VietVoice::CreateInstance (const std::wstring path)
{
	try
	{
		IUnknown * pIUnknown = static_cast <IVietnameseVoice *> (new VietnamienVoice (path));
		return pIUnknown;
	}
	catch (VietnameseTTException ex)
	{
		::MessageBox (NULL, ex.GetMsg ().c_str (), L"Error", MB_OK);
	}
}