#if !defined (SILENCEADDER_H)

#define SILENCEADDER_H
#include <map>
#include <vector>
#include "useunicode.h"
#include "treatment.h"
#include "wordselector.h"

namespace VietVoice
{
	/**
	* Class Name:         SilenceAdder.java
	*
	* Author(s):          Benoit Lanteigne
	*
	* Creation Date:      October 22 2007
	*
	* Description:        Class used to find the accent of a word
	*
	* Copyright:          All rights reserved Moncton University
	*                     Tang-Ho L� Ph. D. - Computer Science Department
	*
	*/

	/**
	* CODE MODIFICATIONS AND BUG CORRECTIONS
	* --------------------------------------
	* 
	*/

	class SilenceAdder : public Treatement
	{
	public:

		/**
		* Constructor
		*
		* Parameters:
		*
		* VectorAccent & va: A reference to the VectorAccent object use to determine the accent of a word
		* std::wstring extra:  Name of the file containign the "extra", better described as the words from
	    *					   the 3rd database
		*/
		SilenceAdder (std::wstring adjectiveName,std::wstring lightName,std::wstring prosodyName, std::wstring silenceDictionaryName2, std::wstring silenceDictionaryName3, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, std::wstring linkingFile, WordSelector & _selector);
//		SilenceAdder (std::wstring prosodyName, std::wstring silenceDictionaryName, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, WordSelector & _selector);

		/**
		* Copy constructor
		*/
		SilenceAdder  (const SilenceAdder & silenceAdder);

		/**
		* Destructor
		*/
		virtual ~SilenceAdder ();

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const SilenceAdder & silenceAdder);

		/**
		* Find the accent for every word in the utterance.
		*
		* Return value:  The data to be spoken after modification
		*
		* Parameters:
		* ToBeSpoken toSpoke: A reference to the data to be spoken that is being processed
		* 
		*/
		void Treat (ToBeSynthetise & toSynthetise);
		long VietVoice::SilenceAdder::v_getBreakSilence1(); // VT
		long VietVoice::SilenceAdder::v_getBreakSilence2(); // VT
		long VietVoice::SilenceAdder::v_getVolume(); // VT
		long VietVoice::SilenceAdder::v_getSpeed(); // VT
		long VietVoice::SilenceAdder::v_getPitch(); //// VT
		long VietVoice::SilenceAdder::v_getCompress(); //// VT
		void VietVoice::SilenceAdder::v_getLight(std::wstring &v_lws); // VT
		void VietVoice::SilenceAdder::v_getAdjective(std::wstring &v_ads); // VT
		bool VietVoice::SilenceAdder::v_isStopConsonant(std::wstring v_word, int v_accent); // VT
	private:

		/**
		* Read all extra words from a file
		*
		* Parameters:
		*
		* std::wstring filename: filename The name of the file
		*/
		void _LoadSilenceDictionnary (std::wstring filename); // Viet chu thich: khong xai nua

		void _LoadSilencesSize(std::wstring filename);
		void _LoadWordsSize(std::wstring filename);

		int ReadIntFromFile(File::Reader & reader);

	protected:

		std::map <std::wstring, std::wstring> _silenceDictionnary2; // Contain all special words (words from the third database).
		std::map <std::wstring, std::wstring> _silenceDictionnary3; // VT
		std::vector<std::wstring> m_articlesDictionary;
		std::vector<std::wstring> m_linkingDictionary;
		WordSelector * _selector;

		long smallWord;
		long mediumWord;
		long longWord;
		
		long smallSilence;
		long mediumSilence;
		long longSilence;
		long m_silenceSize;
		long m_silenceSize2;

		long tooSmall;
		long v_break_silence1; // VT
		long v_break_silence2; // VT
		float v_break_compound; // VT
		long v_compound_min; // VT
		int v_stop_silence; // VT
		long v_volume; // VT
		long v_speed; // VT
		long v_pitch; // VT
		long v_compress; // VT
		long v_adjectives_sl; // VT
		long v_linking_sl; // VT
		long v_number_sl; // VT
		std::wstring v_light_Words ; // VT
		std::wstring v_adjective_Words ; // VT
	};
}

#endif