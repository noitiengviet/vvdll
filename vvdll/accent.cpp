
#include "accent.h"

/**
* Copy constructor
*/
VietVoice::Accent::Accent  (const Accent & accent)
{
	if (this != &accent) // Make sure we are not copying an object into itself
	{
		_accent = accent._accent;
		_replaceWith = accent._replaceWith;
	}
}

/**
* Assignement operator used to avoid memory leaks
*/
void VietVoice::Accent::operator = (const Accent & accent)
{
	if (this != &accent) // Make sure we are not copying an object into itself
	{
		_accent = accent._accent;
		_replaceWith = accent._replaceWith;
	}
}

/**
* Constructor
*/
VietVoice::Accent::Accent ()
{
	_replaceWith  = L"";
}

/**
* Constructor.
*
*
* Parameters:
*
* int a: Numerical value representing an accent.
* std::wstring w The character used to replace the character with the accent represented by the numerical value
*/
VietVoice::Accent::Accent (int a, std::wstring w)
{
	_accent = a;
	_replaceWith = w;
}

/**
* Obtain the numerical accent
*
* Return value:  A numerical value representing the accent
*/
int VietVoice::Accent::GetAccent ()
{
	return _accent ;
}

/**
* Obtain the character used to replace the character with the accent in a word
*
* Return value:  The character used to replace the character with the accent in a word
*/
std::wstring VietVoice::Accent::GetReplaceWith ()
{
	return _replaceWith ;
}

/**
* Change the numerical value accent
*
* Parameters:
*
* int a: A numerical value representing the new accent
*/
void VietVoice::Accent::SetAccent (int a)
{
	_accent = a;
}

/**
* Changes the character used to replace the character with the accent in a word
*
* Parameters:
*
* std::wstring w The new charater used to replace the character with the accent in a word
*/
void VietVoice::Accent::SetReplaceWith (std::wstring w)
{
	_replaceWith = w ;
}