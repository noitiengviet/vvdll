Công ty Mạng Tinh Hoa xin thân mến chào bạn.
Có lẽ đây là lần đầu tiên bạn đang được nghe một giọng nói tổng hợp dùng để đọc các văn bản tiếng Việt.  Giọng nói này được tạo ra từ một số vần cơ bản của tiếng Việt mà chúng tôi đã thu âm.  Sau đó chúng tôi dùng các kỹ thuật của công nghệ tiếng nói tiên tiến nhất để làm thành giọng nói tổng hợp mà bạn đang nghe đây.
Chắc bạn cũng biết, so với các ngôn ngữ khác thì tiếng Việt có nhiều dấu nhất, và số lượng vần cũng vô cùng phong phú, với hơn 140 vần.  Vì vậy việc tổng hợp tiếng Việt rất khó khăn.  Phần mềm của chúng tôi có tên là Phát Âm Tiếng Việt, viết tắt là MTH - PATV.  Đây là phiên bản đầu tiên mà chúng tôi mời bạn nghe thử để cho chúng tôi một số nhận xét và đánh giá thẳng thắn của bạn.  Các phản hồi của bạn sẽ giúp chúng tôi cải thiện chất giọng trong phiên bản sắp tới.
Xin bạn lưu ý rằng, phần mềm này có khả năng đọc chữ số các loại, đọc các cách viết chỉ thời gian như giờ giấc, ngày tháng năm, đọc các tiếng viết tắt thông dụng, các đơn vị đo lường và cả các từ ngữ tiếng nước ngoài đã phiên âm sang tiếng Việt.  Chẳng hạn, hôm nay là ngày 18-05-2012.  Ngoài ra, chúng tôi đã cố gắng phát hiện và áp dụng các quy luật về ngữ điệu để làm cho giọng đọc được tự nhiên, nghĩa là biết phân biệt các cụm từ với các từ đơn lẻ, chứ không đọc đều đều cùng một nhịp.  Công việc này hết sức phức tạp, tỉ mỉ và vẫn được cải thiện không ngừng.
Mặc dù chưa hoàn toàn giống như giọng đọc của người thật, song chúng tôi tin rằng phần mềm này cũng phục vụ tốt cho người dùng và sớm được đưa vào ứng dụng trong đời sống.  Các phiên bản kế tiếp sẽ được tự động cập nhật trên máy của bạn mỗi khi bạn dùng phần mềm này.
Xin chân thành cám ơn bạn đã tham gia việc thử nghiệm đầu tiên của chúng tôi.
Trân Trọng
Công ty Mạng Tinh Hoa

