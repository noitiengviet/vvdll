
#include <process.h> 
#include "vietnamienVoice.h"
#include "vvinterfaces.h"

VietVoice::VietnamienVoice::VietnamienVoice (const std::wstring path)
	: _tokensToWords (path + L"\\abbreviations.bin", path + L"\\noSilence.bin",8,path + L"\\Phrase\\",path + L"\\VeryShortWord.txt"),
	  _vectorAccent (path + L"\\vecteurAccent.bin"),
	  _accentFinder (_vectorAccent),
	  _wordModifier (),
	  _silenceAdder (path + L"\\adjectives.txt", path + L"\\lightWords.txt", path + L"\\Prosody.txt", path + L"\\silenceDictionary-2.txt", path + L"\\silenceDictionary-3.txt", path + L"\\silenceDictionary-4.txt", path + L"\\wordssize.txt", path + L"\\silencessize.txt", path + L"\\articles.txt", path + L"\\LinkingWords.txt", path + L"\\Quantity_Words.txt", path + L"\\Noun_Words.txt", path + L"\\Verb_Words.txt", _wordSelector),
	  _wordSelector ( path + L"\\syllable.bin", path + L"\\syllable2.bin",
	  path + L"\\consonne1.bin", path + L"\\consonne2.bin"),
	  _callback (NULL), 
	  _element (VietVoice::PHRASE),
	  _isSpeaking (false),
	  _curVoice (VietVoice::ToBeSynthetise::MALE)
{
	//WCHAR currentDirectory [_MAX_PATH];
	_currentDirectory = path;
	InitTreatements ();
	
	_durMoyenSilence = 1.0f; // 0.35f;
	_speed = 1.0f; // 0.35f;
	_volume = 180.0f; // 0.35f;
	_pitch = 0.0f; // 0.35f;
	_wsola = false ; // true -> error

//	_vbs=2000; VX, version 15 (after words)
	::InitializeCriticalSection (&_critical);
}

VietVoice::VietnamienVoice::~VietnamienVoice ()
{
	::DeleteCriticalSection (&_critical);
}

VietVoice::VietnamienVoice::VietnamienVoice  (const VietnamienVoice & voice)
: _tokensToWords (voice._currentDirectory + L"\\abbreviations.bin", voice._currentDirectory + L"\\noSilence.bin"),
	  _vectorAccent (voice._currentDirectory + L"\\vecteurAccent.bin"),
	  _accentFinder (_vectorAccent, voice._currentDirectory + L"\\extraWords.bin"),
	  _wordModifier (),
	  _silenceAdder (voice._currentDirectory + L"\\adjectives",voice._currentDirectory + L"\\lightWords",voice._currentDirectory + L"\\prosody", voice._currentDirectory + L"\\silenceDictionary-2", voice._currentDirectory + L"\\silenceDictionary-3", voice._currentDirectory + L"\\silenceDictionary-4", voice._currentDirectory + L"\\wordssize.txt", voice._currentDirectory + L"\\silencessize.txt", voice._currentDirectory + L"\\articles.txt", voice._currentDirectory + L"\\LinkingWords.txt", voice._currentDirectory + L"\\Quantity_Words.txt", voice._currentDirectory + L"\\Noun_Words.txt", voice._currentDirectory + L"\\Verb_Words.txt", _wordSelector),
	  _wordSelector (
	  voice._currentDirectory + L"\\database//maleVoice//NormalWord//Base_Unit.bin",
	  voice._currentDirectory + L"\\database//maleVoice//NormalWord//6Accents_Unit.bin" ,
	  voice._currentDirectory + L"\\database//maleVoice//NormalWord//2Accents_Unit.bin", 
	  voice._currentDirectory + L"\\database//maleVoice//NormalWord//Special_Unit.bin", 
	  voice._currentDirectory + L"\\database//maleVoice//NormalWord//Special_Unit.idx",
	   voice._currentDirectory + L"\\database//maleVoice//NormalWord//Control//",

	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//Base_Unit.bin",
	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//6Accents_Unit.bin" ,
	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//2Accents_Unit.bin", 
	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//Special_Unit.bin", 
	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//Special_Unit.idx",
	  voice._currentDirectory + L"\\database//maleVoice//ShortWord//Control//",

	  voice._currentDirectory + L"\\database//maleVoice//VeryShortWord//VeryShort_Word.bin", 
	  voice._currentDirectory + L"\\database//maleVoice//VeryShortWord//VeryShort_Word.idx", 
	  voice._currentDirectory + L"\\database//maleVoice//VeryShortWord//Control//",
	  
	  voice._currentDirectory + L"\\syllable.bin", voice._currentDirectory + L"\\syllable2.bin", 
	  voice._currentDirectory + L"\\consonne1.bin", voice._currentDirectory + L"\\consonne2.bin",	  
	  voice._currentDirectory + L"\\extraWords.bin",
	  voice._currentDirectory + L"\\VeryShortWord.txt"),
	  _callback (NULL),
	  _element (VietVoice::PHRASE),
	  _isSpeaking (false),
	  _curVoice (voice._curVoice)
	  
{
	if (this != &voice)
	{
			_listTreatement = voice._listTreatement; // List of object use to "read" text
			_durMoyenSilence = voice._durMoyenSilence;
			_speed = voice._speed;
			_volume = voice._volume;
			_pitch = voice._pitch;
			_wsola=voice._wsola;
			_tokensToWords = voice._tokensToWords;
			_vectorAccent = voice._vectorAccent;
			_accentFinder = voice._accentFinder;
			_wordModifier = voice._wordModifier;
			_wordSelector = voice._wordSelector;
			_callback = voice._callback;
			_element = VietVoice::PHRASE;
			_pathDb = voice._pathDb;
			_isSpeaking = false;
			::InitializeCriticalSection (&_critical);
	}
}

void VietVoice::VietnamienVoice::operator = (const VietnamienVoice & voice)
{
	if (this != &voice)
	{
		_listTreatement = voice._listTreatement; // List of object use to "read" text
		_durMoyenSilence = voice._durMoyenSilence;
		_speed = voice._speed;
		_volume = voice._volume;
		_pitch = voice._pitch;
		_wsola=voice._wsola;
		_tokensToWords = voice._tokensToWords;
		_vectorAccent = voice._vectorAccent;
		_accentFinder = voice._accentFinder;
		_wordModifier = voice._wordModifier;
		_wordSelector = voice._wordSelector;	
		_pathDb = voice._pathDb;
		::EnterCriticalSection (&_critical);
		_isSpeaking = false;
		::LeaveCriticalSection (&_critical);
		::InitializeCriticalSection (&_critical);
		_curVoice = voice._curVoice;
	}		
}

 /**
 * Reload the abbreviation file.  useful when there is a change in the abbreviations.
 */
HRESULT _stdcall VietVoice::VietnamienVoice::ReloadAbbreviations ()
{
	 _tokensToWords.LoadAbbreviationFile (_currentDirectory + L"\\abbreviations.bin");
	 return S_OK;
}

/**
 * Read a string of text
 * @param str The string to be read
 * @param pos Position from which we start reading
 */
HRESULT _stdcall VietVoice::VietnamienVoice::Speak (std::wstring str, int pos)
{		
	::EnterCriticalSection (&_critical);
	_isSpeaking = true;
	::LeaveCriticalSection (&_critical);
	try
	{

		StringHelper::ReplaceAll (str, L'@', L' ');
		StringHelper::ReplaceAll (str, L'\x00AB', L' '); // VT L"«"
		StringHelper::ReplaceAll (str, L'\x00BB', L' '); // VT L"»"
		StringHelper::ReplaceAll (str, L'|', L' '); // VT L"»"

		StringHelper::ReplaceAll (str, L'\x2018', L' ');
		StringHelper::ReplaceAll (str, L'\x2019', L' ');

//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"Before Speaking file", MB_OK); // VT
Process_0(str, true) ; // Thieu dau cham cuoi cau.
Process_1(str) ; // Thu sang_. -> Thu sang.
Process_2(str) ; // Xu ly tu viet tat (N.T.T)
Process_3(str) ; // Xu ly (72%) -> 72%) 
Process_4(str) ; // Xu ly Time phrase 12:59 -> 12|59
Process_5(str) ; // Xu ly 18:55 - > 18 giờ 55 phút (29-8-2012)
// Process_6(str) ; // ... ngày 19-9 - Ảnh do SCG cung cấp (lỗi 18:55.)
Process_7(str) ; // " “ ” + ; , . !
//		str += L" . "; 
//		str += L"."; // VS
Process_8(str) ; // ... ngày 19-9 - Ảnh do SCG cung cấp
//Process_9(str) ; // Số La Mã
//Process_4(str) ; // 0->9:0->9
//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"After Speaking file", MB_OK); // VT

		if (_callback != NULL)
		{
		
			_callback->OnStartSpeaking (this);
			
		}

		_mustStop = false;

		ToBeSynthetise toSynthetise;
		toSynthetise.SetVoice (_curVoice);
		// toSynthetise.SetMp3Name(_currentDirectory + L"\\database\\tmp.mp3");
		toSynthetise.SetMustPlay (true);
		toSynthetise.SetDurMoyenSilence (_durMoyenSilence) ;
		toSynthetise.SetSpeed(_speed) ;
		toSynthetise.SetVolume (_volume) ;
		toSynthetise.SetPitch (_pitch) ;
		toSynthetise.SetWsola (_wsola) ;
// ====================================================================
		toSynthetise.Set_BS (_vbs) ; // Cho nay se truyen thong so
		v_bs1 = _silenceAdder.v_getBreakSilence1() ;
		_concatenator.Get_BS1(v_bs1);
		v_bs2 = _silenceAdder.v_getBreakSilence2() ;
		_concatenator.Get_BS2(v_bs2);
		_concatenator.Get_VL(_silenceAdder.v_getVolume());




		_concatenator.Get_MV(_silenceAdder.v_get_main_vol());

		_concatenator.Get_SV1(_silenceAdder.v_get_soft_vol1());
		_concatenator.Get_SV21(_silenceAdder.v_get_soft_vol21());
		_concatenator.Get_SV22(_silenceAdder.v_get_soft_vol22());
		_concatenator.Get_SV3(_silenceAdder.v_get_soft_vol3());
		_concatenator.Get_SP(_silenceAdder.v_getSpeed());
		_concatenator.Get_Pitch(_silenceAdder.v_getPitch()); ////
		_concatenator.Get_CP(_silenceAdder.v_getCompress()); ////
		_concatenator.Get_WS(_silenceAdder.v_getWordSize()); ////
		_concatenator.Get_WS2(_silenceAdder.v_getWordSize2()); ////
		_concatenator.v_getCW1(_silenceAdder.v_getCW1()); ////
		_concatenator.v_getCW2(_silenceAdder.v_getCW2()); ////
		_concatenator.v_getStopSilence(_silenceAdder.v_getStopSilence()); ////

		_concatenator.Get_Per_Pitch(_silenceAdder.v_get_per_pitch()) ;
		_concatenator.Get_Pitch0(_silenceAdder.v_get_pitch0()) ;
		_concatenator.Get_Pitch1(_silenceAdder.v_get_pitch1()) ;
		_concatenator.Get_Pitch2(_silenceAdder.v_get_pitch2()) ;
		_concatenator.Get_Pitch3(_silenceAdder.v_get_pitch3()) ;
		_concatenator.Get_Pitch4(_silenceAdder.v_get_pitch4()) ;
		_concatenator.Get_Pitch5(_silenceAdder.v_get_pitch5()) ;

		_concatenator.Get_Com0(_silenceAdder.v_get_com0()) ;
		_concatenator.Get_Com1(_silenceAdder.v_get_com1()) ;
		_concatenator.Get_Com2(_silenceAdder.v_get_com2()) ;
		_concatenator.Get_Com3(_silenceAdder.v_get_com3()) ;
		_concatenator.Get_Com4(_silenceAdder.v_get_com4()) ;
		_concatenator.Get_Com5(_silenceAdder.v_get_com5()) ;
		_concatenator.Get_cut_begin(_silenceAdder.v_get_c_begin()) ;
		_concatenator.Get_cut_end(_silenceAdder.v_get_c_end()) ;

		_silenceAdder.v_getLight(v_lw) ;
		_concatenator.Get_light(v_lw) ;
		_silenceAdder.v_getAdjective(v_ad) ;
		_concatenator.Get_adjective(v_ad) ;
		_concatenator.Get_Dots(_silenceAdder.v_getDots()) ;
		_concatenator.Get_Not_Echo(_silenceAdder.v_getNotEcho()) ;
		_concatenator.Get_Fade(_silenceAdder.v_getDots()) ;
		_concatenator.Get_Sample(_silenceAdder.v_Samples()) ;
		_concatenator.Get_sc_comp (_silenceAdder.v_get_sc_comp()) ;
		_concatenator.v_getLinkingSilence(_silenceAdder.v_getLinkingSilence());
// ====================================================================

		Token token;

		int nb = 0;


//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"Speaking file", MB_OK); // VT


		str += L" ";
		// Parse the string in tokens
		_tokenizer.SetString (str);

		int beginPos = pos;
		int endPos = 0;

		do
		{
			_tokenizer.GetNextToken (token);
			endPos = _tokenizer.GetCurrentPos () + pos;

//// ::MessageBox (NULL, StringHelper::ToString(token.GetToken()).c_str(), L"Vowel", MB_OK) ;

			if (!token.IsEmpty ())
			{
				nb++;
				toSynthetise.AppendToken (token) ;
			}
		
			//if (token.IsEmpty () || token.HasPostPunc () || nb >= 50) // if end ofor string or punctiation mark or number of tokens too big, start speaking
			if (token.IsEmpty ()||token.GetPostPunctuation()==L"." && nb >= 50)
			{
				if (nb >= 0)
				{
					if (_callback != NULL)
					{
						_callback->OnElementSpoken (this, beginPos, endPos);
					}

					beginPos = endPos;

				TreatToBeSpoken(toSynthetise); // Treat the text so it is read //// Doc cho nay

				}
				toSynthetise.Empty () ; // Neu khong xoa lap lai noi dung truoc


//_durMoyenSilence=1.0f ;

				toSynthetise.SetDurMoyenSilence(_durMoyenSilence); // Set the lenght of word below witch silence must be added
				toSynthetise.SetSpeed(_speed); // Set the lenght of word below witch silence must be added
				toSynthetise.SetVolume(_volume); // Set the lenght of word below witch silence must be added
				toSynthetise.SetPitch(_pitch); // Set the lenght of word below witch silence must be added
				toSynthetise.SetWsola(_wsola);
				nb = 0;
			}

		} while (!token.IsEmpty () && _mustStop == false) ;

		if (_callback != NULL)
		{
			_callback->OnStopSpeaking (this);
		}

		return S_OK;
	}
	catch (VietnameseTTException ex)
	{
		::EnterCriticalSection (&_critical);
		_isSpeaking = false;
		::LeaveCriticalSection (&_critical);
		::MessageBox (NULL, ex.GetMsg ().c_str (), L"Error", MB_OK);
		return E_FAIL;
	}

	::EnterCriticalSection (&_critical);
	_isSpeaking = false;
	::LeaveCriticalSection (&_critical);
	delete this; // 24-11-2012
}

HRESULT __stdcall VietVoice::VietnamienVoice::SaveMp3 (std::wstring str, std::wstring filename)
{
	try
	{
		StringHelper::ReplaceAll (str, L'@', L' ');
		StringHelper::ReplaceAll (str, L'\x00AB', L' '); // VT L"«"
		StringHelper::ReplaceAll (str, L'\x00BB', L' '); // VT L"»"
		StringHelper::ReplaceAll (str, L'|', L' '); // VT L"»"

		StringHelper::ReplaceAll (str, L'\x2018', L' ');
		StringHelper::ReplaceAll (str, L'\x2019', L' ');

//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"Before Speaking file", MB_OK); // VT
Process_0(str, true) ; // Thieu dau cham cuoi cau.
Process_1(str) ; // Thu sang_. -> Thu sang.
Process_2(str) ; // Xu ly tu viet tat (N.T.T)
Process_3(str) ; // Xu ly (72%) -> 72%) 
Process_4(str) ; // Xu ly Time phrase 12:59 -> 12|59
Process_5(str) ; // Xu ly 18:55 - > 18 giờ 55 phút (29-8-2012)
Process_7(str) ; // " “ ” + ; , . !
Process_8(str) ; // ... ngày 19-9 - Ảnh do SCG cung cấp

		ToBeSynthetise toSynthetise;
		toSynthetise.SetVoice (_curVoice);
		toSynthetise.SetMp3Name(filename);
		toSynthetise.SetMustPlay (false);
		toSynthetise.SetDurMoyenSilence (_durMoyenSilence) ;
		toSynthetise.SetSpeed (_speed) ;
		toSynthetise.SetVolume (_volume) ;
		toSynthetise.SetPitch (_pitch) ;
		toSynthetise.SetWsola(_wsola);

		toSynthetise.Set_BS (_vbs) ; // Cho nay se truyen thong so
		v_bs1 = _silenceAdder.v_getBreakSilence1() ;
		_concatenator.Get_BS1(v_bs1);
		v_bs2 = _silenceAdder.v_getBreakSilence2() ;
		_concatenator.Get_BS2(v_bs2);


		_concatenator.Get_VL(_silenceAdder.v_getVolume());
		_concatenator.Get_MV(_silenceAdder.v_get_main_vol());
		_concatenator.Get_SV1(_silenceAdder.v_get_soft_vol1());
		_concatenator.Get_SV21(_silenceAdder.v_get_soft_vol21());
		_concatenator.Get_SV22(_silenceAdder.v_get_soft_vol22());
		_concatenator.Get_SV3(_silenceAdder.v_get_soft_vol3());
		_concatenator.Get_SP(_silenceAdder.v_getSpeed());
		_concatenator.Get_Pitch(_silenceAdder.v_getPitch()); 
		_concatenator.Get_CP(_silenceAdder.v_getCompress()); 
		_concatenator.Get_WS(_silenceAdder.v_getWordSize()); 
		_concatenator.Get_WS2(_silenceAdder.v_getWordSize2()); ////
		_concatenator.v_getCW1(_silenceAdder.v_getCW1()); 
		_concatenator.v_getCW2(_silenceAdder.v_getCW2()); 
		_concatenator.v_getStopSilence(_silenceAdder.v_getStopSilence()); 

		_concatenator.Get_Per_Pitch(_silenceAdder.v_get_per_pitch()) ;
		_concatenator.Get_Pitch0(_silenceAdder.v_get_pitch0()) ;
		_concatenator.Get_Pitch1(_silenceAdder.v_get_pitch1()) ;
		_concatenator.Get_Pitch2(_silenceAdder.v_get_pitch2()) ;
		_concatenator.Get_Pitch3(_silenceAdder.v_get_pitch3()) ;
		_concatenator.Get_Pitch4(_silenceAdder.v_get_pitch4()) ;
		_concatenator.Get_Pitch5(_silenceAdder.v_get_pitch5()) ;

		_concatenator.Get_Com0(_silenceAdder.v_get_com0()) ;
		_concatenator.Get_Com1(_silenceAdder.v_get_com1()) ;
		_concatenator.Get_Com2(_silenceAdder.v_get_com2()) ;
		_concatenator.Get_Com3(_silenceAdder.v_get_com3()) ;
		_concatenator.Get_Com4(_silenceAdder.v_get_com4()) ;
		_concatenator.Get_Com5(_silenceAdder.v_get_com5()) ;
		_concatenator.Get_cut_begin(_silenceAdder.v_get_c_begin()) ;
		_concatenator.Get_cut_end(_silenceAdder.v_get_c_end()) ;

		_silenceAdder.v_getLight(v_lw) ;
		_concatenator.Get_light(v_lw) ;
		_silenceAdder.v_getAdjective(v_ad) ;
		_concatenator.Get_adjective(v_ad) ;
		_concatenator.Get_Dots(_silenceAdder.v_getDots()) ;
		_concatenator.Get_Not_Echo(_silenceAdder.v_getNotEcho()) ;
		_concatenator.Get_Fade(_silenceAdder.v_getDots()) ;
		_concatenator.Get_Sample(_silenceAdder.v_Samples()) ;
		_concatenator.Get_sc_comp (_silenceAdder.v_get_sc_comp()) ;
		_concatenator.v_getLinkingSilence(_silenceAdder.v_getLinkingSilence());
// ====================================================================

		Token token;

		int nb = 0;

		str += L" . "; // VT

		StringHelper::ReplaceAll( str, L'«', L' ') ; // VT
		StringHelper::ReplaceAll( str, L'»', L' ') ; // VT

		// Parse the string in tokens
		_tokenizer.SetString (str);

		int beginPos = 0;
		int endPos = 0;

		do
		{
			_tokenizer.GetNextToken (token);
			endPos = _tokenizer.GetCurrentPos ();

			if (!token.IsEmpty ())
			{
				toSynthetise.AppendToken(token);				
			}

			if (token.IsEmpty ()) // if end ofor string or punctiation mark or number of tokens too big, start speaking
			{
				if (nb >= 0)
				{
					if (_callback != NULL)
						_callback->OnElementSpoken (this, beginPos, endPos);

					beginPos = endPos;

					TreatToBeSpoken(toSynthetise); // Treat the text so it is read

				}
		
				toSynthetise.Empty ();	  
				toSynthetise.SetDurMoyenSilence(_durMoyenSilence); // Set the lenght of word below witch silence must be added
				toSynthetise.SetSpeed(_speed); // Set the lenght of word below witch silence must be added
				toSynthetise.SetVolume(_volume); // Set the lenght of word below witch silence must be added
				toSynthetise.SetPitch(_pitch); // Set the lenght of word below witch silence must be added
				toSynthetise.SetWsola(_wsola);
				nb = 0;
			}
		} while (!token.IsEmpty () ) ; // End do ... while
	}
	catch (VietnameseTTException ex)
	{

		return E_FAIL;

	}
	catch (File::Exception ex)
	{
		return E_FAIL;
	}

	return S_OK;
}

/**
* Read a string of text asynchronously (in another thread).
* @param:  str The string to be read.
* @param pos Position from which we start reading
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SpeakAsync (std::wstring str, int pos)
{
	// Prepare the thread parameters
	VietVoice::ThreadSpeakParam * param = new VietVoice::ThreadSpeakParam ();

	param->_voice = this;
	param->_str = str;
	param->_pos = pos;

	_beginthreadex (NULL, 0, ThreadSpeak, (void *) param, 0, NULL); // Create another thread that will speak
	//this->~VietnamienVoice()  ; // Them va xoa 24-11-2012 VVGraph hang up
	return S_OK;
}

/**
* Stop the reading of the text.
*/
HRESULT _stdcall VietVoice::VietnamienVoice::Stop ()
{
	_mustStop = true;
	_concatenator.Stop ();
	::EnterCriticalSection (&_critical);
	_isSpeaking = false;
	::LeaveCriticalSection (&_critical);
	return S_OK;
}

/**
 * Sets the minimal length for a word
 * @param dur
 */
HRESULT _stdcall VietVoice::VietnamienVoice::SetDurMoyenSilence (float dur)
{
	_durMoyenSilence = dur;
	return S_OK;
}
/* HRESULT _stdcall VietVoice::VietnamienVoice::Set_BS (long vbs)
{
	_vbs = vbs;
	return S_OK;
}
*/
/**
 * Obtains the minimal length for a word
 * @return
 */
HRESULT _stdcall VietVoice::VietnamienVoice::GetDurMoyenSilence (double & dur)
{
	dur = _durMoyenSilence;
	return S_OK;
}
HRESULT _stdcall VietVoice::VietnamienVoice::Get_BS (long & vbs)
{
	vbs = _vbs;
	return S_OK;
}

void VietVoice::VietnamienVoice::InitTreatements ()
{
	_listTreatement.push_back (&_tokensToWords);
	_listTreatement.push_back (&_accentFinder);
	_listTreatement.push_back (&_wordModifier);
	//_listTreatement.push_back (&_silenceAdder);
	_listTreatement.push_back (&_wordSelector); // [Main Leak]
	_listTreatement.push_back (&_concatenator);
	//// _listTreatement.clear() ; // Khong doc nua !!!
}
/**
 * Iterrates throught the list of Treatement object and each one does it task
 * @param toSpoke
 */
void VietVoice::VietnamienVoice::TreatToBeSpoken (ToBeSynthetise toSynthetise)
{
	for (std::list<Treatement *>::iterator iter = _listTreatement.begin (); iter != _listTreatement.end (); iter++)
	{
		(*iter)->Treat (toSynthetise);
	}
}

void VietVoice::VietnamienVoice::Process_1 (std::wstring &vstr) // Thu sang_. -> Thu sang.
{
	StringHelper::ReplaceAll (vstr, L'*', L' ');
	StringHelper::ReplaceAll (vstr, L')', L',');
	StringHelper::ReplaceAll (vstr, L'\x0027', L'"');

	unsigned int w = 0;
	unsigned int v_loc = 0;
	do {
		w = vstr.find (L'\x000A', v_loc); // VS
		if (w != vstr.npos && w > 0) {
			if (vstr[w-1] == L'"' || vstr[w-1] == L'”' || vstr[w-1] == L'\x0027') vstr[w-1] = L' ' ; // Thêm 12-9-2012 xu ly " + <-'
			v_loc = w ;
			do {
				w -- ;
			} while (w > 0 && vstr[w] == L' ') ;
			if (vstr[w] == L'.' && (w > 0 && vstr[w-1] == L' ')) vstr[w] = L' ' ;
			w = v_loc ;
			do {
				w -- ;
			} while (w > 0 && vstr[w] == L' ') ;
			if (vstr[w] != L'.' && w < vstr.length()-1 && vstr[w+1] != L'\x000A') vstr[w+1] = L'.' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L')', v_loc) ; 
		if (w != vstr.npos && w > 0 && w < vstr.length()) {
			vstr[w] = vstr[w+1] ; 
			vstr[w+1] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

// "- sat nhau doc khong cach, them 15-9-2012
	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'\x0027', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-1 && vstr[w+1] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'"', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-1 && vstr[w+1] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'”', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-1 && vstr[w+1] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);
// "_- sat nhau doc khong cach, them 15-9-2012
	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'\x0027', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-2 && vstr[w+1] == L' ' && vstr[w+2] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
			vstr[w+2] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'"', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-2 && vstr[w+1] == L' ' && vstr[w+2] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
			vstr[w+2] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);

	w = 0;
	v_loc = 0;
	do {
		w = vstr.find (L'”', v_loc) ; 
		if (w != vstr.npos && w < vstr.length()-2 && vstr[w+1] == L' ' && vstr[w+2] == L'-') {
			vstr[w] = L',' ; 
			vstr[w+1] = L' ' ;
			vstr[w+2] = L' ' ;
		}
		v_loc += 1;
	} while (w != vstr.npos);
}
void VietVoice::VietnamienVoice::Process_2 (std::wstring &vstr) // N.T.T -> NTT
{	
		std::wstring v_AZVN = L"AÁÀẢÃẠĂẮẰẲÃẶÂẤẦẨẪẬEÉÈẺẼẸÊẾỀỂỄỆIÍÌỈĨỊUÚÙỦŨUỰỨÙỬỮỰOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢYÝỲỶỸỴBCDĐFGHJKLMNPQRSTVWXZ" ;
		std::wstring v_azvn = L"aáàảãạăắằẳẵặâấầẩẫậeéèẻẽẹêếềểễệiíìỉĩịuúùủũụưứừửữựoóòỏõọôốồổỗộơớờởỡợyýỳỷỹỵbcđfghjklmnpqrstvwxz" ;
		unsigned int w = 0;
		unsigned int v_loc = 0;
		do
		{
			w = vstr.find (L'.', v_loc); // VS
			if (w != vstr.npos) {
				if (w < vstr.length()-0)
					if (StringHelper::OneOfCharPresent(StringHelper::ToString(vstr[w+1]).c_str(),v_AZVN) == true) {
						vstr[w] = L' ';
						v_loc = w ;
					}
				if (w < vstr.length()-1)
					if (vstr[w+1] == L' ' && StringHelper::OneOfCharPresent(StringHelper::ToString(vstr[w+2]).c_str(),v_azvn) == true) {
						vstr[w] = L' ';
						v_loc = w ;
					}
			}
			v_loc += 1;
		} while (w != vstr.npos);

/*
		std::wstring v_AZVN = L"AÁÀẢÃẠĂẮẰẲÃẶÂẤẦẨẪẬEÉÈẺẼẸÊẾỀỂỄỆIÍÌỈĨỊUÚÙỦŨUỰỨÙỬỮỰOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢYÝỲỶỸỴBCDĐFGHJKLMNPQRSTVWXZ" ;
		std::wstring v_str ; 
		v_str = L"" ;
		bool v_change = false ;
		for (int i=0; i < str.length(); i++)
			if (StringHelper::OneOfCharPresent(StringHelper::ToString(str[i]).c_str(),v_AZVN) == true) // thêm F ngay 10-9-2012
				if (i < str.length()-2) {
					if ((str[i+1] == L'.' || str[i+1] == L'-') && (StringHelper::OneOfCharPresent(StringHelper::ToString(str[i+2]).c_str(),v_AZVN) == true)) {  // thêm F ngay 10-9-2012
						v_str = str.substr(0,i) + str.substr(i+2,str.length()-i-2) ;
						v_change = true ;
					}			
				}
if (v_change == true) str = v_str ;
*/

}
void VietVoice::VietnamienVoice::Process_3 (std::wstring &vstr) // (72%) -> _72%)
{	
	std::wstring v_AZVN = L"AÁÀẢÃẠĂẮẰẲÃẶÂẤẦẨẪẬEÉÈẺẼẸÊẾỀỂỄỆIÍÌỈĨỊUÚÙỦŨUỰỨÙỬỮỰOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢYÝỲỶỸỴBCDĐFGHJKLMNPQRSTVWXZ" ;
	for (unsigned int i = 0; i < vstr.length()-1; i++) {
		if (vstr[i] == L'(' && StringHelper::OneOfCharPresent(StringHelper::ToString(vstr[i+1]).c_str(),L"0123456789") == true)
			vstr[i] = L' ' ;

		if (i>0 && vstr[i-1] == L' ' && vstr[i] == L'(' && StringHelper::OneOfCharPresent(StringHelper::ToString(vstr[i+1]).c_str(),v_AZVN) == true) {
			vstr[i-1] = L',' ;
			vstr[i] = L' ' ;
		}
	}
}
void VietVoice::VietnamienVoice::Process_4 (std::wstring &vstr) // Time phrase
{	
	unsigned int v_loc = 0;
	unsigned int w = 0;
	do
		{
			w = vstr.find (L":", v_loc); // VS
			if (w != vstr.npos)
			{
				if ((w >0 && vstr[w - 1] >= L'0' && vstr[w - 1] <= L'9') && (w < vstr.size () - 1 && vstr[w + 1] >= L'0' && vstr[w + 1] <= L'9')) {vstr[w] = L'|' ; v_loc++ ;} // VT
				else v_loc++ ;
			}

		} while (w != vstr.npos);
}
void VietVoice::VietnamienVoice::Process_5 (std::wstring &vstr) // 18:55 -> 18 giờ 55 phút
{	
	std::wstring v_str = L"" ; 
	bool v_change = false ;
	for (unsigned int i = 0; i < vstr.length(); i++) 
		if (i>1 &&  i+2< vstr.length() && vstr[i] == L':') {
			if (vstr.substr(i-2,2) >= L"00" && vstr.substr(i-2,2) <= L"23" && vstr.substr(i+1,2) >= L"00" && vstr.substr(i+1,2) <= L"59") {
				v_change = true ;
				v_str=vstr.substr(0, i) ;
				v_str += L" giờ " ;
				v_str += vstr.substr(i+1,2) ;             //18:55
				v_str += L" phút" ;
				v_str += vstr.substr(i+3,vstr.length()-i-1-2) ;             //18:55.   l=6 i=2
			}
		}
	if (v_change) vstr = v_str ;
}
void VietVoice::VietnamienVoice::Process_6 (std::wstring &vstr) // ngày 19-9 - Ảnh do SCG cung cấp
{	
	unsigned int v_loc = 0;
	unsigned int w = 0;
	do
		{
			w = vstr.find (L"-", v_loc); // VS
			if (w != vstr.npos)
			{
				if (w >0 && vstr[w - 1] == L' ' && w < vstr.size () - 1 && vstr[w + 1] == L' ') {
					vstr[w-1] = L',' ; 
					v_loc++ ;
				}
				else v_loc++ ;
			}

		} while (w != vstr.npos);
/*
	std::wstring v_str = L"" ;
	bool v_exist = false ;
	int v_loc1 = 0;
	for (int i=0; i < vstr.length()-1; i++)
		if (vstr[i] == L'\x000D' && vstr[i+1] == L'\x000A') {      // abc_0D0A12   length = 8  i = 4
			v_exist = true ;
			v_loc1 = i ;
			v_str = L'\x000D' ;
			v_str = v_str + L'\x000A' + vstr.substr(i+2, vstr.length()- i - 2) ;
			while (i > 0 && (vstr[i-1] == L' ' || vstr[i-1] == L'"')) {
				i -- ;
			}
			v_str = vstr.substr(0, i) + L'.' + v_str ;
			i = v_loc1 + 1;
			vstr = v_str ;
		}
	if (v_exist == true) vstr = v_str ;
	//unsigned int w = 0;
	//unsigned int v_loc = 0;

	for (int i=0; i < vstr.length(); i++)
		if (vstr[i] == L'\x000D') {
			if (i > 0 && vstr[i-1] != L'.') vstr[i]=L'.' ;
		}

	for (int i=0; i < vstr.length(); i++)
		if (vstr[i] == L'-') {
			if (i > 0 && vstr[i-1] == L' ') {vstr[i-1]=L',' ; vstr[i]=L' ' ;}
		}
*/
}
void VietVoice::VietnamienVoice::Process_7 (std::wstring &vstr) // " + ; , . !
{	
		unsigned int w;
		unsigned int v_loc = 0;
		unsigned char v_chr = L',' ;

		do
		{
			w= vstr.find (L'"', v_loc); // VS
			if (w != vstr.npos)
			{
				if (w > 0 && ((vstr[w - 1] == L';') || (vstr[w - 1] == L',') || (vstr[w - 1] == L'.') || (vstr[w - 1] == L'!')))
				{
					vstr[w] = v_chr ;
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != vstr.npos);

		v_loc = 0 ;
		do
		{
			w= vstr.find (L'"', v_loc); // VS
			if (w != vstr.npos)
			{
				if (w < vstr.length() && (vstr[w + 1] == L';' || vstr[w + 1] == L',' || vstr[w + 1] == L'.' || vstr[w + 1] == L'!'))
				{
					vstr[w] = vstr[w + 1];
					vstr[w+1] =  v_chr ;
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != vstr.npos);

		v_loc = 0 ;
		do
		{
			w= vstr.find (L'\x201C', v_loc); // VS L"“"
			if (w != vstr.npos)
			{
				if (w >0 && ((vstr[w - 1] == L';') || (vstr[w - 1] == L',') || (vstr[w - 1] == L'.') || (vstr[w - 1] == L'!')))
				{
					vstr[w] =  v_chr ;
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != vstr.npos);


		v_loc = 0 ;
		do
		{
			w= vstr.find (L'\x201D', v_loc); // VS L"”"
			if (w != vstr.npos)
			{
				if (w < vstr.length() && ((vstr[w + 1] == L';') || (vstr[w + 1] == L',') || (vstr[w + 1] == L'.') || (vstr[w + 1] == L'!')))
				{
					vstr[w] = vstr[w + 1] ;
					vstr[w+1] =  v_chr ;
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != vstr.npos);
}
void VietVoice::VietnamienVoice::Process_8 (std::wstring &vstr) // 
{	
		unsigned int w = 0;
		unsigned int v_loc = 0;
		do
		{
//			w= vstr.find (L"-", 0); VX
			w= vstr.find (L"-", v_loc); // VS

			if (w != vstr.npos)
			{
				if ((w > 0 && vstr[w - 1] == L' '))
				{
					vstr[w-1] = L',';
					vstr[w] = L' ';
				}
				else if ((w < vstr.size () - 1 && vstr[w + 1] == L' '))
					vstr[w] = L',';
				else if (w < vstr.size () - 1 && vstr[w + 1] >= L'0' && vstr[w+1] <= L'9') {vstr[w] = L'-' ; v_loc = w+1 ;} // VT
				else
					vstr[w] = L' ';
		
			}

		} while (w != vstr.npos);
}
void VietVoice::VietnamienVoice::Process_9 (std::wstring &vstr) // Số La Mã
{	
	unsigned int v_loc = 0;
	unsigned int w = 0;
	do
		{
			w= vstr.find (L".", v_loc); // VS
			if (w != vstr.npos)
			{
				if (w >0 && (vstr[w-1] == L'I' || vstr[w-1] == L'V' || vstr[w-1] == L'X' )) vstr[w] = L'/' ; // VT
				else v_loc++ ;
			}

		} while (w != vstr.npos);
}
void VietVoice::VietnamienVoice::Process_0 (std::wstring &vstr, bool v_spk) // Thieu dau . cuoi cau khi save wave
{
	unsigned int w = 0;
	unsigned int v_loc = 0;
	do {
		w = vstr.find (L'\x000A', v_loc); // VS
		if (w != vstr.npos && w > 0) {
			vstr.insert(w, L".") ; 
			v_loc = w +1 ;
		}
		v_loc += 1;
	} while (w != vstr.npos);
	vstr += L'\x000D' ;

	if (v_spk == true) {
		w = 0;
		v_loc = 0;
		do {
			w = vstr.find (L'(', v_loc); // VS
			if (w != vstr.npos && w > 0) {
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				vstr.insert(w, L" _silence_ ") ; 
				v_loc = w + 11 * 7 ;
			}
			v_loc += 1;
		} while (w != vstr.npos);
	}
}
/**
* Read text with male voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseMaleVoice ()
{
	_volume = 50.0f; // 0.35f;
	_curVoice = VietVoice::ToBeSynthetise::MALE;
	//_wordSelector.SetDatabase (_currentDirectory + L"//database//maleVoice//6Accents_Unit.bin", _currentDirectory + L"//database//maleVoice//2Accents_Unit.bin", _currentDirectory + L"//database//maleVoice//Special_Unit.idx", _currentDirectory + L"//database//maleVoice//Base_Unit.bin", _currentDirectory + L"//database//maleVoice//Special_Unit.bin", _currentDirectory + L"//extraWords.bin");
	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//maleVoice//NormalWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//maleVoice//NormalWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//maleVoice//NormalWord//Special_Unit.idx",
		_currentDirectory + L"//database//maleVoice//NormalWord//Base_Unit.bin",
		_currentDirectory + L"//database//maleVoice//NormalWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		 _currentDirectory + L"//database//maleVoice//NormalWord//Control//",1);

	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//maleVoice//ShortWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//maleVoice//ShortWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//maleVoice//ShortWord//Special_Unit.idx",
		_currentDirectory + L"//database//maleVoice//ShortWord//Base_Unit.bin",
		_currentDirectory + L"//database//maleVoice//ShortWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//maleVoice//NormalWord//Control//" ,2);

	_wordSelector.SetDatabaseVeryShortWord(
			_currentDirectory + L"//database//maleVoice//VeryShortWord//VeryShort_Word.idx",
			_currentDirectory + L"//database//maleVoice//VeryShortWord//VeryShort_Word.bin",
			_currentDirectory + L"//VeryShortWord.txt",_currentDirectory + L"//database//maleVoice//VeryShortWord//Control//"

		);
	
	return S_OK;
}

/**
* Read text with female voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseFemaleVoice ()
{
	_volume = 180.0f; // 0.35f;
	_curVoice = VietVoice::ToBeSynthetise::FEMALE;
	
	std::wstring GiongDoc=L"";
	std::wstring DienGiai=L"";
	long GiongHienTai=0;

	File::Reader reader (L"lsAccent.bin");
	long SoGiongDoc = reader.ReadLong ();
	//::MessageBox(NULL, StringHelper::ToString(_speed).c_str(), L"Volume | Volume too high...", MB_OK); // VT
	for(int i=0;i<SoGiongDoc;i++)
	{
		GiongDoc=reader.ReadWString();
		DienGiai=reader.ReadWString();
		GiongHienTai=reader.ReadLong();
		
		//if(GiongHienTai>0)
		if(i==_speed)
		{
			break;
		}
	}

	_accentFinder.PrepareExtraWords(_currentDirectory + L"//database//"+GiongDoc+L"//extraWords.bin");

	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//Special_Unit.idx",
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//Base_Unit.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//NormalWord//Control//",1);

	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//Special_Unit.idx",
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//Base_Unit.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//"+GiongDoc+L"//ShortWord//Control//",2);

	_wordSelector.SetDatabaseVeryShortWord(
			_currentDirectory + L"//database//"+GiongDoc+L"//VeryShortWord//VeryShort_Word.idx",
			_currentDirectory + L"//database//"+GiongDoc+L"//VeryShortWord//VeryShort_Word.bin",
			_currentDirectory + L"//VeryShortWord.txt",
			_currentDirectory + L"//database//"+GiongDoc+L"//VeryShortWord//Control//"

		);

	return S_OK;
}


HRESULT _stdcall VietVoice::VietnamienVoice::UseFemaleVoice_2 ()
{
	_volume = 180.0f; // 0.35f;
	_curVoice = VietVoice::ToBeSynthetise::FEMALE;
	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//femaleVoice//NormalWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//femaleVoice//NormalWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//femaleVoice//NormalWord//Special_Unit.idx",
		_currentDirectory + L"//database//femaleVoice//NormalWord//Base_Unit.bin",
		_currentDirectory + L"//database//femaleVoice//NormalWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//femaleVoice//NormalWord//Control//",1);

	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//femaleVoice//ShortWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//femaleVoice//ShortWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//femaleVoice//ShortWord//Special_Unit.idx",
		_currentDirectory + L"//database//femaleVoice//ShortWord//Base_Unit.bin",
		_currentDirectory + L"//database//femaleVoice//ShortWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//femaleVoice//ShortWord//Control//",2);

	_wordSelector.SetDatabaseVeryShortWord(
			_currentDirectory + L"//database//femaleVoice//VeryShortWord//VeryShort_Word.idx",
			_currentDirectory + L"//database//femaleVoice//VeryShortWord//VeryShort_Word.bin",
			_currentDirectory + L"//VeryShortWord.txt",
			_currentDirectory + L"//database//femaleVoice//VeryShortWord//Control//"

		);

	return S_OK;
}


/**
* Read text with customer voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseCustomerVoice ()
{
	_volume = 150.0f; // 0.35f;
	_curVoice = VietVoice::ToBeSynthetise::CUSTOM;
//	_wordSelector.SetDatabase (_currentDirectory + L"//database//customer//6Accents_Unit.bin", _currentDirectory + L"//database//customer//2Accents_Unit.bin", _currentDirectory + L"//database//customer//Special_Unit.idx", _currentDirectory + L"//database//customer//Base_Unit.bin", _currentDirectory + L"//database//customer//Special_Unit.bin", _currentDirectory + L"//extraWords.bin");

		_wordSelector.SetDatabase (
		_currentDirectory + L"//database//customer//NormalWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//customer//NormalWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//customer//NormalWord//Special_Unit.idx",
		_currentDirectory + L"//database//customer//NormalWord//Base_Unit.bin",
		_currentDirectory + L"//database//customer//NormalWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//customer//NormalWord//Control//", 1);

	_wordSelector.SetDatabase (
		_currentDirectory + L"//database//customer//ShortWord//6Accents_Unit.bin",
		_currentDirectory + L"//database//customer//ShortWord//2Accents_Unit.bin", 
		_currentDirectory + L"//database//customer//ShortWord//Special_Unit.idx",
		_currentDirectory + L"//database//customer//ShortWord//Base_Unit.bin",
		_currentDirectory + L"//database//customer//ShortWord//Special_Unit.bin", 
		_currentDirectory + L"//extraWords.bin",
		_currentDirectory + L"//database//customer//ShortWord//Silence", 2);

	_wordSelector.SetDatabaseVeryShortWord(
			_currentDirectory + L"//database//customer//VeryShortWord//VeryShort_Word.idx",
			_currentDirectory + L"//database//customer//VeryShortWord//VeryShort_Word.bin",
			_currentDirectory + L"//VeryShortWord.txt",
			_currentDirectory + L"//database//customer//VeryShortWord//Control//"
		);

	return S_OK;
}



/**
* Specify which database to use based on the given path
*
* Paramters:
*
* std::wstring path:  Path to the database
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetDatabase (std::wstring path)
{
	_wordSelector.SetDatabase (
		path + L"//NormalWord//6Accents_Unit.bin", 
		path + L"//NormalWord//2Accents_Unit.bin", 
		path + L"//NormalWord//Special_Unit.idx", 
		path + L"//NormalWord//Base_Unit.bin", 
		path + L"//NormalWord//Special_Unit.bin", 
		L"extraWords.bin",
		path + L"//NormalWord//Silence", 1);
	_wordSelector.SetDatabase (
		path + L"//ShortWord//6Accents_Unit.bin", 
		path + L"//ShortWord//2Accents_Unit.bin", 
		path + L"//ShortWord//Special_Unit.idx", 
		path + L"//ShortWord//Base_Unit.bin",
		path + L"//ShortWord//Special_Unit.bin",
			path + L"//ShortWord//Silence",L"extraWords.bin",2);
	_wordSelector.SetDatabaseVeryShortWord(
		path + L"//VeryShortWord//VeryShort_Word.idx", 
		path + L"//VeryShortWord//VeryShort_Word.bin",
		L"VeryShortWord.txt",
			path + L"//VeryShortWord//Control//"
		);

	return S_OK;
}

/**
* Specify which database to use based on the given path
*
* Paramters:
*
* std::wstring path:  Path to the database
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetDatabase (std::wstring pathDatabase, std::wstring pathConfig)
{

	_vectorAccent.Load (pathConfig + L"\\vecteurAccent.bin");

	_tokensToWords.LoadAbbreviationFile (pathConfig + L"\\abbreviations.bin");
	_tokensToWords.LoadNoSilenceWords (pathConfig + L"\\noSilence.bin");

	_accentFinder.PrepareExtraWords (pathConfig + L"\\extraWords.bin");

	_wordSelector.LoadParam (pathConfig + L"\\syllable.bin", pathConfig + L"\\syllable2.bin", pathConfig + L"\\consonne1.bin", pathConfig +L"\\consonne2.bin");

	//_wordSelector.SetDatabase (pathDatabase + L"//6Accents_Unit.bin", pathDatabase + L"//2Accents_Unit.bin", pathDatabase + L"//Special_Unit.idx", pathDatabase + L"//Base_Unit.bin", pathDatabase + L"//Special_Unit.bin", pathConfig + L"\\extraWords.bin");
	
	_wordSelector.SetDatabase (
		pathDatabase + L"//NormalWord//6Accents_Unit.bin", 
		pathDatabase + L"//NormalWord//2Accents_Unit.bin", 
		pathDatabase + L"//NormalWord//Special_Unit.idx", 
		pathDatabase + L"//NormalWord//Base_Unit.bin", 
		pathDatabase + L"//NormalWord//Special_Unit.bin", 
		L"extraWords.bin",
		pathDatabase + L"//NormalWord//Control//", 1);
	_wordSelector.SetDatabase (
		pathDatabase + L"//ShortWord//6Accents_Unit.bin", 
		pathDatabase + L"//ShortWord//2Accents_Unit.bin", 
		pathDatabase + L"//ShortWord//Special_Unit.idx", 
		pathDatabase + L"//ShortWord//Base_Unit.bin",
		pathDatabase + L"//ShortWord//Special_Unit.bin",
		L"extraWords.bin",
		pathDatabase + L"//NormalWord//Control//", 2);
	_wordSelector.SetDatabaseVeryShortWord(
		pathDatabase + L"//VeryShortWord//VeryShort_Word.idx", 
		pathDatabase + L"//VeryShortWord//VeryShort_Word.bin",
		L"VeryShortWord.txt",
		pathDatabase + L"//VeryShortWord//Control//"
		);

	
	
	return S_OK;
}

/**
* Request for an interface of a certain type.
* 
* Return value:  A value indicating wether there was an error or not
*
* Parameters:  
*
* const IDD& idd:  Id representing the type of the interface.
* void **ppv:  Will point on the interface.
*/
HRESULT _stdcall VietVoice::VietnamienVoice::QueryInterface (const IID& idd, void **ppv)
{
	
	if (idd == IID_IUnknown) // User asked for IUnknown interface
	{
		*ppv = static_cast <IUnknown *> (this);
	}
	else if (idd == IID_IVietVoice) // User asked for IVietVoice interface
	{
		*ppv = static_cast <IVietnameseVoice *> (this);
	}
	else // User asked for non existing interface for this object
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef() ; // Increment the reference count

	return S_OK;
}

/**
* A new reference to the object in used, increase reference count.
* 
* Return value:  A value indicating wether there was an error or not
*/
ULONG _stdcall VietVoice::VietnamienVoice::AddRef ()
{
	return ::InterlockedIncrement (&_ref);
}

/**
* One of the reference to the object is not used anymore, decrease reference count.
* 
* Return value:  A value indicating wether there was an error or not
*/
ULONG _stdcall VietVoice::VietnamienVoice::Release ()
{
	if (InterlockedDecrement (&_ref) == 0) // Decrement the ref count and if it is 0, delete the object (no more used)
	{
		delete this; //// [[]]
		return 0;
	}

	return _ref;
}



/**
* Special function used by the SpeakAsync method of the VietVoice::VietnamienVoice object
* as a thread that simply call the Speak method to read the text.  Since it is another thread,
* the program is not blocked.
*/
unsigned int WINAPI VietVoice::ThreadSpeak (void * param)
{
	VietVoice::ThreadSpeakParam * voiceParam = (VietVoice::ThreadSpeakParam *) param;

	voiceParam->_voice->Speak (voiceParam->_str, voiceParam->_pos); // speak the text

	delete voiceParam; // Delete the parameter to avoid a memory leak.
	// free (voiceParam); // Them va xoa 7-12-2012 khong ep phe
	//// voiceParam = NULL ; // Them va xoa 7-12-2012 khong ep phe
	return 0;
}

/**
* The voice can have access to a called back interface used to report various things.
* This method is used to set the said callback interface.
*
* Return Value:  A value indicating wether there was an error or not
* 
* Parameters:
*
* IVietnameseVoiceCallback * callback:  The new callback interface
* 
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetCallback (VietVoice::IVietnameseVoiceCallback * callback)
{
	_callback = callback;
	return S_OK;
}

/* The voice speak in "element".  An element can be a word, sentence or phrase
*
* Return Value:  A value indicating wether there was an error or not
* 
* Parameters:
*
* Element elem:  The new type of element (word, sentence, phrase).
* 
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetElement (VietVoice::Element elem)
{
	_element = elem;

	// Modify the _tokenizer so it use the correct element as token
	switch (elem)
	{
		case VietVoice::WORD:
		{
			_tokenizer.SetSpace ( L"");
			_tokenizer.SetPunctuation (L".,:;?!\n…()[]{} \t\r\"\"“”<>«»'");
			break;
		}
		case VietVoice::SENTENCE:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'\n…()[]{},");
			_tokenizer.SetPunctuation (L".:;?!");
			break;
		}
		case VietVoice::PHRASE:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'");
			_tokenizer.SetPunctuation (L".,:;?!\n…()[]{}");
			break;
		}
		case VietVoice::PARAGRAPH:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'.,:;?!…()[]{}");
			_tokenizer.SetPunctuation (L"\n");
			break;
		  }


	}
	return S_OK;
}

/**
* Obtains the minimal length for a word
* @return
*/
HRESULT __stdcall GetDurMoyenSilence (double & dur)
{
	return S_OK;

}


HRESULT __stdcall  VietVoice::VietnamienVoice::Init (HWND hwnd)
{
	
	return S_OK;
}

HRESULT __stdcall VietVoice::VietnamienVoice::IsSpeaking (bool & isSpeaking)
{
	::EnterCriticalSection (&_critical);
	isSpeaking = _isSpeaking;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}

HRESULT __stdcall VietVoice::VietnamienVoice::SetSpeed (float speed)
{
	::EnterCriticalSection (&_critical);
	_speed = speed;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::SetVolume (float volume)
{
	::EnterCriticalSection (&_critical);
	_volume += volume;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::SetPitch (float pitch)
{
	::EnterCriticalSection (&_critical);
	_pitch += pitch;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::SetWsola (bool wsola)
{
	::EnterCriticalSection (&_critical);
	_wsola = wsola;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::GetSpeed (float & speed)
{
	::EnterCriticalSection (&_critical);
	speed = _speed ;
	// speed = _durMoyenSilence ;
	::LeaveCriticalSection (&_critical);
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::GetVolume (float & volume)
{
	::EnterCriticalSection (&_critical);
	volume = _volume ;
	::LeaveCriticalSection (&_critical);
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::GetPitch (float & pitch)
{
	::EnterCriticalSection (&_critical);
	pitch = _pitch ;
	// ::MessageBox(NULL, StringHelper::ToString(speed).c_str(), L"NULL", MB_OK);
	::LeaveCriticalSection (&_critical);
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::GetWsola (bool & wsola)
{
	::EnterCriticalSection (&_critical);
	wsola = _wsola ;
	// ::MessageBox(NULL, StringHelper::ToString(speed).c_str(), L"NULL", MB_OK);
	::LeaveCriticalSection (&_critical);
	return S_OK;
}
HRESULT __stdcall VietVoice::VietnamienVoice::IsS (bool & isSpeaking)
{
	return S_OK;
}

/*
					if (token.GetPostPunctuation() == L":") {
						std::wstring v_tok = token.GetToken().c_str();
						bool v_tok_num = true ;
						std::wstring v_num = L"0123456789" ;
						for (unsigned int i = 0; i < v_tok.length(); i++) 
							if (v_num.find(v_tok[i],0) == string::npos) v_tok_num = false ;
						if (v_tok_num == false) {
							for (unsigned int i=0; i < (v_bs1 / 26.122) / 2 * 3; i++) // length of silence.wav = 26.22 // * 2
							{
								token.SetToken (L"_silence_") ;
								toSynthetise.AppendToken (token) ;
							}
						} else {
							toSynthetise.AppendToken (token) ;
							token.SetToken (L":") ;
							toSynthetise.AppendToken (token) ;
						}
					} else
*/