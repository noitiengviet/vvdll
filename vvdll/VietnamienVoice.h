#if !defined (VIETNAMIENVOICE_H)

#define VIETNAMIENVOICE_H


#include <iostream>
#include <string>
#include <list>
#include "vvinterfaces.h"
#include "token.h"
#include "tokenizer.h"
#include "tokenstowords.h"
#include "wordmodifier.h"
#include "vectoraccent.h"
#include "accentfinder.h"
#include "silenceadder.h"
#include "wordselector.h"
#include "concatenator.h"

#pragma warning( disable : 4786 )
 
namespace VietVoice
{
	/**
	 * Class Name:         VietnamienVoice
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class representing the vietnamese voice speaking
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * september 20 2005:  Started convertion from java to C++
	 * september 21 2005:  Implemented the speak and treatToBeSpoken methods
	 * November 29 2005 -> Added destructor in case
	 * novembre 29 2005:  Implemented the copy constructor and the = operator
	 * February 4 2006:  Modified the class to use unicode file names.
	 * February 7 2006:  added the ReloadAbbreviation method.
	 * March 6 2006:  Seperated implementation in source and header
	 * March 14 2006 -> Made VietnamienVoice inherit from IVietnameseVoice, modified the methods to u
	 * inherited by IVietnameseVoice, implemented an empty stop method, and modify error treatment of Speak (return E_FAIL).
	 * also implemented AddRef, Release, and QueryInterface method from IUnknown.  Also implemented the CreateInstance method.
	 * March 18 2006 -> Added the SpeakAsync method, as well as the ThreadSpeak function and ThreadSpeakParam structure
	 *					 in order to implement SpeakAsync.
	 * March 22 2006 -> Added the SetElement, SetCallback, and Stop method.
	 * Mai 20 2006 ->  Modified speak method so it could start reading the text at a different possiton.
	 * Mai 25 2006 -> Tried to improved the SetElement method\
	 * June 10 2006: Change the methods name so they start by a capital letter.
	 * September 11 2006 -> Added the SaveMP3 method
	 * September 28 2006 -> Created empty UseMaleVoice, etc methods, still needs to be implemented.
	 * September 29 2007 -> Modified method to set the database
	 */
	class VietnamienVoice : public  IVietnameseVoice
	{

	private:
		
		ToBeSynthetise::VOICESEL _curVoice;
		CRITICAL_SECTION _critical;
		bool _isSpeaking; // Used to determine if the voice is currently speaking;
	public:

		VietnamienVoice (const std::wstring path);

		~VietnamienVoice ();

		VietnamienVoice  (const VietnamienVoice & voice);

		void operator = (const VietnamienVoice & voice);

		/**
		* Reload the abbreviation file.  useful when there is a change in the abbreviations.
		*/
		HRESULT __stdcall ReloadAbbreviations ();

		/**
		* Read a string of text
		* @param str The string to be read
		*/
		HRESULT __stdcall Speak (std::wstring str, int pos = 0);

		/**
		* Save the spoken string of text in a mp3
		* @param str The string to be read
		* std::wstring filename:  The name of the mp3
		*/
		HRESULT __stdcall SaveMp3 (std::wstring str, std::wstring filename);

		/**
		* Read a string of text asynchronously (in another thread).
		* @param:  str The string to be read.
		* @param pos Position from which we start reading
		*/
		HRESULT __stdcall SpeakAsync (std::wstring str, int pos = 0);

		/**
		* Stop the reading of the text.
		*/
		HRESULT __stdcall Stop ();
   
		/**
		* Sets the minimal length for a word
		* @param dur
		*/
		HRESULT __stdcall SetDurMoyenSilence (float dur);
		//HRESULT __stdcall Set_BS (long vbs);

		/**
		* Request for an interface of a certain type.
		* 
		* Return value:  A value indicating wether there was an error or not
		*
		* Parameters:  
		*
		* const IDD& idd:  Id representing the type of the interface.
		* void **ppv:  Will point on the interface.
		*/
		HRESULT _stdcall QueryInterface (const IID & idd, void **ppv); 

		/**
		* A new reference to the object in used, increase reference count.
		* 
		* Return value:  A value indicating wether there was an error or not
		*/
		ULONG _stdcall AddRef ();

		/**
		* One of the reference to the object is not used anymore, decrease reference count.
		* 
		* Return value:  A value indicating wether there was an error or not
		*/
		ULONG _stdcall Release ();

		/**
		* The voice can have access to a called back interface used to report various things.
		* This method is used to set the said callback interface.
		*
		* Return Value:  A value indicating wether there was an error or not
		* 
		* Parameters:
		*
		* IVietnameseVoiceCallback * callback:  The new callback interface
		* 
		*/
		HRESULT __stdcall SetCallback (IVietnameseVoiceCallback * callback);

		/**
		* The voice speak in "element".  An element can be a word, sentence or phrase
		*
		* Return Value:  A value indicating wether there was an error or not
		* 
		* Parameters:
		*
		* Element elem:  The new type of element (word, sentence, phrase).
		* 
		*/
		HRESULT __stdcall SetElement (Element elem);

		/**
		* Obtains the minimal length for a word
		* @return
		*/
		HRESULT __stdcall GetDurMoyenSilence (double & dur);
		HRESULT __stdcall Get_BS (long & vbs);

		/**
		* Read text with male voice
		*/
		HRESULT __stdcall UseMaleVoice ();

		/**
		* Read text with female voice
		*/
		HRESULT __stdcall UseFemaleVoice ();

		HRESULT __stdcall UseFemaleVoice_2 ();

		/**
		* Read text with customer voice
		*/
		HRESULT __stdcall UseCustomerVoice ();

		/**
		* Specify which database to use based on the given path
		*
		* Paramters:
		*
		* std::wstring path:  Path to the database
		*/
		HRESULT __stdcall SetDatabase (std::wstring path);

		HRESULT __stdcall SetDatabase (std::wstring pathDatabase, std::wstring pathConfig);
		HRESULT __stdcall Init (HWND hwnd);
		HRESULT __stdcall IsSpeaking (bool & isSpeaking);
		HRESULT __stdcall SetSpeed (float speed);
		HRESULT __stdcall SetVolume (float volume);
		HRESULT __stdcall SetPitch (float pitch);
		HRESULT __stdcall SetWsola (bool wsola);
		HRESULT __stdcall GetSpeed (float & speed);
		HRESULT __stdcall GetVolume (float & volume);
		HRESULT __stdcall GetPitch (float & pitch);
		HRESULT __stdcall GetWsola (bool & wsola);
HRESULT __stdcall VietVoice::VietnamienVoice::IsS (bool & isSpeaking);
	private:

		/**
		* Initialize the list of treatements
		*/
		void InitTreatements ();

		/**	
		* Iterrates throught the list of Treatement object and each one does it task
		* @param toSpoke
		*/
		void TreatToBeSpoken (ToBeSynthetise toSynthetise);
		void Process_0 (std::wstring &vstr, bool v_spk); // VT 12-9-2012
		void Process_1 (std::wstring &vstr); // VT 11-9-2012
		void Process_2 (std::wstring &vstr); // VT 11-9-2012
		void Process_3 (std::wstring &vstr); // VT 11-9-2012
		void Process_4 (std::wstring &vstr); // VT 11-9-2012
		void Process_5 (std::wstring &vstr); // VT 11-9-2012
		void Process_6 (std::wstring &vstr); // VT 11-9-2012
		void Process_7 (std::wstring &vstr); // VT 11-9-2012
		void Process_8 (std::wstring &vstr); // VT 11-9-2012
		void Process_9 (std::wstring &vstr); // VT 11-9-2012

	private:

		std::list <Treatement *> _listTreatement; // List of object use to "read" text
	    float _durMoyenSilence;
	    float _speed;
	    float _volume;
	    float _pitch;
		bool _wsola;
	    // double _durMoyenSilence;
		long _vbs; // VT
		long v_bs1; // VT
		long v_bs2; // VT
		std::wstring v_lw ; // VT
		std::wstring v_ad ; // VT
		TokensToWords _tokensToWords;
		VectorAccent _vectorAccent;
		AccentFinder _accentFinder;
		WordModifier _wordModifier;
		SilenceAdder _silenceAdder;
		WordSelector _wordSelector;
		Concatenator _concatenator;
		Tokenizer _tokenizer; // Used to divide the text into token (word, sentence, etc)
		Element _element;
		IVietnameseVoiceCallback * _callback;
		bool _mustStop; // Used to implement the Stop method
		long _ref;

		std::wstring _pathDb;
		std::wstring _currentDirectory;
	} ;



	/**
	* Special structure used to contain the parameters of the ThreadSpeak thread.
	*/
	struct ThreadSpeakParam 
	{
		VietnamienVoice * _voice;
		std::wstring _str;
		int _pos;
	};

	/**
	* Special function used by the SpeakAsync method of the VietVoice::VietnamienVoice object
	* as a thread that simply call the Speak method to read the text.  Since it is another thread,
	* the program is not blocked.
	*/
	unsigned int WINAPI ThreadSpeak (void * param);
}

#endif