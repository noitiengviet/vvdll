
#include "vectoraccent.h"

/**
* Constructor
*/
VietVoice::VectorAccent::VectorAccent ()
{}

/**
*
* Constructor Create the map (accent map) from a file
* @param name of the file
*/
VietVoice::VectorAccent::VectorAccent (std::wstring  name) 
{
	Load (name);
}

VietVoice::VectorAccent::~VectorAccent ()
{}

VietVoice::VectorAccent::VectorAccent  (const VectorAccent & vectorAccent)
{
	if (this != &vectorAccent)
	{
		 _accentMap = vectorAccent._accentMap;
	}
}

void VietVoice::VectorAccent::operator = (const VectorAccent & vectorAccent)
{
	if (this != &vectorAccent)
	{
		_accentMap = vectorAccent._accentMap;
	}		
}

/**
*
* Create the map (accent map) from a file
* @param name of the file
*/
void VietVoice::VectorAccent::Load (std::wstring filename)
{
	try
	{
		File::Reader reader (filename);
		std::wstring line;

		long count = reader.ReadLong (); // Read the number of lines in the file


		for (int i = 0; i < count; i++)  // For each line
		{
			line = reader.ReadWString ();  // Read the next line

			if (line != L"")
			{				 
				//  Divide de line in word
				std::vector <std::wstring> tokens;
				StringHelper::Tokenize (tokens, line, L" \t\n");				//StringTokenizer tokenizer (line, L" \t");

				std::wstring key = tokens[0];
	
				std::wstring replaceWith = tokens[2];

				Accent accent (i % 6, replaceWith); // Create an object containing the information on the accent

				std::pair< std::wstring, Accent > tmpPair (key, accent);

				_accentMap.insert(tmpPair);
			}
		}
	}
	catch (File::Exception ex) // Something wrong happened
	{
		throw VietnameseTTException(L"Something went wrong while reading the abbreviation file in TokenToWords" + ex.GetMsg ());
	}
}

/**
* Determines if there is an entry in the Map object for a key in particular
* @param key The key for witch we wish to know if there is an entry in the Map
*/
bool VietVoice::VectorAccent::EntryExist (std::wstring key)
{
	return _accentMap.find (key) != _accentMap.end ();
}

/**
* Obtain a numerical value representing the accent of a character
* @param key The character
* @return A numerical value representing the accent.
*/
int VietVoice::VectorAccent::GetAccent (std::wstring key)
{
	VietVoice::Accent accent = _accentMap [key];
	return  accent.GetAccent () ;
}

/**
 * Obtain the information on the accent of a character in particular
 * @param key The character
 * @return The information about the accent
 */
VietVoice::Accent VietVoice::VectorAccent::GetAccentInfo (std::wstring key)
{
	return _accentMap [key];
}

/**
* Takes a character with an accent and return the "same" character without an accent.
* @param key Le caract�re vietnamien
* @return The character without an accent.
*/
std::wstring VietVoice::VectorAccent::GetReplacement(std::wstring key)
{
	Accent accent = _accentMap [key];
	return accent.GetReplaceWith () ;
}