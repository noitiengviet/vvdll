#if !defined (FILEIO_H)

	#define FILEIO_H

	#include <fstream>
	#include <wchar.h>
	#include <vector>
	#include <windows.h>
	#include "stringhelper.h"

	using std::ios_base;

	namespace File
	{

		/**
		 * February 4 2006:  Updated the file function to use windows file functions.
		 * February 15 2006:  Changed winunicudehelper.h and useunicode by stdafx.h
		 * March 4 2006:  Seperated implementation in source and header
		 * June 9 2006: Change the methods name so they start by a capital letter.
		 */

		/**
		* Exception that is raised when an IO exception occurs
		*/
		class Exception
		{
		public:

			/**
			* Constructor
			*
			* Parameters: 
			*
			* std::wstring const & msg:  Error message
			*/
			Exception (std::wstring const & msg);
		
			/**
			* Obtain the error message of the exception
			*
			* Return value:  The error message
			*/
			std::wstring GetMsg() const;

		private:

			std::wstring _msg;
		};

		/**
		* Class used to read a file
		*/
		class Reader
		{
		public:

			/**
			* Constructor
			*
			* Parameter:
			*
			* std::wstring const & filename:  Name of the file
			*/
			Reader (std::wstring const & filename);

			/**
			* Destructor
			*/
			~Reader ();

				//---------------------------------------------------------------
				// Read an array of bytes from a file
				//---------------------------------------------------------------
				void ReadUnsignedCharArray (
					unsigned char * a_array, // will points on the array of bytes
					int a_size // The number of bytes to read
				);

			/**
			* Skips a certain number of bytes
			*
			* Parameters:
			*
			* long nb: The number of bytes to skip
			*/
			void SkipBytes (long nb);

			/**
			* Read a little endian long value from a file
			*
			* Return value:  The value that was read
			*/
			long ReadLong ();

			/**
			* Read a little endian short value from a file
			*
			* Return value:  The value that was read
			*/
			short ReadShort ();

			/**
			* Read a big endian value from a file
			*
			* Return value:  The value that was read
			*/
			unsigned long ReadLongJava ();
			unsigned short ReadShortJava ();

			/**
			* Read a big endian float value from a file
			*
			* Return value:  The value that was read
			*/
			float ReadFloatJava ();

			/**
			* Read a unicode string from a file
			*
			* Return value: The string that was read
			*/
			std::wstring ReadWString ();

			/**
			* Read an array of bytes
			*
			* Parameter:
			*
			* unsigned char * a:  will points on the array of bytes
			* int size:  size:  Nb. of bytes to read
			*/
			void ReadUnsignedCharArrayJava (unsigned char * a, int size);

			unsigned char ReadByte ();

							// Obtains the size of the file
				unsigned long GetFileSize();

				//---------------------------------------------------------
				// Reads a unicode text file and returns a string 
				// containing the content of the files.  The method
				// is far from perfect, it only works on UTF-16 file in
				// little endian order, but this is enough for the VV dll
				//---------------------------------------------------------
				static std::wstring ReadTextFile(
					std::wstring a_filename // Name of the text file to read
				);

				//---------------------------------------------------------
				// Reads a unicode text file and returns a vector containing
				// the words from the file.  The method
				// is far from perfect, it only works on UTF-16 file in
				// little endian order, but this is enough for the VV dll
				//---------------------------------------------------------
				static void ReadTextFileWords(
					std::vector< std::wstring > & a_words,
					std::wstring a_filename, // Name of the text file to read
					std::wstring a_wordDelimiters = std::wstring( L" \t\r\n" ) // Delemiters used to separate the words
				)
					{
					std::wstring content = ReadTextFile( a_filename );					
					VietVoice::StringHelper::Tokenize( a_words, content , a_wordDelimiters );
					}

				//---------------------------------------------------------
				// Reads a unicode text file and returns a vector containing
				// the lines from the file.  The method
				// is far from perfect, it only works on UTF-16 file in
				// little endian order, but this is enough for the VV dll
				//---------------------------------------------------------
				static void ReadTextFileLines(
					std::vector< std::wstring > & a_lines,
					std::wstring a_filename, // Name of the text file to read
					std::wstring a_wordDelimiters = std::wstring (L"\r\n") // Delemiters used to separate the words
				)
					{
					ReadTextFileWords( a_lines, a_filename, a_wordDelimiters );
					}

		protected:

			HANDLE _file; // Handle of the file
		};

		/**
		* Class used to write a binary file
		*/
		class Saver
		{
		public:

			/**
			* Constructor
			*
			* Parameters:
			* 
			* std::wstring const & filename:  Name of the file to be written
			*/
			Saver (std::wstring const & filename);

			/**
			* Destructor
			**/
			~Saver ();

			/**
			* Save an mp3
			*
			* Parameters:
			*
			* unsigned char * c:  Points on an array of bytes containing the mp3 data
			* int size:  size of the array
			*/
			void SaveMP3 (unsigned char * c, int size);

			/**
			* Save a long value to the file
			*
			* Parameters:
			*
			* long l:  The long value to be saved
			*/
			void SaveLong (long l);

			void SaveFloat (float l);

			/**
			* Save a unicode string to the file
			*
			* Parameters:
			* 
			* std::wstring str:  The string to be saved
			*/
			void SaveWString (std::wstring str);

			void SaveByteArray (unsigned char * array, unsigned int size);

			void SaveShort (short s);

			void SaveUnsignedChar (unsigned char c);

		protected:

			HANDLE _file; // Handle of the file
		};
	}

#endif