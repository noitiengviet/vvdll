#if !defined (TOBESPOKENDATA_H)

#define TOBESPOKENDATA_H

#include "word.h"

#include <string>

#pragma warning( disable : 4786 ) 

namespace VietVoice
{	
	/**
	 * Class Name:         ToBeSpokenData
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class used to read the sound data of a word from the
	 *                     database and contain that data
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * september 20 2005:  Converted from java to C++
	 * september 21 2005:  Implemented the isEmpty and hasPostPunc methods
	 * novembre 5 2005:  Implemented the Destructor and the = operator
	 * March 4 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 17 2006:  Improved comments and spacing of .h and .cpp files.
	 */



	class ToBeSynthetiseData
	{
	public:

		enum WORD_TYPE {
			DATE,
			NUMBER,
			WORD,
			SYMBOL,
			ABBR_S, // VT 31-8-2012
			ABBR_L,// VT 10-9-2012,

			DATE_G, //Thuan Add 11-02-2020
			NUMBER_G,//Thuan Add 11-02-2020
			WORD_G,//Thuan Add 11-02-2020
			SYMBOL_G,//Thuan Add 11-02-2020
			ABBR_S_G, //Thuan Add 11-02-2020
			ABBR_L_G,//Thuan Add 11-02-2020

			DATE_GE, //Thuan Add 11-02-2020
			NUMBER_GE,//Thuan Add 11-02-2020
			WORD_GE,//Thuan Add 11-02-2020
			SYMBOL_GE,//Thuan Add 11-02-2020
			ABBR_S_GE, //Thuan Add 11-02-2020
			ABBR_L_GE,//Thuan Add 11-02-2020

			VSWORD//Thuan Add 11-02-2020
			};
		/**
		* Copy constructor
		*
		* Parameters:
		*
		* const ToBeSpokenData & data:  The object to be copied.
		*/
		ToBeSynthetiseData  (const ToBeSynthetiseData & data);

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const ToBeSynthetiseData & data);

		 /**
         * Constructor
         */
        ToBeSynthetiseData (WORD_TYPE data);

        /**
         * Constructior
		 *
		 * Parameters:
		 *
         * std::wstring word The word this data is related to
         */
        ToBeSynthetiseData (std::wstring word, WORD_TYPE data);

        /**
         * Constructor
		 *
		 * Parameters:
		 *
         * std::wstring word The word this data is related to
         * std::wstring prepunc The prepunc The punctiation before the word
         * std::wstring postpunc The punctuation after the word
         */
        ToBeSynthetiseData (std::wstring word, std::wstring prepunc, std::wstring postpunc, WORD_TYPE data);

		/**
		* Destructor
		*/
		~ToBeSynthetiseData ();

		float m_percentageAdjust;
		float m_volumeAdjust;
		WORD_TYPE m_wordType;
        /**
         * Changes the name of the word
		 *
		 * Parameters:
		 *
         * std::wstring word The new word
         */
		void SetWord (std::wstring word);

        /**
         * Changes the name of the original word
		 *
		 * Parameters:
		 *
         * std::wstring word The new word
         */
		void SetOriginalWord (std::wstring word);

		/**
		 * Obtaih the name of the original word
		 *
		 * Return value: THe name of the original word
		 */
		std::wstring GetOriginalWord ();

        /**
         * Obtains the name of the word
		 *
         * return value: The name of the word
         */
		std::wstring GetWord ();

        /**
         * Changes the accent of the word
		 *
		 * Parameters:
		 *
         * int accent The new accent
         */
        void SetAccent (int accent);

        /**
         * Obtains the accent of the word
		 *
         * return value: The accent
         */
        int GetAccent();

        /**
         * Changes the index of the word
		 *
		 * Parameters:
		 *
         * int index The new index
         */
        void SetIndex(int index);

        /**
         * Obtains the index of the words
		 *
         * return value: The index
         */
        int GetIndex();

        /**
         * Changes the number of time the word must be repeated
		 *
		 * Parameters:
		 *
         * int nbRepeats The new number of time the function is repeated
         */
        void SetNbRepeats(int nbRepeats);

        /**
         * Obtains the number of times the word must be repeated
		 *
         * return value: The number of times the word must be repeaded
         */
        int GetNbRepeats();

        /**
         * Changes the sound data
		 *
		 * Parameters:
		 *
         * Word * word data: The new sound data
         */
        void SetWordData (Word * wordData);

        /**
         * Obtains the sound data
		 *
         * return value:  The sound data
         */
        Word * GetWordData ();

        /**
         * Sets the pre punctuation
		 *
		 * Parameters:
		 *
         * std::wstring punc The new pre punctuation
         */
        void SetPrePunctuation (std::wstring punc);

        /**
         * Gets the pre punctuation
		 *
         * return value:  The pre punctuation
         */
		std::wstring GetPrePunctuation ();

		/**
         * Gets the post punctuation
		 *
         * return value:  The post punctuation
         */
		std::wstring GetPostPunctuation ();

        /**
         * Sets the post punctuation
		 *
		 * Parameters:
		 *
         * std::wstring punc The new post punctuation
         */
        void SetPostPunctuation (std::wstring punc);

		/**
		* Determine if the object does not contain a token
		*
		* return value: true if the object is empty, else false
		**/
		bool IsEmpty ();

		/**
		* Determine if the current token is followed by a
		* punctuation mark
		*
		* return value:  true if the object is followed by a punctuation mark, else false
		**/
		bool HasPostPunc ();

		void SetSilenceLength (long silenceLength);

		void SetTyLeTangVolumeTu (long tyLeTangVolumeTu);

	

		long GetSilenceLength ();

		long GetTyLeTangVolumeTu ();

		void SetWordCut (double wordCut);

		double GetWordCut ();

	protected:

		std::wstring _word; // The word in question
		std::wstring _originalWord;
        std::wstring _prepunc; // Punction before the word
        std::wstring _postpunc; // Punctuation averder the word
        int _accent; // Accent of the word
        int _index; // Possition of word in database
        int _nbRepeats; // Number of time the word is repeated (only useful for adding silence).
		long _silenceLength;
		long _tyLeTangVolumeTu;
		double _wordCut;
        Word * _wordData; // WATCH OUT FOR THIS MAN!!!!
	} ;
}

#endif