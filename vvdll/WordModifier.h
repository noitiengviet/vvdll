#if !defined (WORD_MODIFIER_H)

#define WORD_MODIFIER_H	


#include "stringhelper.h"
#include "treatment.h"

namespace VietVoice
{
	/**
	 * Class Name:         WordModifier.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Used to modify certains words so they can be found in the database
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	/**
	  * CODE MODIFICATIONS AND BUG CORRECTIONS
	  * --------------------------------------
	  * March 3 2005 -> Modified processQy method so the word "qui" was  the same
	  * as quy instead of cui.
	  *
	  * October 5 2005 -> Started converting the file to C++
	  * October 18 2005 -> Converted the treat method, all methods now converted.
	  * November 29 2005 -> Added destructor in case
	  * novembre 29 2005:  Implemented the copy constructor and the = operator
	  * January 12 2005: added == 0 to each condition using the compare function in a if (forgetting that == 0 made comparing two different words equal).
      * June 10 2006: Change the methods name so they start by a capital letter.
	  */

	/**
	 * Utilis� pour modifier certain mots entr� afin qu'ils puissent �tre trouv� dans la base de donn�es
	 *
	 * Used to modify certains words so they can be found in the database
	 */
	class WordModifier : public Treatement
	{
	public:

		WordModifier ();

		/**
		* Modifie some of the words in order to be able to find them in the database
		*
		* Paramters:
		* 
		* ToBeSpoken & toSpoke:  Data about the words that need to be spoken
		*/
		void Treat (ToBeSynthetise & toSynthetise);

		virtual ~WordModifier ();

		WordModifier  (const WordModifier & wm);

		void operator = (const WordModifier & wm);

	private:

		/**
		 * Takes care of things like converting gi to gii, gy to gi, y to i, qu to c or co, etc.
		 * @param tokenVal The word
		 * @return The modified word
		 */
		void ProcessConsonnant (std::wstring & tokenVal);

		/**
		 * Replace the qu part of the word by c or co depending on the word
		 * @param tokenVal String representing the word
		 */
		void ProcessQu (std::wstring & tokenVal);

		/**
		 *
		 * For the appropriate words, replace gi by gii so the algorithme finding the position of the word in the index works properly
		 * @param tokenVal String representing the token
		 */
		void  ProcessGi (std::wstring & tokenVal);

		 /**
		  *
		  * Handle the case where gy musts become either gi or gii
		  * @param tokenVal String representing the token
		  */
		void VietVoice::WordModifier::ProcessGy (std::wstring & tokenVal);
	};
}

#endif