
#include "accentfinder.h"
#include "vectoraccent.h"

/**
* Copy constructor.
*/
VietVoice::AccentFinder::AccentFinder  (const AccentFinder & accentFinder)
	 : _vectorAccent (accentFinder._vectorAccent)
{			
	if (this != &accentFinder) // Make sure we aren't copying the ojbect into itself
	{
		_extraWords = accentFinder._extraWords;
	}
}

/**
* Destructor
*/
VietVoice::AccentFinder::~AccentFinder ()
{}

/**
* Assignement operator used to avoid memory leaks
*/
void VietVoice::AccentFinder::operator = (const AccentFinder & accentFinder)
{			
	if (this != &accentFinder) // Make sure we aren't copying the ojbect into itself
	{
		_extraWords = accentFinder._extraWords;
		_vectorAccent = accentFinder._vectorAccent;
	}
}

/**
* Constructor
*
* Parameters:
*
* VectorAccent & va: A reference to the VectorAccent object use to determine the accent of a word
* std::wstring extra:  Name of the file containign the "extra", better described as the words from
*					   the 3rd database
*/

VietVoice::AccentFinder::AccentFinder(VectorAccent & va):_vectorAccent(va)
{	
}

VietVoice::AccentFinder::AccentFinder(VectorAccent & va, std::wstring extra)
	: _vectorAccent (va)
{	
	PrepareExtraWords (extra);	// Prepare the extra word vector (Need to know these word, cause they are always set to have no accent even if they have one inreal life)
}

/**
* Find the accent for every word in the utterance.
*
* Return value:  The data to be spoken after modification
*
* Parameters:
* ToBeSpoken toSpoke: A reference to the data to be spoken that is being processed
* 
*/
void VietVoice::AccentFinder::Treat (ToBeSynthetise & toSynthetise) 
{
	std::wstring name;
	std::wstring character;
	std::wstring v_name; // VT
	Accent accentInfo;

	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData (); // Get the list of ToBeSpokenData
	std::list<ToBeSynthetiseData> newListData;

	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		ToBeSynthetiseData data = *iter;
		name = data.GetWord ();
		
		

		v_name = name ;
// VT
		if (name[name.length()-1]==L'_') {
			name = name.substr(0,name.length()-1) ;
		}
		if (name==L"_silence_") {
			name = name.substr(1,name.length()-2) ;
		}
// End VT

		////data.SetOriginalWord (name); // Not working? // VX

		

		// Words present in the extraWords vector do not have an accent
		if (_extraWords.find (name) != _extraWords.end ()) // If extra word (from 3rd database) no accent
		{		
			data.SetAccent (-1);
		}
		else
		{ 
			for (unsigned int i = 0; i < name.length(); ++i) //For every character in the word
			{ 	
				character = name.substr(i, 1);
				
				if (_vectorAccent.EntryExist (character)) // Does the caracter has an accent?
				{
					accentInfo = _vectorAccent.GetAccentInfo(character); // Find the data about the accent of the character (if the character has an accent)

					// If word from first database (word from the second always end by one of these:  c, ch, t or p), remove the accent from word
					if (!StringHelper::EndWith(name, L"c")&& !StringHelper::EndWith(name, L"ch") && !StringHelper::EndWith(name, L"p") && !StringHelper::EndWith(name, L"t"))
					{		
						std::wstring tmp = accentInfo.GetReplaceWith();
							
						tmp = tmp[0];

						std::wstring tmpChar = L"";
						tmpChar =  character [0];
							
						StringHelper::ReplaceFirst(name, tmpChar, tmp); // Replace the character with the accent by the same character without the accent.
						StringHelper::ReplaceFirst(v_name, tmpChar, tmp); // VT
					}

					data.SetAccent (accentInfo.GetAccent()); 
					////data.SetWord(name); // VX
					data.SetWord(v_name); // VS

					if (accentInfo.GetAccent() > 0) // Accent has been found, can quit
					{	   
						break;
					}
				}
				else // No accent found
				{
					data.SetAccent (0) ;
				}		
			}
		}
		newListData.push_back(data);	
	}
	toSynthetise.SetListData (newListData) ;
}

/**
* Read all extra words from a file
*
* Parameters:
*
* std::wstring filename: filename The name of the file
*/
void VietVoice::AccentFinder::PrepareExtraWords (std::wstring filename)
{
	std::wstring line = L"";
	std::wstring name;

	try 
	{
		File::Reader reader (filename);

		long count = reader.ReadLong (); // Read the number of lines in the file

		for (int i = 0; i < count; i++)  // For each line
		{
			line = reader.ReadWString();
			StringHelper::RemoveSpace (line);
			if (line != L"") // If line not empty
			{
				//StringTokenizer tokenizer (line, L" \t") ;
				std::vector <std::wstring> tokens;
				StringHelper::Tokenize (tokens, line, L" \t\n");
				 // In case there is more then one word on the line (should not be the case)
				for (std::vector<std::wstring>::iterator iter = tokens.begin (); iter != tokens.end(); iter++)
				{	  		   
					// Add read word to the _extraWords vector
					name = *iter;
	   				std::pair< std::wstring, int > tmpPair (name, i);
					_extraWords.insert(tmpPair) ;
				}
			}
		}
	}
	catch (File::Exception ex) // Something wrong happened
	{
		throw VietnameseTTException(L"Something went wrong while reading the abbreviation file in TokenToWords" + ex.GetMsg ());
	} 
}