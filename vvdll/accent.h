#if !defined (ACCENT_H)

#define ACCENT_H

#include <string>

namespace VietVoice
{
	/**
	 * Class Name:         Accent.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class used to cotain the data about the accent of a word
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * october 19:  Converted the accent class from java to C++
	 * Nomvember 25:  Added copy constructor and = operator
	 * February 16 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 13 2006:  Upgraded the comments and spacing in accent.cpp file.
	 */
	class Accent
	{
	public:

	  /**
	   * Copy constructor
	   */
	    Accent  (const Accent & accent);

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const Accent & accent);

		/**
		* Constructor
		*/
	  Accent ();

	  /**
	   * Constructor.
	   *
	   *
	   * Parameters:
	   *
	   * int a: Numerical value representing an accent.
	   * std::wstring w The character used to replace the character with the accent represented by the numerical value
	   */
	  Accent (int a, std::wstring w);

	  /**
	   * Obtain the numerical accent
	   *
	   * Return value:  A numerical value representing the accent
	   */
	  int GetAccent ();

	  /**
	   * Obtain the character used to replace the character with the accent in a word
	   *
	   * Return value:  The character used to replace the character with the accent in a word
	   */
	  std::wstring GetReplaceWith ();

	  /**
	   * Change the numerical value accent
	   *
	   * Parameters:
	   *
	   * int a: A numerical value representing the new accent
	   */
	  void SetAccent (int a);

	  /**
	   * Changes the character used to replace the character with the accent in a word
	   *
	   * Parameters:
	   *
	   * std::wstring w The new charater used to replace the character with the accent in a word
	   */
	  void SetReplaceWith (std::wstring w);

	private:

	  int _accent; // Represent the accent.
	  std::wstring _replaceWith; // Replace the character by the "same" character, but without an accent.
	};
}

#endif
