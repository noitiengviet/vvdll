
#include <process.h> 
#include "vietnamienVoice.h"
#include "vvinterfaces.h"

VietVoice::VietnamienVoice::VietnamienVoice (const std::wstring path)
	: _tokensToWords (path + L"\\abbreviations.bin", path + L"\\noSilence.bin"),
	  _vectorAccent (path + L"\\vecteurAccent.bin"),
	  _accentFinder (_vectorAccent, path + L"\\extraWords.bin"),
	  _wordModifier (),
	  _silenceAdder (path + L"\\adjectives.txt", path + L"\\lightWords.txt", path + L"\\Prosody.txt", path + L"\\silenceDictionary-2.txt", path + L"\\silenceDictionary-3.txt", path + L"\\wordssize.txt", path + L"\\silencessize.txt", path + L"\\articles.txt", path + L"\\LinkingWords.txt", _wordSelector),
	  _wordSelector (path + L"\\database//maleVoice//Base_Unit.bin", path + L"\\database//maleVoice//6Accents_Unit.bin", path + L"\\database//maleVoice//2Accents_Unit.bin", path + L"\\database//maleVoice//Special_Unit.bin", path + L"\\database//maleVoice//Special_Unit.idx", path + L"\\syllable.bin", path + L"\\syllable2.bin", path + L"\\consonne1.bin", path + L"\\consonne2.bin", path + L"\\extraWords.bin"),
	  _callback (NULL), 
	  _element (VietVoice::PHRASE),
	  _isSpeaking (false),
	  _curVoice (VietVoice::ToBeSynthetise::MALE)
{
	//WCHAR currentDirectory [_MAX_PATH];
	_currentDirectory = path;
	InitTreatements ();
	
	_durMoyenSilence = 0.35f;
//	_vbs=2000; VX, version 15 (after words)
	::InitializeCriticalSection (&_critical);
}

VietVoice::VietnamienVoice::~VietnamienVoice ()
{
	::DeleteCriticalSection (&_critical);
}

VietVoice::VietnamienVoice::VietnamienVoice  (const VietnamienVoice & voice)
: _tokensToWords (voice._currentDirectory + L"\\abbreviations.bin", voice._currentDirectory + L"\\noSilence.bin"),
	  _vectorAccent (voice._currentDirectory + L"\\vecteurAccent.bin"),
	  _accentFinder (_vectorAccent, voice._currentDirectory + L"\\extraWords.bin"),
	  _wordModifier (),
	  _silenceAdder (voice._currentDirectory + L"\\adjectives",voice._currentDirectory + L"\\lightWords",voice._currentDirectory + L"\\prosody", voice._currentDirectory + L"\\silenceDictionary-2", voice._currentDirectory + L"\\silenceDictionary-3", voice._currentDirectory + L"\\wordssize.txt", voice._currentDirectory + L"\\silencessize.txt", voice._currentDirectory + L"\\articles.txt", voice._currentDirectory + L"\\LinkingWords.txt", _wordSelector),
	  _wordSelector (voice._currentDirectory + L"\\database//maleVoice//Base_Unit.bin", voice._currentDirectory + L"\\database//maleVoice//6Accents_Unit.bin" ,voice._currentDirectory + L"\\database//maleVoice//2Accents_Unit.bin", voice._currentDirectory + L"\\database//maleVoice//Special_Unit.bin", voice._currentDirectory + L"\\database//maleVoice//Special_Unit.idx", voice._currentDirectory + L"\\syllable.bin", voice._currentDirectory + L"\\syllable2.bin", voice._currentDirectory + L"\\consonne1.bin", voice._currentDirectory + L"\\consonne2.bin", voice._currentDirectory + L"\\extraWords.bin"),
	  _callback (NULL),
	  _element (VietVoice::PHRASE),
	  _isSpeaking (false),
	  _curVoice (voice._curVoice)
	  
{
	if (this != &voice)
	{
			_listTreatement = voice._listTreatement; // List of object use to "read" text
			_durMoyenSilence = voice._durMoyenSilence;
			_tokensToWords = voice._tokensToWords;
			_vectorAccent = voice._vectorAccent;
			_accentFinder = voice._accentFinder;
			_wordModifier = voice._wordModifier;
			_wordSelector = voice._wordSelector;
			_callback = voice._callback;
			_element = VietVoice::PHRASE;
			_pathDb = voice._pathDb;
			_isSpeaking = false;
			::InitializeCriticalSection (&_critical);
	}
}

void VietVoice::VietnamienVoice::operator = (const VietnamienVoice & voice)
{
	if (this != &voice)
	{
		_listTreatement = voice._listTreatement; // List of object use to "read" text
		_durMoyenSilence = voice._durMoyenSilence;
		_tokensToWords = voice._tokensToWords;
		_vectorAccent = voice._vectorAccent;
		_accentFinder = voice._accentFinder;
		_wordModifier = voice._wordModifier;
		_wordSelector = voice._wordSelector;	
		_pathDb = voice._pathDb;
		::EnterCriticalSection (&_critical);
		_isSpeaking = false;
		::LeaveCriticalSection (&_critical);
		::InitializeCriticalSection (&_critical);
		_curVoice = voice._curVoice;
	}		
}

 /**
 * Reload the abbreviation file.  useful when there is a change in the abbreviations.
 */
HRESULT _stdcall VietVoice::VietnamienVoice::ReloadAbbreviations ()
{
	 _tokensToWords.LoadAbbreviationFile (_currentDirectory + L"\\abbreviations.bin");
	 return S_OK;
}

/**
 * Read a string of text
 * @param str The string to be read
 * @param pos Position from which we start reading
 */
HRESULT _stdcall VietVoice::VietnamienVoice::Speak (std::wstring str, int pos)
{		
	::EnterCriticalSection (&_critical);
	_isSpeaking = true;
	::LeaveCriticalSection (&_critical);
	try
	{
/*
		unsigned short _us=65535;
::MessageBox(NULL, StringHelper::ToString(_us).c_str(), L"Speaking file", MB_OK); // VT
		short int _s = _us;
*/
//long v_dots=200 ;
//float v_hs = (float) ((-32767) / (v_dots + 1) * (v_dots + 1)) / (-32767);
//::MessageBox(NULL, StringHelper::ToString(v_hs).c_str(), L"Speaking file", MB_OK); // VT
//::MessageBox(NULL, StringHelper::ToString(::floorl( (-32768) / (v_dots + 1)) * (v_dots +1) / v_hs).c_str(), L"Speaking file", MB_OK); // VT

		//StringHelper::ReplaceAll (str, L'\r', L' ');
		//StringHelper::ReplaceAll (str, L'\n', L' '); // VX
		StringHelper::ReplaceAll (str, L'@', L' ');
		StringHelper::ReplaceAll (str, L'\x00AB', L''); // VT
		StringHelper::ReplaceAll (str, L'\x00BB', L''); // VT

		unsigned int w = 0;
		unsigned int v_loc = 0;

		for (int i=0; i < str.length(); i++)
			if (str[i] == L'\x000D') {
				if (i > 0 && str[i-1] != L'.') str[i]=L'.' ;
			}

		for (int i=0; i < str.length(); i++)
			if (str[i] == L'-') {
				if (i > 0 && str[i-1] == L' ') {str[i-1]=L',' ; str[i]=L' ' ;}
			}


		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w > 0 && ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!')))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w < str.length() && (str[w + 1] == L';' || str[w + 1] == L',' || str[w + 1] == L'.' || str[w + 1] == L'!'))
				{
					str[w] = str[w + 1];
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201C', v_loc); // VS
			if (w != str.npos)
			{
				if (w >0 && ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!')))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);


		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201D', v_loc); // VS
			if (w != str.npos)
			{
				if (w < str.length() && ((str[w + 1] == L';') || (str[w + 1] == L',') || (str[w + 1] == L'.') || (str[w + 1] == L'!')))
				{
					str[w] = str[w + 1] ;
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);


		//StringHelper::ReplaceAll (str, L'"', L','); // VT VX
		
		str += L" . "; 
//		str += L"."; // VS

		//unsigned int w = 0;
		//unsigned int v_loc = 0;
		do
		{
//			w= str.find (L"-", 0); VX
			w= str.find (L"-", v_loc); // VS

			if (w != str.npos)
			{
				if ((w > 0 && str[w - 1] == L' '))
				{
					str[w-1] = L',';
					str[w] = L' ';
				}
				else if ((w < str.size () - 1 && str[w + 1] == L' '))
					str[w] = L',';
				else if (w < str.size () - 1 && str[w + 1] >= L'0' && str[w+1] <= L'9') {str[w] = L'-' ; v_loc = w+1 ;} // VT
				else
					str[w] = L' ';
		
			}

		} while (w != str.npos);
// VT
	v_loc=0;
	do
		{
			w= str.find (L".", v_loc); // VS
			if (w != str.npos)
			{
				if (w >0 && (str[w-1] == L'I' || str[w-1] == L'V' || str[w-1] == L'X' )) str[w] = L'/' ; // VT
				else v_loc++ ;
			}

		} while (w != str.npos);

	v_loc=0;
	do
		{
			w= str.find (L":", v_loc); // VS
			if (w != str.npos)
			{
				if ((w >0 && str[w - 1] >= L'0' && str[w - 1] <= L'9') && (w < str.size () - 1 && str[w + 1] >= L'0' && str[w + 1] <= L'9')) {str[w] = L'|' ; v_loc++ ;} // VT
				else v_loc++ ;
			}

		} while (w != str.npos);

		if (_callback != NULL)
		{
		
			_callback->OnStartSpeaking (this);
			
		}

		_mustStop = false;

		ToBeSynthetise toSynthetise;
		toSynthetise.SetVoice (_curVoice);
		toSynthetise.SetMp3Name(_currentDirectory + L"\\database\\tmp.mp3");
		toSynthetise.SetMustPlay (true);
		toSynthetise.SetDurMoyenSilence (_durMoyenSilence) ;
// ====================================================================
		toSynthetise.Set_BS (_vbs) ; // Cho nay se truyen thong so
		v_bs1 = _silenceAdder.v_getBreakSilence1();
		_concatenator.Get_BS1(v_bs1);
		v_bs2 = _silenceAdder.v_getBreakSilence2();
		_concatenator.Get_BS2(v_bs2);
		v_vl = _silenceAdder.v_getVolume();
		_concatenator.Get_VL(v_vl);

		v_mvol = _silenceAdder.v_get_main_vol();
		_concatenator.Get_MV(v_mvol);

		v_svol1 = _silenceAdder.v_get_soft_vol1();
		_concatenator.Get_SV1(v_svol1);
		v_svol21 = _silenceAdder.v_get_soft_vol21();
		_concatenator.Get_SV21(v_svol21);
		v_svol22 = _silenceAdder.v_get_soft_vol22();
		_concatenator.Get_SV22(v_svol22);
		v_svol3 = _silenceAdder.v_get_soft_vol3();
		_concatenator.Get_SV3(v_svol3);

		v_sp = _silenceAdder.v_getSpeed();
		_concatenator.Get_SP(v_sp);
		v_ph = _silenceAdder.v_getPitch(); ////
		_concatenator.Get_Pitch(v_ph); ////
		v_cp = _silenceAdder.v_getCompress(); ////
		_concatenator.Get_CP(v_cp); ////

		v_ws = _silenceAdder.v_getWordSize(); ////
		_concatenator.Get_WS(v_ws); ////

		v_cw1 = _silenceAdder.v_getCW1(); ////
		_concatenator.v_getCW1(v_cw1); ////
		v_cw2 = _silenceAdder.v_getCW2(); ////
		_concatenator.v_getCW2(v_cw2); ////
		v_ss = _silenceAdder.v_getStopSilence(); ////
		_concatenator.v_getStopSilence(v_ss); ////

		_concatenator.Get_Per_Pitch(_silenceAdder.v_get_per_pitch()) ;
		_concatenator.Get_Pitch0(_silenceAdder.v_get_pitch0()) ;
		_concatenator.Get_Pitch1(_silenceAdder.v_get_pitch1()) ;
		_concatenator.Get_Pitch2(_silenceAdder.v_get_pitch2()) ;
		_concatenator.Get_Pitch3(_silenceAdder.v_get_pitch3()) ;
		_concatenator.Get_Pitch4(_silenceAdder.v_get_pitch4()) ;
		_concatenator.Get_Pitch5(_silenceAdder.v_get_pitch5()) ;

		_concatenator.Get_Com0(_silenceAdder.v_get_com0()) ;
		_concatenator.Get_Com1(_silenceAdder.v_get_com1()) ;
		_concatenator.Get_Com2(_silenceAdder.v_get_com2()) ;
		_concatenator.Get_Com3(_silenceAdder.v_get_com3()) ;
		_concatenator.Get_Com4(_silenceAdder.v_get_com4()) ;
		_concatenator.Get_Com5(_silenceAdder.v_get_com5()) ;

		std::wstring v_lw = L"" ;
		_silenceAdder.v_getLight(v_lw) ;
		_concatenator.Get_light(v_lw) ;
		std::wstring v_ad = L"" ;
		_silenceAdder.v_getAdjective(v_ad) ;
		_concatenator.Get_adjective(v_ad) ;
		v_d = _silenceAdder.v_getDots() ;
		_concatenator.Get_Dots(v_d) ;
		v_f = _silenceAdder.v_getDots() ;
		_concatenator.Get_Fade(v_f) ;
		v_sa = _silenceAdder.v_Samples() ;
		_concatenator.Get_Sample(v_sa) ;
// ====================================================================

		Token token;

		int nb = 0;

		str += L" ";
		// Parse the string in tokens
		_tokenizer.SetString (str);

		int beginPos = pos;
		int endPos = 0;

		do
		{
			_tokenizer.GetNextToken (token);
			endPos = _tokenizer.GetCurrentPos () + pos;


			if (!token.IsEmpty ())
			{
				nb++;
				toSynthetise.AppendToken (token) ;
			}
			
			if (token.IsEmpty () || token.HasPostPunc () || nb >= 50) // if end ofor string or punctiation mark or number of tokens too big, start speaking
			{
				if (nb >= 0)
				{
					if (_callback != NULL)
					{
						_callback->OnElementSpoken (this, beginPos, endPos);
					}

					beginPos = endPos;

				TreatToBeSpoken(toSynthetise); // Treat the text so it is read

				}
				
				toSynthetise.Empty ();	  
				toSynthetise.SetDurMoyenSilence(_durMoyenSilence); // Set the lenght of word below witch silence must be added

				nb = 0;
			}

		} while (!token.IsEmpty () && _mustStop == false) ;

		if (_callback != NULL)
		{
			_callback->OnStopSpeaking (this);
		}

		return S_OK;
	}
	catch (VietnameseTTException ex)
	{
		::EnterCriticalSection (&_critical);
		_isSpeaking = false;
		::LeaveCriticalSection (&_critical);
		::MessageBox (NULL, ex.GetMsg ().c_str (), L"Error", MB_OK);
		return E_FAIL;
	}

	::EnterCriticalSection (&_critical);
	_isSpeaking = false;
	::LeaveCriticalSection (&_critical);
}

HRESULT __stdcall VietVoice::VietnamienVoice::SaveMp3 (std::wstring str, std::wstring filename)
{
	try
	{
		StringHelper::ReplaceAll (str, L'“', L' ');
		StringHelper::ReplaceAll (str, L'”', L' ');
		StringHelper::ReplaceAll (str, L'\x0027', L' ');
		std::wstring v_str ;
		_mustStop = false;
		str = L"_silence_ _silence_ _silence_ _silence_ " + str ; // VT

/*		unsigned int w = 0;
		unsigned int v_loc = 0;

		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w > 0)
					if ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!'))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);


		v_loc = 0 ;
		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w < str.length() && (str[w + 1] == L','))
				{
					str[w] = str[w + 1];
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201C', v_loc); // VS Dau nhay kep dau
			if (w != str.npos)
			{
				if (w >0 && ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!')))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201D', v_loc); // VS Dau nhay kep cuoi
			if (w != str.npos)
			{
				if (w < str.length() && ((str[w + 1] == L';') || (str[w + 1] == L',') || (str[w + 1] == L'.') || (str[w + 1] == L'!')))
				{
					str[w] = str[w + 1] ;
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

*/

		unsigned int w = 0;
		unsigned int v_loc = 0;

for (int i=0; i < str.length(); i++)
	if (str[i] == L'\x000D') {
//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"str[i-1]", MB_OK); // VT
		if (i > 0 && str[i-1] != L'.') 
			str[i]=L'.' ;
			str[i+1]=L' ' ;
		}

		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w > 0 && ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!')))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'"', v_loc); // VS
			if (w != str.npos)
			{
				if (w < str.length() && (str[w + 1] == L';' || str[w + 1] == L',' || str[w + 1] == L'.' || str[w + 1] == L'!'))
				{
					str[w] = str[w + 1];
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);

		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201C', v_loc); // VS
			if (w != str.npos)
			{
				if (w >0 && ((str[w - 1] == L';') || (str[w - 1] == L',') || (str[w - 1] == L'.') || (str[w - 1] == L'!')))
				{
					str[w] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);


		v_loc = 0 ;
		do
		{
			w= str.find (L'\x201D', v_loc); // VS
			if (w != str.npos)
			{
				if (w < str.length() && ((str[w + 1] == L';') || (str[w + 1] == L',') || (str[w + 1] == L'.') || (str[w + 1] == L'!')))
				{
					str[w] = str[w + 1] ;
					str[w+1] = L' ';
					v_loc = w ;
				}
			}
			v_loc += 1;
		} while (w != str.npos);


		//StringHelper::ReplaceAll (str, L'"', L','); // VT VX
//::MessageBox(NULL, StringHelper::ToString(str).c_str(), L"Reading file", MB_OK); // VT
		
		str += L" . "; 


		int i = 0 ;
		for (i = 0; i < str.length(); i++) 
			if(i+1 < str.length() && str[i] == L'\x000D' && str[i+1] == L'\x000A') 
			{
				if (i > 0) { 
					if (str[i-1] == L':') {
//::MessageBox(NULL, StringHelper::ToString(str[i-1]).c_str(), L"trr[i-1]", MB_OK); // VT
						str[i-1] = L'.' ;
						v_str[v_str.length()-1] = L'.' ;
					}
					if (str[i-1] != L'.') {
						str[i] =L'.';
						str[i+1] = L'_';
					} else {
						str[i]=L'_';
						str[i+1]=L'_';
					}
				}
				else { 
					i++ ;
				}
				v_str += str.substr(i, 2) ;
				do {
					i++ ;
				} while (i+1 < str.length() && (str[i+1] == L'\x000D' || str[i+1] == L'\x000A' || str[i+1] == L' ' || str[i+1] == L'(' || str[i+1] == L'"')) ;
			} else {
				v_str += str[i] ;
			}
str += L' .' ;
			str = v_str ; // VT
//::MessageBox(NULL, StringHelper::ToString(v_str).c_str(), L"v_str", MB_OK); // VT
		
		// ::SetCurrentDirectory (_currentDirectory.c_str ());


		StringHelper::ReplaceAll (str, L'\r', L' ');
		StringHelper::ReplaceAll (str, L'\n', L' ');
		StringHelper::ReplaceAll (str, L'@', L' ');

		ToBeSynthetise toSynthetise;
		toSynthetise.SetVoice (_curVoice);
		toSynthetise.SetMp3Name(filename);
		toSynthetise.SetMustPlay (false);
		toSynthetise.SetDurMoyenSilence (_durMoyenSilence) ;

// VT

// **************************************************************************************************
/*
		toSynthetise.Set_BS(_vbs) ; 
		v_bs1 = _silenceAdder.v_getBreakSilence1();
		_concatenator.Get_BS1(v_bs1);
		v_bs2 = _silenceAdder.v_getBreakSilence2();
		_concatenator.Get_BS2(v_bs2);
		v_vl = _silenceAdder.v_getVolume();
		_concatenator.Get_VL(v_vl);

		v_mvol = _silenceAdder.v_get_main_vol();
		_concatenator.Get_MV(v_mvol);

		v_svol1 = _silenceAdder.v_get_soft_vol1();
		_concatenator.Get_SV1(v_svol1);
		v_svol21 = _silenceAdder.v_get_soft_vol21();
		_concatenator.Get_SV21(v_svol21);
		v_svol22 = _silenceAdder.v_get_soft_vol22();
		_concatenator.Get_SV22(v_svol22);
		v_svol3 = _silenceAdder.v_get_soft_vol3();
		_concatenator.Get_SV3(v_svol3);

		v_sp = _silenceAdder.v_getSpeed();
		_concatenator.Get_SP(v_sp);
		v_ph = _silenceAdder.v_getPitch(); ////
		_concatenator.Get_Pitch(v_ph); ////
		v_cp = _silenceAdder.v_getCompress(); ////
		_concatenator.Get_CP(v_cp); ////

		v_ws = _silenceAdder.v_getWordSize(); ////
		_concatenator.Get_WS(v_ws); ////

		v_cw1 = _silenceAdder.v_getCW1(); ////
		_concatenator.v_getCW1(v_cw1); ////
		v_cw2 = _silenceAdder.v_getCW2(); ////
		_concatenator.v_getCW2(v_cw2); ////
		v_ss = _silenceAdder.v_getStopSilence(); ////
		_concatenator.v_getStopSilence(v_ss); ////

		std::wstring v_lw = L"" ;
		_silenceAdder.v_getLight(v_lw) ;
		_concatenator.Get_light(v_lw) ;
		std::wstring v_ad = L"" ;
		_silenceAdder.v_getAdjective(v_ad) ;
		_concatenator.Get_adjective(v_ad) ;
		v_d = _silenceAdder.v_getDots() ;
		_concatenator.Get_Dots(v_d) ;
*/
// End VT
// **************************************************************************************************
// ====================================================================
		toSynthetise.Set_BS (_vbs) ; // Cho nay se truyen thong so
		v_bs1 = _silenceAdder.v_getBreakSilence1();
		_concatenator.Get_BS1(v_bs1);
		v_bs2 = _silenceAdder.v_getBreakSilence2();
		_concatenator.Get_BS2(v_bs2);
		v_vl = _silenceAdder.v_getVolume();
		_concatenator.Get_VL(v_vl);

		v_mvol = _silenceAdder.v_get_main_vol();
		_concatenator.Get_MV(v_mvol);

		v_svol1 = _silenceAdder.v_get_soft_vol1();
		_concatenator.Get_SV1(v_svol1);
		v_svol21 = _silenceAdder.v_get_soft_vol21();
		_concatenator.Get_SV21(v_svol21);
		v_svol22 = _silenceAdder.v_get_soft_vol22();
		_concatenator.Get_SV22(v_svol22);
		v_svol3 = _silenceAdder.v_get_soft_vol3();
		_concatenator.Get_SV3(v_svol3);

		v_sp = _silenceAdder.v_getSpeed();
		_concatenator.Get_SP(v_sp);
		v_ph = _silenceAdder.v_getPitch(); ////
		_concatenator.Get_Pitch(v_ph); ////
		v_cp = _silenceAdder.v_getCompress(); ////
		_concatenator.Get_CP(v_cp); ////

		v_ws = _silenceAdder.v_getWordSize(); ////
		_concatenator.Get_WS(v_ws); ////

		v_cw1 = _silenceAdder.v_getCW1(); ////
		_concatenator.v_getCW1(v_cw1); ////
		v_cw2 = _silenceAdder.v_getCW2(); ////
		_concatenator.v_getCW2(v_cw2); ////
		v_ss = _silenceAdder.v_getStopSilence(); ////
		_concatenator.v_getStopSilence(v_ss); ////

		_concatenator.Get_Per_Pitch(_silenceAdder.v_get_per_pitch()) ;
		_concatenator.Get_Pitch0(_silenceAdder.v_get_pitch0()) ;
		_concatenator.Get_Pitch1(_silenceAdder.v_get_pitch1()) ;
		_concatenator.Get_Pitch2(_silenceAdder.v_get_pitch2()) ;
		_concatenator.Get_Pitch3(_silenceAdder.v_get_pitch3()) ;
		_concatenator.Get_Pitch4(_silenceAdder.v_get_pitch4()) ;
		_concatenator.Get_Pitch5(_silenceAdder.v_get_pitch5()) ;

		_concatenator.Get_Com0(_silenceAdder.v_get_com0()) ;
		_concatenator.Get_Com1(_silenceAdder.v_get_com1()) ;
		_concatenator.Get_Com2(_silenceAdder.v_get_com2()) ;
		_concatenator.Get_Com3(_silenceAdder.v_get_com3()) ;
		_concatenator.Get_Com4(_silenceAdder.v_get_com4()) ;
		_concatenator.Get_Com5(_silenceAdder.v_get_com5()) ;

		std::wstring v_lw = L"" ;
		_silenceAdder.v_getLight(v_lw) ;
		_concatenator.Get_light(v_lw) ;
		std::wstring v_ad = L"" ;
		_silenceAdder.v_getAdjective(v_ad) ;
		_concatenator.Get_adjective(v_ad) ;
		v_d = _silenceAdder.v_getDots() ;
		_concatenator.Get_Dots(v_d) ;
		v_f = _silenceAdder.v_Fade() ;
		_concatenator.Get_Fade(v_f) ;
		v_sa = _silenceAdder.v_Samples() ;
		_concatenator.Get_Sample(v_sa) ;
// ====================================================================

		Token token;

		int nb = 0;

		str += L" . "; // VT
		//StringHelper::ReplaceAll( str, L'"', L' ') ; // VT
		//StringHelper::ReplaceAll (str, L'\x00AB', L''); // VT
		//StringHelper::ReplaceAll (str, L'\x00BB', L''); // VT
		StringHelper::ReplaceAll( str, L'«', L' ') ; // VT
		StringHelper::ReplaceAll( str, L'»', L' ') ; // VT
//::MessageBox(NULL, str.c_str(), L"Edited Token", MB_OK); // VT

		// Parse the string in tokens
		_tokenizer.SetString (str);

		int beginPos = 0;
		int endPos = 0;

		do
		{

			_tokenizer.GetNextToken (token);
			endPos = _tokenizer.GetCurrentPos ();

			if (!token.IsEmpty ())
			{
////MessageBox(NULL, token.GetToken().c_str(), L"Tokens", MB_OK); // VT
				nb++;
//----------------------------------------------------------------------------------------
				Token v_token ;
				if (token.GetToken().substr(0,1) == L"_") {
					std::wstring s_token = token.GetToken() ;
					for (unsigned int i=0; i< (v_bs2 / 26.122) / 2 * 3; i++)  // * 2
						{
							token.SetToken (L"_silence_") ;
							toSynthetise.AppendToken (token) ;
						}
					do {
						s_token = s_token.substr(1,s_token.length()-1) ;
					} while (s_token.substr(0,1)==L"_") ;
					token.SetToken (s_token) ;
					toSynthetise.AppendToken (v_token) ;
////::MessageBox(NULL, s_token.c_str(), L"Edited Token", MB_OK); // VT
				}

				if (token.GetPrePunctuation()==L"(") {
					v_token = token ;
					for (int i=0; i < (v_bs1 / 26.122) / 2 * 2.5; i++) // length of silence.wav = 26.22
						{
							token.SetToken (L"_silence_") ;
							toSynthetise.AppendToken (token) ;
						}
				toSynthetise.AppendToken (v_token) ;
				} else
//----------------------------------------------------------------------------------------
				toSynthetise.AppendToken (token) ;
// VT
//----------------------------------------------------------------------------------------
				
				if (token.GetPostPunctuation()==L"," || token.GetPostPunctuation()==L"?" || token.GetPostPunctuation()==L"!" || token.GetPostPunctuation()==L":" || token.GetPostPunctuation()==L")") {
					for (unsigned int i=0; i < (v_bs1 / 26.122) / 2 * 3; i++) // length of silence.wav = 26.22 // * 2
						{
							token.SetToken (L"_silence_") ;
							toSynthetise.AppendToken (token) ;
						}
				} 
				else if (token.GetPostPunctuation()==L"." || token.GetPostPunctuation()==L";")
				{
					for (unsigned int i=0; i< (v_bs2 / 26.122) / 2 * 3; i++)  // * 2
						{
							token.SetToken (L"_silence_") ;
							toSynthetise.AppendToken (token) ;
						}
				}
// End VT				
			}

			if (token.IsEmpty ()) // if end ofor string or punctiation mark or number of tokens too big, start speaking
			{
				if (nb >= 0)
				{
					if (_callback != NULL)
						_callback->OnElementSpoken (this, beginPos, endPos);

					beginPos = endPos;

					TreatToBeSpoken(toSynthetise); // Treat the text so it is read

				}
		
				toSynthetise.Empty ();	  
				toSynthetise.SetDurMoyenSilence(_durMoyenSilence); // Set the lenght of word below witch silence must be added

				nb = 0;
			}
		} while (!token.IsEmpty () ) ; // End do ... while
	}
	catch (VietnameseTTException ex)
	{

		return E_FAIL;

	}
	catch (File::Exception ex)
	{
		return E_FAIL;
	}

	return S_OK;
}

/**
* Read a string of text asynchronously (in another thread).
* @param:  str The string to be read.
* @param pos Position from which we start reading
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SpeakAsync (std::wstring str, int pos)
{
	
	// Prepare the thread parameters
	VietVoice::ThreadSpeakParam * param = new VietVoice::ThreadSpeakParam ();

	param->_voice = this;
	param->_str = str;
	param->_pos = pos;

	_beginthreadex (NULL, 0, ThreadSpeak, (void *) param, 0, NULL); // Create another thread that will speak

	return S_OK;
}

/**
* Stop the reading of the text.
*/
HRESULT _stdcall VietVoice::VietnamienVoice::Stop ()
{
	
	_mustStop = true;
	_concatenator.Stop ();
	::EnterCriticalSection (&_critical);
	_isSpeaking = false;
	::LeaveCriticalSection (&_critical);
	return S_OK;
}

/**
 * Sets the minimal length for a word
 * @param dur
 */
HRESULT _stdcall VietVoice::VietnamienVoice::SetDurMoyenSilence (float dur)
{
	_durMoyenSilence = dur;
	return S_OK;
}
/* HRESULT _stdcall VietVoice::VietnamienVoice::Set_BS (long vbs)
{
	_vbs = vbs;
	return S_OK;
}
*/
/**
 * Obtains the minimal length for a word
 * @return
 */
HRESULT _stdcall VietVoice::VietnamienVoice::GetDurMoyenSilence (double & dur)
{
	dur = _durMoyenSilence;
	return S_OK;
}
HRESULT _stdcall VietVoice::VietnamienVoice::Get_BS (long & vbs)
{
	vbs = _vbs;
	return S_OK;
}

void VietVoice::VietnamienVoice::InitTreatements ()
{
	_listTreatement.push_back (&_tokensToWords);
	_listTreatement.push_back (&_accentFinder);
	_listTreatement.push_back (&_wordModifier);
	_listTreatement.push_back (&_silenceAdder);
	_listTreatement.push_back (&_wordSelector);
	_listTreatement.push_back (&_concatenator);
}
/**
 * Iterrates throught the list of Treatement object and each one does it task
 * @param toSpoke
 */
void VietVoice::VietnamienVoice::TreatToBeSpoken (ToBeSynthetise toSynthetise)
{
	for (std::list<Treatement *>::iterator iter = _listTreatement.begin (); iter != _listTreatement.end (); iter++)
	{
		(*iter)->Treat (toSynthetise);
	}
}

/**
* Read text with male voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseMaleVoice ()
{
	_curVoice = VietVoice::ToBeSynthetise::MALE;
	_wordSelector.SetDatabase (_currentDirectory + L"//database//maleVoice//6Accents_Unit.bin", _currentDirectory + L"//database//maleVoice//2Accents_Unit.bin", _currentDirectory + L"//database//maleVoice//Special_Unit.idx", _currentDirectory + L"//database//maleVoice//Base_Unit.bin", _currentDirectory + L"//database//maleVoice//Special_Unit.bin", _currentDirectory + L"//extraWords.bin");
	return S_OK;
}

/**
* Read text with female voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseFemaleVoice ()
{
	_curVoice = VietVoice::ToBeSynthetise::FEMALE;
	_wordSelector.SetDatabase (_currentDirectory + L"//database//femaleVoice//6Accents_Unit.bin", _currentDirectory + L"//database//femaleVoice//2Accents_Unit.bin", _currentDirectory + L"//database//femaleVoice//Special_Unit.idx", _currentDirectory + L"//database//femaleVoice//Base_Unit.bin", _currentDirectory + L"//database//femaleVoice//Special_Unit.bin", _currentDirectory + L"//extraWords.bin");
	return S_OK;
}

/**
* Read text with customer voice
*/
HRESULT _stdcall VietVoice::VietnamienVoice::UseCustomerVoice ()
{_curVoice = VietVoice::ToBeSynthetise::CUSTOM;
	_wordSelector.SetDatabase (_currentDirectory + L"//database//customer//6Accents_Unit.bin", _currentDirectory + L"//database//customer//2Accents_Unit.bin", _currentDirectory + L"//database//customer//Special_Unit.idx", _currentDirectory + L"//database//customer//Base_Unit.bin", _currentDirectory + L"//database//customer//Special_Unit.bin", _currentDirectory + L"//extraWords.bin");
	return S_OK;
}

/**
* Specify which database to use based on the given path
*
* Paramters:
*
* std::wstring path:  Path to the database
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetDatabase (std::wstring path)
{
	_wordSelector.SetDatabase (path + L"//6Accents_Unit.bin", path + L"//2Accents_Unit.bin", path + L"//Special_Unit.idx", path + L"//Base_Unit.bin", path + L"//Special_Unit.bin", L"extraWords.bin");
	return S_OK;
}

/**
* Specify which database to use based on the given path
*
* Paramters:
*
* std::wstring path:  Path to the database
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetDatabase (std::wstring pathDatabase, std::wstring pathConfig)
{

	_vectorAccent.Load (pathConfig + L"\\vecteurAccent.bin");

	_tokensToWords.LoadAbbreviationFile (pathConfig + L"\\abbreviations.bin");
	_tokensToWords.LoadNoSilenceWords (pathConfig + L"\\noSilence.bin");

	_accentFinder.PrepareExtraWords (pathConfig + L"\\extraWords.bin");

	_wordSelector.LoadParam (pathConfig + L"\\syllable.bin", pathConfig + L"\\syllable2.bin", pathConfig + L"\\consonne1.bin", pathConfig +L"\\consonne2.bin");

	_wordSelector.SetDatabase (pathDatabase + L"//6Accents_Unit.bin", pathDatabase + L"//2Accents_Unit.bin", pathDatabase + L"//Special_Unit.idx", pathDatabase + L"//Base_Unit.bin", pathDatabase + L"//Special_Unit.bin", pathConfig + L"\\extraWords.bin");
	return S_OK;
}

/**
* Request for an interface of a certain type.
* 
* Return value:  A value indicating wether there was an error or not
*
* Parameters:  
*
* const IDD& idd:  Id representing the type of the interface.
* void **ppv:  Will point on the interface.
*/
HRESULT _stdcall VietVoice::VietnamienVoice::QueryInterface (const IID& idd, void **ppv)
{
	
	if (idd == IID_IUnknown) // User asked for IUnknown interface
	{
		*ppv = static_cast <IUnknown *> (this);
	}
	else if (idd == IID_IVietVoice) // User asked for IVietVoice interface
	{
		*ppv = static_cast <IVietnameseVoice *> (this);
	}
	else // User asked for non existing interface for this object
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef() ; // Increment the reference count

	return S_OK;
}

/**
* A new reference to the object in used, increase reference count.
* 
* Return value:  A value indicating wether there was an error or not
*/
ULONG _stdcall VietVoice::VietnamienVoice::AddRef ()
{
	return ::InterlockedIncrement (&_ref);
}

/**
* One of the reference to the object is not used anymore, decrease reference count.
* 
* Return value:  A value indicating wether there was an error or not
*/
ULONG _stdcall VietVoice::VietnamienVoice::Release ()
{
	if (InterlockedDecrement (&_ref) == 0) // Decrement the ref count and if it is 0, delete the object (no more used)
	{
		delete this;
		return 0;
	}

	return _ref;
}



/**
* Special function used by the SpeakAsync method of the VietVoice::VietnamienVoice object
* as a thread that simply call the Speak method to read the text.  Since it is another thread,
* the program is not blocked.
*/
unsigned int WINAPI VietVoice::ThreadSpeak (void * param)
{
	VietVoice::ThreadSpeakParam * voiceParam = (VietVoice::ThreadSpeakParam *) param;

	voiceParam->_voice->Speak (voiceParam->_str, voiceParam->_pos); // speak the text

//	delete voiceParam; // Delete the parameter to avoid a memory leak.
	return 0;
}

/**
* The voice can have access to a called back interface used to report various things.
* This method is used to set the said callback interface.
*
* Return Value:  A value indicating wether there was an error or not
* 
* Parameters:
*
* IVietnameseVoiceCallback * callback:  The new callback interface
* 
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetCallback (VietVoice::IVietnameseVoiceCallback * callback)
{
	_callback = callback;
	return S_OK;
}

/* The voice speak in "element".  An element can be a word, sentence or phrase
*
* Return Value:  A value indicating wether there was an error or not
* 
* Parameters:
*
* Element elem:  The new type of element (word, sentence, phrase).
* 
*/
HRESULT __stdcall VietVoice::VietnamienVoice::SetElement (VietVoice::Element elem)
{
	_element = elem;

	// Modify the _tokenizer so it use the correct element as token
	switch (elem)
	{
		case VietVoice::WORD:
		{
			_tokenizer.SetSpace ( L"");
			_tokenizer.SetPunctuation (L".,:;?!\n…()[]{} \t\r\"\"“”<>«»'");
			break;
		}
		case VietVoice::SENTENCE:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'\n…()[]{},");
			_tokenizer.SetPunctuation (L".:;?!");
			break;
		}
		case VietVoice::PHRASE:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'");
			_tokenizer.SetPunctuation (L".,:;?!\n…()[]{}");
			break;
		}
		case VietVoice::PARAGRAPH:
		{
			_tokenizer.SetSpace (L" \t\r\"\"“”<>«»'.,:;?!…()[]{}");
			_tokenizer.SetPunctuation (L"\n");
			break;
		  }


	}
	return S_OK;
}

/**
* Obtains the minimal length for a word
* @return
*/
HRESULT __stdcall GetDurMoyenSilence (double & dur)
{
	return S_OK;

}


HRESULT __stdcall  VietVoice::VietnamienVoice::Init (HWND hwnd)
{
	
	return S_OK;
}

HRESULT __stdcall VietVoice::VietnamienVoice::IsSpeaking (bool & isSpeaking)
{
	::EnterCriticalSection (&_critical);
	isSpeaking = _isSpeaking;
	::LeaveCriticalSection (&_critical);
	
	return S_OK;
}
