
#include "vietnameseTTSException.h"

VietVoice::VietnameseTTException::VietnameseTTException (std::wstring const & msg)
	:  _msg(msg)
{}

VietVoice::VietnameseTTException::~VietnameseTTException ()
{}

VietVoice::VietnameseTTException::VietnameseTTException  (const VietnameseTTException & ex)
{
	if (this != &ex)
	{
		_msg = ex._msg;
	}
}

void VietVoice::VietnameseTTException::operator = (const VietnameseTTException & ex)
{
	if (this != &ex)
	{
		_msg = ex._msg;
	}		
}
		
std::wstring VietVoice::VietnameseTTException::GetMsg() const
{ 
	return _msg;
}