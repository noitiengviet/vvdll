
#include "tokenizer.h"

// Constructor
VietVoice::Tokenizer::Tokenizer ()
{
	_space  = L" \t\r\"\"��<>��'";
	_punctuation = L".,:;?!\n�()[]{}";
	_tokenize = L"";
	_pos = 0;
}

VietVoice::Tokenizer::~Tokenizer ()
{}

VietVoice::Tokenizer::Tokenizer  (const Tokenizer & tokenizer)
{
	if (this != &tokenizer)
	{
		_space = tokenizer._space; 
		_punctuation = tokenizer._punctuation; 
		_tokenize = tokenizer._tokenize; 
		_pos = tokenizer._pos;
	}
}

void VietVoice::Tokenizer::operator = (const Tokenizer & tokenizer)
{
	if (this != &tokenizer)
	{
		_space = tokenizer._space; 
		_punctuation = tokenizer._punctuation; 
		_tokenize = tokenizer._tokenize; 
		_pos = tokenizer._pos;
	}
}

/**
* Sets the string to be divided in tokens
* @param tokenize The string that will be divide din tokens
*/
void VietVoice::Tokenizer::SetString (wstring tokenize)
{
	_tokenize = tokenize;
	_pos = 0;
}

/**
 * Sets the caracters that are recognized as spaces
 * @param space:  List of the caracters recognized as spaces
 */
void VietVoice::Tokenizer::SetSpace (wstring space)
{
	_space = space;
}

/**
 * Sets the caracters that are recognized as punctuation
 * @param space:  List of the caracters recognized as punctuation
 */
void VietVoice::Tokenizer::SetPunctuation (wstring punctuation)
{
	_punctuation = punctuation;
}

/**
* Determines if there are more tokens in the string
* @return true if there are more tokens, else false
*/
bool VietVoice::Tokenizer::HasMoreToken ()
{
	return _pos < _tokenize.length ();
}

/**
 * Determine if a particular character is a space or not
 * @param myChar:  the char which is either a space or not
 * @return True if its a space, else false
 */

bool VietVoice::Tokenizer::CharIsSpace (wchar_t myChar)
{
	
	for (unsigned int i = 0; i < _space.length (); i++)
	{
		if (_space [i] == myChar)
			return true;
	}

	return false;
}

/**
 * Determine if a particular character is a punctuation or not
 * @param myChar:  the char which is either a space or not
 * @return True if its a space, else false
 */

bool VietVoice::Tokenizer::CharIsPunc (wchar_t myChar)
{
	for (unsigned int i = 0; i < _punctuation.length (); i++)
	{
		if (_punctuation [i] == myChar)
			return true;
	}

	return false;
}

/**
 * Obtains the next token in the string
 * @return The next token in the string
 */
bool VietVoice::Tokenizer::GetNextToken (Token & token)
{
	int begin = 0;
	wstring pre;
	wstring post;
	wstring ok;
			
	token.SetPrePunctuation (L"");
	token.SetPostPunctuation (L"");

	if (!HasMoreToken ()) // End of string, return null
	{
		token.SetToken (L"");
		return false ;
	}

	// Find begining of next token (or end of string)
	while (_pos < _tokenize.length () && CharIsSpace (_tokenize [_pos]) == true || CharIsPunc (_tokenize [_pos]) == true)
	{
		_pos++;
	}

	begin = _pos;
	_pos++;

	if (!HasMoreToken ()) // If end of string, return null
	{
		token.SetToken (L"");
		return false;
	}

	bool continu = false;
	bool dot = false;

	// Find the end of the token.
   do
   {
		continu = false;
		// Find the next punctioan or space.
		while (_pos < _tokenize.length() &&
			CharIsSpace (_tokenize [_pos]) == false &&
			CharIsPunc (_tokenize [_pos]) == false)
		{
			_pos++;
		}

		// If we reach a dot or coma in middle of number, must continue.
		if (_pos < _tokenize.length() && _pos + 1 < _tokenize.length() && (_tokenize[_pos] == L'.' || _tokenize [_pos] == L',') && _tokenize[_pos + 1] >= L'0' && _tokenize[_pos + 1] <= L'9')
		{
			continu = true;
			_pos++;
		}

		if (_pos < _tokenize.length() && _pos + 1 < _tokenize.length() && _tokenize[_pos] == L'.' && _tokenize[_pos + 1] >= L'a' && _tokenize[_pos + 1] <= L'z')
		{
			continu = true;
			_pos++;
		}

	} while (continu);

	while (_pos > _tokenize.length ())
		_pos--;

	if (begin > 0)
		pre = _tokenize.substr (begin - 1, 1); // Find the preceding symbol (space or punc)
	else
		pre = L"";

	post = _tokenize.substr (_pos, 1) ; // Finds the symbol after the token (space or punc)

	// If punctuation before token, set the prepunctuation data
	if (CharIsPunc (pre[0]) == true)
	{
		token.SetPrePunctuation (pre);
	}

	// If punctuation after token, set the postpunctuation data
	if (CharIsPunc (post[0]) == true)
	{
		token.SetPostPunctuation (post) ;
	}

	token.SetToken (_tokenize.substr (begin, _pos - begin)) ;

	_pos++;
	
	if (token.GetToken () == L"")
	{
		token.SetToken (L"");
		return false;
	}

	return true;
}

/**
* Obtains the current possition
* @return:  The possition where the tokenizer is currently at.
*/
int VietVoice::Tokenizer::GetCurrentPos ()
{
	return _pos;
}