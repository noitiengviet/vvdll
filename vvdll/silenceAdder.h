#if !defined (SILENCEADDER_H)

#define SILENCEADDER_H
#include <map>
#include <vector>
#include "useunicode.h"
#include "treatment.h"
#include "wordselector.h"
using namespace std;
namespace VietVoice
{
	/**
	* Class Name:         SilenceAdder.java
	*
	* Author(s):          Benoit Lanteigne
	*
	* Creation Date:      October 22 2007
	*
	* Description:        Class used to find the accent of a word
	*
	* Copyright:          All rights reserved Moncton University
	*                     Tang-Ho L� Ph. D. - Computer Science Department
	*
	*/

	/**
	* CODE MODIFICATIONS AND BUG CORRECTIONS
	* --------------------------------------
	* 
	*/

	class SilenceAdder : public Treatement
	{
	public:

		/**
		* Constructor
		*
		* Parameters:
		*
		* VectorAccent & va: A reference to the VectorAccent object use to determine the accent of a word
		* std::wstring extra:  Name of the file containign the "extra", better described as the words from
	    *					   the 3rd database
		*/
		SilenceAdder (std::wstring adjectiveName,std::wstring lightName,std::wstring prosodyName, std::wstring silenceDictionaryName2, std::wstring silenceDictionaryName3, std::wstring silenceDictionaryName4, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, std::wstring linkingFile, std::wstring quantityFile, std::wstring nounFile, std::wstring verbFile, WordSelector & _selector);
//		SilenceAdder (std::wstring prosodyName, std::wstring silenceDictionaryName, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, WordSelector & _selector);

		/**
		* Copy constructor
		*/
		SilenceAdder  (const SilenceAdder & silenceAdder);

		/**
		* Destructor
		*/
		virtual ~SilenceAdder ();

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const SilenceAdder & silenceAdder);

		/**
		* Find the accent for every word in the utterance.
		*
		* Return value:  The data to be spoken after modification
		*
		* Parameters:
		* ToBeSpoken toSpoke: A reference to the data to be spoken that is being processed
		* 
		*/

//std::vector<SilenceAdder*> SilenceAdder::m_nounDictionary;
//static bool VietVoice::SilenceAdder::v_find_Noun(std::wstring v_exp) ; // VT 2016

		void Treat (ToBeSynthetise & toSynthetise);
		long VietVoice::SilenceAdder::v_getBreakSilence1(); // VT
		long VietVoice::SilenceAdder::v_getBreakSilence2(); // VT
		long VietVoice::SilenceAdder::v_getVolume(); // VT
		long VietVoice::SilenceAdder::v_get_main_vol(); // VT
		long VietVoice::SilenceAdder::v_get_soft_vol1(); // VT
		long VietVoice::SilenceAdder::v_get_soft_vol21(); // VT
		long VietVoice::SilenceAdder::v_get_soft_vol22(); // VT
		long VietVoice::SilenceAdder::v_get_soft_vol3(); // VT
		long VietVoice::SilenceAdder::v_getSpeed(); // VT
		long VietVoice::SilenceAdder::v_getPitch(); //// VT
		long VietVoice::SilenceAdder::v_getCompress(); //// VT
		long VietVoice::SilenceAdder::v_getWordSize(); // VT
		long VietVoice::SilenceAdder::v_getWordSize2(); // VT
		void VietVoice::SilenceAdder::v_getLight(std::wstring &v_lws); // VT
		void VietVoice::SilenceAdder::v_getAdjective(std::wstring &v_ads); // VT
		bool VietVoice::SilenceAdder::v_isStopConsonant(std::wstring v_word, int v_accent); // VT
		long VietVoice::SilenceAdder::v_getCW1(); // VT
		long VietVoice::SilenceAdder::v_getCW2(); // VT
		long VietVoice::SilenceAdder::v_getStopSilence(); // VT
		long VietVoice::SilenceAdder::v_getDots() ; // VT
long VietVoice::SilenceAdder::v_getNotEcho() ; // VT
		long VietVoice::SilenceAdder::v_Fade() ; // VT
		long VietVoice::SilenceAdder::v_Samples() ; // VT
	long VietVoice::SilenceAdder::v_get_per_pitch() ; // VT	
	long VietVoice::SilenceAdder::v_get_pitch0() ; // VT
	long VietVoice::SilenceAdder::v_get_pitch1() ; // VT
	long VietVoice::SilenceAdder::v_get_pitch2() ; // VT
	long VietVoice::SilenceAdder::v_get_pitch3() ; // VT
	long VietVoice::SilenceAdder::v_get_pitch4() ; // VT
	long VietVoice::SilenceAdder::v_get_pitch5() ; // VT

	long VietVoice::SilenceAdder::v_get_com0() ; // VT
	long VietVoice::SilenceAdder::v_get_com1() ; // VT
	long VietVoice::SilenceAdder::v_get_com2() ; // VT
	long VietVoice::SilenceAdder::v_get_com3() ; // VT
	long VietVoice::SilenceAdder::v_get_com4() ; // VT
	long VietVoice::SilenceAdder::v_get_com5() ; // VT
long VietVoice::SilenceAdder::v_get_c_begin() ; // VT
long VietVoice::SilenceAdder::v_get_c_end() ; // VT
long VietVoice::SilenceAdder::v_get_sc_comp() ; // VT


long VietVoice::SilenceAdder::v_getLinkingSilence(); // VT



vector <wstring> SilenceAdder::m_nounDictionary; ///////////////////////////////////////// static unresolved error





		std::vector<std::wstring> m_quantityDictionary;
		std::vector<std::wstring> m_verbDictionary;
		static int VietVoice::SilenceAdder::v_count_words(std::wstring v_exp);

 
	private:

		/**
		* Read all extra words from a file
		*
		* Parameters:
		*
		* std::wstring filename: filename The name of the file
		*/

		void _LoadSilencesSize(std::wstring filename);
		void _LoadWordsSize(std::wstring filename);

		int ReadIntFromFile(File::Reader & reader);

	protected:





		std::map <std::wstring, std::wstring> _silenceDictionnary2; // Contain all special words (words from the third database).
		std::map <std::wstring, std::wstring> _silenceDictionnary3; // VT
		std::map <std::wstring, std::wstring> _silenceDictionnary4; // VT
		std::vector<std::wstring> m_articlesDictionary;
		std::vector<std::wstring> m_linkingDictionary;




		WordSelector * _selector;

		long smallWord;
		long mediumWord;
		long longWord;
		
		long smallSilence;
		long mediumSilence;
		long mediumSilence2;
		long longSilence;
		long m_silenceSize;
		long m_silenceSize2;

		long tooSmall;
		long v_break_silence1; // VT
		long v_break_silence2; // VT
		float v_break_compound; // VT
		long v_compound_min; // VT
		int v_stop_silence; // VT
		long v_volume; // VT
		long v_main_vol; // VT
		long v_soft_vol1; // VT
		long v_soft_vol21; // VT
		long v_soft_vol22; // VT
		long v_soft_vol3; // VT
		long v_dots; // VT
		long v_not_echo; // VT
		long v_speed; // VT
		long v_pitch; // VT
		long v_compress; // VT
		long v_adjectives_sl; // VT
		long v_linking_sl; // VT
		long v_number_sl; // VT
		long v_fade; // VT
		long v_samples; // VT

		int v_per_pitch; // VT
		int v_pitch0; // VT
		int v_pitch1; // VT
		int v_pitch2; // VT
		int v_pitch3; // VT
		int v_pitch4; // VT
		int v_pitch5; // VT

		int v_compress0; // VT
		int v_compress1; // VT
		int v_compress2; // VT
		int v_compress3; // VT
		int v_compress4; // VT
		int v_compress5; // VT
		int v_cut_begin ; // VT
		int v_cut_end ; // VT
		int v_sc_comp ; // VT

		std::wstring v_light_Words ; // VT
		std::wstring v_adjective_Words ; // VT
	};
}

#endif