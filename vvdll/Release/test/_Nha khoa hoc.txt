Nhiều nhà khoa học “ngủ quên” trong ảo tưởng bao cấp

(Dân trí) - Nhiều nhà khoa học phàn nàn Nhà nước chưa đầu tư xứng đáng cho công tác nghiên cứu khoa học. Dù có thực tế này, nhưng ngược lại rất nhiều người chỉ dùng chức danh khoa học để nhận lương hàng tháng mà không cống hiến gì.
Đó là chia sẻ của PGS.TS Nguyễn Thanh Tuấn về chiến lược phát triển khoa học, công nghệ nước nhà.
 
Trong chiến lược phát triển Khoa học và Công nghệ (KH&CN) giai đoạn 2011-2020 vừa được Thủ tướng Chính phủ phê duyệt, mục tiêu được đưa ra là đến năm 2020, KH&CN Việt Nam có một số lĩnh vực đạt trình độ tiên tiến, hiện đại của khu vực ASEAN và thế giới. Bộ Khoa học và Công nghệ cũng đặt mục tiêu, đến năm 2020, giá trị sản phẩm công nghệ cao và sản phẩm ứng dụng công nghệ cao đạt khoảng 45% GDP….

Tuy nhiên, theo Bộ trưởng Bộ KH&CN, ông Nguyễn Quân, để đạt được mục tiêu này, Việt Nam buộc phải giải quyết những khó khăn, thách thức vẫn đang là nỗi nhức nhối hiện nay. “Muốn KH-CN phát triển quốc gia nào cũng phải đầu tư. Nhưng hiện nguồn đầu tư cho lĩnh vực này của Việt Nam còn quá khiêm tốn. Mặc dù nhà nước dành 2% tổng chi ngân sách quốc gia hàng năm, nhưng tính ra giá trị tuyệt đối mới có khoảng gần 700 triệu USD, tính bình quân trên đầu người theo dân số, mỗi người chưa được 10 USD dành cho KH-CN. Đây là mức quá thấp so với các nước trên thế giới, kể cả các nước lân cận trong khu vực, trong khi Hàn Quốc có mức đầu tư trên đầu người tới 1.000 USD, hay Trung Quốc mức đầu tư bình quân đầu người đã vượt quá 30 USD”- Bộ trưởng trăn trở. 
 
Bộ trưởng Nguyễn Quân cho rằng đầu tư cho KH-CN hiện còn quá thấp so với yêu cầu.
 
"Không phát triển KH-CN, doanh nghiệp nói riêng và nền kinh tế nước nhà nói chung sẽ không thể phát triển theo hướng bền vững" - Đó là nhận định của ông Nguyễn Tăng Cường, Giám đốc xí nghiệp cơ khí Quang Trung. Thực tế đã chứng minh, xí nghiệp cơ khí Quang Trung với hơn 300 sáng kiến cải tiến kỹ thuật, trong đó có nhiều sáng kiến cấp nhà nước, việc phát triển KH&CN không chỉ giúp doanh nghiệp này nâng tỷ lệ nội địa hóa thiết bị của chính mình mà còn đưa thương hiệu của cơ khí Quang Trung trở thành doanh nghiệp uy tín hàng đầu trong lĩnh vực chế tạo cơ khí, với giá trị tài sản trên 600 triệu USD. Mới đây, với cụm công trình "Ứng dụng 5 giải pháp KHCN chế tạo các loại thiết bị nâng hạ tại Việt Nam, cá nhân ông Cường được trao Giải thưởng Hồ Chí Minh.

Với cách tính của một nhà đầu tư, ông Cường cho rằng, để đạt mục tiêu đến năm 2020 nền KHCN Việt Nam vươn lên đạt trình độ tiên tiến, hiện đại trong khu vực ASEAN và thế giới, trong khoảng 5 năm tới, Chính phủ cần đầu tư khoảng 100 tỉ USD cho cơ sở hạ tầng, nhân lực.

Tuy nhiên, trên thực tế, đây là một khoản kinh phí khó khả thi nếu so thực trạng đầu tư từ ngân sách nhà nước cho KHCN hiện đạt khoảng 700 triệu USD/năm, cộng thêm khoản đầu tư từ nguồn của doanh nghiệp đạt 350-400 triệu USD/năm, tổng đạt khoảng 1 tỷ USD/năm. “Nút tháo gỡ cơ bản hiện nay là cơ chế, nếu Chính phủ đưa ra cơ chế phù hợp, tạo điều kiện thỏa đáng để doanh nghiệp yên tâm đầu tư hơn nữa trong phát triển KH&CN, mục tiêu sẽ đạt được…”- ông Cường chia sẻ.
 
Về vấn đề này, Bộ trưởng Quân khẳng định, Chính phủ đang nỗ lực tạo cơ chế khuyến khích các doanh nghiệp và mọi tổ chức, cá nhân đầu tư kinh phí cho hoạt động khoa học và công nghệ như: miễn thuế xuất nhập khẩu công nghệ, miễn thuế thu nhập cá nhân từ nguồn hợp đồng nghiên cứu. Luật Thuế thu nhập doanh nghiệp sửa đổi đã quy định doanh nghiệp được trích tới 10% lợi nhuận trước thuế thành lập Quỹ phát triển khoa học và công nghệ của doanh nghiệp...

Bộ trưởng Nguyễn Quân cũng cho rằng, để phát triển KHCN chỉ riêng nguồn vốn của Chính phủ rất khó khăn, do đó, cần tăng được nguồn đầu tư  từ doanh nghiệp. "Chính phủ đã kêu gọi doanh nghiệp dành 10% lợi nhuận trước thuế cho phát triển KH&CN, nhưng rất ít doanh nghiệp làm được việc này. Nguyên nhân chính do chưa có chế tài đủ mạnh buộc doanh nghiệp và các tổ chức đầu tư cho công tác này. Cùng đó là thực trạng, các doanh nghiệp nhỏ có lợi nhuận trước thuế thấp, nên nguồn trích lập Quỹ phát triển KH&CN của doanh nghiệp cũng không đủ để đổi mới công nghệ của chính mình"- Bộ trưởng Nguyễn Quân nhìn nhận.

Nếu được đầu tư đúng hướng, nghiên cứu của các nhà khoa học sẽ thúc đẩy kinh tế xã hội mạnh mẽ.
 
Bộ trưởng Bộ KH&CN cho biết, Bộ đang kiến nghị với Chính phủ, Quốc hội đưa ra cơ chế huy động nguồn đóng góp của doanh nghiệp vào Quỹ phát triển khoa học và công nghệ của địa phương, của tỉnh. Như vậy, khi huy động được nguồn đóng góp của hàng nghìn doanh nghiệp tại địa phương, tỉnh sẽ có đủ nguồn đầu tư cho một số doanh nghiệp hàng đầu hay trọng yếu của địa phương đó để đổi mới công nghệ, tạo sản phẩm mới.
Một trong những vấn đề cũng được Bộ trưởng cho rằng cần có sự thay đổi lớn, đó là cơ chế chính sách dành cho đội ngũ làm khoa học nước nhà. Theo Bộ trưởng, do cơ chế quản lý và đầu tư cứng nhắc bao cấp nên đã tạo ra tình trạng không ít đề tài nghiên cứu khoa học chủ yếu giải quyết khâu công ăn việc làm, không có địa chỉ ứng dụng hoặc chất lượng sản phẩm đề tài nghiên cứu khoa học không cao hoặc không thể ứng dụng được trong thực tiễn.

Xác nhận thực trạng này, PGS.TS Nguyễn Thanh Tuấn - một trong 3 cá nhân đoạt giải thưởng Hồ Chí Minh với công trình lý luận về xây dựng Đảng- cho rằng, mặc dù nước ta đã chuyển sang cơ chế thị trường 20 năm nay, nhưng phần lớn đội ngũ khoa học nước nhà phần lớn vẫn “ngủ quên” trong cơ chế bao cấp.

“Rất nhiều nhà khoa học phàn nàn Nhà nước chưa đầu tư xứng đáng cho công tác nghiên cứu khoa học. Dù có thực tế này nhưng ngược lại cũng đang diễn ra tình trạng trong không ít người trong đội ngũ cán bộ khoa học chưa chịu làm việc và cống hiến thực sự, họ chỉ sống bằng danh, chấp nhận ăn lương hàng tháng. Trong khi đó, những người biết nắm bắt cơ hội, dám bơi trong cơ chế thị trường hoàn toàn có thể sống thoải mái bằng chất xám của mình”- ông Tuấn chia sẻ.

Đồng thuận với ý kiến này, ông Nguyễn Tăng Cường cho rằng, với nền kinh tế thị trường hiện nay, mỗi nhà khoa học nên trang bị  thêm cho mình vốn kiến thức quản trị- kinh doanh và sự năng động. Khi đó, công trình nghiên cứu thực sự có tính ứng dụng cao được “tiếp thị” bài bản sẽ tìm được nhà đầu tư tốt. Kết quả sẽ là lợi nhuận cho cả doanh nghiệp và nhà khoa học, kéo theo đó là sự phát triển bền vững về KHCN của toàn xã hội.

Thanh Trầm

