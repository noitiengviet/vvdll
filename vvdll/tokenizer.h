#if !defined (TOKENIZER_H)

#define TOKENIZER_H

#include "token.h"

#pragma warning( disable : 4786 ) 

namespace VietVoice
{
	/**
	 * Class Name:         Tokenizer
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 13 2004
	 *
	 * Description:        Class used to the data of token
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * January 28 2005-> Modification to the getNextToken method so
	 * number with space like 1 000 are divided properly
	 * september 17 2005:  Converted from java to C++
	 * septemper 19 2005  Corrected some bugs:
	 *                   -Now parse number correctly
	 *                   -Before, skipped the last word, not anymore
	 * November 29 2005 -> Added destructor in case
  	 * novembre 29 2005:  Implemented the copy constructor and the = operator
	 * March 4 2006:  Seperated implementation in source and header
	 * Mai 25 2006 -> Added the setSpace and setPunctuation method.
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 17 2006:  Improved comments in the .h file.
	 */

	/***
	 * Class used to divide a String in token
	 */
	class Tokenizer
	{
	public:

		/**
		* Constructor
		*/
		Tokenizer ();

		/**
		* Destructor
		*/
		~Tokenizer ();

		/**
		* Copy constructor
		*
		* Parameters;
		*
		* const Tokenizer & tokenizer:  Object that is been copyied.
		*/
		Tokenizer  (const Tokenizer & tokenizer);

		/**
		* Assignement operator implemented to avoid a memory leak.
		*/
		void operator = (const Tokenizer & tokenizer);

		/**
		 * Sets the string to be divided in tokens
		 * 
		 * Parameters:
		 *
		 * wstring tokenize: tokenize The string that will be divide din tokens
		 */
		void SetString (wstring tokenize);

		/**
		 * Sets the caracters that are recognized as spaces
		 *
		 * Parameters:
		 *
		 * wstring space:  List of the caracters recognized as spaces
		 */
		void SetSpace (wstring space);

		/**
		 * Sets the caracters that are recognized as punctuation
		 *
		 * Parameters:
		 *
		 * wstring punc:  List of the caracters recognized as punctuation
		 */
		void SetPunctuation (wstring);

		/**
		 * Determines if there are more tokens in the string
		 *
		 * return value: true if there are more tokens, else false
		 */
		bool HasMoreToken ();

		/**
		 * Determine if a particular character is a space or not
		 *
		 * return value: True if its a space, else false
		 *
		 * Parameters:
		 *
		 * wchar_t myChar:  the char which is either a space or not
		 */
		bool CharIsSpace (wchar_t myChar);

		/**
		 * Determine if a particular character is a punctuation or not
		 *
		 * @return True if its a space, else false
		 *
		 * @param myChar:  the char which is either a space or not
		 */
		bool CharIsPunc (wchar_t myChar);

		/**
		 * Obtains the next token in the string
		 *
		 * return value: The next token in the string
		 */
		bool GetNextToken (Token & token);

		/**
		 * Obtains the current possition
		 *
		 * return value:  The possition where the tokenizer is currently at.
		 */
		int GetCurrentPos ();

	protected:

		wstring _space; // Symbols representing a space
		wstring _punctuation; // Symbols representing a punctionation
		wstring _tokenize; // String to convert in token
		unsigned int _pos; // Current position in the string
	};
}

#endif