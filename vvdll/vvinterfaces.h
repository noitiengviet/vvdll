#if !defined INTERFACE_VIET_VOICE

#define INTERFACE_VIET_VOICE

#include <windows.h>
#include <basetyps.h>
#include <objbase.h>
#include <string>

// Macro used to facilitate the exporting of class, function, etc.
#ifdef _EXPORT_DLL_
    #define EXPORT_DLL extern "C" __declspec( dllexport )
#else
    #define EXPORT_DLL  extern "C" __declspec( dllimport )
#endif

namespace VietVoice
{
	const IID IID_IVietVoice =  // ID of VietVoice
	{ 0xa78380b, 0x1940, 0x4746, //Data1,Data2,Data3
	{ 0x8b, 0x3a, 0x5e, 0x68, 0x4, 0x53, 0xa2, 0x77 } };  //Data4

	enum Element {WORD = 0, SENTENCE = 1, PHRASE = 2, PARAGRAPH = 3};

	/**
	* Basic COM interface for the vietnamese Voice.
	*
	*/

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * March 14 2005 -> Created the file and the interface.
	 * March 15 2005 -> Added the macro EXPORT_DLL
	 * March 18 2006 -> Added the SpeakAsync method to the interface.  Added the IVietnameseVoiceCallback.
	 * September 11 2006 -> Added the SaveMp3 to the interface
	 * September 28 2006 -> Added the SetMaleVoice, etc to the interface
	 * April 10 2007 -> Added the SetDatabase to the interface
	 */
	interface IVietnameseVoiceCallback;

	interface IVietnameseVoice : public IUnknown
	{
		virtual HRESULT __stdcall Speak (std::wstring str, int pos = 0) = 0;
		virtual HRESULT __stdcall SpeakAsync (std::wstring str, int pos = 0) = 0;
		virtual HRESULT __stdcall Stop () = 0;
		virtual HRESULT __stdcall ReloadAbbreviations () = 0;
		virtual HRESULT __stdcall SetCallback (IVietnameseVoiceCallback * callback) = 0;
		virtual HRESULT __stdcall SaveMp3 (std::wstring str, std::wstring filename) = 0;
		virtual HRESULT __stdcall UseMaleVoice ()= 0;
		virtual HRESULT __stdcall UseFemaleVoice () = 0;
		/*virtual HRESULT __stdcall UseFemaleVoice_2 () = 0;*/
		virtual HRESULT __stdcall UseCustomerVoice () = 0;
		virtual HRESULT __stdcall SetDatabase (std::wstring path) = 0;
		virtual HRESULT __stdcall SetDatabase (std::wstring pathDatabase, std::wstring pathConfig) = 0;
		virtual HRESULT __stdcall IsSpeaking (bool & isSpeaking) = 0;
		virtual HRESULT __stdcall SetSpeed (float speed) = 0;
		virtual HRESULT __stdcall SetVolume (float volume) = 0;
		virtual HRESULT __stdcall SetPitch (float pitch) = 0;
		virtual HRESULT __stdcall SetWsola (bool wsola) = 0;
		virtual HRESULT __stdcall GetSpeed (float & speed) = 0; // !!!!! Dem xuong duoi khong chay !!!!
		virtual HRESULT __stdcall GetVolume (float & volume) = 0; // !!!!! Dem xuong duoi khong chay !!!!
		virtual HRESULT __stdcall GetPitch (float & pitch) = 0; // !!!!! Dem xuong duoi khong chay !!!!
		virtual HRESULT __stdcall GetWsola (bool & wsola) = 0; // !!!!! Dem xuong duoi khong chay !!!!
		virtual HRESULT __stdcall IsS (bool & isSpeaking) = 0;
	} ;

	/**
	* Callback interface for the vietnamese voice.
	*/
	interface IVietnameseVoiceCallback
	{
		virtual STDMETHODIMP OnStartSpeaking (IVietnameseVoice * voice) = 0;
		virtual STDMETHODIMP OnStopSpeaking (IVietnameseVoice * voice) = 0;
		virtual STDMETHODIMP OnElementSpoken (IVietnameseVoice * voice, int elementStart, int elementEnd) = 0;
	};

	/**
	* Creates a VietnamienVoice object and return a IUnknown pointer to it.
	*
	* Return value:  An IUnknwon pointer on the VietnamienVoice object that was created.
	*/
	EXPORT_DLL IUnknown * CreateInstance (const std::wstring path);
}

#endif