
#include "token.h"

/**
* Constructor
*/
VietVoice::Token::Token ()
{}

/**
* Destructor
*/
VietVoice::Token::~Token ()
{}

/**
* Copy constructor.
*
* Parameters:
*
* const Token & token:  Object that is bein copyied
*/
VietVoice::Token::Token  (const Token & token)
{		
	if (this != &token) // Make sure we aren't copying the object into itself
	{
		_token = token._token;
		_prepunc = token._prepunc;
		_postpunc = token._postpunc;	
	}
}

/**
* Assignement operator used to avoid a memory leak
*/
void VietVoice::Token::operator = (const Token & token)
{
	if (this != &token) // Make sure we aren't copying the object into itself
	{
		_token = token._token;
		_prepunc = token._prepunc;
		_postpunc = token._postpunc;
	}		
}

/**
* Changes the token name
*
* Parameters:
* 
* std::wstring token The new token name
*/
void VietVoice::Token::SetToken (std::wstring token)
{
	_token = token;
}

/**
* Obtains the token name
*
* return value: The token name
*/
wstring VietVoice::Token::GetToken ()
{
	return _token;
}

/**
* Sets the token's pre-punctuation
*
* Parameters
*
* wstring punc The new pre-punctuation
*/
void VietVoice::Token::SetPrePunctuation (wstring punc)
{
	_prepunc = punc;
}

/**
* Obtains the token prePunctuation
*
* return value: The token prePunctuation
*/
wstring VietVoice::Token::GetPrePunctuation ()
{
	return _prepunc;
}

/**
* Obtains the token postPunctuation
*
* return value: The token postPunctuation
*/
void VietVoice::Token::SetPostPunctuation (wstring punc)
{
	_postpunc = punc;
}

/**
* Obtains the token postPunctuation
*
* return value: The token postPunctuation
*/
wstring VietVoice::Token::GetPostPunctuation ()
{
	return _postpunc;
}

/**
* Determine if the object does not contain a token
*
* return value: true if the object is empty, else false
**/
bool VietVoice::Token::IsEmpty ()
{
	return _token.compare (L"") == 0;
}

/**
* Determine if the current token is followed by a
* punctuation mark
*
* return value; true if the object is followed by a punctuation mark, else false
**/
bool VietVoice::Token::HasPostPunc ()
{
	return _postpunc.compare (L"") != 0 ;
}