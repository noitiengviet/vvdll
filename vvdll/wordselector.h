#if !defined (WORD_SELECTOR_H)

#define WORD_SELECTOR_H

#pragma warning( disable : 4786 ) 


#include "stringhelper.h"
#include "worddatabase.h"
#include "treatment.h"

namespace VietVoice
{
	/**
	 * Class Name:         WordSelector.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        For each word to be spoken, gets the sound data from the databases
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	/**
	  * CODE MODIFICATIONS AND BUG CORRECTIONS
	  * --------------------------------------
	  * November 3 2005:  Started converting the file:  constructer, LoadFromFile, findWord2Accent, findWord6Accent, findWord
	  * November 5 2005:  converted the treat method
	  * November 29 2005 -> Added destructor in case
	  * novembre 29 2005:  Implemented the copy constructor and the = operator
	  * January 11 2005:  solved a minor error in the consonne2 data file that made kh and ch words unreadable
      * January 11 2005:  solved a minor problem in which the special keyword silenceSpecial would produce a beep instead of silence
	  * January 13 2005:  A bug in the findword6accent function caused words like a, e, o,u and ma, mu, etc to be not found.  It has bein corrected
	  * March 4 2006:  Seperated implementation in source and header
	  * March 6 2006:  Seperated implementation in source and header
	  * June 10 2006: Change the methods name so they start by a capital letter.
	  * September 29 2007 -> Modified method to set the database
	  */

	class WordSelector : public Treatement
	{

	public:

		/**
		 * Constructor.
		 *
		 * @param String datatabase Path leading to the database
		 * @param String index Path leading to the index of the database
		 * @param String syl Path leading to a file containing all vietnamese _syllables
		 * @param String cons1 Path leading to a file containing vietnamese consonant formed from one letter (d, m, b, ect)
		 * @param String cons2 Path leading to a file containing vietnamese consonant formed from two letter th, tr, ph, etc)
		 */
		WordSelector(std::wstring databaseMain, std::wstring index6Accents, std::wstring index2Accents, std::wstring databaseExtra, std::wstring indexExtra, std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2, std::wstring extra);

		/////////////Thuan add 13-02-2020/////////////
		WordSelector(std::wstring databaseMain, std::wstring index6Accents, std::wstring index2Accents, std::wstring databaseExtra, std::wstring indexExtra, std::wstring silence,
			std::wstring databaseMain2, std::wstring index6Accents2, std::wstring index2Accents2, std::wstring databaseExtra2, std::wstring indexExtra2,std::wstring silence2, 
			std::wstring databaseVeryShortWord, std::wstring indexVeryShortWord,std::wstring silenceVeyShort,
			std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2, 
			std::wstring extra,std::wstring veryShortWord);
		/////////// End Thuan add///////////////////


		///// Thuan add 26-05-2020////////////////
			WordSelector(std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2);
		/////////// End Thuan add///////////////
		virtual ~WordSelector ();

		WordSelector  (const WordSelector & ws);

		void operator = (const WordSelector & ws);

		/**
		 * Obtains the sound data from the database for the words that must be spokened.
		 *
		 * @param ToBeSpoken toSpoke
		 */
		void Treat(ToBeSynthetise & toSpoke);

		/**
		 * Finds a word in the database
		 * @param wordName Name of the word
		 * @param accent Accent of the word
		 * @return The data of the word
		 */
		Word * FindWord (std::wstring wordName, int accent,u_int type);

		/**
		 * Finds a word in the database for words with 6 accents
		 * @param wordName Name of the word
		 * @param accent Accent of the word
		 * @return The data of the word
		 */
		Word * FindWord6Accents (std::wstring wordName, int accent,u_int type);

		/**
		 * Finds a word in the database for words with 2 accents
		 * @param wordName Name of the word
		 * @param accent Accent of the word
		 * @return The data of the word
		 */
		Word * FindWord2Accents (std::wstring wordName,u_int type);

		Word * GetExtraWord (std::wstring wordName,u_int type);

		Word * GetVeryShortWord (std::wstring wordName);

		/**
		* Specify wich database to use for TTS (voice)
		*
		* Parameters:
		*
		* std::wstring index6Accents:  Path to the 6 accents index
		* std::wstring index2Accents:  Path to the 2 accents index
		* std::wstring indexExtra:  Path to the extra index
		* std::wstring database6Accents:  Path to the 6 accents database
		* std::wstring database2Accents:  Path to the 2 accents database
		* std::wstring databaseExtra:  Path to the extra database
		* std::wstring extra:  Path to the file listing all extrawords
		*/
		void SetDatabase (std::wstring accents6idx, std::wstring accents2idx, std::wstring specialidx, std::wstring accentsMain, std::wstring specialbin, std::wstring extra,std::wstring silence,u_int type);
		
		///////////////Thuan add 13-02-2020////////////////
		void SetDatabaseVeryShortWord(std::wstring index,std::wstring database,std::wstring pathFileVeryShortWord,std::wstring silence);
		/////////End Thuan add///////////////////////////////////


		/**
		* When a word is unknown, the first letter is spoken
		*
		* Parameters:
		*
		* std::wstring:  the unknwon word
		* std::list<VietVoice::ToBeSpokenData> & listData:  Contain the vocal (mp3) data
		*/
		void SpellFirstLetter (std::wstring word, std::list<VietVoice::ToBeSynthetiseData> & listData,u_int type);

		void VietVoice::WordSelector::LoadParam (std::wstring syl, std::wstring syl2, std::wstring cons1, std::wstring cons2);
		private:

			/**
			 * Method used to read the files containing the _syllable and consonnant
			 * @param filename Name of the file
			 * @param array Array containing the data of the file
			 */
			void LoadFromFile (std::wstring filename, std::wstring array [] , int size);
			
		private:

			VietVoice::WordDatabase _wordDatabase; // The database

			// Used to find the index in the database
			std::wstring _syllable [85];
			std::wstring _syllable2 [95];
			std::wstring  _consonne1 [23];
			std::wstring _consonne2 [23];
			float _durMoyen;
			int _nbSilencesAfter2Acc;

	};
}

#endif