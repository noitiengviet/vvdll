
#include "riffwriter.h"

RiffWriter::RiffWriter (std::wstring const & filename)
: File::Saver (filename)
{}

/**
* Each chunks start with a 4 bytes name used as a ID.  writeChunkName
* is used to write such a name from a file.
*
* std::string name:  A string containing the name of the chunk.
*/
void RiffWriter::SaveChunkName (std::string name)
{
	DWORD nbBytesWritten;

	if (name.size () > 4) // Make sure name is correct size
		throw File::Exception (TEXT("Invalid riff chunk name, must be 4 caracer or less"));

	if (::WriteFile (_file, (LPCVOID) &name [0], sizeof (char) * (DWORD) name.length (), &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}