Thứ Hai, 08/11/2010, 02:58 (GMT+7) 
Đồng lương thấp, cuộc sống khó khăn, công việc đơn điệu và tiềm ẩn nhiều rủi ro (sa thải, thất nghiệp, môi trường làm việc độc hại...), ít có cơ hội tiếp xúc với người khác giới, đổ vỡ chuyện tình cảm... khiến một bộ phận công nhân bị stress. Stress khiến sức khỏe giảm sút, sinh bệnh tật, tinh thần bất an và năng suất lao động kém đi. Càng ngày dấu hiệu công nhân bị stress càng nặng nề hơn. 

Stress “bào mòn” công nhân - Kỳ 1: Lạc trong vòng luẩn quẩn

TT - N.H.T., công nhân làm việc trong phòng thí nghiệm thủy sản của một công ty thuộc Khu công nghiệp Vĩnh Lộc (TP.HCM), mới 30 tuổi nhưng có triệu chứng mất ngủ, bất an, trướng bụng sau khi ăn và luôn mệt mỏi. “Tôi luôn cảm thấy ức chế” - anh nói.

 
Gần đây chị L.T.T. tìm kinh Phật để đọc. Chị bảo việc đó giúp mình tĩnh tâm, bớt căng thẳng - Ảnh: N.Nam
 


Công việc hằng ngày của T. là đến bộ phận chế biến lấy mẫu thành phẩm, nước, muối kiểm tra vi sinh. Cứ lặp đi lặp lại, quanh quẩn trong phòng thí nghiệm không có nhiều người. Thường mỗi ngày anh chỉ nói được vài ba câu. Các đồng nghiệp khác cũng vậy.

"Sáng thức dậy ăn sáng rồi đi làm, chiều tối về uống cà phê rồi đi ngủ. Người khác nhìn vào tưởng vậy là an nhàn, tốt đẹp nhưng tôi thì thấy tẻ nhạt quá, giống như một cái máy" 

Công nhân N.H.T.
 

Điều quan trọng nhất khiến anh căng thẳng, lo lắng là chuyện thu nhập. Anh cho rằng tiền lương nhận được không tương xứng với năng lực làm việc của mình. T. thường xuyên rơi vào tâm trạng cảm thấy thiếu thốn, phải vay mượn bạn bè nên luôn lo lắng. 

Những lúc như vậy anh thấy đời thật chán, tinh thần sa sút và không ham muốn làm việc gì. T. có ý định nghỉ việc. “Thời gian tới công ty vẫn không cải thiện thì tôi chắc chắn sẽ nghỉ việc. Tôi không thấy toại nguyện trong nghề nghiệp, công việc này không có khả năng thăng tiến”, T. nói. Anh đã tìm đến công đoàn công ty đề đạt một số nguyện vọng nhưng chỉ được ghi nhận và để đó nên càng làm anh không biết cậy vào đâu.

Thỉnh thoảng T. thấy cơn mệt mỏi tự dưng ập xuống khiến anh không muốn đi đâu và làm gì. T. sụt cân nhanh. Những biểu hiện khác của stress mà T. đang có là anh thấy mình thường dễ bị xúc động, suy nghĩ nhiều, khó khăn khi quyết định một vấn đề gì đó. T. luôn thấy bị đối xử tệ, anh càng ít nói chuyện với người khác hơn. Đôi lúc T. không muốn gặp ai, ngồi đâu đó một mình để thầm trách cuộc sống công nhân chán chường mà mình đang mang.

Chị L.T.T. (34 tuổi, quê Nghệ An) công nhân may trong Khu công nghiệp Tân Tạo, TP.HCM được 13 năm nay. Một ngày của chị bắt đầu lúc 5 giờ 30 sáng, ăn cơm tại phòng trọ rồi đi làm từ 7-11 giờ. Buổi trưa chị nghỉ được một tiếng rồi làm tiếp đến 4 giờ chiều. Điều làm chị mệt mỏi nhất là quan hệ với người quản lý. “Quản lý chỉ biết la mắng mà không chịu nghe giãi bày, khiến tôi nhiều khi thấy mình bị mắng oan và chịu nhục”, chị nói.

Điều khiến chị bận tâm nữa là vấn đề tình cảm. Chị đã chia tay với chồng khi mang thai con được 4 tháng. Nay con gái được gần 6 tuổi chị gửi về Nghệ An ở với ông bà ngoại. Nỗi lo lắng về tương lai con gái luôn thường trực trong chị. Bây giờ con đã đi học, chị phải chi tiêu rất tiết kiệm mới có thể dư ra từ đồng lương đi may ít ỏi để nuôi con. Chị không dám nghĩ đến những khoản giải trí, vui chơi cho bản thân. Cuộc sống vật chất thiếu thốn làm cuộc sống tinh thần của chị tẻ nhạt theo, quanh quẩn nơi khu trọ, trò chuyện với những công nhân ở đó sau giờ làm.

Chuyện tình cảm lần đầu khiến chị thấy mình sai lầm và luôn nghĩ về điều đó. Hiện chị đang có tình cảm với một người đàn ông khác hơn một năm nay, nhưng về sau phát hiện anh ta có quá khứ không tốt, làm nhiều chuyện vi phạm pháp luật và có mối quan hệ với nhiều phụ nữ khác. 

Những tâm tư rối bời ấy khiến chị suy nghĩ, lo lắng, đầu óc căng thẳng nhiều đêm không ngủ được. Những cơn nhức đầu ập đến ngày càng thường xuyên hơn. “Đôi khi tôi muốn buông xuôi mọi việc, suy nghĩ bất cần, nghĩ chuyện gì cũng thấy toàn những trường hợp xấu”, chị L.T.T cho hay.

Báo động stress trong công nhân

“16,4% công nhân bị stress ở mức độ thường xuyên, mức độ thỉnh thoảng là 60,6%” là kết quả nghiên cứu “vấn đề stress của công nhân ở một số khu chế xuất, khu công nghiệp trên địa bàn tp.hcm” của thạc sĩ tâm lý Đào Thị Duy Duyên, được thực hiện trên 378 công nhân đang làm việc tại các kcn Tân Tạo, Vĩnh Lộc và kcx Tân Thuận vào tháng 6-2010. Nguyên nhân ảnh hưởng đến mức độ stress của công nhân không chỉ giới hạn trong phạm vi nghề nghiệp mà còn do rất nhiều tác động khác như yếu tố thể lý, tâm lý, quan hệ xã hội, đời sống vật chất và tinh thần. 

Trong đó, những nguyên nhân cụ thể gây stress nhiều nhất cho công nhân là thu nhập thấp (75,1%), đời sống vật chất thiếu thốn (81,1%), lo lắng, lo sợ về nhiều vấn đề trong cuộc sống (61,7%), sống xa gia đình (72,5%), mâu thuẫn, gây gổ, giận hờn người yêu (69,7%), kinh tế gia đình khó khăn (75,2%).

Tỉ lệ công nhân bị stress khá cao nhưng những kiến thức, hiểu biết về stress của họ còn hạn chế, khi có 60,1% công nhân rất ít hoặc chưa hiểu biết về stress. Thực tế công nhân ít tìm đến sự hỗ trợ xã hội khi bị stress nhưng họ lại có nhu cầu rất cần sự hỗ trợ này. Trong đó, nhu cầu cao nhất của công nhân là được tạo bầu không khí làm việc vui vẻ, thoải mái (92,3%), tăng thu nhập (92,9%), tổ chức các lớp tập huấn, nói chuyện chuyên đề định kỳ để trang bị kiến thức về stress (81%).
 


NGUYỄN NAM
