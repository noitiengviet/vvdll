Khẩn cấp điều tra nguyên nhân vỡ đập Ia Krêl
12/06/2013 17:03 (GMT + 7)   
TTO - Liên quan đến việc công trình thủy điện Ia Krêl (huyện Đức Cơ, tỉnh Gia Lai) bị vỡ, ngày 12-6 ông Phạm Thế Dũng - chủ tịch UBND tỉnh Gia Lai - cùng các ban ngành đã trực tiếp đến hiện trường đoạn đập vỡ tìm hiểu sự việc và thăm hỏi người dân.

Hàng nghìn mét khối đất đá bị cuốn tấp ngổn ngang về phía hạ lưu đập - Ảnh: T. B. DŨNG

Chiều cùng ngày, UBND tỉnh Gia Lai đã tổ chức họp khẩn với sự tham dự của địa phương và các đơn vị liên quan.

Vỡ 40m đập thủy điện ở Gia Lai, hoa màu chìm trong nước
Tại buổi họp, đại diện các đơn vị cho biết tính đến chiều 12-6 việc cứu hộ đã hoàn thành, lực lượng chức năng đã cứu được khoảng 10 người thoát khỏi dòng nước lớn khi những người dân này chạy nạn bằng cách leo lên các thân cây lớn.

Vụ vỡ đập thủy điện xảy ra quá bất ngờ nên nhiều người dân làm rẫy ở hạ nguồn không kịp trở tay, hàng chục căn nhà, hơn 60ha hoa màu, nhiều ôtô và hàng trăm hecta cao su đã bị nước cuốn trôi.

Đại diện huyện đội và Công an huyện Đức Cơ cho biết để đảm bảo tuyệt đối việc cứu dân, lực lượng bộ đội vẫn tiếp tục dò tìm ở các khu vực nước còn ngập để tìm kiếm những người còn bị mắc kẹt.

Ông Phạm Thế Dũng cho biết từ khi xảy ra sự việc đến nay, chủ đầu tư là Công ty cổ phần Công nghiệp và thủy điện Bảo Long - Gia Lai không xuất hiện, không có mặt tại hiện trường để tham gia cứu hộ. “Trách nhiệm của chủ đầu tư như vậy là chưa cao” - ông Dũng nói.

Ông Phạm Thế Dũng cũng đốc thúc cơ quan điều tra, Sở Công thương, Sở Xây dựng tỉnh Gia Lai khẩn trương triển khai việc giám định chất lượng công trình, sớm có kết luận nguyên nhân xảy ra sự việc nói trên.

THÁI BÁ DŨNG
