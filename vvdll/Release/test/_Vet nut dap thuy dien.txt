Cần đầu tư máy móc tối tân để thăm dò các vết nứt thủy điện

(Dân trí) - Là người đã và đang tham gia xây dựng nhiều đập thuỷ điện, GS-VS Viện Hàn lâm khoa học Pháp, Bùi Huy Đường, cho rằng Việt Nam phải đầu tư máy móc, phương tiện, dụng cụ tối tân về kỹ thuật để thăm dò, khắc phục các vết nứt thuỷ điện.
Bên lề hội nghị quốc tế “Những thành tựu tiên tiến trong cơ học tính toán - Acome 2012” đang diễn ra tại trường ĐH Tôn Đức Thắng (Q.7, TPHCM), Giáo sư - Viện sĩ Bùi Huy Đường, đến từ trường ĐH Bách khoa Paris (Pháp), Viện sĩ Viện Hàn lâm châu Âu, đánh giá cao về sự phát triển của toán học Việt Nam. Tuy nhiên, vị giáo sư hàng đầu thế giới về Cơ học chất rắn, Cơ học phá huỷ cũng bày tỏ những băn khoăn về các dự án đập thuỷ điện và điện hạt nhân của Việt Nam.

GS.VS Đường đã, đang tham gia xây dựng nhiều đập thuỷ điện, trong đó có đập đang xảy ra tình trạng rò rỉ như đập thuỷ điện Sông Tranh 2 ở Bắc Trà My, Quảng Nam. GS.VS Bùi Huy Đường cho rằng, để khắc phục những vết nứt ở các đập thuỷ điện cần phải có những phương pháp vật lý thích hợp, sau đó mới giải bài toán ngược để tìm ra vết nứt ở các đập thuỷ điện. Muốn vậy, Việt Nam cần đầu tư để có các máy móc, phương tiện, dụng cụ tối tân về kỹ thuật để thăm dò vết nứt.


GS.VS Bùi Huy Đường cho rằng cần phải mời các chuyên gia cơ học tham gia vụ thuỷ điện sông Tranh 2 rò rỉ nước

Đối với nhà máy điện hạt nhân, cần chú ý đến sự bào mòn hay vết nứt của các lò hạt nhân. GS.VS Đường dẫn chứng những sự cố xảy ra trong các nhà máy điện hạt nhân ở Pháp. Theo đó, các ống dẫn bị bào mòn có liên quan đến cơ học chất rắn, cơ học chất lỏng và hoá học... Chính vì vậy, cần phải có đủ các máy móc thiết bị hiện đại để kiểm tra, đo đạc. Đồng thời, phải có chuyên gia tính toán được về cơ học vết nứt, tức là cơ học chất rắn cũng như tuân thủ tuyệt đối yêu cầu của chuyên gia về cơ học chất rắn. Tuy nhiên, Việt Nam hiện chưa có chuyên gia trong lĩnh vực này. Ngoài việc đào tạo nhiều nhà khoa học chuyên môn về cơ học chất rắn, về cơ học chất lỏng, về vật lý hạt nhân, hoá học, về thép, toán áp dụng, tính toán bằng máy, cơ học vật liệu... Để đảm bảo an toàn cho điện hạt nhân, Việt Nam có thể mời các chuyên gia, nhà khoa học của Pháp cộng tác.


Khắc phục sự cố rò rỉ thuỷ điện Sông Tranh 2
Hội nghị Acome 2012 (kéo dài từ 14-16/8) thay thế cho Hội nghị quốc tế thường niên về cơ học được tổ chức hai năm một lần tại Việt Nam. Đây là sự kiện hoạt động khoa học công nghệ có tầm ảnh hưởng trong khu vực và có sự tham gia của các chuyên gia hàng đầu thế giới trong lĩnh vực cơ học-kỹ thuật từ Pháp, Italy, Đức, Hàn Quốc, Nga, Nhật Bản, Singapore, Bỉ, Tây Ban Nha, Australia, Thái Lan, Mỹ và Việt Nam.

Tại hội nghị, các nhà khoa học trong và ngoài nước trao đổi kinh nghiệm và trình bày các kết quả nghiên cứu mới liên quan đến lý thuyết và ứng dụng trong lĩnh vực “cơ học – kỹ thuật”. Hội nghị này có 78 bài tham luận bao gồm các vấn đề về lý thuyết và ứng dụng các phương pháp tính toán và giải pháp cho các bài toán cơ học trên máy tính về những lĩnh vực cơ học phá hủy; thủy động lực học; cơ học vật liệu; cơ xây dựng. Trong đó sẽ có khoảng 25 bài tham luận đăng tải trên các tạp chí quốc tế.

Công Quang
