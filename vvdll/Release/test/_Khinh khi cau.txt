Khinh khí cầu triệu đô tại biển Nha Trang bị mưa giông xé toạc
(Dân trí) - Vụ việc xảy ra khoảng 15h chiều ngày 16/8. Chiếc khinh khí cầu trên bãi biển Nha Trang đã bị xé toạc trong mưa, giông, lốc lớn.
Theo ông Nguyễn Thiên Thanh - Giám đốc Khu vui chơi Khinh khí cầu (thuộc Công ty Cổ phần Hòn Tằm biển Nha Trang), chiếc khinh khí cầu này đưa vào sử dụng được gấn 4 tháng. Vào khoảng 15h chiều nay, 16/8, thấy mây đen và giông ập tới, ông Thanh đã lệnh cho đội kỹ thuật neo thấp quả cầu xuống cách mặt đất khoảng 5m.
 

Khinh khí cầu khi chưa gặp sự cố.
Tuy nhiên, giông, lốc lớn làm quả cầu chao qua, chao lại khoảng 4 phút quả cầu chịu không nổi. Do khí bị nén, quả cầu đã rách toang trong lúc trời đang mưa.

Theo ông Thanh, để đảm bảo an toàn cho du khách, khi có giông, khu du lịch không phục vụ nên may mắn đã không có ai bị thương. Thiệt hại hiện chưa thể ước tính được.

Hiện trạng khinh khí cầu khi bị mưa giông xé toạc.
Được biết, chiếc khinh khí cầu trên trị giá 1,2 triệu euro, do một công ty sản xuất kinh khí cầu của Đức sản xuất. Khinh khí cầu có đường kính 22m, có hệ thống đèn chiếu sáng ban đêm, bay lên cao bằng một sợi dây cáp chịu lực. Khinh khí cầu được bao quanh 46 tấm nhựa đặc biệt chống cháy và chịu được áp lực mạnh. Có 48 sợi neo nhỏ và 8 dây cáp lớn dài từ 150 đến 300 mét giữ chiếc khinh khí cầu với cabin hành khách.

Khoang hành khách làm bằng thép có sức chứa từ 25 đến 30 người cùng lúc bay lên độ cao 150m. Bên trong khí cầu chia làm 2 phần, một phần là khí hơi Helium và một phần là khí tự nhiên. Khoang hành khách là một bộ khung bằng thép vòng tròn, được giữ bởi những sợi dây tổng hợp với chiếc khinh khí cầu, bao quanh là lưới mắt cáo bảo đảm an toàn cho khách.

Trịnh Anh