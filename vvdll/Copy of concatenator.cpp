#include "concatenator.h"
#include "directsound.h"
#include "silenceadder.h" // VT
#include "math.h" // VT
/**
* Constructor
*/
VietVoice::WavePlayer::WavePlayer ()
{}

/**
* Play a wave represented by the given data.
*
* Parameters:
*
* vector<unsigned char> spokenData:  the wave data
*/
void VietVoice::WavePlayer::Play (std::vector<unsigned char> & spokenData)
{
			
	WAVEFORMATEX format;

	format.wFormatTag = WAVE_FORMAT_PCM ;
	format.nChannels = 1 ; 
	format.nSamplesPerSec = 44100 ; // 44100
	format.nAvgBytesPerSec = 88200 ; // 88200
	format.nBlockAlign = 2 ; // 2
	format.wBitsPerSample = 16 ; // 16
	format.cbSize = sizeof (format); 

	waveOutOpen( &_hWave, WAVE_MAPPER, &format, NULL, NULL, CALLBACK_NULL); 
	WAVEHDR wavehdr;
	wavehdr.lpData = (LPSTR)&spokenData[0];
	wavehdr.dwBufferLength = spokenData.size ();
	wavehdr.dwBytesRecorded = 0;
	wavehdr.dwUser = 0;
	wavehdr.dwFlags = 0;
	wavehdr.dwLoops = 0;

	waveOutPrepareHeader (_hWave, &wavehdr, sizeof (wavehdr));

	waveOutWrite  (_hWave, &wavehdr, sizeof (wavehdr));

	//while ((wavehdr.dwFlags & WHDR_DONE) == 0);
	while (waveOutUnprepareHeader (_hWave, &wavehdr, sizeof (wavehdr)) == WAVERR_STILLPLAYING)
	{
		::Sleep (100); // Viet xoa
	}

	waveOutClose (_hWave);
		
}

/**
* Stop playing the current wave
*/
void VietVoice::WavePlayer::Stop ()
{
	::waveOutReset (_hWave);
}

/**
* Constructor
*/
VietVoice::Concatenator::Concatenator()
{
	// Put in init method
	/*sound.Create (hwnd);
	sound.SetCoopPriority (hwnd);
			
	primaryBuffer.Create (sound);*/
//v_break_silence=0; // VT
}

/**
* Destructor
*/
VietVoice::Concatenator::~Concatenator ()
{}

/**
* Concategnate the sound data and play it
*
* @param toSpoke Data representing the words to be spoken
* @return The toSpoke data after the treatement
*/
void VietVoice::Concatenator::Treat (ToBeSynthetise & toBeSynthetise) 
{ 	
	std::list <ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
	std::list <ToBeSynthetiseData> newListData ;
	// std::wstring v_dec_vol = L"đề/ giờ/ đi/ đồ/ mình/ tôi/ ngờ/ bè/"; // VT
	// std::wstring v_dec_dur = L"đề/ giờ/ đi/ đồ/ mình/ tôi/ ngờ/ bè/"; // VT
	// std::wstring v_inc_vol = L"quốc/"; // VT
	int v_speed1 = v_speed / 1000 ; int v_speed2 = v_speed % 1000;
	//int m_dots = vdots / 1000 ; int m_per = vdots % 1000;
//::MessageBox (NULL, StringHelper::ToString (m_dots).c_str (), L"", MB_OK) ;
//::MessageBox (NULL, StringHelper::ToString (m_per).c_str (), L"", MB_OK) ;

	int size = toBeSynthetise.GetSizeWords();
	bool firstWord = true;
	int v_punc_break = 0;
				std::wstring v_end_1=L"a/ ă/ â/ e/ ê/ i/ o/ ô/ ơ/ u/ ư/ y/ m/ n" ;
				std::wstring v_end_2=L"ng/ nh" ;

				std::wstring v_begin_1 = L"b/ h/ l/ m/ n/" ;
				std::wstring v_begin_2 = L"ng/ nh/" ;
				int nsamp = m_samples ;
				unsigned v_wsize ;
//======================================================================================
	if (size != 0) // if 1 - Make sure there is sound data to be spoken (prevent a bug when user enter nothing) 
	{			
		vector<unsigned char> spokenData; // Viet move up
		// if 2
		// ------------------------------------------------------------------------------
		if (size > 0) // Makes sure there is some sound data // if 2
		{ 
//		int k = 0; // VX

		// For every word to be spoken, get the data and concatenate it
			std::list<ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
			std::list<Token> listToken;
			std::list<ToBeSynthetiseData> newListData;
			std::wstring v_WA ;
			bool v_continous=false ;
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
			{
				ToBeSynthetiseData data = *iter;
				Word * word = data.GetWordData();
				if (iter != listData.end()) {
					iter ++ ; 
					if (iter != listData.end()) v_WA= iter->GetOriginalWord() ; 
					iter -- ; 
				} // 
				else v_WA=L"" ;
// VT
				if (data.m_percentageAdjust == 49) data.m_percentageAdjust = v_compound1 ;  // v_compound1= % of nen tu 1
				if (data.GetSilenceLength() == 9999) data.SetSilenceLength(v_wordsize) ; // VT
				if (data.GetSilenceLength() == 0) {
					data.SetSilenceLength(v_wordsize) ; // VT
				}

				if (v_WA != L"" && v_isSilenceBefore(v_WA)) {
					v_wsize = data.GetSilenceLength() ;
					data.SetSilenceLength(v_wsize + v_StopSilence * 0.15f) ;
				}
// End VT
				if (data.GetWord().length() == 0) continue;
				if (data.GetPrePunctuation()==L"(" && toBeSynthetise.GetMustPlay()) {
					for (unsigned int j = 0; j < int((v_break_silence1 * 88.2) / 2) * 2; j++) // VT
						spokenData.push_back (0);
				}
				if (data.GetPostPunctuation()==L"," || data.GetPostPunctuation()==L"?" || data.GetPrePunctuation()==L"!" || data.GetPrePunctuation()==L":" || data.GetPrePunctuation()==L")") {
					v_punc_break = v_break_silence1;
				}
				else 
				if ((data.GetPostPunctuation()==L".") || (data.GetPostPunctuation()==L";"))
					v_punc_break = v_break_silence2 ;
				else v_punc_break = 0;
				// unsigned char * tmp = word->Get; // Get the sound data
				unsigned long sizeTmp = word->GetNbSamples () * word->GetBlockAlign ();
				//unsigned long wordCut = word->GetNbSamples () * data.GetWordCut (); // VX VLL
				//wordCut = wordCut * word->GetBlockAlign(); // VX VLL
//::MessageBox (NULL, StringHelper::ToString (data.GetWordCut()).c_str (), StringHelper::ToString (data.GetOriginalWord()).c_str (), MB_OK) ;
				//sizeTmp -= wordCut; // VX VLL
// Concategnate the sound data the number of time necessary
//for (int repeat = 0; repeat < data.GetNbRepeats(); repeat++) 
// ====================================================================
// Xu ly compress words
// ====================================================================
				int v_max_noise = 500 ; // 1000
				// unsigned mBegin = sizeTmp * (100 - v_speed ) / 100; // VT 200
				unsigned mBegin = sizeTmp * (100 - v_speed1 ) / 100; // VT 200
				if (v_isShortStartWord(data.GetWord()) == true) mBegin = sizeTmp * (100 - v_speed1 ) / 200;
				mBegin = mBegin / 2 * 2; // VT
				unsigned int j = mBegin; unsigned char mB1; unsigned char mB2;
				long mTmp; long mTmp1; long mTmp2 ; long mTmp_1; long mTmp_2;
				if (v_speed1 < 100) 
				{
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ; // VT
					bool mStop = false;
					while (mStop==false) 
					{
						j += 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= v_max_noise) // 0
						{
							mStop = true; 
							mBegin = j;
							if (abs(mTmp_1) < abs(mTmp_2)) mBegin -= 2;
						}
						else 
							mTmp_1 = mTmp_2;
					} // End while
				}
				unsigned mEnd = (int) ::ceilf(sizeTmp * (v_speed2) / 100) - 1 ; // VT  
				mEnd = mEnd / 2 * 2; // VT

				if (v_speed2 < 100)
				{
					j = mEnd;
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ;
					bool mStop = false;
					while (mStop==false)
					{
						j -= 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= v_max_noise) // 0
						{
							mStop = true ; 
							mEnd = j;
							if (abs(mTmp_1) < abs(mTmp_2)) 
								mEnd += 2 ;
						}
						else 
							mTmp_1 = mTmp_2 ;
					} // End while
				}

				vector<long> v_word ;
				for (unsigned int j = mBegin; j < mEnd; j++) // VT
				{
// **************************************************************************************
					if (j % 2==0)
					{	
						mTmp = word->GetByte(j) -5 ;//////// v_pitch1 ; // -5;
						if (mTmp < 0) mTmp += 255 ;
					}
					else
					{	
					if (word->GetByte(j) - 5 < 0) 
						mTmp = mTmp + (word->GetByte(j) - 5 + 255) * 256 ;
					else					
						mTmp = mTmp + (word->GetByte(j) - 5) * 256 ;
					v_word.push_back (ByteToWord(mTmp)) ;
					}
				} // End for j

				std::wstring v_word_name = data.GetOriginalWord() ;

				if (v_lightWords.find(data.GetOriginalWord(),0) != string::npos) {
					Change_Volume(v_word, v_volume) ;
					// data.m_percentageAdjust = v_compress + (50 - v_compress)  ; // New new new new
				}

				float v_beta=0;
				v_beta=(1+ (float) v_compress/100) * (float) (-v_pitch + 100) / 100 ;
				if (data.m_percentageAdjust != 0) {
					if (data.m_percentageAdjust > 50) {
						v_beta += (float) ((data.m_percentageAdjust - 50)/100) ; }
					else if (data.m_percentageAdjust < -50) {
						v_beta += (float) ((data.m_percentageAdjust + 50)/100) ; }
					else {
						v_beta += (float) ((data.m_percentageAdjust)/100) ;
					}
				}
				// if (v_dec_dur.find(data.GetOriginalWord(),0) != string::npos) v_beta +=0.1 ;
				if (v_isStopConsonant(data.GetOriginalWord(),data.GetAccent()) && v_word.size()>=7000) {
					v_beta += 0.15f ;
					data.SetSilenceLength(data.GetSilenceLength() + v_word.size() * 15 / 100) ;
				}

				int v_dis = v_per_pitch ; // 45
				if (data.GetAccent()==0) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch0, v_dis, 3); 
					v_beta += (float) v_compress0 / 100 * v_dis / 100; Change_Volume(v_word, 107) ;
				}// tăng cuối từ -10 / +0.10f
				else if (data.GetAccent()==1) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch1, v_dis, 3); 
					v_beta += (float) v_compress1 / 100 * v_dis / 100; 
				}// tăng cuối từ -6  / +0.06f
				else if (data.GetAccent()==2) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch2, v_dis, 3); 
					v_beta += (float) v_compress2 / 100  * v_dis / 100; Change_Volume(v_word, 92) ;
				}// giảm cuối từ -3  / -0.06f
				else if (data.GetAccent()==3) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch3, v_dis, 3); 
					v_beta += (float) v_compress3 / 100 * v_dis / 100; 
				}// giảm cuối từ -10 / -0.06f
				else if (data.GetAccent()==4) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch4, v_dis, 3); 
					v_beta += (float) v_compress4 / 100 * v_dis / 100; 
				}// giảm cuối từ -10 / -0.06f
				else if (data.GetAccent()==5) {
					if (data.GetPostPunctuation() != L"," && data.GetPostPunctuation() != L"." && data.GetPostPunctuation() != L")") 
						Change_Pitch(v_word, v_pitch5, v_dis, 3); 
					v_beta += (float) v_compress5 / 100 * v_dis / 100; Change_Volume(v_word, 90) ;
				}// giảm cuối từ -4   / -0.04f

				if (data.GetPostPunctuation() == L"." || data.GetPostPunctuation() == L"," || data.GetPostPunctuation() == L":") {
					if (data.m_percentageAdjust > 0) v_beta -= 0.15f; // gian tu chua gian o cuoi cau 
				}
//--------------------------------------------------------------------------
// In the past, envelop here
//--------------------------------------------------------------------------
// Change pitch here

				////if (v_isSmallVolStart(data.GetWord())) 
					////Change_Volume_CW(v_word, v_soft_volume1, 30, 1) ; // New // 49
				////else	Change_Volume_CW(v_word, v_soft_volume1, 30, 1) ; // New // 49
				////Change_Volume_CW(v_word, v_soft_volume3, 30, 3) ;

				if (v_continous == true && v_isSmallVolStart(data.GetWord())) 
					Change_Volume_CW(v_word, v_soft_volume1, 40, 1) ;
				else
					Change_Volume_CW(v_word, v_soft_volume1 * 2 / 3, 40, 1) ;
				if (data.GetSilenceLength() == v_wordsize && iter != listData.end() && data.GetPostPunctuation() != L",")  {
					v_continous = true ; 
//::MessageBox (NULL, StringHelper::ToString (data.GetOriginalWord()).c_str (), L"Continious...", MB_OK) ;
				}
				else v_continous = false;
				if (v_continous == true) Change_Volume_CW(v_word, v_soft_volume3, 30, 3) ;
				else Change_Volume_CW(v_word, v_soft_volume3 * 2 / 3, 30, 3) ;

				if (data.m_percentageAdjust > 50) { // New--
					data.m_percentageAdjust -= 50 ; // New-- 
					if (v_end_1.find(data.GetWord().substr(data.GetWord().size()-1,1),0) != string::npos || v_end_2.find(data.GetWord().substr(data.GetWord().size()-2,2),0) != string::npos) { // || v_end_3.find(data.GetWord().substr(data.GetWord().size()-3,3),0) != string::npos
//						if (v_soft_volume21!=0) Change_Volume_CW(v_word, v_soft_volume21, 30, 2) ; // New // -10
//						if (v_soft_volume22!=0) Change_Volume_CW(v_word, v_soft_volume22, 40, 2) ; // New // -20
//::MessageBox (NULL, StringHelper::ToString (data.GetWord()).c_str (), L"end", MB_OK) ;
						////if (v_soft_volume3!=0) Change_Volume_CW(v_word, v_soft_volume3, 30, 3) ; // New // 49
					}
					//Change_Pitch(v_word, 2) ;
				} // New--
				else if (data.m_percentageAdjust < -50) { // New--
					data.m_percentageAdjust += 50 ; // New--
//::MessageBox (NULL, StringHelper::ToString (data.GetWord()).c_str (), L"begin", MB_OK) ;
					if (v_begin_1.find(data.GetWord().substr(0,1),0) != string::npos || v_begin_2.find(data.GetWord().substr(0,2),0) != string::npos) { //  || v_begin_3.find(data.GetWord().substr(0,3),0) != string::npos
						////if (v_soft_volume1!=0) Change_Volume_CW(v_word, v_soft_volume1, 30, 1) ; // New // 10
//						if (v_soft_volume21!=0) Change_Volume_CW(v_word, v_soft_volume21, 30, 2) ; // New // -10
//						if (v_soft_volume22!=0) Change_Volume_CW(v_word, v_soft_volume22, 40, 2) ; // New // -20
					}
					//Change_Pitch(v_word, -3) ; // New--
				}
				else if (data.m_percentageAdjust == 0) { // New--
					////Change_Volume_CW(v_word, 10, 30, 1) ; // New //
					////Change_Volume_CW(v_word, 3, 30, 2) ; // New //
					////Change_Volume_CW(v_word, 26, 30, 3) ; // New //
				}

				if (v_isStopConsonant(data.GetOriginalWord(),data.GetAccent())) {
					// Change_Volume_CW(v_word, -30, 0, 3) ;
					Change_Pitch(v_word, -5, 55, 3) ;
					Change_Volume(v_word, 70) ;
					if (v_wordsize >=12000) {
						v_beta += 0.2f ;
						data.SetSilenceLength(data.GetSilenceLength() + v_StopSilence * 0.5f);
					}
					if (data.GetSilenceLength() <= v_StopSilence)
						data.SetSilenceLength(data.GetSilenceLength() + v_StopSilence);
//::MessageBox (NULL, StringHelper::ToString (data.GetOriginalWord()).c_str (), StringHelper::ToString (data.GetSilenceLength()).c_str (), MB_OK) ;
				}

				if (v_isSameAsSC(data.GetOriginalWord(),data.GetAccent())) {
					if (data.GetSilenceLength() <= v_StopSilence)
						data.SetSilenceLength(data.GetSilenceLength() + v_StopSilence * 0.4f);
				}
/*
				if (v_dec_vol.find(data.GetOriginalWord(),0) != string::npos) {
					Change_Volume(v_word, 65) ;
				}
				if (v_inc_vol.find(data.GetOriginalWord(),0) != string::npos) {
					//Change_Volume(v_word, 125) ;
				}
*/
/*
FILE *tmp_fptr=NULL;
tmp_fptr=fopen("tmptt1.txt","wb");
 
for(int k=0;k<v_word.size();k++)
{	
	fprintf(tmp_fptr,"%d\n",v_word[k]);
}
*/

//				v_echo(v_word, vdots, 2, 0) ;
				Change_Pitch(v_word, v_pitch, 0, 2) ;

v_FadeIn(v_word, m_fade);
v_FadeOut(v_word, m_fade);

//--------------------------------------------------------------------------
				WAV_INFO in_info;
				in_info.data=(float *)malloc((v_word.size())*sizeof(float));
				for (int k=0; k < v_word.size(); k++) {
					in_info.data[k]=float (v_word[k]);
				}

				WAV_INFO out_info;
				long buflen=(long)(1.5f+1.0f/v_beta);
				out_info.data=(float *)malloc(buflen*(v_word.size())*sizeof(float));
				memset(out_info.data,0,buflen*(v_word.size())*sizeof(float));
				in_info.Fs=44100; in_info.Nsamples=v_word.size();

				GET_FINALE_DATA( &in_info, &out_info, v_beta, nsamp);
				// GET_FINALE_DATA( &in_info, &out_info, 1.05f, nsamp);
				// GET_FINALE_DATA( &in_info, &out_info, 0.95f, nsamp);
				v_word.clear();
				int v_k = 0;
				while (out_info.data[v_k] * out_info.data[v_k+1] > 0) v_k++ ;
				for (int k=v_k; k < out_info.Nsamples; k++) {
					v_word.push_back(out_info.data[k]);
				}

				free(in_info.data); // VT
				free(out_info.data); // VT
//--------------------------------------------------------------------------
				// v_echo(v_word, m_dots, 2, m_per) ; // New: move here


				v_echo(v_word, vdots, 2, vnotEcho) ; // New: move here

//v_FadeWord(v_word, 0.9f);
/*
				int t_volume = v_main_volume ; // 70 //// 120
				if (data.m_percentageAdjust > 0) t_volume = t_volume * 70 / 70 ; // 65/70
				else if (data.m_percentageAdjust < 0 && v_isStopConsonant(data.GetOriginalWord(),data.GetAccent())==false) t_volume = t_volume * 70 / 70 ; //95
				else if (v_isStopConsonant(data.GetOriginalWord(),data.GetAccent())) t_volume = t_volume * 60 / 70;
				else t_volume= t_volume * 1 ; //t_volume=80 ; // 100
				if (t_volume!=100) Change_Volume(v_word, t_volume) ; // New
*/
/*
				if (data.GetAccent()==0) Change_Pitch(v_word,0,0,2)	;
				else if (data.GetAccent()==1) Change_Pitch(v_word,    0,0,2) ;
				else if (data.GetAccent()==2) Change_Pitch(v_word, -12,0,2) ; // -12
				else if (data.GetAccent()==3) Change_Pitch(v_word,    0,0,2) ;
				else if (data.GetAccent()==4) Change_Pitch(v_word,    0,0,2) ;
				else if (data.GetAccent()==5) Change_Pitch(v_word,    0,0,2) ; // -10
*/
/*
				if (data.GetOriginalWord().find(L'è') != string::npos || (data.GetOriginalWord().find(L"ờ") != string::npos && data.GetOriginalWord().find(L"ườ") == string::npos)) {
					Change_Pitch(v_word, -3,0,2)	;
				}
				if (data.GetWord().size() >= 2)
					if (((data.GetOriginalWord().substr(data.GetOriginalWord().size()-1,1) == L"i" ||data.GetOriginalWord().substr(data.GetOriginalWord().size()-1,1) == L"y") && data.GetWord().substr(data.GetWord().size()-2,1) != L"a" && data.GetWord().substr(data.GetWord().size()-2,1) != L"o" && data.GetWord().substr(data.GetWord().size()-2,1) != L"ô" && data.GetWord().substr(data.GetWord().size()-2,1) != L"ơ" && data.GetWord().substr(data.GetWord().size()-2,1) != L"u" && data.GetWord().substr(data.GetWord().size()-2,1) != L"ư") || (data.GetOriginalWord().find(L"ia") != string::npos && data.GetOriginalWord().find(L"gia") == string::npos) || data.GetOriginalWord().find(L"iê") != string::npos || data.GetOriginalWord().find(L"yê") != string::npos || data.GetOriginalWord().find(L"iu") != string::npos) {
						Change_Pitch(v_word, -4,0,2) ; 
					}
*/
//---------------------------------------------------------------------------------------
////			spokenData.clear() ; VX

/*
//FILE *tmp_fptr=NULL;
tmp_fptr=fopen("tmptt2.txt","wb");
 
for(int k=0;k<v_word.size();k++)
{	
	fprintf(tmp_fptr,"%d\n",v_word[k]);
}
*/
				for (int j = 0; j < v_word.size(); j++) {
					mB1 = v_word[j] % 256;
					mB2 = v_word[j] / 256;
					spokenData.push_back(mB1) ;
					spokenData.push_back(mB2) ;
				}
					// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// End Viet them
				int mSilence = data.GetSilenceLength () * ((v_speed1 + v_speed2) / 2) / 100; // VT
				mSilence = int(mSilence / 2) * 2; // VT
				for (unsigned int j = 0; j < mSilence  ; j++) // VT
					spokenData.push_back (0);
				v_word.clear() ; // VT
			} // End for iter
// VX
//			float mBreak ; 
//			if (spokenData.size () > 400000) mBreak = 0 ;
//			else mBreak= float (400000 - spokenData.size ()) / 400000 ;
//			::Sleep (v_punc_break * mBreak) ;
//			Word final (spokenData);
// End VX

			if (toBeSynthetise.GetMustPlay ())
			{
// VT
				float mBreak ; 
				if (spokenData.size () > 400000) mBreak = 0 ;
				else mBreak= float (400000 - spokenData.size ()) / 400000 ;
				::Sleep (v_punc_break * mBreak) ;
				Word final (spokenData);
// End VT
				if (spokenData.size () > 0)
				{	//_//soundPlayer.Create (_sound, spokenData);
					//_//soundPlayer.Play ();
					_wavePlayer.Play (spokenData);
					// _mp3Player.Play(L"database\\tmp.wav");
				}
			}
			else
			{
				Word final (spokenData);
				final.Save (toBeSynthetise.GetMp3Name ());
			}
			// v_word.clear(); // VT
			// spokenData.clear(); // VT
			// delete [] spokenData; // V LL
		} // End if 2
		// ------------------------------------------------------------------------------
	} // End if 1
	// ==================================================================================
} // End void Treat

/**
* Save a byte array containning the sound data in a mp3 file
*
* return value: True if success, elsse false
*
* unsigned char * spokenData spokenData: The sound data contained in an array
* int size:  Size of the array.
*/
bool VietVoice::Concatenator::SaveSpokenData (std::wstring filename, unsigned char * spokenData, int size )
{
	try
	{
		//File::Saver saver (filename);		 
		//saver.SaveMP3 (spokenData, size);
	}
	catch (File::Exception ex)
	{
		throw VietnameseTTException(L" Error, could not save temporary file");
	}

	return true;
}

/**
* Stop reading the current text
*/
void VietVoice::Concatenator::Stop ()
{	
	_wavePlayer.Stop ();
}
void VietVoice::Concatenator::Get_BS1 (long v_bs)
{	
	v_break_silence1 = v_bs;
}
void VietVoice::Concatenator::Get_BS2 (long v_bs)
{	
	v_break_silence2 = v_bs;
}
void VietVoice::Concatenator::Get_VL (long v_vl)
{	
	v_volume = v_vl;
}
void VietVoice::Concatenator::Get_MV (long v_mv)
{	
	v_main_volume = v_mv;
}
void VietVoice::Concatenator::Get_SV1 (long v_sv1)
{	
	v_soft_volume1 = v_sv1;
}
void VietVoice::Concatenator::Get_SV21 (long v_sv21)
{	
	v_soft_volume21 = v_sv21;
}
void VietVoice::Concatenator::Get_SV22 (long v_sv22)
{	
	v_soft_volume22 = v_sv22;
}
void VietVoice::Concatenator::Get_SV3 (long v_sv3)
{	
	v_soft_volume3 = v_sv3;
}
void VietVoice::Concatenator::Get_SP (long v_sp)
{	
	v_speed = v_sp;
}
void VietVoice::Concatenator::Get_Pitch (long v_ph)
{	
	v_pitch = v_ph;
}
void VietVoice::Concatenator::Get_light (std::wstring v_light)
{	
	v_lightWords = v_light;
}
void VietVoice::Concatenator::Get_CP (long v_cp)
{	
	v_compress = v_cp;
}
void VietVoice::Concatenator::Get_WS (long v_ws)
{	
	v_wordsize = v_ws;
}
void VietVoice::Concatenator::Get_adjective (std::wstring v_adj)
{	
	v_adjectiveWords = v_adj;
}
long VietVoice::Concatenator::ByteToWord (long v_byte)
{  
	if (v_byte <= 32767) return v_byte;
	else return v_byte - 65535;
//	if (v_byte <= 32767) return 32767 - v_byte;
//	else return 32767 + 65535 - v_byte;
	return v_byte ;
}
void VietVoice::Concatenator::Change_Volume (vector<long> & m_word, int v_volume)
{	
	if (v_volume==100) return ;
	for (int i = 0; i < m_word.size(); i++) {
		m_word[i] = m_word[i] * v_volume /100; 

/*
		if (m_word[i] <= 32767) 
			m_word[i] = m_word[i] * v_volume /100; 
		else
			m_word[i] = ((m_word[i] - 65535) * v_volume / 100) + 65535;
*/
		if (m_word[i]<(-32768) || m_word[i]>32767) 
			::MessageBox(NULL, StringHelper::ToString(m_word[i]).c_str(), L"Volume | Volume too high...", MB_OK); // VT
	}
}
void VietVoice::Concatenator::Change_Volume_CW (vector<long> & m_word, int v_volume, int v_percent, int v_type)
{	
	if (v_volume==100) return;
	int start_w = 0 ;
	int end_w = 0 ;
		if (v_type==1) {
			start_w = 0 ;
			end_w = m_word.size() * v_percent / 100 - 1 ;
		} else if (v_type==2) {
			start_w = m_word.size() * v_percent / 100 - 1 ;
			end_w = m_word.size() * (100 - v_percent) / 100 ;
		} else if (v_type==3) {
			start_w = m_word.size() * (100 - v_percent) / 100 ;
			end_w = m_word.size() ;
		}
		for (int i = start_w; i < end_w; i++) 
			m_word[i] = m_word[i] * (100 + v_volume) /100; 
}
void VietVoice::Concatenator::Compress_Word (vector<long> & tmp_word, int v_percent, std::wstring v_wn)
{	
	if (v_percent == 0) return ;
////	if (v_wn == L"mềm") return ;
	int mi = 0 ;
	int v_num_of_cycles = 0;
	int v_cycle_max = 10 ; // 6
//	if (v_percent > 0) v_cycle_max = 4 ;
	vector<unsigned long> v_sum ;
	vector<unsigned int> v_avg ;
	vector<unsigned int> v_loc ;
	vector<unsigned int> v_dis ;
	vector<unsigned int> v_seg ;
	vector<bool> v_cycle ;

	v_sum.push_back (0) ;
	v_avg.push_back (0) ;
	v_loc.push_back (0) ;
	v_dis.push_back (0) ;
	v_seg.push_back (0) ;
	v_cycle.push_back (false) ;
////::MessageBox (NULL, StringHelper::ToString (v_percent).c_str (), StringHelper::ToString (v_wn).c_str (), MB_OK) ;
	int v_num_of_breaks = 0;
	for (int i = 0; i < (tmp_word.size() - 1); i++)
	{
////		if ((tmp_word[i] <= 0) && (tmp_word[i+1] >= 0) && (i - mi >= 20))
//		if ((tmp_word[i] <= 0) && (tmp_word[i+1] >= 0))
		if ((tmp_word[i] * tmp_word[i+1] <= 0) && (i - mi  >= 20))
		{
			v_sum.push_back (abs(tmp_word[i])) ;
////			v_sum.push_back (tmp_word[i]) ;
			v_avg.push_back (0) ;
			v_loc.push_back (i) ;
			v_dis.push_back (i - mi) ;
			v_seg.push_back (0) ;
			v_cycle.push_back (false) ;
//			v_loc[v_num_of_breaks] = i ;
			v_num_of_breaks ++;
			mi = i ;
		}
		else
		{
		}
//////		v_sum[v_num_of_breaks] += abs(tmp_word[i]) ;
		v_sum[v_num_of_breaks] += tmp_word[i] ;
	}
	for (int i = 0; i < v_num_of_breaks - 0; i++) v_avg[i] = v_sum[i] / (v_loc[i+1] - v_loc[i]) ;
	if (v_num_of_breaks == 0) return;

	int k = 0 ;
	int j = 0 ;
	float v_ratio = 1.3f ; // 0.05 //// 1.06
	float m_ratio_sum ;
	float m_ratio_loc ;
	float m_ratio_dis ;

	int v_cut_cycle = 0 ;
	int v_add_cycle = 0 ;
	int vi = 0 ;
	int v_comp ;
	do {
		k = 2 ; // 2
		do {
			bool v_found = true ;
			j = 0 ;
			if (v_dis[vi + k] > v_dis[vi]) v_comp = 2 ;
			else if (v_dis[vi + k] < v_dis[vi]) v_comp = -1 ;
			else v_comp = 1 ;
			do {
				m_ratio_sum = float (v_avg[vi + k + j]) / v_avg[vi + j] ; // them + j
//				m_ratio_sum = float (v_sum[vi + k]) / v_sum[vi] ;
				m_ratio_sum = abs(1 - m_ratio_sum) + 1;
//				m_ratio_loc = float (v_loc[vi + k]) / v_loc[vi] ;
//				m_ratio_loc = abs(1 - m_ratio_loc) + 1;
				m_ratio_dis = float (v_dis[vi + k + j]) / v_dis[vi + j] ; // them + j
				m_ratio_dis = abs(1 - m_ratio_dis) + 1;
////				if ((m_ratio_sum > v_ratio) || (m_ratio_dis > v_ratio))
				if ((m_ratio_sum > v_ratio) || (m_ratio_dis > v_ratio) || (v_comp == 2 && v_dis[vi + k + j] < v_dis[vi + j]) || (v_comp == -1 && v_dis[vi + k + j] > v_dis[vi + j]))
					v_found = false ;
				j++ ;
			} while (j <= k-1) ;
			if (v_found) {
//				v_cycle[vi] = true ;
				v_seg[vi] = k ;
				v_num_of_cycles ++ ;
				vi += k - 1 ;
				k = v_cycle_max;
			}
			k++ ; 
//////			k++ ; // So chu ky phai chan
		} while (k <= v_cycle_max) ;
		vi ++ ;
	} while (vi < v_num_of_breaks - v_cycle_max * 2 + 1) ;
	if (v_num_of_cycles == 0) return ;
//::MessageBox (NULL, StringHelper::ToString (v_cycle.size()).c_str (), L"v_cycle", MB_OK) ;
	int v_num_of_cycle_bytes = 0 ;
	for (int i = 0; i < v_num_of_breaks; i++) {
//::MessageBox (NULL, StringHelper::ToString (i).c_str (), L"Diem thu", MB_OK) ;
		if (v_seg[i] > 0) {
			v_num_of_cycle_bytes += v_loc[i + v_seg[i]] - v_loc[i] ;
		}
	}
////::MessageBox (NULL, StringHelper::ToString (v_num_of_cycle_bytes).c_str (), StringHelper::ToString (v_wn).c_str (), MB_OK) ;
	float v_avg_cycle = v_num_of_cycle_bytes / v_num_of_cycles ;
//===================================================================================
////::MessageBox (NULL, StringHelper::ToString (v_cut_cycle).c_str (), L"cut number of cycles", MB_OK) ;
////::MessageBox (NULL, StringHelper::ToString (v_num_of_cycles).c_str (), L"number of cycles", MB_OK) ;


//===================================================================================
	vector<long> v_tmp ;
//	if (v_percent > 0) {
////	v_cut_cycle = (tmp_word.size() * v_percent / 100) / v_avg_cycle ;
	v_cut_cycle = (tmp_word.size() * abs(v_percent) / 100) / v_avg_cycle ;
	if (v_num_of_cycles <= v_cut_cycle) {
		for (int i = 0; i < v_num_of_breaks; i++) {
			if (v_seg[i] > 0) {
				v_cycle[i] = true ;
//::MessageBox (NULL, StringHelper::ToString (v_loc[i + v_seg[i]] - 1).c_str (), L"num of break", MB_OK) ;
				for (int j = 0; j < v_seg[i] - 1; j++)
					v_cycle[i + j + 1] = true ;
			}
		}
	} else {
//::MessageBox (NULL, StringHelper::ToString (v_num_of_breaks).c_str (), L"Num of breaks", MB_OK) ;
//::MessageBox (NULL, StringHelper::ToString (v_num_of_cycles).c_str (), L"Num of cycles", MB_OK) ;
//::MessageBox (NULL, StringHelper::ToString (v_cut_cycle).c_str (), L"Num of cut cycles", MB_OK) ;
		vector<int> m_cut;
		for (int i=0; i< v_cut_cycle; i++) {
			m_cut.push_back(int(i *  (float (v_num_of_cycles)/ v_cut_cycle))) ;
////::MessageBox (NULL, StringHelper::ToString (int(i *  (float (v_num_of_cycles)/ v_cut_cycle))).c_str (), L"cut number", MB_OK) ;
		}
		int v_count = -1 ; int j = 0 ;
		for (int i = 0; i< m_cut.size(); i++) {
			do {
				if (v_seg[j] > 0) {
					v_count ++ ;
					if (v_count == m_cut[i] ) {
////::MessageBox (NULL, StringHelper::ToString (v_count).c_str (), L"v_count", MB_OK) ;
////::MessageBox (NULL, StringHelper::ToString (j).c_str (), L"break number", MB_OK) ;
////::MessageBox (NULL, StringHelper::ToString (v_seg[j]).c_str (), L"num of segments", MB_OK) ;
////::MessageBox (NULL, StringHelper::ToString (v_loc[j]).c_str (), L"begin location", MB_OK) ;
////::MessageBox (NULL, StringHelper::ToString (v_loc[j + v_seg[j]] - 1).c_str (), L"end location", MB_OK) ;
//::MessageBox (NULL, StringHelper::ToString (j + v_seg[j]).c_str (), L"j + v_seg[j]", MB_OK) ;
						v_cycle[j] = true ;
						for (int k = 0; k < v_seg[j] - 1; k++)
							v_cycle[j + k + 1] = true ;
						j ++ ;
						break ;
					} 
					else {
//::MessageBox (NULL, StringHelper::ToString (v_count).c_str (), L"False", MB_OK) ;
					}
//						v_count ++;
				}
				j++ ;
			} while (j < v_num_of_breaks && v_count < v_num_of_cycles) ;
		}
		m_cut.clear();
	}
//		vector<long> v_tmp ;
	int v_compressed = 0;
	int v_added = 0 ;
	for (int i = 0; i < v_loc[0]; i++) { // Lay doan dau
		v_tmp.push_back(tmp_word[i]) ;
	}
////	v_compressed += v_dis[0] ;
	int i = 0 ;
	do {
		if (v_cycle[i] == false) {
			for (int j = v_loc[i]; j < v_loc[i + 1]; j++) {// Lay doan thu i
				v_tmp.push_back(tmp_word[j]);
			}
////		v_compressed += v_dis[i] ;
		}
		else {
			if (v_seg[i] > 0) v_compressed += v_loc[i + v_seg[i]] - v_loc[i] ;

			if (v_percent < 0) {


				if (v_seg[i] > 0) {
////::MessageBox (NULL, StringHelper::ToString (v_seg[i]).c_str (), StringHelper::ToString ((1 - float (v_compressed) / tmp_word.size()) * 100).c_str () , MB_OK) ;					
					std::vector<long> v_repeat ;
					for (int j = v_loc[i]; j < v_loc[i + 1]; j++) // Lay doan thu i
						v_repeat.push_back(tmp_word[j]);
					int v_seg_tmp = v_seg[i] ; 
					for (int k = 0; k < v_seg_tmp - 1; k ++) { // Lay so ck - 1 segment tiep theo
						i ++ ;
						for (int j = v_loc[i]; j < v_loc[i + 1]; j++) // Lay doan thu i
							v_repeat.push_back(tmp_word[j]);
					}
					for (int j = 0; j < v_repeat.size(); j ++)
						v_tmp.push_back(v_repeat[j]) ;
//					v_compressed += v_repeat.size() ;
					for (int j = 0; j < v_repeat.size(); j ++)
						v_tmp.push_back(v_repeat[j]) ;
//					v_compressed += v_repeat.size() ;
					v_added += v_repeat.size() ;
					v_repeat.clear() ;
				} else {


				}
////				for (int j = v_loc[i]; j < v_loc[i + 1]; j++) // Lay doan thu i
////					v_tmp.push_back(tmp_word[j]);
////				v_compressed += v_dis[i] ;


			}
		}
		i++ ;
	} while (i < v_num_of_breaks - 0) ; // - 1

	for (int i = v_loc[v_num_of_breaks - 0]; i < tmp_word.size(); i++) // Lay doan cuoi
		v_tmp.push_back(tmp_word[i]) ;
////	v_compressed += v_dis[v_num_of_breaks] ;
//	}
//	else {
//		v_add_cycle = (tmp_word.size() * v_percent / 100) / v_avg_cycle ;







//	}
//===================================================================================



////if (v_percent > 0)	
////::MessageBox (NULL, StringHelper::ToString (v_wn).c_str (), StringHelper::ToString (-(float (v_compressed) / tmp_word.size()) * 100).c_str () , MB_OK) ;
////else
////::MessageBox (NULL, StringHelper::ToString (v_wn).c_str (), StringHelper::ToString ((float (v_added) / tmp_word.size()) * 100).c_str () , MB_OK) ;

	tmp_word.clear() ;
	tmp_word = v_tmp ;

	v_tmp.clear () ;
	v_sum.clear () ;
	v_avg.clear () ;
	v_loc.clear () ;
	v_seg.clear () ;
	v_cycle.clear () ;
//::MessageBox (NULL, StringHelper::ToString (v_loc[i]).c_str (), L"Find cycles", MB_OK) ;
}
void VietVoice::Concatenator::Change_Pitch (vector<long> & m_word, int v_pitch, int v_percent, int v_type)
{	
	if (v_pitch==0) return;
	vector<long> v_tmp ;
	int v_begin; int v_end;
	if (v_type==1) {
		v_begin = 0;
		v_end = m_word.size() * v_percent / 100 ; // * (100 - v_percent) / 100 ;
	} else if (v_type==2) {
		v_begin = m_word.size() * v_percent / 100 ;
		v_end = m_word.size() * (100 - v_percent) / 100 ;
	} else if (v_type==3) {
		v_begin = m_word.size() * (100-v_percent) / 100 ;
		v_end = m_word.size();
	}
	for (int i = 0; i < m_word.size(); i++) 
		if (i < v_begin || i > v_end) {
			v_tmp.push_back(m_word[i]);
		} else

		if (v_pitch>0) {
			if (i % int(200/v_pitch)!=1) v_tmp.push_back(m_word[i]);
		}
		else {
			if (i % int(200/v_pitch)!=1) v_tmp.push_back(m_word[i]);
			else {
				v_tmp.push_back(m_word[i]);
				v_tmp.push_back(m_word[i]);
			}
		}
	//////m_word.clear();
	m_word=v_tmp;
	v_tmp.clear();
}
bool VietVoice::Concatenator::v_isStopConsonant(std::wstring v_word, int v_accent) // VT
{
		if (v_word.size() < 2) return false ;
	if ((v_word.substr(v_word.size()-2,2) == L"ch" || v_word.substr(v_word.size()-1,1) == L"p" || v_word.substr(v_word.size()-1,1) == L"t" || v_word.substr(v_word.size()-1,1) == L"c") && (v_accent == 1 || v_accent == 5))
		return true ;
	return false ;
}
bool VietVoice::Concatenator::v_isSameAsSC(std::wstring v_word, int v_accent) // VT
{	return false ;
	if (v_word.size() < 2) return false ;
	 if (v_word.substr(v_word.size()-2,2) == L"nh" || v_word.substr(v_word.size()-2,2) == L"ng")
	return true ;
	return false ;
}
void VietVoice::Concatenator::ENVELOP_CAL(float *data,long datalen,float **fdata,int nsamp)
{
	int fdatalen;
	int i,j,start;
	float avg;
	fdatalen=datalen/nsamp;
	(*fdata)=(float *)malloc(fdatalen*sizeof(float));
	memset((*fdata),0,fdatalen*sizeof(float));
	
	start=0;
	for (i=0;i<fdatalen;i++)
	{
		avg=0;
		for (j=0;j<nsamp;j++) avg +=data[j+start];
		*((*fdata)+i)=avg/nsamp;
		start +=nsamp;
	}
}
long VietVoice::Concatenator::GET_SIMILE_POS_FAST(float * fx1,float * x1, long n1,float * fx2,float * x2, long n2,int nsamp) //n1,n2²Î¿¼Óë²âÊÔÐÅºÅµÄVAD¸öÊý
{
	float FLT_MAX=3.402823466e+38f;
	long reflen=n1;
	long corlen=n2;
	long cordelta=n2-n1;

	long freflen=reflen/nsamp;
	long fcorlen=corlen/nsamp;
	long fcordelta=cordelta/nsamp;

	float cor;
	float cormax=-FLT_MAX;
	int i,j,bestshift,fbestshift,shift_low,shift_high;

	fbestshift=0; // VT /////////////////////////////////////////////////////////////////

	for (i=0;i<fcordelta;i++)
	{
		cor=0.0f;
		for (j=0;j<freflen;j++) 
		{
			cor +=fx1[j]*fx2[j+i];	
		}

		if (cor>cormax)
		{
			cormax=cor;
			fbestshift=i;
		}
	}
	
	cormax=-FLT_MAX;
	bestshift=fbestshift*nsamp;
	shift_low=bestshift-nsamp;
	shift_high=bestshift+nsamp;
	if (shift_low<0) shift_low=0;
	if (shift_high>cordelta) shift_high=cordelta;
	
	for (i=shift_low;i<shift_high;i++)
	{
		cor=0.0;
		for (j=0;j<reflen;j++)	
		{
			cor+=x1[j]*x2[j+i];
		}
		if (cor>cormax)
		{
			cormax=cor;
			bestshift=i;
		}
	}
	return bestshift;
}

void VietVoice::Concatenator::GET_FINALE_DATA(WAV_INFO *in_info,WAV_INFO *out_info,float beta,int nsamp)
{

	int  i;
	long nref,ncross,DelayEst=0,out_pos=0,outcetr=0;
	long Win_offset=(long)(0.5f+halfwin_sec*(in_info->Fs)/1000.0f);
	long Win_LENGTH=Win_offset*2+1;
	int  deta=(int)(0.5f+deta_sec*(in_info->Fs)/1000.0f);
	int  cross_low=-Win_offset-deta,cross_high=0,k=0;
	int  precross_low=0;
	int  lst_low=0,lst_high=0;
	int  bs_low=0,bs_high=0;
	float *cross_buf,*fcross_buf;
	float *bs_buf,*fbs_buf;
	float *win;
	float *hann_buf;
	float *fdata;

	out_info->Nchan=in_info->Nchan;

	hann_buf=(float *)malloc(Win_LENGTH*sizeof(float));
	memset(hann_buf,0,Win_LENGTH*sizeof(float));

	if(beta!=1.0f)
		ENVELOP_CAL(in_info->data,in_info->Nsamples,&fdata,nsamp);

	nref=Win_LENGTH;
	ncross=Win_LENGTH+2*deta;

	for(i=-Win_offset;i<=Win_offset;i++)
	{
		 hann_buf[i+Win_offset]=0.5f*(1.0f-(float)cos(2.0f*PI*(i+Win_offset)/(Win_LENGTH-1)));
	}
	win=hann_buf+Win_offset;

	for(k=0,i=0;i<=Win_offset;i++)
	{
		out_info->data[i]=(in_info->data[i])*(win[i]);
		k++;
	}
	outcetr+=Win_offset;
	out_pos+=k;

	bs_low=0;
	bs_high=bs_low+Win_LENGTH;

	while(cross_low+(int)(0.5+beta*Win_offset)<in_info->Nsamples)
	{
		if(bs_low>in_info->Nsamples)
		{
			break;
		}
		if(bs_high>in_info->Nsamples)
		{
			bs_high=in_info->Nsamples;
		}

		bs_buf=in_info->data+bs_low;

//printf("in_info->data : %f\n",*in_info->data);
		
		fbs_buf=fdata+bs_low/nsamp;

		cross_low+=(int)(0.5+beta*Win_offset);
		cross_high=cross_low+Win_LENGTH+2*deta;
		precross_low=cross_low;
		
		if(cross_low>in_info->Nsamples)
		{
			break;
		}

		if(cross_low<0)
		{
			while(1)
			{
				cross_low++;
				if(cross_low>=cross_high||cross_low>=0)
				break;
			}
		}

		if(cross_low>=0&&cross_low<cross_high)
		{

			if (cross_high>in_info->Nsamples)
			{
				cross_high=in_info->Nsamples;
			}

			if(cross_low<=bs_low&&cross_high>=bs_high)
			{
				DelayEst=bs_low-cross_low;
			}
			else
			{
				cross_buf=in_info->data+cross_low;
				fcross_buf=fdata+cross_low/nsamp;
				k=cross_high-cross_low;
				ncross=cross_high-cross_low;
				DelayEst=GET_SIMILE_POS_FAST(fbs_buf,bs_buf, nref, fcross_buf,cross_buf, ncross,nsamp);
			}

			lst_low=cross_low+DelayEst;
			lst_high=lst_low+Win_LENGTH;
			if(precross_low<0)
			{
				cross_low=precross_low;
			}
			if (lst_low>in_info->Nsamples)
			{
				break;
			}
			if(lst_high>in_info->Nsamples)
			{
				lst_high=in_info->Nsamples;
			}

			for(k=-Win_offset,i=lst_low;i<lst_high;i++)
			{
				out_info->data[outcetr+k]+=(in_info->data[i])*win[k];
				k++;
			}
						
			if (k>=0)
			{
				out_pos+=k;
				outcetr+=Win_offset;
			}
						
						
			out_info->Nsamples=out_pos;
			out_info->DataLen=out_pos<<1;
			out_info->Rifflen=out_info->DataLen+36;

			if (in_info->Nchan==2)
			{
				out_info->DataLen<<=1;
				out_info->Rifflen=out_info->DataLen+36;
			}
		}

		bs_low=lst_low+Win_offset;
		bs_high=bs_low+Win_LENGTH;
	}

	printf("in_info Nsamples: %d\n",in_info->Nsamples);
	printf("out_info Nsamples: %d\n",out_info->Nsamples);
	// getch();
// 	free(bs_buf);
	// free(hann_buf); VX
// 	free(cross_buf);
//	free(cross_buf); free(fcross_buf);
//	free(bs_buf); free(fbs_buf);
//	free(win);
	free(hann_buf);
//	free(fdata);

}
bool VietVoice::Concatenator::v_isShortStartWord(std::wstring v_word) // VT
{	return false;
	//a, c, e, i, k, o, p, q, u, y, ch,               t, tr, p
	//std::wstring v_vowels = L"a/ ă/ â/ e/ ê/ i/ o/ ô/ ơ/ u/ ư/ y/ c/ k/ q/ " ;
	//if (v_vowels.find(v_word.substr(0,1)) !=  string::npos) return true ;
	if (v_word.substr(0,1) == L"x") return true ;
	if (v_word.size() < 2) return false ;
	//if (v_word.substr(0,1) == L"t" && v_word.substr(0,2) != L"th") return true ;
	//if (v_word.substr(0,1) == L"p" && v_word.substr(0,2) != L"ph") return true ;
	if (v_word.substr(0,2) == L"ch") return true ;
	if (v_word.substr(0,2) == L"tr") return true ;
	return false ;
}
bool VietVoice::Concatenator::v_isSmallVolStart(std::wstring v_word) // VT
{	return false ;
	// std::wstring v_cons = L"b/ d/ đ/ g/ h/ p/ q/ r/ v/ x/" ;
	std::wstring v_cons1 = L"q/ t/ c/ g" ;
	std::wstring v_cons2 = L"kh/ ph/ th/" ;
	if (v_cons1.find(v_word.substr(0,1)) !=  string::npos) return true ;
	if (v_word.size() < 2) return false ;
	if (v_word.substr(0,2) == L"tr") return false ;
	if (v_word.substr(0,2) == L"ch") return false ;
	if (v_word.substr(0,2) == L"gi") return false ;
	if (v_word.substr(0,2) == L"gh") return false ;
	if (v_cons2.find(v_word.substr(0,2)) !=  string::npos) return true ;
	return false ;
}
void VietVoice::Concatenator::v_getCW1 (long v_cw1)
{	
	v_compound1 = v_cw1;
}
void VietVoice::Concatenator::v_getCW2 (long v_cw2)
{	
	v_compound2 = v_cw2;
}
void VietVoice::Concatenator::v_getStopSilence (long v_ss)
{	
	v_StopSilence = v_ss;
}
bool VietVoice::Concatenator::v_isSilenceBefore(std::wstring v_word) // VT
{	if (v_word == L"") return false;
	// c, k, q, t, th, tr,              g, ch
	std::wstring v_part = L"c/ k/ q/ t" ;
	if (v_part.find(v_word.substr(0,1)) !=  string::npos) return true ;
	if (v_word.size() < 2) return false ;
	//if (v_word.substr(0,2) == L"th") return false ;
	//if (v_word.substr(0,2) == L"tr") return false ;
	//if (v_word.substr(0,2) == L"gh") return true ;
	return false ;
}
void VietVoice::Concatenator::v_echo(vector<long> & m_word, long v_dots, int mType, int mPer) // VT
{	
	if (v_dots == 0) return ;
	::std::vector <long> mword = m_word;
	if (mType==1) {
		int msum = 0;
		for (int i = 0; i < m_word.size(); i++) {
			if (i >= v_dots && i <= m_word.size() - v_dots + 0) { // + 1
				msum=0 ;
				for (int j = i - v_dots; j < i + v_dots ; j++) {
					if (j != i) {
						if (m_word[j] >= 0) 
							msum += m_word[j] ; 
						else
							msum += m_word[j] - 0 ; 
					}
				}
				msum /= v_dots * 2 + 0; // + 1
				mword[i] = msum ;
			}
		}
		m_word = mword ;
		mword.clear();
	}
 
	else if (mType==2) {
		if (mPer == 0) {
			float v_hs = (float) ((-32767) / (v_dots + 1) * (v_dots + 1)) / (-32767);
			for (int i = 0; i < m_word.size(); i++) {
				m_word[i] = ::floorl(mword[i] / (v_dots + 1)) * (v_dots +1) / v_hs ; 
			if (m_word[i]<(-32768) || m_word[i]>32767) 
::MessageBox(NULL, StringHelper::ToString(m_word[i]).c_str(), L"Volume too high...", MB_OK); // VT
			}
		} else {
			float v_hs = (float) ((-32767) / (v_dots + 1) * (v_dots + 1)) / (-32767);
			for (int i = m_word.size() * mPer / 100; i < m_word.size(); i++) {
				m_word[i] = ::floorl(mword[i] / (v_dots + 1)) * (v_dots +1) / v_hs ; 
			if (m_word[i]<(-32768) || m_word[i]>32767) 
::MessageBox(NULL, StringHelper::ToString(m_word[i]).c_str(), L"Volume too high...", MB_OK); // VT
			}
		}
		mword.clear();
	}
}

void VietVoice::Concatenator::Get_Dots (long v_dots)
{	
	vdots = v_dots;
}
void VietVoice::Concatenator::Get_Not_Echo (long v_notEcho)
{	
	vnotEcho = v_notEcho;
}
void VietVoice::Concatenator::v_FadeIn (vector<long> & m_word, long v_Fpercent)
{
	if (v_Fpercent==0) return;
	int v_point = m_word.size() * v_Fpercent / 100 ;
	//int v_max = 0 ;
	//for (int i = v_point - 600; i < v_point; i++)
		//if (m_word[i] <= 32767 && v_max < m_word[i]) v_max = m_word[i];
	for (int i = 0; i < v_point; i++)
////		if (m_word[i] <= 32767)
			// m_word[i] = m_word[i] * sqrt((double) i) / sqrt((double) v_point) ; // para
			m_word[i] = m_word[i] * sqrt(1 - ((v_point - (double) i) / v_point) * ((v_point - (double) i)/ v_point))   ; // para
//			mWord_2(I) = mWord_0(I) * Sqrt(1 - ((mEnd - I - 1) / mEnd) ^ 2) ' Ellipse // VB
////		else
			// m_word[i] = (m_word[i] -65535) * sqrt((double) i) / sqrt((double) v_point) + 65535; // para
////			m_word[i] = (m_word[i] -65535) * sqrt(1 - ((v_point - (double) i) / v_point) * ((v_point - (double) i)/ v_point)) + 65535; // para
//			mWord_2(I) = (mWord_0(I) - 65535) * Sqrt(1 - ((mEnd - I - 1) / mEnd) ^ 2) + 65535 ' Ellipse // VB
}
void VietVoice::Concatenator::v_FadeOut (vector<long> & m_word, long v_Fpercent)
{
	if (v_Fpercent==0) return;
	int v_point = m_word.size() * (100 - v_Fpercent) / 100 ;
	//int v_max = 0 ;

	//for (int i = v_point + 600; i < v_point; i++)
		//if (m_word[i] <= 32767 && v_max < m_word[i]) v_max = m_word[i];
	for (int i = v_point; i < m_word.size(); i++)
////		if (m_word[i] <= 32767)
			// m_word[i] = m_word[i] * sqrt(m_word.size() - (double) i) / sqrt(m_word.size() - (double) v_point) ; // para
			m_word[i] = m_word[i] * sqrt(1 - (((double) i - v_point) / (m_word.size() - (double) v_point)) * (((double) i - v_point) / (m_word.size() - (double) v_point))) ; // ellipse
//			mWord_2(I) = mWord_0(I) * Sqrt(1 - ((I - mBegin) / (TS_Byte / 2 - mBegin)) ^ 2) ' Ellipse // VB
////		else
			// m_word[i] = (m_word[i] -65535) * sqrt(m_word.size() - (double) i) / sqrt(m_word.size() - (double) v_point) + 65535; // para
////			m_word[i] = (m_word[i] -65535) * sqrt(1 - (((double) i - v_point) / (m_word.size() - (double) v_point)) * (((double) i - v_point) / (m_word.size() - (double) v_point))) + 65535; // ellipse
//			mWord_2(I) = ((mWord_0(I) - 65535) * Sqrt(1 - ((I - mBegin) / (TS_Byte / 2 - mBegin)) ^ 2)) + 65535 ' Ellipse // VB
}
void VietVoice::Concatenator::Get_Fade (long v_fade)
{	
	m_fade = v_fade;
}
void VietVoice::Concatenator::Get_Sample (long v_samples)
{	
	m_samples = v_samples;
}
void VietVoice::Concatenator::v_FadeWord (vector<long> & m_word, float v_ratio)
{
	if (m_word.size()==0) return;
	for (int i = 0; i < m_word.size(); i++) {
		if (i+1<=m_word.size()/2) {
				m_word[i] = m_word[i] * sqrt(1 - ((m_word.size() / 2 - (double) i) / (m_word.size() / 2)) * ((m_word.size() / 2 - (double) i) / (m_word.size() / 2))) ; // ellipse
		}
		else {
				m_word[i] = m_word[i] * sqrt(1 - (((double) i - m_word.size() / 2) / (m_word.size() / 2)) * (((double) i - m_word.size() / 2) / (m_word.size() / 2))) ; // ellipse
		}
	m_word[i] *= v_ratio ;
	}
/*
	for (int i = 0; i < m_word.size(); i++) {
		if (i+1<=m_word.size()/2) {
			if (m_word[i] <= 32767)
				m_word[i] = m_word[i] * sqrt(1 - ((m_word.size() / 2 - (double) i) / (m_word.size() / 2)) * ((m_word.size() / 2 - (double) i) / (m_word.size() / 2))) ; // ellipse
			else
				m_word[i] = (m_word[i] -65535) * sqrt(1 - ((m_word.size() / 2 - (double) i) / (m_word.size() / 2)) * ((m_word.size() / 2 - (double) i) / (m_word.size() / 2))) + 65535; // ellipse
		}
		else {
			if (m_word[i] <= 32767)
				m_word[i] = m_word[i] * sqrt(1 - (((double) i - m_word.size() / 2) / (m_word.size() / 2)) * (((double) i - m_word.size() / 2) / (m_word.size() / 2))) ; // ellipse
			else
				m_word[i] = (m_word[i] -65535) * sqrt(1 - (((double) i - m_word.size() / 2) / (m_word.size() / 2)) * (((double) i - m_word.size() / 2) / (m_word.size() / 2))) + 65535; // ellipse
		}
	}
*/
}
void VietVoice::Concatenator::Get_Per_Pitch (int v_p_p)
{	
	v_per_pitch = v_p_p;
}
void VietVoice::Concatenator::Get_Pitch0 (int v_p0)
{	
	v_pitch0 = v_p0;
}
void VietVoice::Concatenator::Get_Pitch1 (int v_p1)
{	
	v_pitch1 = v_p1;
}
void VietVoice::Concatenator::Get_Pitch2 (int v_p2)
{	
	v_pitch2 = v_p2;
}
void VietVoice::Concatenator::Get_Pitch3 (int v_p3)
{	
	v_pitch3 = v_p3;
}
void VietVoice::Concatenator::Get_Pitch4 (int v_p4)
{	
	v_pitch4 = v_p4;
}
void VietVoice::Concatenator::Get_Pitch5 (int v_p5)
{	
	v_pitch5 = v_p5;
}
void VietVoice::Concatenator::Get_Com0 (int v_c0)
{	
	v_compress0 = v_c0;
}
void VietVoice::Concatenator::Get_Com1 (int v_c1)
{	
	v_compress1 = v_c1;
}
void VietVoice::Concatenator::Get_Com2 (int v_c2)
{	
	v_compress2 = v_c2;
}
void VietVoice::Concatenator::Get_Com3 (int v_c3)
{	
	v_compress3 = v_c3;
}
void VietVoice::Concatenator::Get_Com4 (int v_c4)
{	
	v_compress4 = v_c4;
}
void VietVoice::Concatenator::Get_Com5 (int v_c5)
{	
	v_compress5 = v_c5;
}
