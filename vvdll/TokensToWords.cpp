
#include <algorithm>
#include <wchar.h>
#include <string>
#include <sstream>
#include "tokensToWords.h"

/**
* Constructor
* @param date String utilé pour innitialisé l'objet / String used to innitialize the object
*/
VietVoice::Date::Date (std::wstring date)
{
	TokenizeDate (date);
}

VietVoice::Date::Date  (const Date & date)
{
	if (this != &date)
	{
		_month = date._month ;
		_day = date._day;
		_year = date._year;
	}
}

void VietVoice::Date::operator = (const Date & date)
{
	if (this != &date)
	{
		_month = date._month ;
		_day = date._day;
		_year = date._year;
	}		
}

/**
* Obtient le jours de la date
* @return Le jours sous forme de String
*
* Gets the day of the date
* @return the day as a String
*/
std::wstring VietVoice::Date::GetDayString ()
{
	return _day ;
}

/**
* Obtient le mois de la date
* @return Le mois sous forme de String
*
* Gets the month of the date
* @return the month as a String
*/
std::wstring VietVoice::Date::GetMonthString ()
{
	return _month ;
}

/**
* Obtient l'année de la date
* @return L'année sous forme de String
*
* Gets the year of the date
* @return the year as a String
*/
std::wstring VietVoice::Date::GetYearString ()
{
	return _year;
}

/**
* Obtient le jours de la date
* @return Le jours sous forme de int
*
* Gets the day of the date
* @return the day as a int
*/
int VietVoice::Date::GetDayInt ()
{
	wchar_t * tmp = &_day[_day.length () - 1];
	return ::wcstol (_day.c_str (), &tmp, 10);
}

/**
* Obtient le mois de la date
* @return Le mois sous forme de int
*
* Gets the month of the date
* @return the month as a int
*/
int VietVoice::Date::GetMonthInt ()
{
	wchar_t * tmp = &_month[_month.length () - 1];
	return ::wcstol (_month.c_str (), &tmp, 10);    
}

/**
* Obtient l'année de la date
* @return L'année sous forme de int
*
* Gets the year of the date
* @return the year as a int
*/
int VietVoice::Date::GetYearInt ()
{
	wchar_t * tmp = &_year[_year.length () - 1];
	return ::wcstol (_year.c_str (), &tmp, 10);
}

/**
* Innitialise l'objet
* @param date Un objet String contenant les info sur la date
*
* Innitialize the object
* @param date A String object containing the informations about the date
*/
void VietVoice::Date::TokenizeDate (std::wstring date)
{
	size_t index = 0;

	StringHelper::ReplaceAll(date, L'-', L'.');
	StringHelper::ReplaceAll(date, L'/', L'.');

	size_t nbDots = StringHelper::NbChar (date, '.') ;

	index = date.find_first_of (L".", 0) ;

	_day = date.substr (0, index) ;

	if (nbDots == 2)
	{
		size_t indexFrom = index + 1;

		size_t count = date.find_first_of (L".", index + 1) - index - 1;

		index = date.find_first_of (L".", index + 1);
		_month = date.substr(indexFrom, count);
		_year = date.substr(index + 1, date.size () - index - 1);

	}
	else
	{
		_month = date.substr(index + 1, date.size() - index - 1);
		_year = L"-1";
	}
}

/**
* Constructor
*/
VietVoice::TokensToWords::TokensToWords (std::wstring fileAbb, std::wstring fileNoSilence)
{

	//Contains word for thousand, million, etc.
	_threeDigits[0] = L"silence";
	_threeDigits[1] = L"ngàn";
	_threeDigits[2] = L"tri\x1EC7u";
	_threeDigits[3] = L"t\x1EF7";

	// Contains words for number from 1 to 9
	_oneDigit[0] = L"không";
	_oneDigit[1] = L"m\x1ED9t";
	_oneDigit[2] = L"hai";
	_oneDigit[3] = L"ba";
	_oneDigit[4] = L"b\x1ED1n";
	_oneDigit[5] = L"n\x0103m";
	_oneDigit[6] = L"sáu";
	_oneDigit[7] = L"b\x1EA3y";
	_oneDigit[8] = L"tám";
	_oneDigit[9] = L"chín";

	//Contains the names for the day of the week
	_dayOfWeek[0] = L"nh\x1EADt"; //Sunday
	_dayOfWeek[1] = L"hai";
	_dayOfWeek[2] = L"ba";
	_dayOfWeek[3] = L"t\x01B0";
	_dayOfWeek[4] = L"n\x0103m";
	_dayOfWeek[5] = L"sáu";

	_dayOfWeek[6] =L"b\x1EA3y"; //Saturday

	LoadAbbreviationFile (fileAbb);
	LoadNoSilenceWords (fileNoSilence);

	_vietnameseLocal = ::_create_locale (LC_ALL, ".1258");
}


VietVoice::TokensToWords::TokensToWords (std::wstring fileAbb, std::wstring fileNoSilence,u_int nbOfWordInPhrase,std::wstring pathPhrase, std::wstring pathVeryShortWord)
{

	//Contains word for thousand, million, etc.
	_threeDigits[0] = L"silence";
	_threeDigits[1] = L"ngàn";
	_threeDigits[2] = L"tri\x1EC7u";
	_threeDigits[3] = L"t\x1EF7";

	// Contains words for number from 1 to 9
	_oneDigit[0] = L"không";
	_oneDigit[1] = L"m\x1ED9t";
	_oneDigit[2] = L"hai";
	_oneDigit[3] = L"ba";
	_oneDigit[4] = L"b\x1ED1n";
	_oneDigit[5] = L"n\x0103m";
	_oneDigit[6] = L"sáu";
	_oneDigit[7] = L"b\x1EA3y";
	_oneDigit[8] = L"tám";
	_oneDigit[9] = L"chín";

	//Contains the names for the day of the week
	_dayOfWeek[0] = L"nh\x1EADt"; //Sunday
	_dayOfWeek[1] = L"hai";
	_dayOfWeek[2] = L"ba";
	_dayOfWeek[3] = L"t\x01B0";
	_dayOfWeek[4] = L"n\x0103m";
	_dayOfWeek[5] = L"sáu";

	_dayOfWeek[6] =L"b\x1EA3y"; //Saturday


	LoadAbbreviationFile (fileAbb);
	LoadNoSilenceWords (fileNoSilence);

	_nbOfWordInPhrase=nbOfWordInPhrase;
	_phrasePath=pathPhrase;
	//File::Reader::ReadTextFileLines( _phrase, pathPhrase, L"\t\r\n" );
	File::Reader::ReadTextFileLines( _phraseVeryShort, L"PhraseVeryShort.txt", L"\t\r\n" );

	std::vector<std::wstring> _tmpNon;
	File::Reader::ReadTextFileLines( _tmpNon, L"PhraseNonVeryShort.txt", L"\t\r\n" );
	for(int i=0;i<_tmpNon.size();i++)
	{
		std::vector<std::wstring> token;
		StringHelper::Tokenize(token,_tmpNon[i],L"_");	
		if(_phraseNotVeryShort.find(token[0])==_phraseNotVeryShort.end())
		{
			std::vector<std::wstring> lisPhrase;
			lisPhrase.push_back(token[1]);
			std::pair< std::wstring, std::vector<std::wstring> > tmpPair (token[0],lisPhrase);		
			_phraseNotVeryShort.insert(tmpPair) ;
		}
		else
		{
			_phraseNotVeryShort[token[0]].push_back(token[1]);
		}	
		 
	}


	std::vector<std::wstring> _tmp1;
	File::Reader::ReadTextFileLines( _tmp1, pathVeryShortWord, L"\t\r\n" );
	for(int i=0;i<_tmp1.size();i++)
	{
		std::vector<std::wstring> token;
		StringHelper::Tokenize(token,_tmp1[i],L" ");
		std::size_t pos = 0 ;
        int number = _wtoi( token[1].c_str() ) ;
		std::pair< std::wstring, int > tmpPair (token[0],number);		
		_veryShortWord.insert(tmpPair) ;
		 
	}

	_vietnameseLocal = ::_create_locale (LC_ALL, ".1258");
}



/**
* Constructor
*/
VietVoice::TokensToWords::TokensToWords  (const TokensToWords & tokensToWords)
{
	if (this != &tokensToWords)
	{
		//Contains word for thousand, million, etc.
		_threeDigits[0] = L"silence";
		_threeDigits[1] = L"ngàn";
		_threeDigits[2] = L"tri\x1EC7u";
		_threeDigits[3] = L"t\x1EF7";

		// Contains words for number from 1 to 9
		_oneDigit[0] = L"không";
		_oneDigit[1] = L"m\x1ED9t";
		_oneDigit[2] = L"hai";
		_oneDigit[3] = L"ba";
		_oneDigit[4] = L"b\x1ED1n";
		_oneDigit[5] = L"n\x0103m";
		_oneDigit[6] = L"sáu";
		_oneDigit[7] = L"b\x1EA3y";
		_oneDigit[8] = L"tám";
		_oneDigit[9] = L"chín";

		//Contains the names for the day of the week
		_dayOfWeek[0] = L"nh\x1EADt"; //Sunday
		_dayOfWeek[1] = L"hai";
		_dayOfWeek[2] = L"ba";
		_dayOfWeek[3] = L"t\x01B0";
		_dayOfWeek[4] = L"n\x0103m";
		_dayOfWeek[5] = L"sáu";
		_dayOfWeek[6] =L"b\x1EA3y"; //Saturday

		_wordBefore = tokensToWords._wordBefore;
		_abreviations = tokensToWords._abreviations ;
		_noSilenceAfter = tokensToWords._noSilenceAfter ;
		_toSynthetise = NULL ;
		_vietnameseLocal = tokensToWords._vietnameseLocal;
	}
}

void VietVoice::TokensToWords::operator = (const TokensToWords & tokensToWords)
{
	if (this != &tokensToWords)
	{
		//Contains word for thousand, million, etc.
		_threeDigits[0] = L"silence";
		_threeDigits[1] = L"ngàn";
		_threeDigits[2] = L"tri\x1EC7u";
		_threeDigits[3] = L"t\x1EF7";

		// Contains words for number from 1 to 9
		_oneDigit[0] = L"không";
		_oneDigit[1] = L"m\x1ED9t";
		_oneDigit[2] = L"hai";
		_oneDigit[3] = L"ba";
		_oneDigit[4] = L"b\x1ED1n";
		_oneDigit[5] = L"n\x0103m";
		_oneDigit[6] = L"sáu";
		_oneDigit[7] = L"b\x1EA3y";
		_oneDigit[8] = L"tám";
		_oneDigit[9] = L"chín";

		//Contains the names for the day of the week
		_dayOfWeek[0] = L"nh\x1EADt"; //Sunday
		_dayOfWeek[1] = L"hai";
		_dayOfWeek[2] = L"ba";
		_dayOfWeek[3] = L"t\x01B0";
		_dayOfWeek[4] = L"n\x0103m";
		_dayOfWeek[5] = L"sáu";
		_dayOfWeek[6] =L"b\x1EA3y"; //Saturday

		_wordBefore = tokensToWords._wordBefore;
		_abreviations = tokensToWords._abreviations ;
		_noSilenceAfter = tokensToWords._noSilenceAfter ;
		_toSynthetise = NULL ;
		_vietnameseLocal = tokensToWords._vietnameseLocal;
	}		
}

/**
*  Destructor
*/
VietVoice::TokensToWords::~TokensToWords ()
{}

void VietVoice::TokensToWords::Treat (ToBeSynthetise & toSynthetise)
{
	_toSynthetise = &toSynthetise;

	std::list <Token> listToken = toSynthetise.GetListToken ();
	Token token;
	u_int type=1;	
	u_int typeB1=-1;
	u_int typeB2=-1;

	for (std::list<Token>::iterator iter = listToken.begin (); iter != listToken.end (); iter++) // for 1
	{

		std::wstring phrase=L"";
		int nbWord=0;
		int arrayLength[40];
		std::list<Token>::iterator iterTmp=iter;
		for(int i=0;i<_nbOfWordInPhrase;i++)
		{
			token = *iterTmp;
			std::wstring tokenVal = token.GetToken ();
			
			for (unsigned int i = 0; i < tokenVal.length (); i++)
			{
				tokenVal [i] = ::_towlower_l (tokenVal[i], _vietnameseLocal) ;
			}
			

			phrase+=L" "+tokenVal;
			arrayLength[i]=phrase.length()-1;
			nbWord++;
			iterTmp++;
			if(iterTmp==listToken.end())
			{			
				break;
			}
		}		

		if(phrase.length()>0)
		{
			phrase=phrase.substr(1);
		}


		token = *iter;
		std::wstring tokenVal = token.GetToken ();
		//::MessageBox(NULL, StringHelper::ToString(token.GetPostPunctuation()).c_str(), L"size", MB_OK);

		for (unsigned int i = 0; i < tokenVal.length (); i++)
		{
			tokenVal [i] = ::_towlower_l (tokenVal[i], _vietnameseLocal) ;
		}
			

		std::vector<std::wstring> _phrase=LoadPhrase(tokenVal);

		while(nbWord>1)
		{	
			if(std::find(_phrase.begin(),_phrase.end(),phrase)!=_phrase.end())
			{		
			
				break;
			}
			else
			{
				nbWord--;
				if(nbWord>1)
				{
					phrase=phrase.substr(0,arrayLength[nbWord-1]);
				}

			}
		}

		for(int kt=0;kt<nbWord;kt++) // for 2
		{
			if(kt!=0)
			{
				iter++;
			}
			token = *iter;
			std::wstring tokenVal = token.GetToken ();	
			for (unsigned int i = 0; i < tokenVal.length (); i++)
			{
				tokenVal [i] = ::_towlower_l (tokenVal[i], _vietnameseLocal) ;
			}

			
			type=1;
			if(nbWord>1)
			{
				if(kt!=nbWord-1)
				{
					type=2;
				}
				else
				{
					type=3;
				}
			}


			//fix cham do thieu cum tu////////////////////////////////////////////////
			

			//////////// ket thuc fix cham do thieu cum tu////////////////////////////

			//::MessageBox(NULL, StringHelper::ToString(tokenVal).c_str(), L"size", MB_OK);
			if(type<3)
			{
				if(_veryShortWord.find(tokenVal)!=_veryShortWord.end())
				{					
					int typeVeryShort=_veryShortWord[tokenVal];					
					if(typeVeryShort==1)
					{
						// Xử lý trong trường hợp cụm từ veryshort
						std::list<Token>::iterator iterTam=iter;
						Token tokenTam=*iterTam;
						std::wstring valTam=tokenTam.GetToken();
						for(int i=0;i<5;i++)
						{
							iterTam++;
							if(iterTam!=listToken.end())
							{
								 tokenTam=*iterTam;
								 valTam +=L" "+tokenTam.GetToken();
								 if(std::find(_phraseVeryShort.begin(),_phraseVeryShort.end(),valTam)!=_phraseVeryShort.end())
								 {
									type=4;
									break;
								 }
								 else
								 {
									break;
								 }
							}
							else
							{
								break;
							}
						}
					}
					else
					{
						if(_phraseNotVeryShort.find(tokenVal)!=_phraseNotVeryShort.end())
						{
							std::vector<std::wstring> listPhrase=_phraseNotVeryShort[tokenVal];

							std::list<Token>::iterator iterTam=iter;
							Token tokenTam=*iterTam;
							std::wstring valTam=tokenTam.GetToken();
							for(int i=0;i<5;i++)
							{								
								if(iterTam!=listToken.begin())
								{
									iterTam--;
									 tokenTam=*iterTam;
									 valTam =tokenTam.GetToken()+L" "+valTam;									 
								}
								else
								{
									break;
								}
							}

							iterTam=iter;
							for(int i=0;i<5;i++)
							{			
								iterTam++;
								if(iterTam!=listToken.end())
								{									
									 tokenTam=*iterTam;
									 valTam +=L" "+tokenTam.GetToken();									 
								}
								else
								{
									break;
								}
							}
							
							bool ktVS=true;
							for (std::vector<std::wstring>::iterator it = listPhrase.begin (); it != listPhrase.end (); it++) // for 1
							{
								if(valTam.find(*it)!=std::string::npos)
								{
									ktVS=false;
									break;
								}
							}

							if(ktVS==true)
							{
								type=4;
							}

						}
						else
						{
							type=4;
						}
					}
				}
			}
			
			

			v_tokenVal = tokenVal; // VT
			if (tokenVal == L"v.v" || tokenVal == L" v.v" || tokenVal == L"   v.v") {
				ProcessWord(L"vân",type);
				ProcessWord(L"vân",type);
				_wordBefore = tokenVal;
				continue;
			}
			if (iter != listToken.end()) {
				iter ++;
				if (iter != listToken.end()) {
					std::wstring wordAfter = iter->GetToken ();
					v_wordAfter = wordAfter; // VT
				} else v_wordAfter = L"" ;
				iter --;
			} 
			// End Viet them - Xoa
			// VT
			if(StringHelper::OneOfCharPresent(tokenVal, L"-") && _wordBefore != L"ngày" && _wordBefore != L"hôm" && _wordBefore != L"nay" && _wordBefore != L"mai" && _wordBefore != L"qua" && _wordBefore != L"tháng" && _wordBefore != L"sáng" && _wordBefore != L"chiều" && _wordBefore != L"tối") {
				int v_loc = 0;
				for (unsigned int i = 0; i < tokenVal.length(); i++) 
					if (tokenVal[i] == L'-') v_loc = i ;
				if (v_loc > 0 && IsTokenNumberOrSymbol(tokenVal.substr(0,v_loc)) && IsTokenNumberOrSymbol(tokenVal.substr(v_loc+1,tokenVal.length()-v_loc-1)))  
				{
					//			::MessageBox(NULL, tokenVal.substr(v_loc+1,tokenVal.length()-v_loc-1).c_str(), L"Test", MB_OK);
					PreprocessNumbers(tokenVal.substr(0,v_loc), token,type);
					ProcessWord(L"đến",type);
					PreprocessNumbers(tokenVal.substr(v_loc+1,tokenVal.length()-v_loc-1), token,type);
					_wordBefore = tokenVal;
					continue ;
				}
			}
			// End VT
			StringHelper::ReplaceAll (tokenVal, L'\x00D0', L'\x0111'); // Two kinds of D- (code 208 and 272) replace the D- 208 by D- 272.
			StringHelper::ReplaceAll (tokenVal, L'\x0110', L'\x0111');

			StringHelper::ReplaceAll (tokenVal, L'\x00CC', L'\x00EC'); // VT Ì
			StringHelper::ReplaceAll (tokenVal, L'\x00D2', L'\x00F2'); // VT Ò


			for (unsigned int i = 0; i < tokenVal.length (); i++)
			{
				tokenVal [i] = ::_towlower_l (tokenVal[i], _vietnameseLocal) ;
			}
			StringHelper::ReplaceAll(tokenVal, L'–', L'-'); // - and – are the same, change – for - so they are treated the save

			ToBeSynthetiseData::WORD_TYPE word_typeWORD=ToBeSynthetiseData::WORD;
			if(type==2)
				word_typeWORD=ToBeSynthetiseData::WORD_G;
			if(type==3)
				word_typeWORD=ToBeSynthetiseData::WORD_GE;
			if(type==4)
				word_typeWORD=ToBeSynthetiseData::VSWORD;


			if (_abreviations.find (tokenVal) != _abreviations.end ()) // If the word is a known abreviation or symbol
			{
				if (v_tokenVal != L"Ca" && v_tokenVal != L"Ca" && v_tokenVal != L"ca") {
					std::list <std::wstring> list = _abreviations[tokenVal];
					int countAbreviations=0;
					for (std::list<std::wstring>::iterator iter1 = list.begin (); iter1 != list.end (); iter1++) 
					{
						std::wstring str1 = iter1->c_str ();
						//Thuan add 13-02-2020/////
						std::list<std::wstring>::iterator iterNext1=iter1;
						iterNext1++;

						VietVoice::Token tokenTmp=token;
						if(iter1==list.begin())
						{
							tokenTmp.SetPostPunctuation(L"");	
							if(type<4)
							{
								type=2;
							}
						}
						else
						{
							if(iterNext1!=list.end ())
							{
								tokenTmp.SetPostPunctuation(L"");
								tokenTmp.SetPrePunctuation(L"");
								if(type<4)
								{
									type=2;
								}
							}
							else
							{
								tokenTmp.SetPrePunctuation(L"");
								if(type<4)
								{
									type=1;
								}
							}
						}
						///End Thuan add///////////

											
						if(type==2)
							word_typeWORD=ToBeSynthetiseData::WORD_G;
						if(type==3)
							word_typeWORD=ToBeSynthetiseData::WORD_GE;
						if(type==4)
							word_typeWORD=ToBeSynthetiseData::VSWORD;
						if (str1.length() == 1) { // 13-9-2012							
								ProcessNormalWord (str1, tokenTmp, word_typeWORD,type) ; // 31-8-2012 SYMBOL -> ABBR	
						}
						else {
							ToBeSynthetiseData::WORD_TYPE word_typeABBR_S=ToBeSynthetiseData::ABBR_S;
							if(type==2)
								word_typeABBR_S=ToBeSynthetiseData::ABBR_S_G;
							if(type==3)
								word_typeABBR_S=ToBeSynthetiseData::ABBR_S_GE;
							if(type==4)
								word_typeABBR_S=ToBeSynthetiseData::VSWORD;
							ProcessNormalWord (str1, tokenTmp, word_typeABBR_S,type) ; // 31-8-2012 SYMBOL -> ABBR						
						}
					}
				}
				else
					ProcessNormalWord(tokenVal, token, word_typeWORD,type);
			} else				
				if (ProcessDate (tokenVal, token,type,token.GetPostPunctuation())) //  Process the token if it's a date.
				{
					
				}
				else if (ProcessTime(tokenVal, token,type)) // VT
				{
				}
				else if (ProcessUnit(tokenVal, token,type)) // VT
				{
				}
				else
				{

					//Take care of spaces between numbers.
					if (iter != listToken.end () && IsTokenNumberOrSymbol (tokenVal) &&  StringHelper::NbChar (tokenVal, L'-') != tokenVal.length())
					{
						std::wstring tmpTokenVal = L"";
						Token tmpToken;
						tokenVal = L"";
						bool firstNumber = true; // First number no need to be 3 digit long

						// Combine following tokens containing number when appropriate (3 in length)
						do
						{
							tmpToken = *iter;
							tmpTokenVal = tmpToken.GetToken();
							StringHelper::ReplaceAll(tmpTokenVal, L'–', L'-');// - and – are the same, change – for - so they are treated the save
							tmpTokenVal = _wcslwr (&tmpTokenVal[0]);

							// If the next token is a number that is 3 digit or the first number
							if (IsTokenNumberOrSymbol(tmpTokenVal)&& ((tmpTokenVal.length() == 3 && firstNumber == false) || (firstNumber == true)))
								tokenVal += tmpTokenVal;
							else
								break;

							iter++;

							firstNumber = false;
						} while (iter != listToken.end ());

						if (iter != listToken.begin ()) // One token to far, go back
							iter--;
					}

					if ( StringHelper::NbChar (tokenVal, '-') != tokenVal.length() && ProcessSlashBetweenNumbers (tokenVal, token,type))// Process the token if it's a number
					{

					}
					else if (ProcessAlphaNumeric (tokenVal, token,type)) // Process the token if it is an alpha-numeric string
					{

					}
					else if (ProcessSymbols(tokenVal, token,type)) 
					{ 
					}
					else if (ProcessRoman(tokenVal, token,type)) // VT
					{
					}
					else // Process "normal" words
					{
						// Create a new word from the token

						ProcessNormalWord(tokenVal, token, word_typeWORD,type);
					}

				}
				_wordBefore = tokenVal;

		}// end for 2

	} // End for 1

	
}

/*
* Convert a number to text if it is a number
* @param tokenVal String representing the token
* @param tokenItem Object representing the token
* return true if the conversion was possible else false
*/
// Type : ( 1: normal, 2: not end phrase, 3: end phrase, 4: veryshort)
bool VietVoice::TokensToWords::PreprocessNumbers (std::wstring tokenVal, VietVoice::Token & token,u_int type)
{
	//	::MessageBox(NULL, tokenVal.c_str(), L"", MB_OK);
	size_t lastComa = tokenVal.find_last_of(L'.') ; // Find position of the last coma in the number VX
	//size_t lastComa = tokenVal.find_last_of(L',') ; // Find position of the last coma in the number // VS

	size_t lengthNumber = tokenVal.size ();

	// Get rid of potential useless punctiation like coma after number
	while (lengthNumber > 0 && (tokenVal[lengthNumber - 1] < L'0' || tokenVal[lengthNumber - 1] > L'9'))
		lengthNumber--;

	size_t indexDot = tokenVal.find_first_of(L'.'); // Index of next dot

	std::wstring tmpTokenVal = L"";

	// Determines if number with dot is decimal number OR thousand, millions ect.
	for (unsigned int j = 0;j < tokenVal.size();j++) // For every digits
	{
		if (j != indexDot) // If not dot position
		{
			tmpTokenVal += tokenVal [j]; // Copy current character
		}
		else // Determine if dot must be remove cause number is thousand, million, etc and not decimal (1.000 = 1000)
		{
			// Check if the next dot is not 3 digits away, dot must not be removed
			if (j + 4 < tokenVal.size() && (tokenVal[j + 4] != L'.' || ( tokenVal[j + 3] == L'.' || tokenVal[j +2] == L'.'|| tokenVal[j +1] == L'.')))
			{
				tmpTokenVal += L".";
			}
			else if (j + 4 > tokenVal.size()) // If the last dot is not three digit from the end, dot must not be removed
			{
				tmpTokenVal += L".";
			} // if four digit away is end of number but there are other dots after the dot, the dot must not be removed
			else if (j + 4 == tokenVal.size() && (tokenVal[j + 3] == L'.' || tokenVal[j +2] == L'.'|| tokenVal[j +1] == L'.'))
			{
				tmpTokenVal += L".";
			}

			// If all that is false, dot is removed (cases like 1.000 or 1.000.000
			indexDot = tokenVal.find_first_of (L'.',indexDot + 1);
		}
	}

	tokenVal = tmpTokenVal;

	std::vector <std::wstring> tokens;

	if (StringHelper::NbChar (tokenVal,L'.') <= 1) // if only one point, decimal number else number divided by points
	{
		StringHelper::ReplaceAll(tokenVal, L'/', L'-') ; // Replace all'/' by '-' to facilitate treatement
		StringHelper::Tokenize (tokens, tokenVal, L"-"); 
	}
	else
	{
		// Replace '/' and '-' by '.' to facilitate treatement
		StringHelper::ReplaceAll(tokenVal, L'/', L'.');
		StringHelper::ReplaceAll(tokenVal, L'-', L'.');

		//tokenizer = new StringTokenizer (tokenVal, L".");
		StringHelper::Tokenize (tokens, tokenVal, L".");
	}

	// Gets every number between '.', '-' and '/'
	// while(tokenizer->HasMoreTokens ())
	for (std::vector<std::wstring>::iterator iter = tokens.begin (); iter != tokens.end(); iter++)
	{
		tokenVal = *iter ;
		std::wstring v_char = L"";
		// VX size_t index = tokenVal.find_first_of (L"."); // Find a dot if there is one
		size_t index = tokenVal.find_first_of (L"."); // VS Find a comma if there is one		
		if (index == -1) {
			index = tokenVal.find_first_of (L","); // VT
			if (index != -1) v_char = L"phẩy" ;
		}
		else v_char = L"chấm" ;
		std::wstring numberBeforeDot = L"";
		std::wstring numberAfterDot = L"";
		std::wstring symbol = L"";

		// ::MessageBox(NULL, StringHelper::ToString (v_char).c_str(), L"Token to Words", MB_OK); // VT

		if (index != -1)  // Dot present, decimal number
		{
			numberBeforeDot = tokenVal.substr(0, index); // Get number before dot
			numberAfterDot = tokenVal.substr(index + 1, tokenVal.length() - index - 1); // Get number after dot

			// Treats the case there is a %, $ or us$ or &can, or usdafter the number
			if (StringHelper::EndWith(numberAfterDot, L"%"))
			{
				symbol = numberAfterDot.substr(numberAfterDot.length() - 1, 1);
				numberAfterDot = numberAfterDot.substr(0, numberAfterDot.length() - 1);

			}
			else if (StringHelper::EndWith(numberAfterDot, L"$us") ||
				StringHelper::EndWith(numberAfterDot, L"usd") ||
				StringHelper::EndWith(numberAfterDot, L"us\x0111") ||
				StringHelper::EndWith(numberAfterDot, L"us$")) 
			{
				symbol = numberAfterDot.substr(numberAfterDot.length() - 3, 3);
				numberAfterDot = numberAfterDot.substr(0, numberAfterDot.length() - 3);
			}
			else if (StringHelper::EndWith(tokenVal, L"$")) 
			{
				symbol = numberAfterDot.substr(numberAfterDot.length() - 1, 1);
				numberAfterDot = numberAfterDot.substr(0,numberAfterDot.length() - 1);
			}
			else if (StringHelper::EndWith(numberAfterDot, L"$can")) 
			{
				symbol = numberAfterDot.substr(numberAfterDot.length() - 4,4);
				numberAfterDot = numberAfterDot.substr(0, numberAfterDot.length() - 4);
			}
		}
		else  // Not a decimal number
		{
			numberBeforeDot = tokenVal; // No dot, so no number after dot
			symbol = L"";
			numberAfterDot = L"";

			// Treats the case there is a %, $ or us$ or &can, or usdafter the number
			if (StringHelper::EndWith(numberBeforeDot, L"%"))
			{
				symbol = numberBeforeDot.substr(numberBeforeDot.length() - 1, 1);
				numberBeforeDot = numberBeforeDot.substr(0, numberBeforeDot.length() - 1);
				//::MessageBox(NULL, StringHelper::ToString(symbol).c_str(), L"Phan tram", MB_OK);
			}
			else if (StringHelper::EndWith(numberBeforeDot, L"$us") ||
				StringHelper::EndWith(numberBeforeDot, L"us$") ||
				StringHelper::EndWith(numberBeforeDot, L"usd") ||
				StringHelper::EndWith(numberBeforeDot, L"us\x0111")) 
			{
				symbol = numberBeforeDot.substr(numberBeforeDot.length() - 3, 3);
				numberBeforeDot = numberBeforeDot.substr(0, numberBeforeDot.length() - 3);
			}
			else if (StringHelper::EndWith(numberBeforeDot, L"$")) 
			{
				symbol = numberBeforeDot.substr(numberBeforeDot.length() - 1, 1);
				numberBeforeDot = numberBeforeDot.substr(0,
					numberBeforeDot.length() - 1);
			}

			else if (StringHelper::EndWith(numberBeforeDot, L"$can"))
			{
				symbol = numberBeforeDot.substr(numberBeforeDot.length() - 4, 4);
				numberBeforeDot = numberBeforeDot.substr(0, numberBeforeDot.length() - 4);
			}
		}
		// Number is too long
		if (numberBeforeDot.length() > 12) //|| numberAfterDot.length () > 12)
		{
			return false;
		}

		// Mising number before or after '.'
		if (numberBeforeDot.length() <= 0 && numberAfterDot.length() <= 0)
		{
			return false;
		}

		// Symbol before '.' is not a number
		if (! (numberBeforeDot.length() <= 0) && !IsTokenNumber(numberBeforeDot))
		{
			return false;
		}
		// Symbol after '.' is not a number
		if (! (numberAfterDot.length() <= 0) && !IsTokenNumber(numberAfterDot))
		{
			return false;
		}
		// Process the part of the numer before the '.'
		if (numberBeforeDot.length() > 0)
			ProcessNumbers(numberBeforeDot,type,L"");
		// If there is a '.' prononce the '.'
		if (index != -1) 
		{
			ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
			if(type==2)
				word_type=ToBeSynthetiseData::NUMBER_G;
			if(type==3)
				word_type=ToBeSynthetiseData::NUMBER_GE;
			if(type==3)
				word_type=ToBeSynthetiseData::VSWORD;

			ToBeSynthetiseData w1(v_char, word_type); // VS
			//::MessageBox(NULL, StringHelper::ToString(v_char).c_str(), L"Saving file", MB_OK); // VT

			//ToBeSpokenData w2 (L"silence");

		

			_toSynthetise->AppendData (w1);


		}

		// Process the part of the numer after  the '.'
		if (numberAfterDot.length() > 0)
			ProcessDecimal(numberAfterDot,type);

		// If there was a symbol after the number, process it
		if (symbol.length() > 0) 
		{
			//::MessageBox(NULL, StringHelper::ToString(symbol).c_str(), L"Symbol", MB_OK); // VT
			ProcessSymbols(symbol, token,type);
		}
	}

	return true;
}

/**
* Treats the number by removing slashes between them if there are some and then pass each number to another method for further processing
* @param tokenVal The name of the token
* @param token The token object
* @return True if the token was processed as a number, else false
*/
bool VietVoice::TokensToWords::ProcessSlashBetweenNumbers (std::wstring tokenVal, VietVoice::Token token,u_int type)
{
	//::MessageBox (NULL, StringHelper::ToString (tokenVal).c_str (), L"Input: Slash", MB_OK) ;

	size_t indexFrom = 0;
	size_t index = 0;
	std::wstring subTokenVal = L"";

	if (!IsTokenNumberOrSymbol (tokenVal)) // Determines if a token is indeed a number
		return false;

	//::MessageBox (NULL, StringHelper::ToString (tokenVal).c_str (), L"Symbol: Slash", MB_OK) ;

	// All this while removes '/', if no '/' does nothing
	do
	{
		// Find the next /
		index = tokenVal.find_first_of('/', indexFrom);

		if (index == 0) 
		{
			indexFrom = 1;
		}
		else if (index == tokenVal.size() - 1)
		{
			break;
		}

		else if (index != -1) 
		{
			subTokenVal = tokenVal.substr(indexFrom, index - indexFrom); // Get the next number between somme '/'

			indexFrom = index + 1;

			if (subTokenVal != L"") // If there was indeed a '/' prononce it
			{
				//MessageBox(NULL, StringHelper::ToString(tokenVal.substr(index + 1)).c_str(), StringHelper::ToString(index).c_str(), MB_OK) ;
				if ((subTokenVal == L"1" || subTokenVal == L"2" || subTokenVal == L"3" ||subTokenVal == L"4" || 
					subTokenVal == L"5" || subTokenVal == L"6" || subTokenVal == L"7" || subTokenVal == L"8" || 
					subTokenVal == L"9" || subTokenVal == L"01" || subTokenVal == L"01" || subTokenVal == L"03" || 
					subTokenVal == L"04" || subTokenVal == L"05" || subTokenVal == L"06" || subTokenVal == L"07" || 
					subTokenVal == L"08" || subTokenVal == L"09" || subTokenVal == L"10" || subTokenVal == L"11" || 
					subTokenVal == L"12") && StringHelper::ToInt(tokenVal.substr(index + 1)) >= 1900) {
						if (_wordBefore != L"tháng") {
							ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
							if(type==2)
								word_type=ToBeSynthetiseData::NUMBER_G;
							if(type==3)
								word_type=ToBeSynthetiseData::NUMBER_GE;
							if(type==4)
								word_type=ToBeSynthetiseData::VSWORD;

							ToBeSynthetiseData w1(L"tháng", word_type);
							_toSynthetise->AppendData (w1 );
							_wordBefore = L"tháng" ;
						}
				}
				PreprocessNumbers (subTokenVal, token,type);
				if (_wordBefore != L"tháng") {// VT
					ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
					if(type==2)
						word_type=ToBeSynthetiseData::NUMBER_G;
					if(type==3)
						word_type=ToBeSynthetiseData::NUMBER_GE;
					if(type==4)
						word_type=ToBeSynthetiseData::VSWORD;
					ToBeSynthetiseData w1(L"trên", word_type);
					_toSynthetise->AppendData (w1 );
				}
				else {
					ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
					if(type==2)
						word_type=ToBeSynthetiseData::NUMBER_G;
					if(type==3)
						word_type=ToBeSynthetiseData::NUMBER_GE;
					if(type==4)
						word_type=ToBeSynthetiseData::VSWORD;
					ToBeSynthetiseData w1(L"năm", word_type);
					_toSynthetise->AppendData (w1 );
				}
			}
		}
	}
	while (index != -1);

	// process the last number between slashes if there is one
	if (index == tokenVal.length() - 1)
	{
		subTokenVal = tokenVal.substr(indexFrom, tokenVal.length() - 1 - indexFrom);
	}
	else
	{
		subTokenVal = tokenVal.substr(indexFrom, tokenVal.length() - indexFrom);
	}

	if (subTokenVal != L"") // if there is a number, process it
	{
		PreprocessNumbers (subTokenVal, token,type);
	}
	return true;
}

/**
* Determines if the token is a date.
* @param tokenVal String representing the token
* @return true if the token is a date, else false
*/
bool VietVoice::TokensToWords::IsDate (std::wstring tokenVal)
{
	//::MessageBox(NULL, StringHelper::ToString(tokenVal).c_str(), L"Saving file", MB_OK); // VT
	StringHelper::ReplaceAll (tokenVal, L'-', L'.') ; 
	StringHelper::ReplaceAll (tokenVal, L'/', L'.') ;
	//::MessageBox(NULL, StringHelper::ToString(tokenVal).c_str(), L"Tokens to Words", MB_OK); // VT
	size_t index = 0;

	int nbDots = StringHelper::NbChar (tokenVal, L'.') ;

	if (nbDots != 2 && nbDots != 1) //  Two /, - or .
		return false;

	if (nbDots == 2) // Date with year
	{
		if (tokenVal.size() < 5 || tokenVal.size() > 10) //  Valid length
			return false;

		index = tokenVal.find_last_of (L'.') ;

		if (index != 3 && index != 4 && index != 5) // /, - or . at the right place
			return false;
	}
	else
	{
		if (_wordBefore != L"ngày" && _wordBefore != L"sáng" && _wordBefore != L"chiều" && _wordBefore != L"tối" && _wordBefore != L"nay" && _wordBefore != L"hôm" && _wordBefore != L"mai" && _wordBefore != L"qua")
			return false;
		if (tokenVal.size() < 3 || tokenVal.size() > 5) //  Valid length
			return false;
	}

	index = tokenVal.find_first_of (L'.') ; // /, - or . at the right place
	if (index != 1 && index != 2)
		return false ;

	for (unsigned int i = 0; i < tokenVal.size (); i++) // / Only digits 0 to 9, /, - or .
	{
		if ((tokenVal[i] < '0'|| tokenVal[i] > '9') && tokenVal[i] != '.')
			return false;
	}

	return true ;
}

/**
*
* Loads a file containing the abbreviations and how to prononce them.
* @param filename Name and path of the file to be loaded
*/
void VietVoice::TokensToWords::LoadAbbreviationFile (std::wstring filename)
{
	try
	{
		_abreviations.clear (); // Empty the abbreviation map
		std::wstring line;
		File::Reader reader (filename);

		long count = reader.ReadLong (); // Read the number of lines in the file

		for (int i = 0; i < count; i++)  // For each line
		{
			line = reader.ReadWString ();  // Read the next line


			if (line != L"")
			{
				// Cannot use wcstok on a wstring object it doesnt work well, so copy the the string into
				// a a wchar_t array.  At the same thime, use _wcslwr in order to 
				// put the text to lower case.
				//StringTokenizer tokenizer (line, L" \t");			  
				std::vector <std::wstring> tokens;
				StringHelper::Tokenize (tokens, line, L" \t\n");

				// Use the wcstok function to get the first word of the line!
				std::wstring key;

				std::vector<std::wstring>::iterator iter = tokens.begin();
				if (iter != tokens.end())//(tokenizer.HasMoreTokens ())
				{
					key = *iter;
					iter++;
				}

				// Get the rest of the words in the line, an but them in a list.
				// Basiquely, the first word of the line is an abreviation, and
				// the rest of the line are the words that it abbreviate
				std::list <std::wstring> list;
				std::wstring word;
				for (;iter != tokens.end(); iter++)
				{
					word = *iter;
					list.push_back (word);
					//////// if (word[0] == L'ơ')
					//////// MessageBox(NULL, StringHelper::ToString(word).c_str(), StringHelper::ToString(key).c_str(), MB_OK) ;
				}

				// Add the key/list pairt to a map so they can be easily found again
				std::pair< std::wstring, std::list<std::wstring> > tmpPair (key, list);
				_abreviations.insert(tmpPair); 
			}
		}

	}
	catch (File::Exception ex) // Something wrong happened
	{
		throw VietnameseTTException(L"Something went wrong while reading the abbreviation file in TokenToWords" + ex.GetMsg ());
	}
}



/**
* Determines if the token is a number.
* @param tokenVal String representing the token
* @return true if the token is a number, else false
*/
bool VietVoice::TokensToWords::IsTokenNumber (std::wstring tokenVal)
{
	for (unsigned int i = 0; i < tokenVal.length (); i++) // Passe chaque lettre de la chaine / Pass every letter of the string
	{
		if (!(tokenVal [i] >= '0' && tokenVal [i] <= '9')) // If the letter is not a digit (0-9), then not a number
		{
			return false ;
		}
	}

	return true ;
}

/**
* Loads a file containing a list of words after witch there must be no silence
* @param filename Name of the file to be loaded
*/
void VietVoice::TokensToWords::LoadNoSilenceWords (std::wstring filename) 
{
	try
	{
		std::wstring line;

		File::Reader reader (filename);

		long count = reader.ReadLong (); // Read the number of lines in the file
		for (int i = 0; i < count; i++)  // For each line
		{
			line = reader.ReadWString ();  // Read the next line

			if (line != L"")
			{
				std::pair< std::wstring, bool > tmpPair (line, true);
				_noSilenceAfter.insert(tmpPair); // Can then use the find method
				// to find if a word is in there,
				// if word not there, find return end ().
			}
		}
	}
	catch (File::Exception ex) // Something wrong happened
	{
		throw VietnameseTTException(L"Something went wrong while reading the noSilence file in TokenToWords" + ex.GetMsg ());
	}
}

/*
* Determines if the token is a number or a '.', ',', '-' or '/'.
* @param tokenVal String representing the token
* @return true if the token is a number or a '.', ',', '-' or '/', else false
*/
bool VietVoice::TokensToWords::IsTokenNumberOrSymbol (std::wstring tokenVal)
{
	size_t length = tokenVal.length();

	if (StringHelper::EndWith(tokenVal, L"$") || StringHelper::EndWith(tokenVal, L"%"))
	{
		if (length == 1) // Only the symbol, so not a number
			return false;

		length -= 1;
	}

	// Allow certain symbols after number
	if (StringHelper::EndWith(tokenVal, L"$us") || StringHelper::EndWith(tokenVal, L"usd") ||StringHelper::EndWith(tokenVal, L"us\x0111") ||StringHelper::EndWith(tokenVal, L"us$"))
	{
		if (length == 3)// Only the symbol, so not a number
			return false;

		length -= 3;
	}

	if (StringHelper::EndWith(tokenVal, L"$can"))
	{
		if (length == 1) // Only the symbol, so not a number
			return false;

		length -= 4;
	}

	for (size_t i = 0; i < length; i++) 
	{ // Pass every letter of the string
		if (! (tokenVal[i] >= L'0' && tokenVal[i] <= L'9')  && tokenVal[i] != '.' && tokenVal[i] != L',' && tokenVal[i] != L'-' && tokenVal[i] != L'/') 
		{ // Si la lettre n'est pas un chiffre (0-9), ce n'est pas un nombre / If the letter is not a digit (0-9), then not a number
			return false;
		}
	}
	return true;
}

/**
* Adds 0 to the begining of a string of digit until the total number of digits is a multiple of
* 3
* @param tokenVal String of digit representing the token
* @return The string of digit with some 0 added at the beginning
*/

void VietVoice::TokensToWords::AddZeros (std::wstring & tokenVal)
{
	// Determine the number of 0 to add
	size_t NbChar = tokenVal.length () ;
	size_t nbZeroToAdd = 3 - (NbChar % 3);

	if (nbZeroToAdd != 3) // If nbZeroToAdd == 3, the number of character is already a multiple of three
	{
		//std::wstring zero =
		for(size_t i = 0; i < nbZeroToAdd; i++)
			tokenVal = L"0" + tokenVal ;
	}
}

/**
* Convert the number to text if it is a number
* @param tokenVal String representing the token
* return true if the conversion was possible else false
*/
bool VietVoice::TokensToWords::ProcessNumbers (std::wstring tokenVal,u_int type,std::wstring posPun)
{
	//	std::wstring symbol = L"";

	if (StringHelper::NbChar (tokenVal, L'0') == tokenVal.size ()) // The number is 0
	{
		ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
		if(type==2)
			word_type=ToBeSynthetiseData::NUMBER_G;
		if(type==3)
			word_type=ToBeSynthetiseData::NUMBER_GE;
		if(type==4)
			word_type=ToBeSynthetiseData::VSWORD;

		ToBeSynthetiseData w1(L"không", word_type);
		w1.SetPostPunctuation(posPun);
		_toSynthetise->AppendData (w1);
	}
	else
	{
		AddZeros(tokenVal); //Prepare the string the digits for the convertion
		ConvertNumber(tokenVal,type,posPun); //Convert the string of digit
	}

	return true;
}

/**
* Convert the date to text if it is a date
* @param tokenVal String representing the token
* return true if the conversion was possible else false
*/
bool VietVoice::TokensToWords::ProcessDate (std::wstring tokenVal, Token & token,u_int type,std::wstring posPre)
{

	

	if (!IsDate (tokenVal)) // if its a date
		return false ;

		


	Date date (tokenVal) ;

	int month = date.GetMonthInt ();

	int day = date.GetDayInt ();

	if (day <= 0 || day > 31) // Impossible day
		return false;

	if (month <= 0 || month > 12) // Impossible month
		return false;

	if (!ConvertDay (date,type)) // Convert the day
		return false;

	//Add month
	if (!ConvertMonth (date,type)) //Convert the month
		return false;

	if (date.GetYearInt () >= 0)
		ConvertYear (date,type,posPre) ; // Convert the year
	return true;
}

/**
* Convert the day of the date to texte
* @param date Object representing the date
* @return true if the convertion was succesful, else false
*/
bool VietVoice::TokensToWords:: ConvertDay (Date & date,u_int type)
{
	/* VX
	tm * calendar = NULL;

	if (date.GetYearInt () < 0)
	{
	tm tmpCalendar;

	time_t t = time (NULL);

	calendar = ::gmtime (&t);

	::ZeroMemory (&tmpCalendar, sizeof (tmpCalendar));

	tmpCalendar.tm_mday = date.GetDayInt ();
	tmpCalendar.tm_mon = date.GetMonthInt ();
	tmpCalendar.tm_year = calendar->tm_year;

	t = ::mktime (&tmpCalendar);

	calendar = ::gmtime (&t);
	::MessageBox(NULL, StringHelper::ToString(calendar).c_str(), L"Tokens to Words", MB_OK); // VT

	}
	else
	{
	tm tmpCalendar;

	::ZeroMemory (&tmpCalendar, sizeof (tmpCalendar));

	tmpCalendar.tm_mday = date.GetDayInt ();
	tmpCalendar.tm_mon = date.GetMonthInt ();
	tmpCalendar.tm_year = date.GetYearInt ();
	time_t t = ::mktime (&tmpCalendar);
	calendar = gmtime (&t);
	}

	if (calendar != NULL)
	{
	int day = calendar->tm_wday ; //  Gets the day of week

	if (day == 0) //  Sunday is a special case
	{
	ToBeSynthetiseData  w1 (L"ch\x1EE7", ToBeSynthetiseData::DATE);
	_toSynthetise->AppendData (w1);
	}
	else // all other days
	{
	ToBeSynthetiseData w1 (L"th\x1EE9", ToBeSynthetiseData::DATE);
	_toSynthetise->AppendData (w1);
	}

	ToBeSynthetiseData  w1 (_dayOfWeek [day], ToBeSynthetiseData::DATE);
	_toSynthetise->AppendData (w1);
	}
	*/
	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;

	if (_wordBefore != L"ngày" && _wordBefore != L"hai" && _wordBefore != L"ba" && _wordBefore != L"tư" && _wordBefore != L"năm" && _wordBefore != L"sáu" && _wordBefore != L"bảy" && _wordBefore != L"nhật" && _wordBefore != L"nhựt" && _wordBefore != L"hôm" && _wordBefore != L"mai" && _wordBefore != L"qua") {

		ToBeSynthetiseData  w1 (L"ngày", word_typeDATE);
		_toSynthetise->AppendData (w1);
	}
	ProcessNumbers (date.GetDayString (),type,L"") ; // Add the number of the day (1 to 31)
	return true;
}

/**
* Convert the month of the date to texte
* @param date Object representing the date
* @return true if the convertion was succesful, else false
*/
bool VietVoice::TokensToWords::ConvertMonth (Date & date,u_int type)
{
	int month = date.GetMonthInt ();
	// Sua 7/7/2012
	//ToBeSynthetiseData ws (L"_silence_", ToBeSynthetiseData::WORD); // VT
	//_toSynthetise->AppendData (ws); // VT / Xoa ngay 18-8-2012
	//_toSynthetise->AppendData (ws); // VT/ Xoa ngay 18-8-2012

	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;



	ToBeSynthetiseData w1 (L"tháng", word_typeDATE);
	_toSynthetise->AppendData (w1);

	if (month == 1)  // January is a special case
	{
		ToBeSynthetiseData w2 (L"một", word_typeDATE); // giêng
		_toSynthetise->AppendData (w2);
	}
	else if (month == 4) //  April is a special case
	{
		ToBeSynthetiseData w2 (L"t\x01B0", word_typeDATE);
		_toSynthetise->AppendData (w2);
	}
	else // All other words
		ProcessNumbers(date.GetMonthString(),type,L"");
	return true;
}

/**
* Convert the year of the date to texte
* @param date Object representing the date
* @return true if the convertion was succesful, else false
*/
void VietVoice::TokensToWords::ConvertYear (Date & date,u_int type,std::wstring posPre)
{
	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;


	ToBeSynthetiseData w1 (L"n\x0103m",word_typeDATE);
	_toSynthetise->AppendData (w1);

	std::wstring year = date.GetYearString ();

	if (year != L"-1")
		ProcessNumbers (year,type,posPre); 
}

/**
*
* Take a string of digits and convert it into a string of words
* @param tokenVal The string of digit
*/
void VietVoice::TokensToWords::ConvertNumber (std::wstring tokenVal,u_int type,std::wstring posPun)
{
	bool addLe = false;

	for (unsigned int i = 0; i < tokenVal.length(); i++) // For every digit (character)
	{
		if (i % 3 == 0) //Take care of converting the x in the following exemple:  x00x00x00
		{
			std::wstring pp=L"";
			if(i==tokenVal.length()-1)
			{
				pp=posPun;
			}
			if(ConvertHundred(tokenVal[i],type,pp))
				addLe = true;

		}
		else if(i%3 == 1)  //  Take care of converting the y in the following exemple:  0y00y00y0
		{
			std::wstring pp=L"";
			if(i==tokenVal.length()-1)
			{
				pp=posPun;
			}

			if(ConvertTen(tokenVal[i],type,pp))
				addLe = true;
		}
		else // Take care of converting the z in the following exemple 00z00z00z
		{
			std::wstring pp=L"";
			if(i==tokenVal.length()-1)
			{
				pp=posPun;
			}
			if(ConvertDigit(tokenVal[i], tokenVal[i - 1], tokenVal[i - 2], (int) (tokenVal.length() - i - 1) / 3, addLe,type,pp))
				addLe = true;
		}
	}
}

/**
* Convert the hundred digit
* @param hundredDigit The digit to convert
* @return true if hundred digit is not 0, else false;
*/
bool VietVoice::TokensToWords::ConvertHundred (wchar_t hundredDigit,u_int type,std::wstring posPun)
{
	if (hundredDigit != '0') // if the digit is not 0
	{
		ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
		if(type==2)
			word_type=ToBeSynthetiseData::NUMBER_G;
		if(type==3)
			word_type=ToBeSynthetiseData::NUMBER_GE;
		if(type==4)
			word_type=ToBeSynthetiseData::VSWORD;

		ToBeSynthetiseData w1(_oneDigit[ (int) (hundredDigit - '0')], word_type);
		ToBeSynthetiseData w2 (L"tr\x0103m", word_type);
		w2.SetPostPunctuation(posPun);
		_toSynthetise->AppendData (w1);
		_toSynthetise->AppendData (w2);

		return true;
	}

	return false;
}

/**
* Convert the hundred digit
* @param tensDigit The digit to convert
* @return true if tens digit is not 0, else false;
*/

bool VietVoice::TokensToWords::ConvertTen (wchar_t tensDigit,u_int type,std::wstring posPun)
{
	ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
	if(type==2)
		word_type=ToBeSynthetiseData::NUMBER_G;
	if(type==3)
		word_type=ToBeSynthetiseData::NUMBER_GE;
	if(type==4)
		word_type=ToBeSynthetiseData::VSWORD;


	if (tensDigit == L'1') // 10 to 19 is a special case
	{
		ToBeSynthetiseData w1 (L"m\x01B0\x1EDDi", word_type);
		_toSynthetise->AppendData (w1);

		return true;
	}
	else if (tensDigit != '0') // for 20 to 99 normal convertion
	{
		ToBeSynthetiseData w1 (_oneDigit[(int) tensDigit - '0'], word_type);
		ToBeSynthetiseData w2 (L"m\x01B0\x01A1i", word_type);

		_toSynthetise->AppendData (w1);

		_toSynthetise->AppendData (w2);
		w1.SetPostPunctuation(posPun);
		return true;
	}

	return false;
}

/**
* Convert the "unit" digit
* @param tensDigit The digit to convert
* @return true if "unit" digit is not 0, else false;
*/
bool VietVoice::TokensToWords::ConvertDigit (wchar_t unitDigit, wchar_t tensDigit, wchar_t hundredDigit, int nbGroupsOfThree, bool addLe,u_int type,std::wstring posPun)
{
	ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
	if(type==2)
		word_type=ToBeSynthetiseData::NUMBER_G;
	if(type==3)
		word_type=ToBeSynthetiseData::NUMBER_GE;
	if(type==4)
		word_type=ToBeSynthetiseData::VSWORD;

	if (unitDigit != L'0') // There is a digit to convert
	{
		if (tensDigit == L'0' && addLe == true) // Verify if the word le must be added
		{
			ToBeSynthetiseData w1 (L"l\x1EBB", word_type); // L"lẻ"
			w1.SetPostPunctuation(posPun);
			_toSynthetise->AppendData (w1);
		}

		if (tensDigit > L'1' && unitDigit == L'1') // Special case 21, 31, 41, ... 91
		{
			ToBeSynthetiseData w1 (L"m\x1ED1t", word_type); // L"mốt"
			w1.SetPostPunctuation(posPun);
			_toSynthetise->AppendData (w1);
		}
		else if (tensDigit > L'0' && unitDigit == L'5') //Special case 15, 25, 35, ..., 95
		{
			ToBeSynthetiseData w1 (L"l\x0103m", word_type);
			w1.SetPostPunctuation(posPun);
			_toSynthetise->AppendData (w1);
		} //  normal case
		else
		{
			ToBeSynthetiseData w1 (_oneDigit[ (int) unitDigit - '0'], word_type);
			w1.SetPostPunctuation(posPun);
			_toSynthetise->AppendData (w1);
		}
	}

	if (unitDigit != L'0' || tensDigit != L'0' || hundredDigit != L'0') // ajoute, mille, million, etc si néssésaire / Add thousand, million, etc if necessary
	{
		ToBeSynthetiseData w1 (_threeDigits[nbGroupsOfThree], word_type);
		w1.SetPostPunctuation(posPun);
		_toSynthetise->AppendData (w1);
	}

	if (unitDigit != L'0')
		return true;

	return false;
}

/**
* Process the fractionnal part of a number
* @param decimal The fractionnal part of a number
*/
void VietVoice::TokensToWords::ProcessDecimal (std::wstring decimal,u_int type)
{
	ToBeSynthetiseData::WORD_TYPE word_type=ToBeSynthetiseData::NUMBER;
	if(type==2)
		word_type=ToBeSynthetiseData::NUMBER_G;
	if(type==3)
		word_type=ToBeSynthetiseData::NUMBER_GE;
	if(type==4)
		word_type=ToBeSynthetiseData::VSWORD;


	// Go digit by digit and prononce the name of each digit
	for (unsigned int i = 0; i < decimal.length(); i++)
	{
		ToBeSynthetiseData w1 (_oneDigit[ (int) decimal[i] - '0'], word_type);
		_toSynthetise->AppendData (w1);
	}
}

/**
* Process normal words
*
* @param tokenVal String representing the word
* @param tokenItem Item object representing the word
*/
void VietVoice::TokensToWords::ProcessNormalWord (std::wstring tokenVal, Token & token, ToBeSynthetiseData::WORD_TYPE word_type,u_int type)
{

	size_t indexFrom = 0;
	size_t index = 0;
	std::wstring subTokenVal = L"";
	bool testBegin=true;

	StringHelper::ReplaceAll (tokenVal, L'.', L'-');

	

	// Tout ce while enlève les - / All this while removes -
	do 
	{
		index = tokenVal.find_first_of (L'-', indexFrom);

		if (index == 0) 
		{
			indexFrom = 1;
		}
		else if (index == tokenVal.size() - 1)
			break;

		else if (index != std::wstring::npos) 
		{
			subTokenVal = tokenVal.substr(indexFrom, index - indexFrom);
			indexFrom = index + 1;

			if (subTokenVal != L"")
			{
				/*ToBeSynthetiseData w1 (subTokenVal, word_type);
					_toSynthetise->AppendData (w1);*///Thuan delete 14-02-2020
				
				//Thuan add 14-02-2020////////////
				if(testBegin==true)
				{
					ToBeSynthetiseData w1 (subTokenVal,token.GetPrePunctuation (), L"", word_type);
					_toSynthetise->AppendData (w1);
					testBegin=false;
				}
				else
				{
					ToBeSynthetiseData w1 (subTokenVal, word_type);
					_toSynthetise->AppendData (w1);
				}
				///////End Thuan add////////////////////
				// Pas de silence entre les mots lié par un - / No silence for words linked by a -
			}
		}
	}
	while (index != std::wstring::npos);

	// Obtient le mot sans - / Gets the word without -
	if (index == tokenVal.size() - 1)
	{
		subTokenVal = tokenVal.substr(indexFrom, tokenVal.size() - 1 - indexFrom);
	}
	else
	{
		subTokenVal = tokenVal.substr (indexFrom, tokenVal.size() - indexFrom);
	}

	if (subTokenVal != L"") // Si il y a un mot / if there is a word
	{
		ToBeSynthetiseData::WORD_TYPE word_type1=ToBeSynthetiseData::WORD;
		if(type==2)
			word_type1=ToBeSynthetiseData::WORD_G;
		if(type==3)
			word_type1=ToBeSynthetiseData::WORD_GE;
		if(type==4)
			word_type1=ToBeSynthetiseData::VSWORD;


	/*	ToBeSynthetiseData w1 (subTokenVal,token.GetPrePunctuation (), token.GetPostPunctuation (), word_type1);
			_toSynthetise->AppendData (w1);*///////Thuan delete 14-02-2020

		////////////Thuan add 14-02-2020///////////////
		if(testBegin==true)
		{
			ToBeSynthetiseData w1 (subTokenVal,token.GetPrePunctuation (), token.GetPostPunctuation (), word_type1);
			_toSynthetise->AppendData (w1);
		}
		else
		{
				ToBeSynthetiseData w1 (subTokenVal,L"", token.GetPostPunctuation (), word_type1);
				_toSynthetise->AppendData (w1);
		}
		
		//////////End Thuan add//////////////////////////	
	}
	//::MessageBox(NULL, StringHelper::ToString (subTokenVal).c_str(), L"Token to Words", MB_OK); // VT
}

/**
* Converts the symbols %, $ and $US to text
* @param tokenVal The symbol to convert
* @return true if tokenVal could be convert, else false
*/
bool VietVoice::TokensToWords::ProcessSymbols (std::wstring tokenVal, Token & token,u_int type)
{

	if (_abreviations.find (tokenVal) != _abreviations.end ()) // If the word is a known abreviation or symbol
	{
		std::list <std::wstring> list =  _abreviations[tokenVal];


		// Find every words linked to the abreviation or symbol
		ToBeSynthetiseData::WORD_TYPE word_typeABBRS=ToBeSynthetiseData::ABBR_S;
		if(type==2)
			word_typeABBRS=ToBeSynthetiseData::ABBR_S_G;
		if(type==3)
			word_typeABBRS=ToBeSynthetiseData::ABBR_S_GE;
		if(type==4)
			word_typeABBRS=ToBeSynthetiseData::VSWORD;


		for (std::list<std::wstring>::iterator iter = list.begin (); iter != list.end (); iter++) 
		{
			std::wstring str = iter->c_str ();

			/// ::MessageBox (NULL, StringHelper::ToString (str).c_str (), L"WORDS",MB_OK); // VT

			ProcessNormalWord (str, token, word_typeABBRS,type) ; // 31-8-2012 SYMBOL->ABBR
		}
		return true;
	} else if (IsAbbreviation(v_tokenVal) == true) {
		ToBeSynthetiseData::WORD_TYPE word_typeWORD=ToBeSynthetiseData::WORD;
		if(type==2)
			word_typeWORD=ToBeSynthetiseData::WORD_G;
		if(type==3)
			word_typeWORD=ToBeSynthetiseData::WORD_GE;
		if(type==4)
			word_typeWORD=ToBeSynthetiseData::VSWORD;

		for (unsigned int i = 0; i < v_tokenVal.length (); i++)
		{
			switch (v_tokenVal[i])
			{
			case L'A': ProcessNormalWord(L"a", token, word_typeWORD,type );
				break;
			case L'B': ProcessNormalWord(L"b", token, word_typeWORD,type);
				break;
			case L'C': ProcessNormalWord(L"c", token, word_typeWORD,type);
				break;
			case L'D': ProcessNormalWord(L"d", token, word_typeWORD,type );
				break;
			case L'\x00CF': ProcessNormalWord(L"đ", token, word_typeWORD,type );
				break;
			case L'\x0110': ProcessNormalWord(L"đ", token, word_typeWORD,type );
				break;
			case L'E': ProcessNormalWord(L"e", token, word_typeWORD,type );
				break;
			case L'F': ProcessNormalWord(L"f", token, word_typeWORD,type );
				break;
			case L'G': ProcessNormalWord(L"g", token, word_typeWORD,type);
				break;
			case L'H': ProcessNormalWord(L"h", token, word_typeWORD,type );
				break;
			case L'I': ProcessNormalWord(L"i", token, word_typeWORD,type );
				break;
			case L'J': ProcessNormalWord(L"j", token, word_typeWORD,type );
				break;
			case L'K': ProcessNormalWord(L"k", token, word_typeWORD,type );
				break;
			case L'L': ProcessNormalWord(L"l", token, word_typeWORD,type );
				break;
			case L'M': ProcessNormalWord(L"m", token, word_typeWORD,type ) ;
				break;
			case L'N': ProcessNormalWord(L"n", token, word_typeWORD,type);
				break;
			case L'O': ProcessNormalWord(L"o", token, word_typeWORD,type);
				break;
			case L'P': ProcessNormalWord(L"p", token, word_typeWORD,type );
				break;
			case L'Q': ProcessNormalWord(L"q", token, word_typeWORD,type);
				break;
			case L'R': ProcessNormalWord(L"r", token, word_typeWORD,type );
				break;
			case L'S': ProcessNormalWord(L"s", token, word_typeWORD,type );
				break;
			case L'T': ProcessNormalWord(L"t", token, word_typeWORD,type );
				break;
			case L'U': ProcessNormalWord(L"u", token, word_typeWORD,type);
				break;
			case L'V': ProcessNormalWord(L"v", token, word_typeWORD,type );
				break;
			case L'W': ProcessNormalWord(L"w", token, word_typeWORD,type );
				break;
			case L'X': ProcessNormalWord(L"x", token, word_typeWORD,type );
				break;
			case L'Y': ProcessNormalWord(L"y", token, word_typeWORD,type );
				break;
			case L'Z': ProcessNormalWord(L"z", token, word_typeWORD,type);
				break;
			default: ProcessNormalWord(L"z", token, word_typeWORD,type );
				break;
			}
			ProcessNormalWord(L"_silence_", token, word_typeWORD,type );
		} // end for
		return true ; // end if abbreviation
	} else if (IsName(v_tokenVal)==true && IsName(v_wordAfter) == true) {
		//::MessageBox (NULL, StringHelper::ToString (v_tokenVal.c_str()).c_str (), StringHelper::ToString (v_wordAfter.c_str()).c_str (), MB_OK) ;

		ToBeSynthetiseData::WORD_TYPE word_typeSYMBOL=ToBeSynthetiseData::SYMBOL;
		if(type==2)
			word_typeSYMBOL=ToBeSynthetiseData::SYMBOL_G;
		if(type==3)
			word_typeSYMBOL=ToBeSynthetiseData::SYMBOL_GE;
		if(type==4)
			word_typeSYMBOL=ToBeSynthetiseData::VSWORD;


		ProcessNormalWord(tokenVal.append(L"_"), token, word_typeSYMBOL,type ); 
		return true ;

	} else if (tokenVal == L"%") {
		//ProcessUnit(tokenVal, token) ;
		ProcessWord(L"phần",type); 
		ProcessWord(L"trăm",type); 
		return true ;
	}
	else
	{
		return false;
	}
	return true; // VT
}

/**
* Used to find out if the token is alpha-numeric
* @param str the string that is verified
* @return true if the token is alpha-numeric else false
*/
bool VietVoice::TokensToWords::IsTokenAlphaNumeric (std::wstring str)
{
	bool alpha   = false;
	bool numeric = false;

	for (unsigned int i = 0; i < str.size(); i++)
	{
		wchar_t theChar = str[i];

		if (theChar >= L'0' && theChar <= '9') // if the token contain a number
			numeric = true;
		else if ((theChar >= L'a' && theChar <= L'z') || (theChar >= L'A' && theChar <= L'Z')) // If the token contain a letter
			alpha = true;
	}

	return alpha && numeric; // If the string contain both letter and number
}

/*
* Process an alpha-numeric token
* @param tokenVal String representing the token
* @param tokenItem Object representing the token
* return true if the conversion was possible else false
*/
bool VietVoice::TokensToWords::ProcessAlphaNumeric (std::wstring tokenVal, Token & token,u_int type)
{
	std::wstring tmpToken = L"";
	std::wstring tmpChar = L"";

	if (!IsTokenAlphaNumeric (tokenVal)) // Make sur token is alpha-numeric
		return false;

	for (unsigned int i = 0; i < tokenVal.length(); i++)
	{
		tmpChar = tokenVal[i]; // Get the next digit

		if (IsTokenNumberOrSymbol (tmpChar)) // If part of number
		{
			tmpToken = L"";
			for (; i < tokenVal.length(); i++) // Get the whoe number
			{
				tmpChar = tokenVal[i];
				if (!IsTokenNumberOrSymbol(tmpChar))
				{
					i--;
					break;
				}

				tmpToken += tmpChar;
			}

			ProcessSlashBetweenNumbers (tmpToken, token,type); // Process the number
		}
		else // Normal letter, spell it
		{
			tmpToken = tokenVal[i];
			if (!ProcessSymbols(tmpToken, token,type)) // Consonant are abreviation, voyelle are normal words
			{
				//Create a new word from the token
				ToBeSynthetiseData::WORD_TYPE word_type1=ToBeSynthetiseData::WORD;
				if(type==2)
					word_type1=ToBeSynthetiseData::WORD_G;
				if(type==3)
					word_type1=ToBeSynthetiseData::WORD_GE;
				if(type==4)
					word_type1=ToBeSynthetiseData::VSWORD;

				ProcessNormalWord(tmpToken, token, word_type1,type);
			}

		}
	}

	return true;
}
bool VietVoice::TokensToWords::ProcessRoman (std::wstring tokenVal, Token & token,u_int type)
{
	// ::MessageBox(NULL, StringHelper::ToString(tokenVal).c_str(), L"Vietnamien Voice", MB_OK); // VT
	if (_wordBefore != L"thứ" && _wordBefore != L"khóa" && _wordBefore != L"hội" && tokenVal[tokenVal.size()-1] != L'/') return false ;
	unsigned int v_number = 0 ;
	bool v_found = true ;
	for (unsigned int i = 0; i < tokenVal.length (); i++)
		if (tokenVal[i] != L'x' && tokenVal[i] != L'v' && tokenVal[i] != L'i' && tokenVal[i] != L'/') v_found = false ;
	if (!v_found) return false;
	v_found = true ;
	ToBeSynthetiseData::WORD_TYPE word_typeWORD=ToBeSynthetiseData::WORD;
	if(type==2)
		word_typeWORD=ToBeSynthetiseData::WORD_G;
	if(type==3)
		word_typeWORD=ToBeSynthetiseData::WORD_GE;
	if(type==4)
		word_typeWORD=ToBeSynthetiseData::VSWORD;


	for (unsigned int i = 0; i < tokenVal.length (); i++)
	{
		if (tokenVal[i] == L'x') v_number +=10 ;
		else if (tokenVal[i] == L'v') v_number +=5 ;
		else if (tokenVal[i] == L'i') {
			if (i == tokenVal.length ()-1)
				v_number +=1;
			else if	(tokenVal[i+1] == L'x' || tokenVal[i+1] == L'v') 
				v_number -=1 ;
			else v_number +=1;
		}
		else if (tokenVal[tokenVal.size()-1] == L'/') ;
		else v_found = false ;
	}
	if (v_found) {
		if (tokenVal[tokenVal.size()-1] == L'/' && _wordBefore != L"thứ") {
			ProcessNumbers(StringHelper::ToString(v_number),type,L"") ;
			ProcessNormalWord(L"la", token, word_typeWORD,type );
			ProcessNormalWord(L"mã", token, word_typeWORD,type );
		}
		else
			ProcessNumbers(StringHelper::ToString(v_number),type,L"") ;
	}
	return v_found ;
}
bool VietVoice::TokensToWords::IsTime (std::wstring tokenVal)
{

	size_t index = 0;
	int nbDots = StringHelper::NbChar (tokenVal, L'|') ;

	if (nbDots != 2 && nbDots != 1) //  Two /, - or .
		return false;

	if (nbDots == 2) // Time with second
	{
		if (tokenVal.size() < 5 || tokenVal.size() > 8) //  Valid length
			return false;

		index = tokenVal.find_last_of (L'|') ;

		if (index != 3 && index != 4 && index != 5) // /, - or . at the right place
			return false;
	}
	else
	{
		if (tokenVal.size() < 3 || tokenVal.size() > 5) //  Valid length
			return false;
	}

	index = tokenVal.find_first_of (L'|') ; // /, - or . at the right place
	if (index != 1 && index != 2)
		return false ;

	for (unsigned int i = 0; i < tokenVal.size (); i++) // / Only digits 0 to 9, /, - or .
	{
		if ((tokenVal[i] < '0'|| tokenVal[i] > '9') && tokenVal[i] != '|')
			return false;
	}
	return true ;
}
bool VietVoice::TokensToWords::ProcessTime (std::wstring tokenVal, Token & token,u_int type)
{	
	bool v_time = true;
	if (tokenVal.find(L"|",0) == -1) return false ;
	if (!IsTime (tokenVal)) v_time = false ; // if its a time
	StringHelper::ReplaceAll (tokenVal, L'|', L'.') ;

	Date date (tokenVal) ;

	int day = date.GetDayInt ();
	if (day < 0 || day > 23) // Impossible hour // <= 11-9-2012
		v_time = false;

	int month = date.GetMonthInt ();
	if (month < 0 || month > 59) // Impossible minute <= 11-9-2012
		v_time = false;

	int year = date.GetYearInt ();
	if ((year < 0 || year > 59) && year != -1) // Impossible second <= 11-9-2012
		v_time = false;

	if (v_time) {
		ConvertHour (date, L"giờ",type);
		ConvertMinute (date, L"phút",type);
		if (year != -1) ConvertSecond (date, L"giây",type);
	}
	else {
		tokenVal.append(L".");
		unsigned int w = 0;
		std::wstring v_str = L"";
		do
		{
			if (tokenVal[w] != L'.') {
				v_str += tokenVal[w] ; // VT
			}
			else {
				ProcessNumbers(v_str,type,L"");
				v_str = L"";
			}
			w++;
		} while (w < tokenVal.length());
	}
	return true;
}
void VietVoice::TokensToWords::ConvertHour (Date & date, std::wstring wa,u_int type)
{
	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;

	ProcessNumbers (date.GetDayString (),type,L"") ; // Add the number of the day (1 to 31)
	if (wa !=L"") { // wa = L"giờ"
		ToBeSynthetiseData w1 (wa, word_typeDATE);
		_toSynthetise->AppendData (w1);
	}
}
void VietVoice::TokensToWords::ConvertMinute (Date & date, std::wstring wa,u_int type)
{
	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;

	ProcessNumbers (date.GetMonthString (),type,L"") ; // Add the number of the day (1 to 31)
	if (wa !=L"") {
		ToBeSynthetiseData w1 (wa, word_typeDATE);
		_toSynthetise->AppendData (w1);
	}
}
void VietVoice::TokensToWords::ConvertSecond (Date & date, std::wstring wa,u_int type)
{
	ToBeSynthetiseData::WORD_TYPE word_typeDATE=ToBeSynthetiseData::DATE;
	if(type==2)
		word_typeDATE=ToBeSynthetiseData::DATE_G;
	if(type==3)
		word_typeDATE=ToBeSynthetiseData::DATE_GE;
	if(type==4)
		word_typeDATE=ToBeSynthetiseData::VSWORD;

	ProcessNumbers (date.GetYearString (),type,L"") ; // Add the number of the day (1 to 31)
	if (wa !=L"") {
		ToBeSynthetiseData w1 (wa,word_typeDATE);
		_toSynthetise->AppendData (w1);
	}
}
bool VietVoice::TokensToWords::ProcessUnit (std::wstring tokenVal, Token & token,u_int type)
{
	// if (!IsTokenNumber(_wordBefore)) return false;
	if (!IsTokenNumberOrSymbol(_wordBefore) && _wordBefore != L"hàng") return false; // thêm && 9-9-2012
	//	::MessageBox(NULL, tokenVal.c_str(), L"m", MB_OK) ;
	if (tokenVal == L"%") {
		ProcessWord(L"phần",type);
		ProcessWord(L"trăm",type);
	}
	if (tokenVal == L"mm") {
		ProcessWord(L"mi",type);
		ProcessWord(L"li",type);
		ProcessWord(L"mét",type);
	}
	else if (tokenVal == L"mm2") {
		ProcessWord(L"mi",type);
		ProcessWord(L"li",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"vuông",type);
	}
	else if (tokenVal == L"mm3") {
		ProcessWord(L"mi",type);
		ProcessWord(L"li",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"khối",type);
	}
	else if(tokenVal == L"cm") {
		ProcessWord(L"xăng",type);
		ProcessWord(L"ti",type);
		ProcessWord(L"mét",type);
	}
	else if(tokenVal == L"cm2") {
		ProcessWord(L"xăng",type);
		ProcessWord(L"ti",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"vuông",type);
	}
	else if(tokenVal == L"cm3") {
		ProcessWord(L"xăng",type);
		ProcessWord(L"ti",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"khối",type);
	}
	else if(tokenVal == L"dm") {
		ProcessWord(L"đề",type);
		ProcessWord(L"xi",type);
		ProcessWord(L"mét",type);
	}
	else if(tokenVal == L"dm2") {
		ProcessWord(L"đề",type);
		ProcessWord(L"xi",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"vuông",type);
	}
	else if(tokenVal == L"dm3") {
		ProcessWord(L"đề",type);
		ProcessWord(L"xi",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"khối",type);
	}
	else if(tokenVal == L"m") {
		ProcessWord(L"mét",type);
	}
	else if(tokenVal == L"m2") {
		ProcessWord(L"mét",type);
		ProcessWord(L"vuông",type);
	}
	else if(tokenVal == L"m3") {
		ProcessWord(L"mét",type);
		ProcessWord(L"khối",type);
	}
	else if(tokenVal == L"m/s") {
		ProcessWord(L"mét",type);
		ProcessWord(L"trên",type);
		ProcessWord(L"giây",type);
	}
	else if(tokenVal == L"km") {
		ProcessWord(L"kí",type);
		ProcessWord(L"lô",type);
		ProcessWord(L"mét",type);
	}
	else if(tokenVal == L"kmh" || tokenVal == L"km/h") {
		ProcessWord(L"kí",type);
		ProcessWord(L"lô",type);
		ProcessWord(L"mét",type);
		ProcessWord(L"giờ",type);
	}
	else if(tokenVal == L"ml") {
		ProcessWord(L"mí",type);
		ProcessWord(L"li",type);
		ProcessWord(L"lít",type);
	}
	else if(tokenVal == L"cl") {
		ProcessWord(L"xăng",type);
		ProcessWord(L"ti",type);
		ProcessWord(L"lít",type);
	}
	else if(tokenVal == L"dl") {
		ProcessWord(L"đề",type);
		ProcessWord(L"ci",type);
		ProcessWord(L"lít",type);
	}
	else if(tokenVal == L"l") {
		ProcessWord(L"lít",type);
	}
	else if(tokenVal == L"ha") {
		ProcessWord(L"hét",type);
		ProcessWord(L"ta",type);
	}
	else if(tokenVal == L"g") {
		if (IsTokenNumber(_wordBefore) && IsTokenNumber(v_wordAfter) && v_wordAfter != L"") 
			ProcessWord(L"giờ",type);
		else
			ProcessWord(L"gam",type);
	}
	else if(tokenVal == L"kg") {
		ProcessWord(L"kí",type);
		ProcessWord(L"lô",type);
		ProcessWord(L"gam",type);
	}
	else return false;
	return true;
}
void VietVoice::TokensToWords::ProcessWord (std::wstring v_word,u_int type)
{
	ToBeSynthetiseData::WORD_TYPE word_typeWORD=ToBeSynthetiseData::WORD;
	if(type==2)
		word_typeWORD=ToBeSynthetiseData::WORD_G;
	if(type==3)
		word_typeWORD=ToBeSynthetiseData::WORD_GE;
	if(type==4)
		word_typeWORD=ToBeSynthetiseData::VSWORD;

	ToBeSynthetiseData w1 (v_word, word_typeWORD);
	_toSynthetise->AppendData (w1);
}
bool VietVoice::TokensToWords::IsAbbreviation (std::wstring tokenVal)
{
	bool abbreviation = true;

	for (unsigned int i = 0; i < tokenVal.length (); i++)
	{
		if ((tokenVal[i]<L'A' || tokenVal[i]>L'Z') && tokenVal[i]!= L'\x00CF' && tokenVal[i]!=L'\x0110') abbreviation = false; // && m_tokenVal[i]!= L'\x00D0' && m_tokenVal[i]!=L'\x0111'
		if (tokenVal[i]==L'A' || tokenVal[i]==L'Á'  || tokenVal[i]==L'À'  || tokenVal[i]==L'Ả' || tokenVal[i]==L'Ã' || tokenVal[i]==L'Ạ') abbreviation = false ;
		if (tokenVal[i]==L'Ă' || tokenVal[i]==L'Ắ'  || tokenVal[i]==L'Ằ'  || tokenVal[i]==L'Ẳ' || tokenVal[i]==L'Ẵ' || tokenVal[i]==L'Ặ') abbreviation = false ;
		if (tokenVal[i]==L'Â' || tokenVal[i]==L'Ấ'  || tokenVal[i]==L'Ầ'  || tokenVal[i]==L'Ẩ' || tokenVal[i]==L'Ẫ' || tokenVal[i]==L'Ậ') abbreviation = false ;
		if (tokenVal[i]==L'E' || tokenVal[i]==L'É'  || tokenVal[i]==L'È'  || tokenVal[i]==L'Ẻ' || tokenVal[i]==L'Ẽ' || tokenVal[i]==L'Ẹ') abbreviation = false ;
		if (tokenVal[i]==L'Ê' || tokenVal[i]==L'Ế'  || tokenVal[i]==L'Ề'  || tokenVal[i]==L'Ể' || tokenVal[i]==L'Ễ' || tokenVal[i]==L'Ệ') abbreviation = false ;
		if (tokenVal[i]==L'I' || tokenVal[i]==L'Í'  || tokenVal[i]==L'Ì'  || tokenVal[i]==L'Ỉ' || tokenVal[i]==L'Ĩ' || tokenVal[i]==L'Ị') abbreviation = false ;
		if (tokenVal[i]==L'U' || tokenVal[i]==L'Ú'  || tokenVal[i]==L'Ù'  || tokenVal[i]==L'Ủ' || tokenVal[i]==L'Ũ' || tokenVal[i]==L'Ụ') abbreviation = false ;
		if (tokenVal[i]==L'Ư' || tokenVal[i]==L'Ứ'  || tokenVal[i]==L'Ừ'  || tokenVal[i]==L'Ử' || tokenVal[i]==L'Ữ' || tokenVal[i]==L'Ự') abbreviation = false ;
		if (tokenVal[i]==L'O' || tokenVal[i]==L'Ó'  || tokenVal[i]==L'Ò'  || tokenVal[i]==L'Ỏ' || tokenVal[i]==L'Õ' || tokenVal[i]==L'Ọ') abbreviation = false ;
		if (tokenVal[i]==L'Ô' || tokenVal[i]==L'Ố'  || tokenVal[i]==L'Ồ'  || tokenVal[i]==L'Ổ' || tokenVal[i]==L'Ỗ' || tokenVal[i]==L'Ộ') abbreviation = false ;
		if (tokenVal[i]==L'Ơ' || tokenVal[i]==L'Ớ'  || tokenVal[i]==L'Ờ'  || tokenVal[i]==L'Ở' || tokenVal[i]==L'Ỡ' || tokenVal[i]==L'Ợ') abbreviation = false ;
		if (tokenVal[i]==L'Y' || tokenVal[i]==L'Ý'  || tokenVal[i]==L'Ỳ'  || tokenVal[i]==L'Ỷ' || tokenVal[i]==L'Ỹ' || tokenVal[i]==L'Ỵ') abbreviation = false ;
	}
	return abbreviation ;
}
bool VietVoice::TokensToWords::IsName (std::wstring tokenVal)
{
	// Sua 22-12-2012 vi Language in Property 
	//wstring v_upper = L"AÁÀẢÃẠĂẮẰẲÃẶÂẤẦẨẪẬEÉÈẺẼẸÊẾỀỂỄỆIÍÌỈĨỊUÚÙỦŨUỰỨÙỬỮỰOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢYÝỲỶỸỴBCDĐFGHJKLMNPQRSTVWXZ" ;
	//wstring v_lower = L"aáàảãạăắằẳẵặâấầẩẫậeéèẻẽẹêếềểễệiíìỉĩịuúùủũụưứừửữựoóòỏõọôốồổỗộơớờởỡợyýỳỷỹỵbcdđfghjklmnpqrstvwxz" + L'\x00FD' + L'\x1EBF' + L'\x00FA' ;
	bool v_name = true ;
	if (IsUpper(tokenVal[0]) == false) v_name = false ;

	for (unsigned int i = 1; i < tokenVal.length (); i++)
		if (IsUpper(tokenVal[i] == false)) v_name = false ;
	//if (v_name == true)
	//	::MessageBox (NULL, L"True", StringHelper::ToString (tokenVal).c_str (), MB_OK) ;
	return v_name ;
}
bool VietVoice::TokensToWords::IsUpper (WCHAR letter)
{
	if ((letter<L'A' || letter>L'Z') && letter!= L'\x00CF' && letter!=L'\x0110') return false; // && m_letter!= L'\x00D0' && m_letter!=L'\x0111'
	else if (letter==L'A' || letter==L'Á'  || letter==L'À'  || letter==L'Ả' || letter==L'Ã' || letter==L'Ạ') return true ;
	else if (letter==L'Ă' || letter==L'Ắ'  || letter==L'Ằ'  || letter==L'Ẳ' || letter==L'Ẵ' || letter==L'Ặ') return true ;
	else if (letter==L'Â' || letter==L'Ấ'  || letter==L'Ầ'  || letter==L'Ẩ' || letter==L'Ẫ' || letter==L'Ậ') return true ;
	else if (letter==L'E' || letter==L'É'  || letter==L'È'  || letter==L'Ẻ' || letter==L'Ẽ' || letter==L'Ẹ') return true ;
	else if (letter==L'Ê' || letter==L'Ế'  || letter==L'Ề'  || letter==L'Ể' || letter==L'Ễ' || letter==L'Ệ') return true ;
	else if (letter==L'I' || letter==L'Í'  || letter==L'Ì'  || letter==L'Ỉ' || letter==L'Ĩ' || letter==L'Ị') return true ;
	else if (letter==L'U' || letter==L'Ú'  || letter==L'Ù'  || letter==L'Ủ' || letter==L'Ũ' || letter==L'Ụ') return true ;
	else if (letter==L'Ư' || letter==L'Ứ'  || letter==L'Ừ'  || letter==L'Ử' || letter==L'Ữ' || letter==L'Ự') return true ;
	else if (letter==L'O' || letter==L'Ó'  || letter==L'Ò'  || letter==L'Ỏ' || letter==L'Õ' || letter==L'Ọ') return true ;
	else if (letter==L'Ô' || letter==L'Ố'  || letter==L'Ồ'  || letter==L'Ổ' || letter==L'Ỗ' || letter==L'Ộ') return true ;
	else if (letter==L'Ơ' || letter==L'Ớ'  || letter==L'Ờ'  || letter==L'Ở' || letter==L'Ỡ' || letter==L'Ợ') return true ;
	else if (letter==L'Y' || letter==L'Ý'  || letter==L'Ỳ'  || letter==L'Ỷ' || letter==L'Ỹ' || letter==L'Ỵ') return true ;
	else if (letter==L'B' || letter==L'C'  || letter==L'D'  || letter==L'\u0110' || letter==L'E' || letter==L'F') return true ;
	else if (letter==L'G' || letter==L'H'  || letter==L'I'  || letter==L'J' || letter==L'K' || letter==L'L') return true ;
	else if (letter==L'M' || letter==L'N'  || letter==L'O'  || letter==L'P' || letter==L'Q' || letter==L'R') return true ;
	else if (letter==L'S' || letter==L'T'  || letter==L'U'  || letter==L'V' || letter==L'W' || letter==L'X') return true ;
	else if (letter==L'Y' || letter==L'Z'  || letter!= L'\x00CF' || letter!=L'\x0110') return true ;
	return false;
}
bool VietVoice::TokensToWords::IsLower (WCHAR letter)
{
	if ((letter<L'a' || letter>L'z') && letter != L'\x00FD'  && letter != L'\x1EBF' && letter != L'\x00FA') return false ;
	else if (letter==L'a' || letter==L'á'  || letter==L'à'  || letter==L'ả' || letter==L'ã' || letter==L'ạ') return true ;
	else if (letter==L'ă' || letter==L'ắ'  || letter==L'ằ'  || letter==L'ẳ' || letter==L'ẵ' || letter==L'ặ') return true ;
	else if (letter==L'â' || letter==L'ấ'  || letter==L'ầ'  || letter==L'ẩ' || letter==L'ẫ' || letter==L'ậ') return true ;
	else if (letter==L'e' || letter==L'é'  || letter==L'è'  || letter==L'ẻ' || letter==L'ẽ' || letter==L'ẹ') return true ;
	else if (letter==L'ê' || letter==L'ế'  || letter==L'ề'  || letter==L'ể' || letter==L'ễ' || letter==L'ệ') return true ;
	else if (letter==L'i' || letter==L'í'  || letter==L'ì'  || letter==L'ỉ' || letter==L'ĩ' || letter==L'ị') return true ;
	else if (letter==L'u' || letter==L'ú'  || letter==L'ù'  || letter==L'ủ' || letter==L'ũ' || letter==L'ụ') return true ;
	else if (letter==L'ư' || letter==L'ứ'  || letter==L'ừ'  || letter==L'ử' || letter==L'ữ' || letter==L'ự') return true ;
	else if (letter==L'o' || letter==L'ó'  || letter==L'ò'  || letter==L'ỏ' || letter==L'õ' || letter==L'ọ') return true ;
	else if (letter==L'ô' || letter==L'ố'  || letter==L'ồ'  || letter==L'ổ' || letter==L'ỗ' || letter==L'ộ') return true ;
	else if (letter==L'ơ' || letter==L'ớ'  || letter==L'ờ'  || letter==L'ở' || letter==L'ỡ' || letter==L'ợ') return true ;
	else if (letter==L'y' || letter==L'ý'  || letter==L'ỳ'  || letter==L'ỷ' || letter==L'ỹ' || letter==L'ỵ') return true ;
	else if (letter==L'b' || letter==L'c'  || letter==L'd'  || letter==L'đ' || letter==L'e' || letter==L'f') return true ;
	else if (letter==L'g' || letter==L'h'  || letter==L'i'  || letter==L'j' || letter==L'k' || letter==L'l') return true ;
	else if (letter==L'm' || letter==L'n'  || letter==L'o'  || letter==L'p' || letter==L'q' || letter==L'r') return true ;
	else if (letter==L's' || letter==L't'  || letter==L'u'  || letter==L'v' || letter==L'w' || letter==L'x') return true ;
	else if (letter==L'y' || letter==L'z' || letter == L'\x00FD' || letter ==L'\x1EBF' || letter == L'\x00FA') return true ;
	return false;
}



std::vector<std::wstring> VietVoice::TokensToWords::LoadPhrase(std::wstring tu)
{
	std::vector<std::wstring> _phrase; // list of Phrase
	std::wstring str=StringHelper::ChuyenTu(tu);
	
	try
	{
		File::Reader reader (_phrasePath+str);
		
		long SoDong = reader.ReadLong ();
		for(int i=0;i<SoDong;i++)
		{
			std::wstring phr=reader.ReadWString();
			_phrase.push_back(phr);
		}	
	}
	catch (File::Exception ex) // Something wrong happened
	{
		
	} 	
	return _phrase;
  

}