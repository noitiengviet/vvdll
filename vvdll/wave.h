#if !defined WAVE_H
#include "stdafx.h"
#include <vector>
#include <cmath>
#include "fileio.h"
#include "vietnameseTTSException.h"
#include "riffwriter.h"
#include "riffreader.h"


using namespace std;


	class Wave
	{
	private:
		vector<unsigned char> data ; //The samples data
		short formatTag ; // A tag indicating the format of the wave (PCM = 1)
		long nbChannels; // Number of channel (1 = Mono, 2 = Stereo)
		unsigned long samplesPerSecond; //The sampling rate of the wave (44100, 11000, etc)
		unsigned long averageBytesPerSecond; //Number of bytes being "played" in a second
		long blockAlign; // Size of the sample frame in term of bytes
		long bitsPerSample;  //Bits used to represents the sample (8 or 16 bits)
		long size; // Number of bytes forming the sample data
		long nbSamples; //Number of samples in the wave
		const static int MAGIC;//

	public:

		/**
		* Construct an empty wave
		*/
		Wave();

	  /**
	   * Creates a new wave from separate data members
	   * @param data The byte data of the wave
	   * @param formatTag The format tag of the wave
	   * @param nbChannels The number of channel of the wave
	   * @param samplesPerSecond The sample rate of the wave
	   * @param averageBytesPerSecond The average bytes per second for the wave
	   * @param blockAlign The block align for the wave
	   * @param bitsPerSample The bitsPerSample for the wave
	   * @param size The size of the wave in bytes
	   * @param nbSamples The size of the wave in samples
	   */
		Wave (vector<unsigned char> data, short formatTag, long nbChannels, unsigned long samplesPerSecond, unsigned long averageBytesPerSecond, long blockAlign, long bitsPerSample, long size, long nbSamples);

		/**
		* Creates a new wave by combining parts of 2 other waves.
		* @param w1  The first wave used to create the new wave.
		* @param from1  Starting point in the first wave.
		* @param nbSamples1  Number of samples used from the first wave to create the new wave.
		* @param w2  The second wave used to create the new wave.
		* @param from2 Starting point in the second wave.
		* @param nbSamples2 Number of samples used from the second wave to create the new wave.
		*/
		Wave (Wave &w1, int percent1, Wave & w2, int percent2);

		/**
		* Obtains the format tag of the wave.
		*
		* @return The format tag.
		*/
		short GetFormatTag ();

		/**
		* Obtains the number of channels of the wave.
		*
		* @return The number of channels.
		*/

		long GetNbChannels ();

		/**
		* Obtains the sample rate of the wave.
		*
		* @return The sample rate.
		*/
		unsigned long GetSampleRate ();

		/**
		* Obtains the byte rate per second of the wave.
		*
		* @return The byte rate.
		*/
		unsigned long GetBytesPerSecond ();

		/**
		* Obtains the block alignement of the wave.
		*
		* @return The block alignement.
		*/
		long GetBlockAlign ();

		/**
		* Obtains the number of bits forming a sample for the wave.
		*
		* @return The number of bits forming a sample.
		*/
		long GetBitsPerSample ();

		/**
		* Obtains the size in bytes of the sample data .
		*
		* @return The size in bytes of the sample data.
		*/

		int GetSize ();

		/**
		* Obtains the number of samples of the wave.
		*
		* @return The number of samples.
		*/

		int GetNbSamples ();

		void Load (File::Reader & reader);

		void Load (std::wstring fileName);
	  /**
		* Saves a wave to a file. Only a subset of the wave can be saved.
		*
		* @param fileName The name of the file.
		* @param firstSample the first sample to be saved
		* @param len The number of samples to be saved
		* @throws FileNotFoundException
		* @throws IOException
		*/
	  void Save (std::wstring const fileName, int firstSample, int len);

	  void Save (std::wstring fileName);

	  void SetDefault ();

	  unsigned char GetByte (int index);
	};


#define WAVE_H

#endif
