#if !defined (TOBESPOKEN_H)

#define TOBESPOKEN_H

#include <string>
#include <list>
#include "token.h"
#include "ToBeSpokenData.h"

#pragma warning( disable : 4786 ) 

namespace VietVoice
{
	/**
	 * Class Name:         ToBeSpoken
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class encapsulating all the data used to read a
	 *                     specified text.
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *                     � DKNS Project (1999-2002)
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * september 20 2005:  Converted from java to C++
	 * october 18 2005:  Converted the setListData method
	 * November 8 2005:  Converted the GetSizeWords method
	 * Nomvember 25:  Added copy constructor and = operator
	 * March 4 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 17 2006:  Improved comments and spacing in both .h and .cpp file.
	 */
	class ToBeSynthetise
	{
	public:
		enum VOICESEL {MALE = 0, FEMALE = 1, CUSTOM = 2};
		/**
		* Copy constructor
		*
		* Parameters:
		* 
		* const ToBeSynthetise& toBeSynthetise:  The objct being copied
		*/
		ToBeSynthetise  (const ToBeSynthetise & toBeSynthetise);

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const ToBeSynthetise & toBeSpoken);

	   /**
	    * Constructor
		*/
		ToBeSynthetise ();

		/**
		* Retreive a token from the list of tokens
		*
		* return value: The desired token
		*
		* Parameters:
		*
		* unsigned int index Index of the desired token
		* 
		*/
		Token * GetToken (unsigned int index);

		/**
		* Appends a token at the end of the list of token
		*
		* Parameters:
		*
		* Token & token The token to be appended.
		*/
		void AppendToken (Token & token);

		/**
		* Obtains the list of tokens
		*
		* return value: The list of tokens
		*/
		list<Token> & GetListToken ();

		/**
		* Obtains a specific ToBeSpokenData object from the list
		* 
		* return value: The desired ToBeSpokenData object
		*
		* Parameters:
		*
		* unsigned int: index The index of the ToBeSpokenData object
		* 
		*/
		ToBeSynthetiseData * GetData (unsigned int index);

		/**
		* Appends a token at the end of the list of token
		*
		* return value:
		*
		* ToBeSpokenData & token: The token to be appended.
		*/
		void AppendData (ToBeSynthetiseData & data);

		/**
		* Obtains the list of tokens
		*
		* return value: The list of tokens
		*/
		list<ToBeSynthetiseData> & GetListData ();

		/**
		* Changes the avgLengthSilence member
		*
		* Parameters:
		* 
		* float dur The new value for avgLengthSilence
		*/
		void SetDurMoyenSilence (float dur);
		void SetSpeed (float speed);
		void SetVolume (float volume);
		void SetPitch (float pitch);
		void SetWsola (bool wsola);
		void Set_BS (long vbs);
		long Get_Voice ();

		/**
		* Obtains the avgLengthSilence member
		*
		* return value: The value of avgLengthSilence
		*/
		float GetDurMoyenSilence ();
		float GetSpeed ();
		float GetVolume ();
		float GetPitch ();
		bool GetWsola ();
		long Get_BS ();
		/**
		* Empty all data contained in the ToBeSpoken object
		*/
		void Empty ();

		/**
		* Changes the list of ToBeSpokenData objects
		*
		* Parameters:
		*
		* std::list<ToBeSpokenData> & listData: listData The new list
		*/
		void SetListData (std::list<ToBeSynthetiseData> & listData);

		/**
		* Calculate the size in bytes of the sound data to be played
		*
		* return value: The size in bytes of the sound data to be played
		*/
		int GetSizeWords ();

			/**
			* Set the _mustPlay flag
			*
			* bool mustPlay:  The new value of the mustPlay flag
			*/
			void SetMustPlay (bool mustPlay);

			/**
			* Set the file name in which the mp3 data is saved
			*
			* std::wstring mp3Name:  The new file name
			*/
			void SetMp3Name (std::wstring mp3Name);

			std::wstring GetMp3Name ();
			bool GetMustPlay ();

			void SetVoice (VOICESEL voice);

			VOICESEL GetVoice () const;


	private:

		list <Token> _listToken; // Contains the tokens forming the text
		list <ToBeSynthetiseData> _listData; // Constain all sound data and other data used for synthetisation
		float _avgLengthSilence; // Words shorter then this length have a silence added after them.
		float _speed; // Words shorter then this length have a silence added after them.
		float _volume; // Words shorter then this length have a silence added after them.
		float _pitch; // Words shorter then this length have a silence added after them.
		bool _wsola ;
		long _vbs;
		int _voice;
		bool	  _mustPlay;  // Must the mp3 be played after it is created (not if save mp3)
		std::wstring    _mp3Name; // Name of the file where the mp3 must be saved.
		VOICESEL _curVoice;
	};
}

#endif