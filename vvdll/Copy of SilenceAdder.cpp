#include "SilenceAdder.h"
#include <fstream>
#include <cstdlib>
#include <algorithm>
VietVoice::SilenceAdder::SilenceAdder (std::wstring adjectiveName, std::wstring lightName, std::wstring prosodyName,std::wstring silenceDictionnaryName2,std::wstring silenceDictionnaryName3, std::wstring wordsSizeFileName, std::wstring silencesSizeFileName, std::wstring articlesFile, std::wstring linkingFile, WordSelector & selector)
: _selector (&selector)
{

	//LoadSilenceDictionnary (silenceDictionnaryName);
	//LoadSilencesSize(silencesSizeFileName);
	//LoadWordsSize(wordsSizeFileName);
	std::vector< std::wstring > combinedWords2;
	File::Reader::ReadTextFileLines( combinedWords2, silenceDictionnaryName2, L"\t\r\n" );

	for(int i = 0; i < combinedWords2.size(); ++i)
	{
		StringHelper::RemoveSpace(combinedWords2[i]);
		std::pair<std::wstring, std::wstring> p (combinedWords2[i], combinedWords2[i]);
		_silenceDictionnary2.insert (p);
	}
// Viet them
	std::vector< std::wstring > combinedWords3;
	File::Reader::ReadTextFileLines( combinedWords3, silenceDictionnaryName3, L"\t\r\n" );

	for(int i = 0; i < combinedWords3.size(); ++i)
	{
		StringHelper::RemoveSpace(combinedWords3[i]);
		std::pair<std::wstring, std::wstring> p (combinedWords3[i], combinedWords3[i]);
		_silenceDictionnary3.insert (p);
	}
// End Viet them
	File::Reader::ReadTextFileLines( m_articlesDictionary, articlesFile);
	File::Reader::ReadTextFileLines( m_linkingDictionary, linkingFile);

// Viet them ==========================================================
	std::vector<std::wstring> v_prosody;
	File::Reader::ReadTextFileLines( v_prosody, prosodyName);
	this->v_volume = StringHelper::ToInt(v_prosody[0]);
	this->v_speed = StringHelper::ToInt(v_prosody[1]);
	this->v_pitch = StringHelper::ToInt(v_prosody[2]);
	this->v_compress = StringHelper::ToInt(v_prosody[3]); // speed = 47%
// End Viet them ==========================================================


	std::vector<std::wstring> silenceSize;
	File::Reader::ReadTextFileLines( silenceSize, silencesSizeFileName);
	this->m_silenceSize = StringHelper::ToInt(silenceSize[0]);
	this->m_silenceSize2 = StringHelper::ToInt(silenceSize[1]);
	this->tooSmall = StringHelper::ToInt(silenceSize[2])*this->v_compress/100;
	this->v_break_silence1 = StringHelper::ToInt(silenceSize[3])*this->v_compress/100; // VT
	this->v_break_silence1 = this->v_break_silence1 / 2 * 2;
	this->v_break_silence2 = StringHelper::ToInt(silenceSize[4])*this->v_compress/100; // VT
	this->v_break_silence2 = this->v_break_silence2 / 2 * 2;
	this->v_break_compound = StringHelper::ToInt(silenceSize[5])*this->v_compress/100; // VT
	this->v_break_compound = this->v_break_compound / 2 * 2;
	this->v_compound_min = StringHelper::ToInt(silenceSize[6])*this->v_compress/100; // VT
	this->v_compound_min = this->v_compound_min / 2 * 2; 
	this->v_stop_silence = StringHelper::ToInt(silenceSize[7])*this->v_compress/100; // VT
	this->v_stop_silence = this->v_stop_silence / 2 * 2; 
	this->v_adjectives_sl = StringHelper::ToInt(silenceSize[8])*this->v_compress/100; // VT
	this->v_adjectives_sl = this->v_adjectives_sl / 2 * 2;
	this->v_linking_sl = StringHelper::ToInt(silenceSize[9])*this->v_compress/100; // VT
	this->v_linking_sl = this->v_linking_sl / 2 * 2;
	this->v_number_sl = StringHelper::ToInt(silenceSize[10])*this->v_compress/100; // VT
	this->v_number_sl = this->v_number_sl / 2 * 2; 

	std::vector<std::wstring> silenceSize2;
	File::Reader::ReadTextFileLines( silenceSize2, wordsSizeFileName);
	this->mediumSilence = StringHelper::ToInt(silenceSize2[0])*this->v_compress/100;
/*
0::20 (nén xx % của từ kép thứ nhất) 17 - giá trị nầy phải < 50
1::-15 (tăng xx % từ thứ hai) -10 - giá trị nầy phải > -50
2::22000 (word size nhỏ hơn giá trị nầy / 2 thì không compress) 7000
3::300 (ms sau dấu phẩy) 300
4::400 (ms sau dấu chấm) 400
5::20 (thêm silence = xx % của tổng độ dài hai từ của từ kép vào sau từ thứ hai) 20
6::5000 (nếu giá trị thêm vào ở thông số trên < xxxx thì thêm xxxx) 5000
7::4000 (thêm silence = xxxx vào sau các stop consonant) 3000
8::7000 (thêm xxxx / 2 sau các Adjectives) 12000
9::2000 (thêm xxxx / 2 sau các number hay date-time) 2000
*/
	std::vector<std::wstring> v_light;
	File::Reader::ReadTextFileLines(v_light, lightName);
	this->v_light_Words = StringHelper::ToString(v_light[0]);

	std::vector<std::wstring> v_adjective;
	File::Reader::ReadTextFileLines(v_adjective, adjectiveName);
	this->v_adjective_Words = StringHelper::ToString(v_adjective[0]);
// End VT =============================================================
}

VietVoice::SilenceAdder::SilenceAdder (const SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary2 = silenceAdder._silenceDictionnary2;
		_selector = silenceAdder._selector;
	}
}

VietVoice::SilenceAdder::~SilenceAdder ()
{}

void VietVoice::SilenceAdder::operator = (const VietVoice::SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary2 = silenceAdder._silenceDictionnary2;
		_selector = silenceAdder._selector;
	}
}

void VietVoice::SilenceAdder::Treat (VietVoice::ToBeSynthetise & toSynthetise) 
{
	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData();
	std::list<ToBeSynthetiseData> newListData;
	bool v_check = true ;
	int v_wordSize ;
	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		Word * word = NULL;
		int accent = iter->GetAccent();
		// Obains the accent for the word
		std::wstring wordName = iter->GetWord ();
		if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
		{
			word = _selector->FindWord(wordName, accent);
		}
		else if (wordName != L"silence") // An extra word, get it in database unless silence
		{
			word = _selector->GetExtraWord(wordName);
		}
//		if( word == NULL ) // VX
		if( word == NULL || v_check == false) // VS
		{
			newListData.push_back(*iter);
			v_check = true ;
			continue;
		}

		std::list<ToBeSynthetiseData>::iterator iterAfter = iter;
		iterAfter++;
		ToBeSynthetiseData data = *iter;
		
		std::wstring expression2 = L"";
		if (iterAfter != listData.end())
		{
			expression2 = data.GetOriginalWord(); //expression += L" "; // VLL
			expression2 += iterAfter->GetOriginalWord(); //StringHelper::RemoveSpace(expression);
		}
// VT
		std::wstring expression3 = L"";
		if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"")
		{
			if (iterAfter->GetPostPunctuation() == L"")	{
				iterAfter ++ ;
				if (iterAfter != listData.end()) 
					expression3 = expression2 + iterAfter->GetOriginalWord(); //StringHelper::RemoveSpace(expression);
				iterAfter -- ;
			}
		}
// End VT
		if (wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
		{

			int nbSamples = word->GetNbSamples();
			if(nbSamples < this->mediumSilence / 2)
			{
				if(data.GetSilenceLength() == 0) {
////					data.SetSilenceLength( ((this->mediumSilence / 2) - nbSamples) * word->GetBlockAlign () ); // VX
////					int v_wordLength = data.GetSilenceLength() ;
////					data.SetSilenceLength(v_wordLength + (this->mediumSilence / 2) * word->GetBlockAlign () ); // VX
				}
			}
			if (v_isStopConsonant(data.GetOriginalWord(),data.GetAccent())) {
////			data.SetSilenceLength(word->GetNbSamples() * word->GetBlockAlign() / 3);
				if (data.GetSilenceLength() == 0)
					data.SetSilenceLength(v_stop_silence / 2 * word->GetBlockAlign());
			}
			int v_wordLength = data.GetSilenceLength() ;
//new			data.SetSilenceLength(v_wordLength + (this->mediumSilence / 2) * word->GetBlockAlign ()); // VX

// Viet them linking words -----------------------------------------------------------
			if (iterAfter != listData.end()) 
				if ( std::find( this->m_linkingDictionary.begin(), this->m_linkingDictionary.end(), iterAfter->GetOriginalWord ()) != this->m_linkingDictionary.end() ) // VS
				{
					//* if (iter->GetSilenceLength() == 0)
					iterAfter->m_percentageAdjust=this->m_silenceSize2 ; // New new new
					data.SetSilenceLength(v_linking_sl) ; // v_adjectives_sl
//*::MessageBox(NULL, StringHelper::ToString(iter->GetSilenceLength()).c_str(), StringHelper::ToString(L"|" + iter->GetOriginalWord() + L"|ràng|").c_str(), MB_OK); // VT
				}
// End Viet them linking words -----------------------------------------------------------
			
// **********************************************************************************
			if (_silenceDictionnary3.find (expression3) != _silenceDictionnary3.end())
			{
				v_check = false ; // VT
				int v_wordSize1 = 0 ;
				if(nbSamples > this->tooSmall / 2)
				{
					data.m_percentageAdjust = this->m_silenceSize + 50 ;
					v_wordSize1 = word->GetNbSamples() ; // VT
				} // Từ thứ nhất


				std::wstring wordName = iterAfter->GetWord ();
				Word * word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName);
				}
				if(word != NULL) 
				{
					int v_wordSize2 = word->GetNbSamples() ; // VT
// Viet sua adjective ---------------------------------------------------------------
					std::wstring v_after = L"" ;
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
////::MessageBox(NULL, StringHelper::ToString(iterAfter->GetOriginalWord()).c_str(), L"Word", MB_OK); // VT

					if(word->GetNbSamples() > this->tooSmall / 2) {
						iterAfter->m_percentageAdjust = this->m_silenceSize + 50;
						if (iterAfter->GetSilenceLength() == 0)
							iterAfter->SetSilenceLength(0) ;
					} // Từ thứ hai
						iterAfter ++ ;
						if (iterAfter->GetSilenceLength() == 0)
							iterAfter->SetSilenceLength(this->tooSmall / 2) ;
						iterAfter -- ;
						if (iterAfter->GetPostPunctuation() == L"")	{
							iterAfter ++;
							if (iterAfter != listData.end()) {
								v_after = iterAfter->GetOriginalWord() ;
								iterAfter->m_percentageAdjust = this->m_silenceSize2 - 50 ;
							}
							iterAfter --;
						}
					}
////					if (v_adjective_Words.find(v_after,0) == string::npos)
////						iterAfter->SetSilenceLength(25000) ; // VT
////					else
////						iterAfter->m_percentageAdjust = this->m_silenceSize ; // VT
// End Viet sua adjective -----------------------------------------------------------
					if( word->GetNbSamples() < this->tooSmall / 2)
					{
//						iterAfter->SetSilenceLength( (( this->tooSmall - word->GetNbSamples() ) / 2) * word->GetBlockAlign () ); VX
					}
				}
				//iterAfter->m_percentageAdjust = this->m_silenceSize;
			}
// **********************************************************************************
			else if (_silenceDictionnary2.find (expression2) != _silenceDictionnary2.end())
			{
				v_check = false ; // VT
				int v_wordSize1 = 0 ;
//::MessageBox(NULL, StringHelper::ToString(this->tooSmall).c_str(), StringHelper::ToString(v_compress).c_str(), MB_OK); // VT
				if (nbSamples > (this->tooSmall * (100 - v_compress) / 100) / 2)
				{
					data.m_percentageAdjust = this->m_silenceSize + 50 ; // nén từ thứ nhất
				}
				else
				{
					data.m_percentageAdjust = 1 + 50 ; // nén từ thứ nhất
				}

				v_wordSize1 = word->GetNbSamples() ; // VT
				std::wstring wordName = iterAfter->GetWord ();
				Word * word = NULL;
				if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
				{
					word = _selector->FindWord(wordName, accent);
				}
				else if (wordName != L"silence") // An extra word, get it in database unless silence
				{
					word = _selector->GetExtraWord(wordName);
				}
				if (word != NULL) 
				{
					int v_wordSize2 = word->GetNbSamples() ; // VT
					v_wordSize = (v_wordSize1 + v_wordSize2) * v_break_compound / 100 ;
					if (v_wordSize < v_compound_min) v_wordSize = v_compound_min ; // VT
// Viet sua adjective ---------------------------------------------------------------
					std::wstring v_after = L"" ;
					if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
					{
						if (iterAfter->GetPostPunctuation() == L"")	{
							iterAfter ++;
							if (iterAfter != listData.end()) {
								v_after = iterAfter->GetOriginalWord() ;
								v_after.append(L"/") ;
							}
							iterAfter --;
						}
					}
					if (v_adjective_Words.find(v_after,0) == string::npos) { // từ sau từ thứ hai không là từ after
						if (word->GetNbSamples() > this->tooSmall / 2)  // <
							if (iterAfter->GetSilenceLength() == 0) {
//*::MessageBox(NULL, StringHelper::ToString(iterAfter->GetSilenceLength()).c_str(), StringHelper::ToString(iterAfter->GetOriginalWord()).c_str(), MB_OK); // VT
								iterAfter->SetSilenceLength(v_wordSize) ; // VT
							}
						if (v_isStopConsonant(iterAfter->GetOriginalWord(),iterAfter->GetAccent())==false) // khong stretch SC
							iterAfter->m_percentageAdjust = this->m_silenceSize2 / 2 - 50 ; // VT : giản từ thứ 2
//							iterAfter->SetSilenceLength(this->tooSmall / 2) ; // VT
					} else { // từ sau từ thứ hai là after
						if (v_after != L"") { // v_after là từ thứ ba, iterAfter là từ thừ nhì của từ kép
							if (v_isStopConsonant(iterAfter->GetOriginalWord(), iterAfter->GetAccent()) == false) { // V moi them
								iterAfter->m_percentageAdjust = this->m_silenceSize + 50; // VT không là SC
//---------------------------------------------------------------------------------
								iterAfter++;
//*								iterAfter->SetSilenceLength(this->mediumSilence);
								if (iterAfter->GetSilenceLength() == 0)
									iterAfter->SetSilenceLength(this->v_adjectives_sl);
								iterAfter--;
//---------------------------------------------------------------------------------
							}
							else { // Viet moi them - Them silence vao sau stop consonant
// new xóa						iterAfter->SetSilenceLength(iterAfter->GetSilenceLength() +  (v_stop_silence / 2) * word->GetBlockAlign() );
// -------------------------
								iterAfter++;
								if (iterAfter->GetSilenceLength() == 0)
									iterAfter->SetSilenceLength(this->mediumSilence);
								iterAfter--;
							}
//-----------------------
						}
						else {
							if (v_isStopConsonant(iterAfter->GetOriginalWord(),iterAfter->GetAccent())==false) // khong stretch SC
								iterAfter->m_percentageAdjust = this->m_silenceSize2 / 2 - 50 ; // VT : giản từ thứ hai
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength(v_wordSize) ;
						}
					}
				}
			}
// End Viet sua adjective -----------------------------------------------------------
// **********************************************************************************
// **********************************************************************************
			else if( data.m_wordType == ToBeSynthetiseData::DATE ||data.m_wordType == ToBeSynthetiseData::NUMBER || data.m_wordType == ToBeSynthetiseData::SYMBOL)
			{
				if(nbSamples > (this->tooSmall * (100 - v_compress) / 100) / 2)
				{
					if (v_isStopConsonant(data.GetOriginalWord(), data.GetAccent()) == false) 
						data.m_percentageAdjust = this->m_silenceSize + 50;
					if (data.GetSilenceLength() == 0)
						data.SetSilenceLength(this->v_number_sl /2); // Number va date-time stop and non stop consonant deu them silence
				} 
			}
			//,   ;   .   ?   !   :  
//				else if ( std::find( this->m_articlesDictionary.begin(), this->m_articlesDictionary.end(), wordName ) != this->m_articlesDictionary.end() ) // VX
// **********************************************************************************
			else if ( std::find( this->m_articlesDictionary.begin(), this->m_articlesDictionary.end(), iter->GetOriginalWord ()) != this->m_articlesDictionary.end() ) // VS
			{
				if( data.GetPostPunctuation() != L"," &&
					data.GetPostPunctuation() != L";" &&
					data.GetPostPunctuation() != L"." &&
					data.GetPostPunctuation() != L"?" &&
					data.GetPostPunctuation() != L"!" &&
					data.GetPostPunctuation() != L":" ) 
				{
					v_check = true ; // VT
					if(nbSamples > (this->tooSmall * (100 - v_compress) / 100) / 2) // VLL
					{ // VLL
						data.m_percentageAdjust = this->m_silenceSize + 50; // VLL
						if (data.GetSilenceLength() == 0)
							data.SetSilenceLength(0) ;
					} // VLL
				} // chu thich: medium silence = word silence sau moi tu
				else
					if (data.GetSilenceLength() == 0)
						data.SetSilenceLength(this->v_adjectives_sl /2 * word->GetBlockAlign()) ; // VT các mạo từ có silence length = word length
			}
// **********************************************************************************
// Viet sua adjective ---------------------------------------------------------------
			else 
			{
				std::wstring v_after = L"" ;
				if (iterAfter != listData.end() && iterAfter->GetOriginalWord() != L"") 
				{
					if (iter->GetPostPunctuation() == L"")	{
//						iterAfter ++;
						if (iterAfter != listData.end()) 
							v_after = iterAfter->GetOriginalWord() ;
//						iterAfter --;
						if (v_adjective_Words.find(v_after,0) != string::npos) {
							if (v_isStopConsonant(data.GetOriginalWord(), data.GetAccent()) == false)
								data.m_percentageAdjust = this->m_silenceSize + 50 ; // VT
							if (iterAfter->GetSilenceLength() == 0)
								iterAfter->SetSilenceLength((iter->GetWord().size() + iterAfter->GetWord().size()) * v_break_compound / 100) ; // VT
						}
					}
				}
			}
		}
		newListData.push_back( data );
	} // End for

	toSynthetise.SetListData (newListData);
}

void VietVoice::SilenceAdder::_LoadSilencesSize(std::wstring filename) 
{
	File::Reader reader (filename);
reader.ReadShort();
	
	smallSilence = ReadIntFromFile(reader);
	mediumSilence = ReadIntFromFile(reader);
	longSilence = ReadIntFromFile(reader);
}

void VietVoice::SilenceAdder::_LoadWordsSize(std::wstring filename)
{
	File::Reader reader (filename);

	reader.ReadShort();
	smallWord  = ReadIntFromFile(reader);
	mediumWord  = ReadIntFromFile(reader);
	longWord  = ReadIntFromFile(reader);
}

int VietVoice::SilenceAdder::ReadIntFromFile(File::Reader & reader)
{
	wchar_t car;
	std::wstring line;
	do 
	{
		car = reader.ReadShort();
		if(car != L'\n')
		{
			line.push_back(car);
		}
	} while (car != L'\n');
	std::wistringstream stream( line );
	int value = 0;
	stream >> value;
	return value;
}

void VietVoice::SilenceAdder::_LoadSilenceDictionnary (std::wstring filename) 
{
// Viet chu thich: void nay khong xai nua
	File::Reader reader (filename);


	std::wstring line;
	wchar_t car;
	reader.ReadShort ();

	_locale_t vietnameseLocal = ::_create_locale (LC_ALL, ".1258");
	while (true)
	{
		car = reader.ReadShort ();
		if (car  == L'\t' || car  == L'\n' || car  == L'\r' || car == L'~')
		{
			if (!line.empty ())
			{
				for (unsigned int i = 0; i < line.length (); i++)
				{
					line [i] = ::_towlower_l (line[i], vietnameseLocal) ;
				}

				StringHelper::RemoveSpace (line);
				std::pair<std::wstring, std::wstring> p (line, line);
				_silenceDictionnary2.insert (p);
				line.clear ();
				::MessageBox(NULL, line.c_str(), L"aaa", MB_OK);
			}
			if (car == L'~')
				break;
		}
		else
		{
			line += car ;
		}
	}
}
long VietVoice::SilenceAdder::v_getBreakSilence1() // VT
{
	return v_break_silence1;
}
long VietVoice::SilenceAdder::v_getBreakSilence2() // VT
{
	return v_break_silence2;
}
long VietVoice::SilenceAdder::v_getVolume() // VT
{
	return v_volume;
}
long VietVoice::SilenceAdder::v_getSpeed() // VT
{
	return v_speed;
}
long VietVoice::SilenceAdder::v_getPitch() ////// VT
{
	return v_pitch;
}
void VietVoice::SilenceAdder::v_getLight(std::wstring &v_lws) // VT
{
	v_lws = v_light_Words;
}
void VietVoice::SilenceAdder::v_getAdjective(std::wstring &v_ads) // VT
{
	v_ads = v_adjective_Words;
}
bool VietVoice::SilenceAdder::v_isStopConsonant(std::wstring v_word, int v_accent) // VT
{
	if (v_word.size() < 2) return false ;
	if (v_word.substr(v_word.size()-2,2) != L"ch" && v_word.substr(v_word.size()-1,1) != L"p" && v_word.substr(v_word.size()-1,1) != L"t" && v_word.substr(v_word.size()-1,1) != L"c")
		return false ;
	if (v_accent != 1 && v_accent != 5) return false ;
	return true ;
}
long VietVoice::SilenceAdder::v_getCompress() // VT
{
	return v_compress;
}
/*
- những chữ như:  du khách, mát máy, Bãi Cháy, ngày nay...  có vẻ bị cắt hơi lố
- những chữ loại "nhỏ nhẹ" : ở, và, ... còn khá dài.
- chấm xuống hàng không có silence hay quá ít.

Đọc ngày 19/2 là 19 trên 2, có lẽ giải thuật cho vụ này còn thiếu sót vì quá nhiều case phải xử lý.
*/
