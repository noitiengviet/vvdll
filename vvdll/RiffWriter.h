#if !defined RIFFWRITER_H

#define RIFFWRITER_H

#include "fileio.h"
 /**
  * Class Name:          RiffReader.java
  *
  * Author(s):           Benoit Lanteigne
  *
  * Description:         RiffWritter is a class used to help write RIFF file.  RIFF file
  *                      are file organized in chunks.  A program reads the chunk it is
  *                      interrested in an ignore the others.  Wave file are a type
  *                      of RIFF file.
  *
  * Copyright:           All rights reserved Moncton University
  *                      Tang-Ho L� Ph. D. - Computer Science Department
  *
  */

 /**
   * CODE MODIFICATIONS AND BUG CORRECTIONS
   * --------------------------------------
   * FIel created on september 8 2007
   */

class RiffWriter : public File::Saver
{
public:

	RiffWriter (std::wstring const & filename);
	/**
	* Each chunks start with a 4 bytes name used as a ID.  writeChunkName
	* is used to write such a name from a file.
	*
	* std::string name:  A string containing the name of the chunk.
	*/
	void  SaveChunkName (std::string name);
};

#endif