#include "SilenceAdder.h"
#include <fstream>
VietVoice::SilenceAdder::SilenceAdder (std::wstring silenceDictionnaryName, WordSelector & selector)
: _selector (&selector)
{
	LoadSilenceDictionnary (silenceDictionnaryName);
}

VietVoice::SilenceAdder::SilenceAdder (const SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary = silenceAdder._silenceDictionnary;
		_selector = silenceAdder._selector;
	}
}

VietVoice::SilenceAdder::~SilenceAdder ()
{}

void VietVoice::SilenceAdder::operator = (const VietVoice::SilenceAdder & silenceAdder)
{
	if (this != &silenceAdder)
	{
		_silenceDictionnary = silenceAdder._silenceDictionnary;
		_selector = silenceAdder._selector;
	}
}

void VietVoice::SilenceAdder::Treat (VietVoice::ToBeSynthetise & toSynthetise) 
{
	long smallWord = 15000;
	long mediumWord = 20000;
	long longWord = 21000;
	
	long smallSilence = 0;
	long mediumSilence = 4000;
	long longSilence = 8000;
	
	long longestWord = 0;
	int wordCount = 0;
	int accent = 0;
	std::wstring wordName;
	std::list<ToBeSynthetiseData> listData = toSynthetise.GetListData ();
	bool addSilenceForce = false;
	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{
		ToBeSynthetiseData data = *iter;
		

		// Obains the accent for the word
		wordName = data.GetWord ();

		accent = data.GetAccent ();
		
		Word * word = NULL;
		if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
		{
			word = _selector->FindWord(wordName, accent);
		}
		else if (wordName != L"silence") // An extra word, get it in database unless silence
		{
			word = _selector->GetExtraWord(wordName);
		}
		
		if (word != NULL && wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
		{
			if (word->GetNbSamples () > longestWord)
			{
				
				longestWord +=word->GetNbSamples ();
				++wordCount;
			}
		}
		
		delete word;
		
	}

	if (wordCount > 0)
	{
		longestWord /= wordCount;
	}

	std::list<ToBeSynthetiseData> newListData;
	for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
	{

		
		
		std::list<ToBeSynthetiseData>::iterator iterAfter = iter;
		iterAfter++;
		ToBeSynthetiseData data = *iter;
		

		// Obains the accent for the word
		wordName = data.GetWord ();
		
		std::wstring expression = L"";
		if (iterAfter != listData.end())
		{
			expression = wordName;
			expression += L" ";
			expression += iterAfter->GetWord ();
			StringHelper::RemoveSpace(expression);
		}
		accent = data.GetAccent ();
		
		Word * word = NULL;
		if (accent != -1) // If not an extra word, find the word in one of the 2 "normal database"
		{
			word = _selector->FindWord(wordName, accent);
		}
		else if (wordName != L"silence") // An extra word, get it in database unless silence
		{
			word = _selector->GetExtraWord(wordName);
		}

	
		if (toSynthetise.GetVoice () == ToBeSynthetise::MALE)
		{
		if (word != NULL && wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
		{
				if (!addSilenceForce)
				{
					if (_silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						addSilenceForce = true;
					}
					
					if (static_cast<double>(word->GetNbSamples ()) < 13500 && _silenceDictionnary.find (expression) == _silenceDictionnary.end())
					{
						//long lengthSilence = longestWord - word->GetNbSamples ();
				
						//if (lengthSilence > 5000)
						long lengthSilence = 2000;
						lengthSilence *= word->GetBlockAlign ();
						data.SetSilenceLength (lengthSilence);
					
					}
					else if (word->GetNbSamples () < 10000 &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						long lengthSilence = 2000;
						lengthSilence *= word->GetBlockAlign ();
						data.SetSilenceLength (lengthSilence);
					}
					else if (word->GetNbSamples () >27500 &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						//data.SetWordCut (0.05);
					}

					/*if (expression.compare (L"") != 0 && _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						
						data.SetWordCut (0.15);
					}*/
				}
				else
				{
					addSilenceForce=false;
					long lengthSilence = 5000;
					lengthSilence *= word->GetBlockAlign ();
					data.SetSilenceLength (lengthSilence);
				}
			
		}

		}
		else if (toSynthetise.GetVoice () == ToBeSynthetise::FEMALE)
		{
			if (word != NULL && wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
			{

				if (!addSilenceForce)
				{
					if (_silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						addSilenceForce = true;
					}
					
					if (static_cast<double>(word->GetNbSamples ()) < 16000 && _silenceDictionnary.find (expression) == _silenceDictionnary.end())
					{
						//long lengthSilence = longestWord - word->GetNbSamples ();
				
						//if (lengthSilence > 5000)
						long lengthSilence = 5000;
						lengthSilence *= word->GetBlockAlign ();
						data.SetSilenceLength (lengthSilence);
					
					}
					else if (word->GetNbSamples () < 10000 &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						long lengthSilence = 5000;
						lengthSilence *= word->GetBlockAlign ();
						data.SetSilenceLength (lengthSilence);
					}
					else if (word->GetNbSamples () >27500 &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						data.SetWordCut (0.08);
					}

					/*if (expression.compare (L"") != 0 && _silenceDictionnary.find (expression) != _silenceDictionnary.end())
					{
						
						data.SetWordCut (0.15);
					}*/
				}
				else
				{
					addSilenceForce=false;
					long lengthSilence = 5000;
					lengthSilence *= word->GetBlockAlign ();
					data.SetSilenceLength (lengthSilence);
				}

				
			}		

		}
		else 
			{
			if (word != NULL && wordName != L"silence" && wordName != L"silenceSpecial" && wordName != L"") 
			{
					if (!addSilenceForce)
					{
						if (_silenceDictionnary.find (expression) != _silenceDictionnary.end())
						{
							addSilenceForce = true;
						}
						
						if (static_cast<double>(word->GetNbSamples ()) < smallWord && _silenceDictionnary.find (expression) == _silenceDictionnary.end())
						{
							//long lengthSilence = longestWord - word->GetNbSamples ();
					
							//if (lengthSilence > 5000)
							long lengthSilence = longSilence;
							lengthSilence *= word->GetBlockAlign ();
							data.SetSilenceLength (lengthSilence);
						
						}
						else if (word->GetNbSamples () < mediumWord &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
						{
							long lengthSilence = mediumSilence;
							lengthSilence *= word->GetBlockAlign ();
							data.SetSilenceLength (lengthSilence);
						}
						else if (word->GetNbSamples () < longWord &&  _silenceDictionnary.find (expression) != _silenceDictionnary.end())
						{
							long lengthSilence = smallSilence;
							lengthSilence *= word->GetBlockAlign ();
							data.SetSilenceLength (lengthSilence);
						}

						/*if (expression.compare (L"") != 0 && _silenceDictionnary.find (expression) != _silenceDictionnary.end())
						{
							
							data.SetWordCut (0.15);
						}*/
					}
					else
					{
						addSilenceForce=false;
						long lengthSilence = 5000;
						lengthSilence *= word->GetBlockAlign ();
						data.SetSilenceLength (lengthSilence);
					}
				
				}
			}
		
		// NOTE: After stranger word, wont have pause after punc now
		if (iterAfter == listData.end() && word != NULL)
		{
			data.SetSilenceLength (40000 * word->GetBlockAlign ());
		}
		newListData.push_back (data);
		
		delete word;
	}

	toSynthetise.SetListData (newListData);




}

void VietVoice::SilenceAdder::LoadSilenceDictionnary (std::wstring filename) 
{
	File::Reader reader (filename);


	std::wstring line;
	wchar_t car;
	reader.ReadShort ();

	_locale_t vietnameseLocal = ::_create_locale (LC_ALL, ".1258");
	while (true)
	{

		car = reader.ReadShort ();
		
		if (car  == L'\t' || car  == L'\n' || car  == L'\r' || car == L'~')
		{
			if (!line.empty ())
			{
				for (unsigned int i = 0; i < line.length (); i++)
				{
					line [i] = ::_towlower_l (line[i], vietnameseLocal) ;
				}

				StringHelper::RemoveSpace (line);
				std::pair<std::wstring, std::wstring> p (line, line);
				_silenceDictionnary.insert (p);
				line.clear ();
				
			}
			
			if (car == L'~')
				break;
		}
		else
		{
			line += car ;
		}

		
	}

}
