#include "concatenator.h"
#include "directsound.h"
#include "silenceadder.h" // VT
/**
* Constructor
*/
VietVoice::WavePlayer::WavePlayer ()
{}

/**
* Play a wave represented by the given data.
*
* Parameters:
*
* vector<unsigned char> spokenData:  the wave data
*/
void VietVoice::WavePlayer::Play (std::vector<unsigned char> & spokenData)
{
			
	WAVEFORMATEX format;

	format.wFormatTag = WAVE_FORMAT_PCM;
	format.nChannels = 1; 
	format.nSamplesPerSec = 44100; 
	format.nAvgBytesPerSec = 88200; 
	format.nBlockAlign = 2; 
	format.wBitsPerSample =16; 
	format.cbSize = sizeof (format); 

	waveOutOpen( &_hWave, WAVE_MAPPER, &format, NULL, NULL, CALLBACK_NULL); 
	WAVEHDR wavehdr;
	wavehdr.lpData = (LPSTR)&spokenData[0];
	wavehdr.dwBufferLength = spokenData.size ();
	wavehdr.dwBytesRecorded = 0;
	wavehdr.dwUser = 0;
	wavehdr.dwFlags = 0;
	wavehdr.dwLoops = 0;

	waveOutPrepareHeader (_hWave, &wavehdr, sizeof (wavehdr));

	waveOutWrite  (_hWave, &wavehdr, sizeof (wavehdr));

	//while ((wavehdr.dwFlags & WHDR_DONE) == 0);
	while (waveOutUnprepareHeader (_hWave, &wavehdr, sizeof (wavehdr)) == WAVERR_STILLPLAYING)
	{
		::Sleep (100); // Viet xoa
	}

	waveOutClose (_hWave);
		
}

/**
* Stop playing the current wave
*/
void VietVoice::WavePlayer::Stop ()
{
	::waveOutReset (_hWave);
}

/**
* Constructor
*/
VietVoice::Concatenator::Concatenator()
{
	// Put in init method
	/*sound.Create (hwnd);
	sound.SetCoopPriority (hwnd);
			
	primaryBuffer.Create (sound);*/
//v_break_silence=0; // VT
}

/**
* Destructor
*/
VietVoice::Concatenator::~Concatenator ()
{}

/**
* Concategnate the sound data and play it
*
* @param toSpoke Data representing the words to be spoken
* @return The toSpoke data after the treatement
*/
void VietVoice::Concatenator::Treat (ToBeSynthetise & toBeSynthetise) 
{ 	
	std::list <ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
	std::list <ToBeSynthetiseData> newListData ;
	int size = toBeSynthetise.GetSizeWords();
	bool firstWord = true;
	std::wstring v_lightWords = L"với/và/mà/là/thì/đã/sẽ";
	std::wstring v_heavyWords = L"nhưng/vẫn/chỉ/đâu/đó/tuy/đây/nầy/này/nếu/phải";
	int v_punc_break = 0;
	// ==================================================================================
	if (size != 0) // if 1 - Make sure there is sound data to be spoken (prevent a bug when user enter nothing) 
	{			
		//unsigned char * spokenData = new unsigned char[toSpoke.GetSizeWords()]; // Will contain all the sound data
		vector<unsigned char> spokenData;
		//spokenData.resize ();
		// if 2
		// ------------------------------------------------------------------------------
		if (size > 0) // Makes sure there is some sound data // if 2
		{ 
//		int k = 0; // VX

		// For every word to be spoken, get the data and concatenate it
			std::list<ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();
			std::list<Token> listToken;
			std::list<ToBeSynthetiseData> newListData;
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
			{
				ToBeSynthetiseData data = *iter;
				Word * word = data.GetWordData();
				
				if (data.GetWord().length() == 0) continue;
				if (data.GetPostPunctuation()==L",") v_punc_break = v_break_silence1;
				else 
				if ((data.GetPostPunctuation()==L".") || (data.GetPrePunctuation()==L")"))
					v_punc_break = v_break_silence2 ;
				else v_punc_break = 0;
				// unsigned char * tmp = word->Get; // Get the sound data
				unsigned long sizeTmp = word->GetNbSamples ()  * word->GetBlockAlign ();
				unsigned long wordCut = word->GetNbSamples () * data.GetWordCut ();
				wordCut = wordCut * word->GetBlockAlign();
				sizeTmp -= wordCut;
// Concategnate the sound data the number of time necessary
//for (int repeat = 0; repeat < data.GetNbRepeats(); repeat++) 
//				{
//::MessageBox(NULL, StringHelper::ToString(::ceilf(sizeTmp * v_speed /100)).c_str(), L"Concatenator", MB_OK);
// ====================================================================
// Xu ly compress words
// ====================================================================
//			sizeTmp = sizeTmp * v_speed /100; // VT
//			sizeTmp = sizeTmp /2 * 2; // VT
				unsigned mBegin = sizeTmp * (100 - v_speed ) / 200; // VT
				mBegin = mBegin / 2 * 2; // VT
				unsigned int j = mBegin; unsigned char mB1; unsigned char mB2;
				long mTmp; long mTmp1; long mTmp2 ; long mTmp_1; long mTmp_2;
				if (v_speed < 100) 
				{
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ; // VT
					bool mStop = false;
					while (mStop==false) 
					{
						j += 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= 0) 
						{
							mStop = true; 
							mBegin = j;
							if (abs(mTmp_1) < abs(mTmp_2)) mBegin -= 2;
						}
						else 
							mTmp_1 = mTmp_2;
					} // End while
				}
				unsigned mEnd = sizeTmp - mBegin; // VT
				mEnd = mEnd / 2 * 2; // VT
				if (v_speed < 100)
				{
					j = mEnd;
					mTmp1 = word->GetByte(j) -5;
					mTmp1 = mTmp1 + (word->GetByte(j+1) -5) * 256;
					mTmp_1 = ByteToWord(mTmp1) ;
					bool mStop = false;
					while (mStop==false)
					{
						j -= 2;
						mTmp2 = word->GetByte(j) -5;
						mTmp2 = mTmp2 + (word->GetByte(j+1) -5) * 256;
						mTmp_2 = ByteToWord(mTmp2) ;
						if (mTmp_1 * mTmp_2 <= 0) 
						{
							mStop = true ; 
							mEnd = j;
							if (abs(mTmp_1) < abs(mTmp_2)) 
								mEnd += 2 ;
						}
						else 
							mTmp_1 = mTmp_2 ;
					} // End while
				}
//===============================================================
/*std::vector <int> mData;
for (unsigned int j = 0; j < sizeTmp; j++) 
			{
			  if (j % 2==0)
			  {
			    mTmp = word->GetByte(j) -5;
			  }
			  else
			  {
				mTmp = mTmp + (word->GetByte(j) -5) * 256;
				if (mTmp <= 32767) mData.push_back (mTmp);
				else mData.push_back (mTmp - 65536);
				mB1 = mTmp % 256;
				mB2 = (mTmp - mB1) / 256;
			  }
			}
int mKC = 0;
int mjOld = 0;
for (unsigned j=0; j< mData.size () - 1; j++)
{
  if ((mData[j]*mData[j+1]<0) && (j - mjOld > 10))
	{
mjOld = j;
//::MessageBox (NULL, StringHelper::ToString (j).c_str (), L"Concatenator",MB_OK);
//::MessageBox (NULL, StringHelper::ToString (mData[j]).c_str (), L"Data 1",MB_OK);
//::MessageBox (NULL, StringHelper::ToString (mData[j+1]).c_str (), L"Data 2",MB_OK);

  }
}
*/
//===============================================================
//			for (unsigned int j = 0; j < sizeTmp; j++) // VX
					// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
				for (unsigned int j = mBegin; j < mEnd; j++) // VT
//for (unsigned int j = 0; j < (int) ::ceilf(sizeTmp * v_speed /100); j++) 
				{
					/* if (firstWord == true)
					{
						j = 0;
						firstWord = false;
					} */
					//::MessageBox (NULL, StringHelper::ToString (word->GetByte (j)).c_str (), L"aaa", MB_OK);
					if ((v_lightWords.find(data.GetOriginalWord(),0) != string::npos) || (v_heavyWords.find(data.GetOriginalWord(),0) != string::npos))
					{
						long mTmp ; unsigned char mB1; unsigned char mB2;
						if (j % 2==0)
							mTmp = word->GetByte(j) -5;
						else
						{
							mTmp = mTmp + (word->GetByte(j) -5) * 256;
							if (v_lightWords.find(data.GetOriginalWord(),0) != string::npos) 
							{
								mTmp = ChangeVolume(mTmp , v_volume) ;
							}
							else
							{
								mTmp = ChangeVolume(mTmp , 200 - v_volume) ;
							}
							mB1 = mTmp % 256;
							mB2 = (mTmp - mB1) / 256;
							spokenData.push_back ((unsigned char)mB1);
							spokenData.push_back ((unsigned char)mB2);
						}
						/* if (word->GetByte (j)<127)
						{
							spokenData.push_back (( (unsigned char)word->GetByte (j) - 5) * v_volume /100 );//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
							//::MessageBox (NULL, StringHelper::ToString (v_temp).c_str (), L"Concatenator",MB_OK);
						}
						else
						{
							spokenData.push_back ((((unsigned char)word->GetByte (j) - 5 - 255) * v_volume /100)+255) ;//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
							//::MessageBox (NULL, StringHelper::ToString (word->GetByte (j) - 5).c_str (), L"Concatenator",MB_OK);
						}
*/
					}
					else
						spokenData.push_back ((unsigned char)word->GetByte (j) - 5);//(unsigned char) (tmp[j]); // Substract 5 to normalize the data.  IN the database, each byte is aded 5 so cannot convert back to mp3.
					//spokenData [k] -= 5;
				//k++;
				} // End for j
					// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// End Viet them
//::MessageBox (NULL, StringHelper::ToString (data.GetSilenceLength ()).c_str (), L"aaa", MB_OK);
//			for (unsigned int j = 0; j < data.GetSilenceLength (); j++) // VX
				int mSilence = data.GetSilenceLength () * v_speed / 100; // VT
				mSilence = mSilence / 2 * 2; // VT
				for (unsigned int j = 0; j < mSilence  ; j++) // VT
					spokenData.push_back (0);
			} // End for iter
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//			} 
			float mBreak ; 
			if (spokenData.size () > 400000) mBreak = 0 ;
			else mBreak= float (400000 - spokenData.size ()) / 400000 ;
			for (unsigned int j = 0; j < (v_punc_break * mBreak * 88.2f) / 2 * 2 ; j++) // VT
				spokenData.push_back (0);
			Word final (spokenData);
			if (toBeSynthetise.GetMustPlay ())
			{
				//::MessageBox (NULL, StringHelper::ToString (spokenData.size ()).c_str (), L"aaa", MB_OK);
				if (spokenData.size () > 0)
				{	//_//soundPlayer.Create (_sound, spokenData);
					//_//soundPlayer.Play ();
					_wavePlayer.Play (spokenData);
					//_mp3Player.Play(L"database\\tmp.wav");
				}
			}
			else
			{
				Word final (spokenData);
				final.Save (toBeSynthetise.GetMp3Name ());
			}
			// delete [] spokenData;
		} // End if 2
		// ------------------------------------------------------------------------------
	} // End if 1
	// ==================================================================================
} // End void Treat

/**
* Save a byte array containning the sound data in a mp3 file
*
* return value: True if success, elsse false
*
* unsigned char * spokenData spokenData: The sound data contained in an array
* int size:  Size of the array.
*/
bool VietVoice::Concatenator::SaveSpokenData (std::wstring filename, unsigned char * spokenData, int size )
{
	try
	{
		//File::Saver saver (filename);		 
		//saver.SaveMP3 (spokenData, size);
	}
	catch (File::Exception ex)
	{
		throw VietnameseTTException(L" Error, could not save temporary file");
	}

	return true;
}

/**
* Stop reading the current text
*/
void VietVoice::Concatenator::Stop ()
{	
	_wavePlayer.Stop ();
}
void VietVoice::Concatenator::Get_BS1 (long v_bs)
{	
	v_break_silence1 = v_bs;
}
void VietVoice::Concatenator::Get_BS2 (long v_bs)
{	
	v_break_silence2 = v_bs;
}
void VietVoice::Concatenator::Get_VL (long v_vl)
{	
	v_volume = v_vl;
}
void VietVoice::Concatenator::Get_SP (long v_sp)
{	
	v_speed = v_sp;
}
long VietVoice::Concatenator::ByteToWord (unsigned long v_byte)
{	
	if (v_byte <= 32767) return v_byte;
	else return v_byte - 65536;
}
long VietVoice::Concatenator::ChangeVolume (long v_byte, long v_volume)
{	
	if (v_byte <= 32767) return v_byte * v_volume /100; 
	else
	return ((v_byte - 65536) * v_volume / 100) + 65536;
}