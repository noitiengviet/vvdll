
#include "fileio.h"

File::Exception::Exception (std::wstring const & msg)
	: _msg(msg)
{}
		
std::wstring File::Exception::GetMsg() const
{ 
	return _msg;
}

File::Reader::Reader (std::wstring const & filename)
{

	_file = ::CreateFile (filename.c_str (), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (_file == NULL)
		throw File::Exception (TEXT("Could not open file"));
}

File::Reader::~Reader ()
{
	::CloseHandle (_file);
}

void File::Reader::SkipBytes (long nb)
{
	::SetFilePointer (_file, nb, NULL, FILE_CURRENT);
		
}

long File::Reader::ReadLong ()
{
	long l;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &l, sizeof (long), &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of long"));

	return l;
}

short File::Reader::ReadShort()
{
	short l;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &l, sizeof (short), &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of short"));

	return l;
}

unsigned char File::Reader::ReadByte ()
{
	unsigned char l;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &l, sizeof (unsigned char), &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of byte"));

	return l;
}

unsigned long File::Reader::ReadLongJava ()
{
	unsigned long l;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &l, sizeof (long), &nbBytesRead, NULL) == FALSE)
	{
		throw File::Exception (TEXT("Could not read file"));
	}

	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of long java"));

	BYTE * b = (BYTE *) &l;

	BYTE tmp;

	tmp = b[0];
	b[0] = b[3];
	b[3] = tmp;

	tmp = b [1];
	b [1] = b[2];
	b [2] = tmp;

	return l;
}
unsigned short File::Reader::ReadShortJava ()
{
	unsigned short l;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &l, sizeof (short), &nbBytesRead, NULL) == FALSE)
	{
		throw File::Exception (TEXT("Could not read file"));
	}

	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of short java"));

	BYTE * b = (BYTE *) &l;

	BYTE tmp;

	tmp = b[0];
	b[0] = b[1];
	b[1] = tmp;

	return l;
}

float File::Reader::ReadFloatJava ()
{
	float f;
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) &f, sizeof (float), &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of float java"));

	BYTE * b = (BYTE *) &f;

	BYTE tmp;

	tmp = b[0];
	b[0] = b[3];
	b[3] = tmp;

	tmp = b [1];
	b [1] = b[2];
	b [2] = tmp;

	return f;
}

std::wstring File::Reader::ReadWString ()
{
	long len = ReadLong ();

	std::wstring str;

	str.resize (len);

	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) (&str [0]), len * 2, &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of wstring"));
		
	return str;
}

void File::Reader::ReadUnsignedCharArrayJava (unsigned char * a, int size)
{
	DWORD nbBytesRead;

	if (::ReadFile (_file, (LPVOID) a, sizeof (unsigned char) * size, &nbBytesRead, NULL) == FALSE)
		throw File::Exception (TEXT("Could not read file"));
				
	if (nbBytesRead == 0)
		throw File::Exception (TEXT("Unespected end of file of array java"));
}

File::Saver::Saver (std::wstring const & filename)				
{
	_file = ::CreateFile (filename.c_str (), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (_file == NULL)
		throw File::Exception (TEXT("Could not open file"));
	
}

File::Saver::~Saver ()
{
	::CloseHandle (_file);
}

void File::Saver::SaveMP3 (unsigned char * c, int size)
{
	DWORD nbBytesWritten;
				
	if (::WriteFile (_file, (LPCVOID) c, sizeof (unsigned char) * size, &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}

void File::Saver::SaveLong (long l)
{
	DWORD nbBytesWritten;

	if (::WriteFile (_file, &l, sizeof (long), & nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}


void File::Saver::SaveFloat (float l)
{
	DWORD nbBytesWritten;
	if (::WriteFile (_file, &l, sizeof (float), & nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}

void File::Saver::SaveWString (std::wstring str)
{
	DWORD nbBytesWritten;
				
	SaveLong ((long) str.length ());

	if (::WriteFile (_file, (LPCVOID) &str [0], sizeof (wchar_t) * (DWORD) str.length (), &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}

void File::Saver::SaveByteArray (unsigned char * ar, unsigned int size)
{
	DWORD nbBytesWritten;
	

	if (::WriteFile (_file, ar, sizeof (unsigned char) * size, &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
	
}

void File::Saver::SaveShort (short s)
{
	DWORD nbBytesWritten;

	if (::WriteFile (_file, &s, sizeof (short), &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}

void File::Saver::SaveUnsignedChar (unsigned char c)
{
	DWORD nbBytesWritten;

	if (::WriteFile (_file, &c, sizeof (unsigned char), &nbBytesWritten, NULL) == FALSE)
		throw File::Exception (TEXT("Could not write the file"));
}


//---------------------------------------------------------------
// Read an array of bytes from a file
//---------------------------------------------------------------
void File::Reader::ReadUnsignedCharArray (
	unsigned char * a_array, // will points on the array of bytes
	int a_size // The number of bytes to read
)
	{
	DWORD nbBytesRead;

	// Tries to read the array, throws on error
	if ( ::ReadFile( _file, (LPVOID) a_array, sizeof (unsigned char) * a_size, &nbBytesRead, NULL ) == FALSE )
		{
		throw File::Exception ( TEXT( "Could not read file" ) );
		}
		
	// Check the number of bytes that were read, throw if 0
	if (nbBytesRead == 0)
		{
		throw File::Exception ( TEXT( "Unespected end of file of array" ) );
		}
	}

// Obtains the size of the file
unsigned long File::Reader::GetFileSize() 
	{
	return ::GetFileSize( _file, NULL );
	}

//---------------------------------------------------------
// Reads a unicode text file and returns a string 
// containing the content of the files.  The method
// is far from perfect, it only works on UTF-16 file in
// little endian order, but this is enough for the VV dll
//---------------------------------------------------------
std::wstring File::Reader::ReadTextFile(
	std::wstring a_filename
) 
	{
	Reader reader( a_filename );
	
	// Ignore unicode indicating character
	reader.SkipBytes( 2 );

	// Remove 2 bytes from the file size du to the previously removed characters
	unsigned long fileSize = reader.GetFileSize() - 2;

	// Prepare a string that will hold the content of the file
	// In particular, makes sur it is big enough
	std::wstring fileContent;
	fileContent.reserve( fileSize / sizeof( wchar_t ) + 1);
	fileContent.resize( fileSize / sizeof( wchar_t ) );

	// Read the content of the text file
	reader.ReadUnsignedCharArray( reinterpret_cast<unsigned char *>( &fileContent[0]), fileSize );

	return  fileContent.c_str();
	}