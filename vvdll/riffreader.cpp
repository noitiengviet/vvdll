
#include "riffreader.h"

RiffReader::RiffReader (std::wstring const & filename)
: File::Reader (filename)
{}

/**
* Each chunks start with a 4 bytes name used as a ID.  readChunkName
* is used to read such a name from a file.
*
* return value:  A String containing the name of the chunk.
*/
std::string RiffReader::ReadChunkName ()
{
	char nameData[5];

	ReadUnsignedCharArrayJava (reinterpret_cast<unsigned char *>(nameData), 4);
    nameData[4] = '\0';
	return nameData ; //Convert the bytes to a String
}

/**
* Skip a chunk in the file.  The name (id) of the chunk must have been read
* before calling that method, but not the size.
*/
void RiffReader::SkipChunk ()
{
	SkipBytes (ReadLong ());
}

/**
* Finds a particular chunck in the file.  The chunks situated before the desired
* chunk are skipped.
*
* parameters:
*
* std::string name The name of the desired chunk.
*/
void RiffReader::FindChunk (std::string name)
{
	std::string chunkName;
	do
	{
		 chunkName = ReadChunkName(); //Read the name of the next chunk
		 if (chunkName.compare (name) != 0) //If it is not the right one, skip the chunk
		   SkipChunk ();
	}
	while (chunkName.compare (name) != 0); //Stop when the chunk was found.
}