#if !defined (ACCENTFINDER_H)

#define ACCENTFINDER_H
#include <map>
#include "vectoraccent.h"
#include "treatment.h"

namespace VietVoice
{
	/**
	* Class Name:         AccentFinder.java
	*
	* Author(s):          Benoit Lanteigne
	*
	* Creation Date:      December 15 2004
	*
	* Description:        Class used to find the accent of a word
	*
	* Copyright:          All rights reserved Moncton University
	*                     Tang-Ho L� Ph. D. - Computer Science Department
	*
	*/

	/**
	* CODE MODIFICATIONS AND BUG CORRECTIONS
	* --------------------------------------
	* october 19 2005:  Created the file and implemented prepareExtraWords
	* Nomvember 25:  Added copy constructor and = operator
	* January 9 2005:  corrected an error with the substr function that made some word with accents unfindable.
	* February 4 2006:  Modified the class to use unicode file names.
	* February 16 2006:  Seperated implementation in source and header
	* June 9 2006: Change the methods name so they start by a capital letter.
	* June 13 2006:  Upgraded the comments and spacing in accentfinder.cpp file.
	* June 15 2006:  Upgraded the comments and spacing in accentfinder.cpp file some more
	*/

	class AccentFinder : public Treatement
	{
	public:

		/**
		* Constructor
		*
		* Parameters:
		*
		* VectorAccent & va: A reference to the VectorAccent object use to determine the accent of a word
		* std::wstring extra:  Name of the file containign the "extra", better described as the words from
	    *					   the 3rd database
		*/
		AccentFinder(VectorAccent & va);
		AccentFinder(VectorAccent & va, std::wstring extra);

		/**
		* Copy constructor
		*/
		AccentFinder  (const AccentFinder & accentFinder);

		/**
		* Destructor
		*/
		virtual ~AccentFinder ();

		/**
		* Assignement operator used to avoid memory leaks
		*/
		void operator = (const AccentFinder & accentFinder);

		/**
		* Find the accent for every word in the utterance.
		*
		* Return value:  The data to be spoken after modification
		*
		* Parameters:
		* ToBeSpoken toSpoke: A reference to the data to be spoken that is being processed
		* 
		*/
		void Treat (ToBeSynthetise & toSynthetise);

		/**
		* Read all extra words from a file
		*
		* Parameters:
		*
		* std::wstring filename: filename The name of the file
		*/
		void PrepareExtraWords (std::wstring filename);

	protected:

		std::map <std::wstring, int> _extraWords; // Contain all special words (words from the third database).
		VectorAccent & _vectorAccent; //Used to find the accent of a word.
	};
}

#endif