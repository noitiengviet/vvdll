#include "concatenator.h"
//#include "directsound.h" // Xoa tai Ha Noi
//#include "silenceadder.h" // VT
//#include "silenceAdder.cpp"
#include "math.h" // VT
#include "stdio.h" // VT
#include <float.h> // VT
/**
* Constructor
*/
VietVoice::WavePlayer::WavePlayer ()
{}

/**
* Play a wave represented by the given data.
*
* Parameters:
*
* vector<unsigned char> spokenData:  the wave data
*/
void VietVoice::WavePlayer::Play (std::vector<unsigned char> & spokenData)
{
	unsigned int v_speed = 1;	
	std::vector<unsigned char> v_spoke;
	for (unsigned int i = 0; i < spokenData.size(); i ++)
		if (i % (v_speed * 2) < 2) v_spoke.push_back(spokenData[i]);
	spokenData = v_spoke;

	WAVEFORMATEX format;

	format.wFormatTag = WAVE_FORMAT_PCM ;
	format.nChannels = 1 ; 
	format.nSamplesPerSec = 4000 / v_speed ; // 44100
	format.nAvgBytesPerSec = 8000 / v_speed ; // 88200
	format.nBlockAlign = 2 ; // 2
	format.wBitsPerSample = 16 ; // 16
	format.cbSize = sizeof (format); 

	waveOutOpen( &_hWave, WAVE_MAPPER, &format, NULL, NULL, CALLBACK_NULL); 
	WAVEHDR wavehdr;
	wavehdr.lpData = (LPSTR)&spokenData[0];
	wavehdr.dwBufferLength = spokenData.size ();
	wavehdr.dwBytesRecorded = 0;
	wavehdr.dwUser = 0;
	wavehdr.dwFlags = 0;
	wavehdr.dwLoops = 0;

	waveOutPrepareHeader (_hWave, &wavehdr, sizeof (wavehdr));

	waveOutWrite  (_hWave, &wavehdr, sizeof (wavehdr));

	//while ((wavehdr.dwFlags & WHDR_DONE) == 0);
	while (waveOutUnprepareHeader (_hWave, &wavehdr, sizeof (wavehdr)) == WAVERR_STILLPLAYING)
	{
		::Sleep (100); // Viet xoa
	}

	waveOutClose (_hWave);

}

/**
* Stop playing the current wave
*/
void VietVoice::WavePlayer::Stop ()
{
	::waveOutReset (_hWave);
}

/**
* Constructor
*/
VietVoice::Concatenator::Concatenator()
{
	std::wstring quantityFile = L"Quantity_Words.txt" ;
	std::wstring articleFile = L"Article_Words.txt" ;
	std::wstring nounFile = L"Noun_Words.txt" ;
	std::wstring verbFile = L"Verb_Words.txt" ;
	std::wstring adverbFile = L"Adverb_Words.txt" ;
	File::Reader::ReadTextFileLines( v_quantityDictionary, quantityFile);
	File::Reader::ReadTextFileLines( v_articleDictionary, articleFile);
	File::Reader::ReadTextFileLines( v_nounDictionary, nounFile);
	File::Reader::ReadTextFileLines( v_verbDictionary, verbFile);
	File::Reader::ReadTextFileLines( v_adverbDictionary, adverbFile);

	// Put in init method
	/*sound.Create (hwnd);
	sound.SetCoopPriority (hwnd);

	primaryBuffer.Create (sound);*/
	//v_break_silence=0; // VT
}

/**
* Destructor
*/
VietVoice::Concatenator::~Concatenator ()
{}

/**
* Concategnate the sound data and play it
*
* @param toSpoke Data representing the words to be spoken
* @return The toSpoke data after the treatement
*/
void VietVoice::Concatenator::Treat(ToBeSynthetise & toBeSynthetise) 
{ 	

	int v_speed1 = v_speed / 1000 ; int v_speed2 = v_speed % 1000;
	v_speed1=100;
	v_speed2=99;

	int size = toBeSynthetise.GetSizeWords() ;
	bool firstWord = true;
	int v_punc_break = 0;
	std::wstring v_number = L"không/ lẻ/ linh/ một/ mốt/ hai/ ba/ bốn/ năm/ lăm/ sáu/ bảy/ tám/ chín/ mười/ mươi/ phẩy" ;
	std::wstring v_number1 = L"không/ một/ hai/ ba/ bốn/ năm/ lăm/ sáu/ bảy/ tám/ chín/ mười/ lẻ/" ;
	std::wstring v_part = L"phẩy/ ngàn/ trăm/ triệu/ tỉ/ mươi/ mười/" ;
	std::wstring v_letter = L"a/ b/ c/ d/ đ/ e/ f/ g/ h/ i/ j/ k/ l/ m/ n/ o/ p/ q/ r/ s/ t/ u/ v/ w/ x/ y/ z" ;
	int nsamp = m_samples ;
	unsigned long v_B_Vl = 0;

	//======================================================================================
	if (size != 0) // if 1 - Make sure there is sound data to be spoken (prevent a bug when user enter nothing) 
	{			
		vector<unsigned char> spokenData; // Viet move up
		bool v_continous;
		// if 2
		// ------------------------------------------------------------------------------
		if (size > 0) // Makes sure there is some sound data // if 2
		{ 
			std::list<ToBeSynthetiseData> listData = toBeSynthetise.GetListData ();			
			std::wstring v_W, v_WA, v_WAA, v_WAAA, v_WAAAA, v_WAAAAA ; 
			
			std::wstring      v_WB, v_WBB, v_WBBB, v_WBBBB, v_WBBBBB ; 
		
			v_continous = false ; // bool
			unsigned long sizeTmp;
			int v_max_noise;
			unsigned mBegin;
			long j;
			unsigned char mB1; 
			unsigned char mB2;
			long mTmp; 
			long mTmp1; 
			long mTmp2 ; 
			long mTmp_1; 
			long mTmp_2;
			bool mStop;
			unsigned long mEnd;
			vector<long> v_word ;
			std::wstring v_word_name;			
			bool v_checked = false;				
			std::wstring v_aft;			
			float mBreak;
			bool v_silence = false;
			bool b_volume = false;
			float v_avg_word_vl = 0 ;
			float v_max_word_vl = 0 ;
			float size_avg = 0 ;
			float size_max = 0 ;
			unsigned int v_num_word = 0 ;

			float TyLe_CuongDo_TruongDo = 0 ;


			//
		
		File::Saver saver (L"analysis.bin");

		File::Saver saverTest (L"testfloat.bin");

		long SoTu=listData.size();
		saver.SaveLong(SoTu);

		for (std::list<ToBeSynthetiseData>::iterator iter = listData.begin (); iter != listData.end (); iter++) // For each words
		{
				ToBeSynthetiseData data = *iter;
				Word * word = data.GetWordData();			
				//::MessageBox(NULL, StringHelper::ToString(data.GetTyLeTangVolumeTu()).c_str(), L"P3333re-Processing save file", MB_OK); // VT 
				if (data.GetWord().length() == 0) continue;						
				if (data.GetOriginalWord().length() == 0 || data.GetOriginalWord() == L"_silence_") continue ;
				sizeTmp = word->GetNbSamples () * word->GetBlockAlign (); // unsigned long
				v_word.resize(0);
				for(int j=0;j<word->GetSize();j++)
				{
					if (j % 2==0)
					{	
						mTmp = word->GetByte(j) -5 ;//////// v_pitch1 ; // -5;
						if (mTmp < 0) mTmp += 255 ;
					}
					else
					{	
						if (word->GetByte(j) - 5 < 0) 
							mTmp = mTmp + (word->GetByte(j) - 5 + 255) * 256 ;
						else					
							mTmp = mTmp + (word->GetByte(j) - 5) * 256 ;
						v_word.push_back (ByteToWord(mTmp)) ;
					}
				}					

				if(word->GetTuGhep())
				{
					CanDoiAmLuongGhepTu(v_word,word->GetNbSamplePU1(),word->GetPercentVolumePU1(),word->GetPercentVolumePU2());
				}

				long TyLeTangVolume=data.GetTyLeTangVolumeTu();				
			    Change_Volume(v_word,TyLeTangVolume);
				
				unsigned long silenceComa=2800;
				unsigned long silenceDot=4000;
				if(data.GetPrePunctuation().compare(L"(")==0)
				{
					for (unsigned long j = 0; j < silenceComa ; j++) 
						spokenData.push_back (0);
				}

				for (unsigned int j = 0; j < v_word.size(); j++) {
					mB1 = unsigned char (v_word[j] % 256) ;
					mB2 = unsigned char (v_word[j] / 256) ;
					spokenData.push_back(mB1) ;
					spokenData.push_back(mB2) ;
				}

				
				bool ktDau=false;
				long SoByteSilenceDB=data.GetSilenceLength();
				if(data.GetPostPunctuation().compare(L".")==0 || data.GetPostPunctuation().compare(L"?")==0 || data.GetPostPunctuation().compare(L"!")==0)
				{
					data.SetSilenceLength(silenceDot);
					ktDau=true;
				}
				else
				{
					if(data.GetPostPunctuation().compare(L",")==0 || data.GetPostPunctuation().compare(L":")==0 || data.GetPostPunctuation().compare(L";")==0 )
					{
						data.SetSilenceLength(silenceComa);
						ktDau=true;
					}						
				}
				//mSilence = unsigned long (data.GetSilenceLength () / toBeSynthetise.GetDurMoyenSilence()); // int [/2]
				//mSilence = unsigned long (mSilence / 2) * 2; // VT
				
				long SoByte=data.GetSilenceLength();
				
				//////// Ghi nhat ky phan tich tu //////////////////////////
				long type=1;
				if(data.m_wordType==ToBeSynthetiseData::WORD||data.m_wordType==ToBeSynthetiseData::SYMBOL 
					||data.m_wordType==ToBeSynthetiseData::ABBR_S||data.m_wordType==ToBeSynthetiseData::ABBR_L)
				{
					type=1;
				}
				else
				{
					if(data.m_wordType==ToBeSynthetiseData::DATE_G || data.m_wordType==ToBeSynthetiseData::NUMBER_G
					||data.m_wordType==ToBeSynthetiseData::WORD_G||data.m_wordType==ToBeSynthetiseData::SYMBOL_G 
					||data.m_wordType==ToBeSynthetiseData::ABBR_S_G||data.m_wordType==ToBeSynthetiseData::ABBR_L_G)
					{
						type=2;
					}
					else
					{
						if(data.m_wordType==ToBeSynthetiseData::DATE_GE || data.m_wordType==ToBeSynthetiseData::NUMBER_GE
						||data.m_wordType==ToBeSynthetiseData::WORD_GE||data.m_wordType==ToBeSynthetiseData::SYMBOL_GE 
						||data.m_wordType==ToBeSynthetiseData::ABBR_S_GE||data.m_wordType==ToBeSynthetiseData::ABBR_L_GE)
						{
							type=1;
						}
						else
						{
							if(data.m_wordType==ToBeSynthetiseData::VSWORD)
								type=3;
							else
							{
								if(data.m_wordType==ToBeSynthetiseData::DATE || data.m_wordType==ToBeSynthetiseData::NUMBER)
								{
									if(data.GetOriginalWord().compare(L"tháng")==0 || data.GetOriginalWord().compare(L"năm")==0 || data.GetOriginalWord().compare(L",")==0)
										type=1;
									else
										type=2;
								}
							}
						}
					}
				}
				
				saver.SaveWString(data.GetOriginalWord());
				saver.SaveLong(type);
				saver.SaveLong(SoByteSilenceDB);
				saver.SaveLong(TyLeTangVolume);
				saver.SaveFloat(data.GetWordData()->GetPercentPU1());
				saver.SaveFloat(data.GetWordData()->GetPercentPU2());
				saver.SaveFloat(data.GetWordData()->GetPercentVolumePU1());
				saver.SaveFloat(data.GetWordData()->GetPercentVolumePU2());
				saver.SaveLong(data.GetWordData()->GetViTri());
				saver.SaveShort(data.GetWordData()->GetLoaiIndex());
				saver.SaveLong(data.GetWordData()->GetPositionPU1());
				saver.SaveLong(data.GetWordData()->GetPositionPU2());

				
				//saverTest.SaveFloat(8);
			

			
				///////////////////////////////////////////////////////////



				for (unsigned long j = 0; j <SoByte  ; j++) // VT
				   spokenData.push_back (0);

				

				v_word.clear() ; // VT []		

			}
			
			if (toBeSynthetise.GetMustPlay ())
			{
				unsigned int v_max = 400000 ;
				if (spokenData.size () > v_max) mBreak = 0 ;
				else mBreak = float (v_max - spokenData.size ()) / v_max ;
				Word final (spokenData);
				//if (mBreak > 0) ::Sleep (unsigned long (v_break_silence2 * mBreak / 2)) ; // v_punc_break
				if (spokenData.size () > 0)
				{	//_//soundPlayer.Create (_sound, spokenData); // Viet LL va xoa 05-08-2012 (bao loi SsoundPlayer)
					//_//soundPlayer.Play ();
					_wavePlayer.Play (spokenData); 
					//// spokenData.clear() ; //// []
					// _mp3Player.Play(L"database\\tmp.wav");
				}
			}
			else
			{
				Word final (spokenData);
				final.Save (toBeSynthetise.GetMp3Name ());
			}
			
			spokenData.clear();

					

			
		} // End if 2
		// ------------------------------------------------------------------------------
	} // End if 1
	// ==================================================================================
	//fclose(tmp_fptr);
} // End void Treat



void VietVoice::Concatenator::Stop ()
{	
	_wavePlayer.Stop ();
}
void VietVoice::Concatenator::Get_BS1 (long v_bs)
{	
	v_break_silence1 = v_bs;
}
void VietVoice::Concatenator::Get_BS2 (long v_bs)
{	
	v_break_silence2 = v_bs;
}
void VietVoice::Concatenator::Get_VL (long v_vl)
{	
	v_volume = v_vl;
}
void VietVoice::Concatenator::Get_MV (long v_mv)
{	
	v_main_volume = v_mv;
}
void VietVoice::Concatenator::Get_SV1 (long v_sv1)
{	
	v_soft_volume1 = v_sv1;
}
void VietVoice::Concatenator::Get_SV21 (long v_sv21)
{	
	v_soft_volume21 = v_sv21;
}
void VietVoice::Concatenator::Get_SV22 (long v_sv22)
{	
	v_soft_volume22 = v_sv22;
}
void VietVoice::Concatenator::Get_SV3 (long v_sv3)
{	
	v_soft_volume3 = v_sv3;
}
void VietVoice::Concatenator::Get_SP (long v_sp)
{	
	v_speed = v_sp;
}
void VietVoice::Concatenator::Get_Pitch (long v_ph)
{	
	v_pitch = v_ph;
}
void VietVoice::Concatenator::Get_light (std::wstring v_light)
{	
	v_lightWords = v_light;
}
void VietVoice::Concatenator::Get_CP (long v_cp)
{	
	v_compress = v_cp;
}
void VietVoice::Concatenator::Get_WS (long v_ws)
{	
	v_wordsize = v_ws;
}
void VietVoice::Concatenator::Get_WS2 (long v_ws)
{	
	v_wordsize2 = v_ws;
}
void VietVoice::Concatenator::Get_adjective (std::wstring v_adj)
{	
	v_adjectiveWords = v_adj;
}
long VietVoice::Concatenator::ByteToWord (long v_byte)
{  
	if (v_byte <= 32767) return v_byte;
	else return v_byte - 65535;
	return v_byte ;
}
void VietVoice::Concatenator::Change_Volume (vector<long> & m_word, int v_volume)
{	
	// 2018************************************	
	//return;
	if (v_volume == 100) return ;
	for (unsigned int i = 0; i < m_word.size(); i++) {
		m_word[i] = m_word[i] * v_volume /100; 
		//if (m_word[i]<(-32768) || m_word[i]>32767) 
		//	::MessageBox(NULL, StringHelper::ToString(m_word[i]).c_str(), L"Volume | Volume too high...", MB_OK); // VT
	}
}
void VietVoice::Concatenator::Change_Volume_CW (vector<long> & m_word, int v_volume, int v_percent, int v_type)
{	return ;
if (v_volume==100) return;
int start_w = 0 ;
int end_w = 0 ;
if (v_type==1) {
	start_w = 0 ;
	end_w = m_word.size() * v_percent / 100 - 0 ; // -1
} else if (v_type==2) {
	start_w = m_word.size() * v_percent / 100 - 0 ; // -1
	end_w = m_word.size() * (100 - v_percent) / 100 ;
} else if (v_type==3) {
	start_w = m_word.size() * (100 - v_percent) / 100 ;
	end_w = m_word.size() ;
}
for (int i = start_w; i < end_w; i++) 
m_word[i] = m_word[i] * (100 + v_volume) /100; 
}
void VietVoice::Concatenator::Change_Pitch (vector<long> & m_word, int v_pitch, int v_percent, int v_type)
{	
	// 2018*******************************
	return;

	if (v_pitch == 0) return; // v_pitch >=5
	vector<long> v_tmp ;
	unsigned long v_begin1 ; unsigned long v_end1 ;
	unsigned long v_begin2 ; unsigned long v_end2 ;
	unsigned long v_begin3 ; unsigned long v_end3 ;
	unsigned long v_begin4 ; unsigned long v_end4 ;
	if (v_type==1) {
		v_begin1 = 0; // v_begin
		v_begin2 = unsigned long (m_word.size() * v_percent / 100 * 0.6f) ;
		v_begin3 = unsigned long (m_word.size() * v_percent / 100 * 0.8f) ;
		v_begin4 = unsigned long (m_word.size() * v_percent / 100 * 0.9f) ;
		v_end1 = v_begin2 - 1 ;
		v_end2 = v_begin3 - 1 ;
		v_end3 = v_begin4 - 1 ;
		v_end4 = m_word.size() * v_percent / 100 ; // * (100 - v_percent) / 100 ; v_end
	} else if (v_type==2) {
		v_begin1 = m_word.size() * v_percent / 100 ;
		v_end4 = m_word.size() * (100 - v_percent) / 100 ;

		//: :MessageBox (NULL, StringHelper::ToString (v_begin1).c_str (), L"Begin", MB_OK) ;
		// ::MessageBox (NULL, StringHelper::ToString (v_end4).c_str (), L"End", MB_OK) ;

	} else if (v_type==3) {
		v_begin1 = unsigned long (m_word.size() * (100-v_percent) / 100) ;
		v_begin2 = unsigned long (m_word.size() * (100-v_percent) / 100 + (m_word.size() - m_word.size() * (100-v_percent) / 100) * 0.1f) ;
		v_begin3 = unsigned long (m_word.size() * (100-v_percent) / 100 + (m_word.size() - m_word.size() * (100-v_percent) / 100) * 0.2f) ;
		v_begin4 = unsigned long (m_word.size() * (100-v_percent) / 100 + (m_word.size() - m_word.size() * (100-v_percent) / 100) * 0.4f) ;
		v_end1 = v_begin2 - 1 ;
		v_end2 = v_begin3 - 1 ;
		v_end3 = v_begin4 - 1 ;
		v_end4 = m_word.size();
	}
	if (v_type == 2) {
		for (unsigned long i = v_begin1; i < v_end4; i++) 


			if (v_pitch>0) {
				if (i % int(200 / v_pitch) != 1) v_tmp.push_back(m_word[i]);
			}
			else {
				if (i % int(200 / abs(v_pitch)) != 1) v_tmp.push_back(m_word[i]);
				else {
					v_tmp.push_back(m_word[i]);
					v_tmp.push_back(m_word[i]);
				}
			}


			//if (i < v_begin1 || i > v_end4) {
			//	v_tmp.push_back(m_word[i]);
			//} else
			//	if (i % int(200 / v_pitch) != 1) v_tmp.push_back(m_word[i]);
			//	else {
			//		v_tmp.push_back(m_word[i]);
			//		v_tmp.push_back(m_word[i]);
			//	}

			m_word=v_tmp;
			v_tmp.clear();
			return ;
	}



	//::MessageBox(NULL, StringHelper::ToString(v_pitch).c_str(), StringHelper::ToString(v_type).c_str(), MB_OK);


	for (unsigned long i = 0; i < m_word.size(); i++) 
		if (i < v_begin1 || i > v_end4) {
			v_tmp.push_back(m_word[i]);
		} else
			if (v_pitch>0) {
				if (i >= v_begin1 && i <= v_end1) {
					if (i % int(200 / (v_pitch * 10 / 10))!=1) v_tmp.push_back(m_word[i]);
				}	else if (i >= v_begin2 && i <= v_end2) {
					if (i % int(200 / (v_pitch * 9 / 10))!=1) v_tmp.push_back(m_word[i]);
				} else if (i >= v_begin3 && i <= v_end3) {
					if (i % int(200 / (v_pitch * 6 / 10))!=1) v_tmp.push_back(m_word[i]);
				} else 
					if (i % int(200 / (v_pitch * 3.3333333399f / 10))!=1) v_tmp.push_back(m_word[i]);
			}
			else {
				if (i >= v_begin1 && i <= v_end1) {
					if (i % int(200 / (abs(v_pitch) * 3.3333333399f / 10))!=1) v_tmp.push_back(m_word[i]);
					else {
						v_tmp.push_back(m_word[i]);
						v_tmp.push_back(m_word[i]);
					}
				} else if (i >= v_begin2 && i <= v_end2) {
					if (i % int(200 / (abs(v_pitch) * 6 / 10))!=1) v_tmp.push_back(m_word[i]);
					else {
						v_tmp.push_back(m_word[i]);
						v_tmp.push_back(m_word[i]);
					}
				} else if (i >= v_begin3 && i <= v_end3) {
					if (i % int(200 / (abs(v_pitch) * 9 / 10))!=1) v_tmp.push_back(m_word[i]);
					else {
						v_tmp.push_back(m_word[i]);
						v_tmp.push_back(m_word[i]);
					}
				} else if (i >= v_begin4 && i <= v_end4) {
					if (i % int(200 / (abs(v_pitch) * 10 / 10))!=1) v_tmp.push_back(m_word[i]);
					else {
						v_tmp.push_back(m_word[i]);
						v_tmp.push_back(m_word[i]);
					}
				}
			}
			m_word=v_tmp;
			v_tmp.clear();
}
int VietVoice::Concatenator::v_isStopConsonant(std::wstring v_word, int v_accent) // VT
{
	if (v_word.size() == 1 || v_word == L"_silence_" || v_word == L"silence" || v_word == L"") return 0;
	if (v_word.substr(v_word.length()-1, 1) == L"_") v_word = v_word.substr(0, v_word.length()-1) ;

	if (v_word.length() >= 4) {
	}

	if (v_word.length() >= 3) {
	}

	if (v_word.length() >= 2) {
	}

	if (v_word.substr(v_word.size() - 2, 2) == L"ch") return 2;
	else if (v_word.substr(v_word.size() - 1, 1) == L"c") return 1;
	else if (v_word.substr(v_word.size() - 1, 1) == L"t") return 1;
	else if (v_word.substr(v_word.size() - 1, 1) == L"p") return 2;
	return 0;
}
bool VietVoice::Concatenator::v_isNoun(std::wstring v_word) 
{
	if (v_word == L"" || v_word == L"_silence_") return false ;

	return true ;
}
int VietVoice::Concatenator::v_isSameAsSC(std::wstring v_word, int v_accent)
{
	return 0;
	if (v_word.substr(0, 1) == L"b") 
		return 1 ; 


	if (v_word.size() == 1 || v_word == L"_silence_" || v_word == L"silence" || v_word == L"") return 0;
	if (v_word.substr(v_word.length()-1, 1) == L"_") v_word = v_word.substr(0, v_word.length()-1) ;

	if (v_word.length() >=4) {
		if (v_word.substr(v_word.length()-4, 4) == L"iêng" || v_word.substr(v_word.length()-4, 4) == L"iếng" || v_word.substr(v_word.length()-4, 4) == L"iềng" || v_word.substr(v_word.length()-4, 4) == L"iểng" || v_word.substr(v_word.length()-4, 4) == L"iễng" || v_word.substr(v_word.length()-4, 4) == L"iệng") 
			return 2 ; 
		else if (v_word.substr(v_word.length()-4, 4) == L"ương" || v_word.substr(v_word.length()-4, 4) == L"ướng" || v_word.substr(v_word.length()-4, 4) == L"ường" || v_word.substr(v_word.length()-4, 4) == L"ưởng" || v_word.substr(v_word.length()-4, 4) == L"ưỡng" || v_word.substr(v_word.length()-4, 4) == L"ượng") 
			return 2 ; 

		else if (v_word.substr(v_word.length()-4, 4) == L"uyên" || v_word.substr(v_word.length()-4, 4) == L"uyến" || v_word.substr(v_word.length()-4, 4) == L"uyền" || v_word.substr(v_word.length()-4, 4) == L"uyển" || v_word.substr(v_word.length()-4, 4) == L"uyễn" || v_word.substr(v_word.length()-4, 4) == L"uyện") 
			return 1 ; 


	}
	if (v_word.length() >=3) {
		if (v_word.substr(v_word.length()-3, 3) == L"iên" || v_word.substr(v_word.length()-3, 3) == L"iến" || v_word.substr(v_word.length()-3, 3) == L"iền" || v_word.substr(v_word.length()-3, 3) == L"iển" || v_word.substr(v_word.length()-3, 3) == L"iễn" || v_word.substr(v_word.length()-3, 3) == L"iện") 
			return 2 ; // Ha Noi
		else if (v_word.substr(v_word.length()-3, 3) == L"inh" || v_word.substr(v_word.length()-3, 3) == L"ính" || v_word.substr(v_word.length()-3, 3) == L"ình" || v_word.substr(v_word.length()-3, 3) == L"ỉnh" || v_word.substr(v_word.length()-3, 3) == L"ĩnh" || v_word.substr(v_word.length()-3, 3) == L"ịnh") 
			return 2 ; // Ha Noi
		else if (v_word.substr(v_word.length()-3, 3) == L"ong" || v_word.substr(v_word.length()-3, 3) == L"óng" || v_word.substr(v_word.length()-3, 3) == L"òng" || v_word.substr(v_word.length()-3, 3) == L"ỏng" || v_word.substr(v_word.length()-3, 3) == L"õng" || v_word.substr(v_word.length()-3, 3) == L"ọng") 
			return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"ông" || v_word.substr(v_word.length()-3, 3) == L"ống" || v_word.substr(v_word.length()-3, 3) == L"ồng" || v_word.substr(v_word.length()-3, 3) == L"ổng" || v_word.substr(v_word.length()-3, 3) == L"ỗng" || v_word.substr(v_word.length()-3, 3) == L"ộng") 
			return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"ung" || v_word.substr(v_word.length()-3, 3) == L"úng" || v_word.substr(v_word.length()-3, 3) == L"ùng" || v_word.substr(v_word.length()-3, 3) == L"ủng" || v_word.substr(v_word.length()-3, 3) == L"ũng" || v_word.substr(v_word.length()-3, 3) == L"ụng") 
			return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"ưng" || v_word.substr(v_word.length()-3, 3) == L"ứng" || v_word.substr(v_word.length()-3, 3) == L"ừng" || v_word.substr(v_word.length()-3, 3) == L"ửng" || v_word.substr(v_word.length()-3, 3) == L"ững" || v_word.substr(v_word.length()-3, 3) == L"ựng") 
			return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"ươi" || v_word.substr(v_word.length()-3, 3) == L"ưới" || v_word.substr(v_word.length()-3, 3) == L"ười" || v_word.substr(v_word.length()-3, 3) == L"ưởi" || v_word.substr(v_word.length()-3, 3) == L"ưỡi" || v_word.substr(v_word.length()-3, 3) == L"ượi") 
			return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"iêu" || v_word.substr(v_word.length()-3, 3) == L"iếu" || v_word.substr(v_word.length()-3, 3) == L"iều" || v_word.substr(v_word.length()-3, 3) == L"iểu" || v_word.substr(v_word.length()-3, 3) == L"iễu" || v_word.substr(v_word.length()-3, 3) == L"iệu") 
			return 2 ; // CT
		else if (v_word.substr(v_word.length()-3, 3) == L"yêu" || v_word.substr(v_word.length()-3, 3) == L"yếu" || v_word.substr(v_word.length()-3, 3) == L"yều" || v_word.substr(v_word.length()-3, 3) == L"yểu" || v_word.substr(v_word.length()-3, 3) == L"yễu" || v_word.substr(v_word.length()-3, 3) == L"yệu") 
			return 2 ; // CT

		else if (v_word.substr(v_word.length()-3, 3) == L"ăng" || v_word.substr(v_word.length()-3, 3) == L"ắng" || v_word.substr(v_word.length()-3, 3) == L"ằng" || v_word.substr(v_word.length()-3, 3) == L"ẳng" || v_word.substr(v_word.length()-3, 3) == L"ẵng" || v_word.substr(v_word.length()-3, 3) == L"ặng") 
			return 1 ; // CT
		else if (v_word.substr(v_word.length()-3, 3) == L"oai" || v_word.substr(v_word.length()-3, 3) == L"oái" || v_word.substr(v_word.length()-3, 3) == L"oài" || v_word.substr(v_word.length()-3, 3) == L"oải" || v_word.substr(v_word.length()-3, 3) == L"oãi" || v_word.substr(v_word.length()-3, 3) == L"oại") 
			return 1 ; // CT
		else if (v_word.substr(v_word.length()-3, 3) == L"oan" || v_word.substr(v_word.length()-3, 3) == L"oán" || v_word.substr(v_word.length()-3, 3) == L"oàn" || v_word.substr(v_word.length()-3, 3) == L"oản" || v_word.substr(v_word.length()-3, 3) == L"oãn" || v_word.substr(v_word.length()-3, 3) == L"oạn") 
			return 1 ; // CT



	}

	if (v_word.size() >= 2) {
		if (v_word.substr(v_word.size() - 2, 2) == L"ai" || v_word.substr(v_word.size() - 2, 2) == L"ái" || v_word.substr(v_word.size() - 2, 2) == L"ài" || v_word.substr(v_word.size() - 2, 2) == L"ải" || v_word.substr(v_word.size() - 2, 2) == L"ãi" || v_word.substr(v_word.size() - 2, 2) == L"ại")
			return 1;
		else if (v_word.substr(v_word.size() - 2, 2) == L"ao" || v_word.substr(v_word.size() - 2, 2) == L"áo" || v_word.substr(v_word.size() - 2, 2) == L"ào" || v_word.substr(v_word.size() - 2, 2) == L"ảo" || v_word.substr(v_word.size() - 2, 2) == L"ão" || v_word.substr(v_word.size() - 2, 2) == L"ạo")
			return 1;
		else if (v_word.substr(v_word.size() - 2, 2) == L"au" || v_word.substr(v_word.size() - 2, 2) == L"áu" || v_word.substr(v_word.size() - 2, 2) == L"àu" || v_word.substr(v_word.size() - 2, 2) == L"ảu" || v_word.substr(v_word.size() - 2, 2) == L"ãu" || v_word.substr(v_word.size() - 2, 2) == L"ạu")
			return 2;
		else if (v_word.substr(v_word.size() - 2, 2) == L"ay" || v_word.substr(v_word.size() - 2, 2) == L"áy" || v_word.substr(v_word.size() - 2, 2) == L"ày" || v_word.substr(v_word.size() - 2, 2) == L"ảy" || v_word.substr(v_word.size() - 2, 2) == L"ãy" || v_word.substr(v_word.size() - 2, 2) == L"ạy")
			return 1;
		else if (v_word.substr(v_word.length()-2, 2) == L"ăm" || v_word.substr(v_word.length()-2, 2) == L"ắm" || v_word.substr(v_word.length()-2, 2) == L"ằm" || v_word.substr(v_word.length()-2, 2) == L"ẳm" || v_word.substr(v_word.length()-2, 2) == L"ẵm" || v_word.substr(v_word.length()-2, 2) == L"ặm") 
			return 2 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ăn" || v_word.substr(v_word.length()-2, 2) == L"ắn" || v_word.substr(v_word.length()-2, 2) == L"ằn" || v_word.substr(v_word.length()-2, 2) == L"ẳn" || v_word.substr(v_word.length()-2, 2) == L"ẵn" || v_word.substr(v_word.length()-2, 2) == L"ặn") 
			return 2 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"âm" || v_word.substr(v_word.length()-2, 2) == L"ấm" || v_word.substr(v_word.length()-2, 2) == L"ầm" || v_word.substr(v_word.length()-2, 2) == L"ẩm" || v_word.substr(v_word.length()-2, 2) == L"ẫm" || v_word.substr(v_word.length()-2, 2) == L"ậm") 
			return 2 ;
		else if (v_word.substr(v_word.size() - 2, 2) == L"ân" || v_word.substr(v_word.size() - 2, 2) == L"ấn" || v_word.substr(v_word.size() - 2, 2) == L"ần" || v_word.substr(v_word.size() - 2, 2) == L"ẩn" || v_word.substr(v_word.size() - 2, 2) == L"ẫn" || v_word.substr(v_word.size() - 2, 2) == L"ận")
			return 2; // Ha Noi
		else if (v_word.substr(v_word.length()-2, 2) == L"âu" || v_word.substr(v_word.length()-2, 2) == L"ấu" || v_word.substr(v_word.length()-2, 2) == L"ầu" || v_word.substr(v_word.length()-2, 2) == L"ẩu" || v_word.substr(v_word.length()-2, 2) == L"ẫu" || v_word.substr(v_word.length()-2, 2) == L"ậu") 
			return 1 ; // Ha Noi 0
		else if (v_word.substr(v_word.size() - 2, 2) == L"ây" || v_word.substr(v_word.size() - 2, 2) == L"ấy" || v_word.substr(v_word.size() - 2, 2) == L"ầy" || v_word.substr(v_word.size() - 2, 2) == L"ẩy" || v_word.substr(v_word.size() - 2, 2) == L"ẫy" || v_word.substr(v_word.size() - 2, 2) == L"ậy")
			return 2;
		else if (v_word.substr(v_word.size() - 2, 2) == L"im" || v_word.substr(v_word.size() - 2, 2) == L"ím" || v_word.substr(v_word.size() - 2, 2) == L"ìm" || v_word.substr(v_word.size() - 2, 2) == L"ỉm" || v_word.substr(v_word.size() - 2, 2) == L"ĩm" || v_word.substr(v_word.size() - 2, 2) == L"ịm")
			return 2; // Ha Noi
		else if (v_word.substr(v_word.size() - 2, 2) == L"in" || v_word.substr(v_word.size() - 2, 2) == L"ín" || v_word.substr(v_word.size() - 2, 2) == L"ìn" || v_word.substr(v_word.size() - 2, 2) == L"ỉn" || v_word.substr(v_word.size() - 2, 2) == L"ĩn" || v_word.substr(v_word.size() - 2, 2) == L"ịn")
			return 2; // Ha Noi 0
		else if (v_word.substr(v_word.length()-2, 2) == L"ua" || v_word.substr(v_word.length()-2, 2) == L"úa" || v_word.substr(v_word.length()-2, 2) == L"ùa" || v_word.substr(v_word.length()-2, 2) == L"ủa" || v_word.substr(v_word.length()-2, 2) == L"ũa" || v_word.substr(v_word.length()-2, 2) == L"ụa") 
			return 1 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"um" || v_word.substr(v_word.length()-2, 2) == L"úm" || v_word.substr(v_word.length()-2, 2) == L"ùm" || v_word.substr(v_word.length()-2, 2) == L"ủm" || v_word.substr(v_word.length()-2, 2) == L"ũm" || v_word.substr(v_word.length()-2, 2) == L"ụm") 
			return 2 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"un" || v_word.substr(v_word.length()-2, 2) == L"ún" || v_word.substr(v_word.length()-2, 2) == L"ùn" || v_word.substr(v_word.length()-2, 2) == L"ủn" || v_word.substr(v_word.length()-2, 2) == L"ũn" || v_word.substr(v_word.length()-2, 2) == L"ụn") 
			return 2 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ưm" || v_word.substr(v_word.length()-2, 2) == L"ứm" || v_word.substr(v_word.length()-2, 2) == L"ừm" || v_word.substr(v_word.length()-2, 2) == L"ửm" || v_word.substr(v_word.length()-2, 2) == L"ữm" || v_word.substr(v_word.length()-2, 2) == L"ựm") 
			return 2 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ưn" || v_word.substr(v_word.length()-2, 2) == L"ứn" || v_word.substr(v_word.length()-2, 2) == L"ừn" || v_word.substr(v_word.length()-2, 2) == L"ửn" || v_word.substr(v_word.length()-2, 2) == L"ữn" || v_word.substr(v_word.length()-2, 2) == L"ựn") 
			return 2 ;


		else if (v_word.substr(v_word.length()-2, 2) == L"oa" || v_word.substr(v_word.length()-2, 2) == L"óa" || v_word.substr(v_word.length()-2, 2) == L"òa" || v_word.substr(v_word.length()-2, 2) == L"ỏa" || v_word.substr(v_word.length()-2, 2) == L"õa" || v_word.substr(v_word.length()-2, 2) == L"ọa") 
			return 1 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"on" || v_word.substr(v_word.length()-2, 2) == L"ón" || v_word.substr(v_word.length()-2, 2) == L"òn" || v_word.substr(v_word.length()-2, 2) == L"ỏn" || v_word.substr(v_word.length()-2, 2) == L"õn" || v_word.substr(v_word.length()-2, 2) == L"ọn") 
			return 1 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ơi" || v_word.substr(v_word.length()-2, 2) == L"ới" || v_word.substr(v_word.length()-2, 2) == L"ời" || v_word.substr(v_word.length()-2, 2) == L"ởi" || v_word.substr(v_word.length()-2, 2) == L"ỡi" || v_word.substr(v_word.length()-2, 2) == L"ợi") 
			return 1 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ưa" || v_word.substr(v_word.length()-2, 2) == L"ứa" || v_word.substr(v_word.length()-2, 2) == L"ừa" || v_word.substr(v_word.length()-2, 2) == L"ửa" || v_word.substr(v_word.length()-2, 2) == L"ữa" || v_word.substr(v_word.length()-2, 2) == L"ựa") 
			return 0 ;
		else if (v_word.substr(v_word.length()-2, 2) == L"ên" || v_word.substr(v_word.length()-2, 2) == L"ến" || v_word.substr(v_word.length()-2, 2) == L"ền" || v_word.substr(v_word.length()-2, 2) == L"ển" || v_word.substr(v_word.length()-2, 2) == L"ễn" || v_word.substr(v_word.length()-2, 2) == L"ện") 
			return 1 ;



	}

	if (v_word.size() >= 1) {
		if (v_word.substr(v_word.length()-1, 1) == L"i" || v_word.substr(v_word.length()-1, 1) == L"í" || v_word.substr(v_word.length()-1, 1) == L"ì" || v_word.substr(v_word.length()-1, 1) == L"ỉ" || v_word.substr(v_word.length()-1, 1) == L"ĩ" || v_word.substr(v_word.length()-1, 1) == L"ị") 
			return 2 ;
		else if (v_word.substr(v_word.length()-1, 1) == L"y" || v_word.substr(v_word.length()-1, 1) == L"ý" || v_word.substr(v_word.length()-1, 1) == L"ỳ" || v_word.substr(v_word.length()-1, 1) == L"ỷ" || v_word.substr(v_word.length()-1, 1) == L"ỹ" || v_word.substr(v_word.length()-1, 1) == L"ỵ") 
			return 2 ;

		else if (v_word.substr(v_word.length()-1, 1) == L"ê" || v_word.substr(v_word.length()-1, 1) == L"ế" || v_word.substr(v_word.length()-1, 1) == L"ề" || v_word.substr(v_word.length()-1, 1) == L"ể" || v_word.substr(v_word.length()-1, 1) == L"ễ" || v_word.substr(v_word.length()-1, 1) == L"ệ") 
			return 1 ;
		else if (v_word.substr(v_word.length()-1, 1) == L"o" || v_word.substr(v_word.length()-1, 1) == L"ó" || v_word.substr(v_word.length()-1, 1) == L"ò" || v_word.substr(v_word.length()-1, 1) == L"ỏ" || v_word.substr(v_word.length()-1, 1) == L"õ" || v_word.substr(v_word.length()-1, 1) == L"ọ") 
			return 1 ;
		else if (v_word.substr(v_word.length()-1, 1) == L"ô" || v_word.substr(v_word.length()-1, 1) == L"ố" || v_word.substr(v_word.length()-1, 1) == L"ồ" || v_word.substr(v_word.length()-1, 1) == L"ổ" || v_word.substr(v_word.length()-1, 1) == L"ỗ" || v_word.substr(v_word.length()-1, 1) == L"ộ") 
			return 1 ;
		else if (v_word.substr(v_word.length()-1, 1) == L"ư" || v_word.substr(v_word.length()-1, 1) == L"ứ" || v_word.substr(v_word.length()-1, 1) == L"ừ" || v_word.substr(v_word.length()-1, 1) == L"ử" || v_word.substr(v_word.length()-1, 1) == L"ữ" || v_word.substr(v_word.length()-1, 1) == L"ự") 
			return 1 ;

	}

	if (v_word.substr(v_word.size() - 2, 2) == L"nh") return 1; // anh/ inh
	//	else if (v_word.substr(v_word.size() - 1, 1) == L"c") return 1;
	//	else if (v_word.substr(v_word.size() - 1, 1) == L"t") return 1;
	//	else if (v_word.substr(v_word.size() - 1, 1) == L"p") return 2;

	//::MessageBox(NULL, StringHelper::ToString(v_word).c_str(), L"ung-error", MB_OK);			


	return 0 ;
}
void VietVoice::Concatenator::ENVELOP_CAL(float *data, long datalen, float **fdata, int nsamp)
{
	int fdatalen;
	int i, j, start;
	float avg;
	fdatalen = datalen / nsamp;
	(*fdata) = (float *) malloc(fdatalen * sizeof(float));
	memset((*fdata), 0, fdatalen * sizeof(float));

	start = 0;
	for (i = 0; i < fdatalen; i++)
	{
		avg = 0;
		for (j = 0; j < nsamp; j++) avg += data[j + start];
		*((*fdata) + i) = avg / nsamp;
		start += nsamp;
	}
	//// free (*fdata); // doc nhao di
}
long VietVoice::Concatenator::GET_SIMILE_POS_FAST(float * fx1, float * x1, long n1, float * fx2, float * x2, long n2, int nsamp) //n1,n2²Î¿¼Óë²âÊÔÐÅºÅµÄVAD¸öÊý
{
	// float FLT_MAX = 3.402823466e+38f;
	long reflen = n1;
	long corlen = n2;
	long cordelta = n2 - n1;

	long freflen = reflen / nsamp;
	long fcorlen = corlen / nsamp;
	long fcordelta = cordelta / nsamp;

	float cor;
	float cormax = -FLT_MAX;
	int i, j, bestshift, fbestshift, shift_low, shift_high;

	fbestshift = 0; // VT ///////////////////////////////////////////////////////////////// Xoa 15-2-2013

	for (i = 0; i < fcordelta; i++)
	{
		cor = 0.0f;
		for (j = 0; j < freflen; j++) 
		{
			cor += fx1[j] * fx2[j+i];	
		}

		if (cor > cormax)
		{
			cormax = cor;
			fbestshift = i;
		}
	}

	cormax = -FLT_MAX;
	bestshift = fbestshift * nsamp;
	shift_low = bestshift - nsamp;
	shift_high = bestshift + nsamp;
	if (shift_low < 0) shift_low = 0;
	if (shift_high > cordelta) shift_high = cordelta;

	for (i = shift_low; i < shift_high; i++)
	{
		cor = 0.0;
		for ( j = 0; j < reflen; j++)	
		{
			cor += x1[j] * x2[j+i];
		}
		if (cor > cormax)
		{
			cormax = cor;
			bestshift = i;
		}
	}
	return bestshift;
}

void VietVoice::Concatenator::GET_FINALE_DATA(WAV_INFO *in_info,WAV_INFO *out_info,float beta,int nsamp)
{

	int  i;
	long nref, ncross, DelayEst = 0, out_pos = 0, outcetr = 0;
	long Win_offset = (long) (0.5f + halfwin_sec * (in_info->Fs) / 1000.0f);
	long Win_LENGTH = Win_offset * 2 + 1;
	int  deta = (int) (0.5f + deta_sec * (in_info->Fs) / 1000.0f);
	int  cross_low = -Win_offset - deta, cross_high = 0, k = 0;
	int  precross_low = 0;
	int  lst_low = 0, lst_high = 0;
	int  bs_low = 0, bs_high = 0;
	float *cross_buf, *fcross_buf;
	float *bs_buf, *fbs_buf;
	float *win; 
	float *hann_buf;
	float *fdata;

	out_info->Nchan = in_info->Nchan;

	hann_buf = (float *) malloc (Win_LENGTH * sizeof(float));
	memset(hann_buf, 0, Win_LENGTH * sizeof(float));

	//	if(beta != 1.0f)
	ENVELOP_CAL(in_info->data, in_info->Nsamples, &fdata, nsamp);

	nref = Win_LENGTH;
	ncross = Win_LENGTH + 2 * deta;

	for(i = -Win_offset; i <= Win_offset; i++)
	{
		hann_buf[i + Win_offset] = 0.5f*(1.0f - (float)cos(2.0f * PI * (i + Win_offset) / (Win_LENGTH - 1)));
	}
	win = hann_buf + Win_offset;

	for(k = 0, i = 0; i <= Win_offset; i++)
	{
		out_info->data[i] = (in_info->data[i]) * (win[i]);
		k++;
	}
	outcetr += Win_offset;
	out_pos += k;

	bs_low = 0;
	bs_high = bs_low + Win_LENGTH;

	while(cross_low + (int) (0.5 + beta * Win_offset) < in_info->Nsamples)
	{
		if(bs_low > in_info->Nsamples)
		{
			break;
		}
		if(bs_high > in_info->Nsamples)
		{
			bs_high = in_info->Nsamples;
		}

		bs_buf = in_info->data + bs_low;

		//printf("in_info->data : %f\n",*in_info->data);

		fbs_buf = fdata + bs_low / nsamp;

		cross_low += (int) (0.5 + beta * Win_offset);
		cross_high = cross_low + Win_LENGTH + 2 * deta;
		precross_low = cross_low;

		if(cross_low > in_info->Nsamples)
		{
			break;
		}

		if(cross_low < 0)
		{
			while(1)
			{
				cross_low ++;
				if(cross_low >= cross_high || cross_low >= 0)
					break;
			}
		}

		if(cross_low >= 0 && cross_low < cross_high)
		{

			if (cross_high > in_info->Nsamples)
			{
				cross_high = in_info->Nsamples;
			}

			if(cross_low <= bs_low && cross_high >= bs_high)
			{
				DelayEst = bs_low - cross_low;
			}
			else
			{
				cross_buf = in_info->data + cross_low;
				fcross_buf = fdata + cross_low / nsamp;
				k = cross_high - cross_low;
				ncross = cross_high - cross_low;
				DelayEst = GET_SIMILE_POS_FAST(fbs_buf, bs_buf, nref, fcross_buf, cross_buf, ncross, nsamp);
			}

			lst_low = cross_low + DelayEst;
			lst_high = lst_low + Win_LENGTH;
			if(precross_low < 0)
			{
				cross_low = precross_low;
			}
			if (lst_low > in_info->Nsamples)
			{
				break;
			}
			if(lst_high > in_info->Nsamples)
			{
				lst_high = in_info->Nsamples;
			}

			for(k = - Win_offset, i = lst_low; i < lst_high; i++)
			{
				out_info->data[outcetr + k] += (in_info->data[i]) * win[k];
				k++;
			}

			if (k >= 0)
			{
				out_pos += k;
				outcetr += Win_offset;
			}


			out_info->Nsamples = out_pos;
			out_info->DataLen = out_pos << 1;
			out_info->Rifflen = out_info->DataLen + 36;

			if (in_info->Nchan == 2)
			{
				out_info->DataLen <<= 1;
				out_info->Rifflen = out_info->DataLen + 36;
			}
		}

		bs_low = lst_low + Win_offset;
		bs_high = bs_low + Win_LENGTH;
	}

	//printf("in_info Nsamples: %d\n",in_info->Nsamples);
	//printf("out_info Nsamples: %d\n",out_info->Nsamples);
	// getch();

	//free (cross_buf);
	//free (fcross_buf);
	//free (bs_buf);
	//free (fbs_buf);
	//free (win);
	//free (hann_buf); // free(hann_buf);
	//free (fdata);








	//free(fdata); // hay hang up // khong can thiet
}
int VietVoice::Concatenator::v_isShortStartWord(std::wstring v_word) // VT
{
	if (v_word.length()>=2) {
		if (v_word.substr(0, 2) == L"kh") return 1 ; 
		else if (v_word.substr(0, 2) == L"gi") return 2 ; 
		else if (v_word.substr(0, 2) == L"ph") return 1 ; 
		else if (v_word.substr(0, 2) == L"tr") return 2 ; 
	}
	if (v_word.substr(0, 1) == L"b") return 2 ;
	else if (v_word.substr(0, 1) == L"c") return 3 ; 
	else if (v_word.substr(0, 1) == L"đ") return 3 ; 
	else if (v_word.substr(0, 1) == L"g") return 3 ; 
	else if (v_word.substr(0, 1) == L"h") return 1 ; // 1
	else if (v_word.substr(0, 1) == L"k") return 1 ; 
	else if (v_word.substr(0, 1) == L"l") return 2 ; 
	else if (v_word.substr(0, 1) == L"m") return 0 ; 
	else if (v_word.substr(0, 1) == L"n") return 2 ; 
	else if (v_word.substr(0, 1) == L"p") return 2 ; 
	else if (v_word.substr(0, 1) == L"s") return 3 ; 
	else if (v_word.substr(0, 1) == L"t") return 3 ; 
	else if (v_word.substr(0, 1) == L"x") return 3 ; 
	return 0; 
}
void VietVoice::Concatenator::v_getCW1 (long v_cw1)
{	
	v_compound1 = v_cw1;
}
void VietVoice::Concatenator::v_getCW2 (long v_cw2)
{	
	v_compound2 = v_cw2;
}
void VietVoice::Concatenator::v_getStopSilence (long v_ss)
{	
	v_StopSilence = v_ss;
}
int VietVoice::Concatenator::v_isSilenceBefore(std::wstring v_word) // VT
{	
	//	a	trấn ải	trấn an		ơ	Việt Nam ơi		1 c		ch	k	kh	1 qu	g	ô	1 t	th	nhẹ		tr	nhẹ		v
	if (v_word == L"" || v_word == L"_silence_") return 0 ;
	if (v_word.length()>=2) {
		if (v_word.substr(0, 2) == L"ch") return 1 ; // HN4
		else if (v_word.substr(0, 2) == L"kh") return 1 ; // HN4
		else if (v_word.substr(0, 2) == L"qu") return 2 ; // HN4
		else if (v_word.substr(0, 2) == L"th") return 1 ; // HN4
		else if (v_word.substr(0, 2) == L"tr") return 1 ; // HN4
	}
	if (v_word.substr(0, 1) == L"a") return 1 ;
	else if (v_word.substr(0, 1) == L"ă") return 1 ;
	else if (v_word.substr(0, 1) == L"â") return 1 ;
	else if (v_word.substr(0, 1) == L"e") return 1 ;
	else if (v_word.substr(0, 1) == L"ê") return 1 ;
	else if (v_word.substr(0, 1) == L"i") return 1 ;
	else if (v_word.substr(0, 1) == L"o") return 1 ;
	else if (v_word.substr(0, 1) == L"ô") return 1 ;
	else if (v_word.substr(0, 1) == L"ơ") return 1 ;
	else if (v_word.substr(0, 1) == L"u") return 1 ;
	else if (v_word.substr(0, 1) == L"ư") return 1 ;
	else if (v_word.substr(0, 1) == L"y") return 1 ;

	else if (v_word.substr(0, 1) == L"c") return 2 ;
	else if (v_word.substr(0, 1) == L"g") return 1 ;
	else if (v_word.substr(0, 1) == L"k") return 1 ;
	else if (v_word.substr(0, 1) == L"t") return 2 ;
	return 0; 
}


int VietVoice::Concatenator::KiemTraAmVang(std::wstring v_word,u_int type) 
{
	//::MessageBox(NULL, StringHelper::ToString(v_word).c_str(), L"Before Speaking file", MB_OK); // VT
	std::transform(v_word.begin(), v_word.end(), v_word.begin(), ::tolower);
	int SoByte0=0;
	
	return SoByte0;
}


int VietVoice::Concatenator::v_isSonantVowel(std::wstring v_word) // VT
{
	if (v_word == L"" || v_word == L"_silence_") return 0 ;
	if (v_word.substr(v_word.length()-1, 1) == L"_") v_word = v_word.substr(0, v_word.length()-1) ;
	if (v_word.length() >= 3) {
		if (v_word.substr(v_word.length()-3, 3) == L"oai") return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"oay") return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"uôi") return 2 ;
		else if (v_word.substr(v_word.length()-3, 3) == L"uây") return 2 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"ươi") return 2 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"uya") return 2 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"oeo") return 0 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"oao") return 0 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"ươu") return 0 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"iêu") return 0 ; 
		else if (v_word.substr(v_word.length()-3, 3) == L"yêu") return 0 ; 
	}
	if (v_word.length() >= 2) {
		if (v_word.substr(v_word.length()-2, 2) == L"ai") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ay") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ơi") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ây") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ưa") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ưi") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ia") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ya") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"oi") return 2 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"oe") return 1 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"oa") return 1 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ôi") return 1 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"âu") return 2 ; 

		else if (v_word.substr(v_word.length()-2, 2) == L"ao") return 0 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"eo") return 0 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"au") return 0 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"êu") return 0 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"ưu") return 1 ; 
		else if (v_word.substr(v_word.length()-2, 2) == L"iu") return 0 ; 
	}
	if (v_word.substr(v_word.length()-1, 1) == L"a") return 2 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"e") return 2 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"ơ") return 2 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"ê") return 2 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"i") return 2 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"ư") return 1 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"o") return 1 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"u") return 1 ; 
	else if (v_word.substr(v_word.length()-1, 1) == L"ô") return 0 ; 
	return 0; 
}
int VietVoice::Concatenator::v_isBegunWithSonantVowel(std::wstring v_word) // VT
{
	if (v_word == L"" || v_word == L"_silence_") return 0 ;
	if (v_word.substr(v_word.length()-1, 1) == L"_") v_word = v_word.substr(0, v_word.length()-1) ;
	if (v_word.length() >= 3) {
		if (v_word.substr(0, 3) == L"oai") return 2 ;
		else if (v_word.substr(0, 3) == L"oay") return 2 ;
		else if (v_word.substr(0, 3) == L"uôi") return 2 ;
		else if (v_word.substr(0, 3) == L"uây") return 2 ; 
		else if (v_word.substr(0, 3) == L"ươi") return 2 ; 
		else if (v_word.substr(0, 3) == L"uya") return 2 ; 
		else if (v_word.substr(0, 3) == L"oeo") return 0 ; 
		else if (v_word.substr(0, 3) == L"oao") return 0 ; 
		else if (v_word.substr(0, 3) == L"ươu") return 0 ; 
		else if (v_word.substr(0, 3) == L"iêu") return 0 ; 
		else if (v_word.substr(0, 3) == L"yêu") return 0 ; 
	}
	if (v_word.length() >= 2) {
		if (v_word.substr(0, 2) == L"ai") return 2 ; 
		else if (v_word.substr(0, 2) == L"ay") return 2 ; 
		else if (v_word.substr(0, 2) == L"ơi") return 2 ; 
		else if (v_word.substr(0, 2) == L"ây") return 2 ; 
		else if (v_word.substr(0, 2) == L"ưa") return 2 ; 
		else if (v_word.substr(0, 2) == L"ưi") return 2 ; 
		else if (v_word.substr(0, 2) == L"ia") return 2 ; 
		else if (v_word.substr(0, 2) == L"ya") return 2 ; 
		else if (v_word.substr(0, 2) == L"oi") return 2 ; 
		else if (v_word.substr(0, 2) == L"oe") return 1 ; 
		else if (v_word.substr(0, 2) == L"oa") return 1 ; 
		else if (v_word.substr(0, 2) == L"ôi") return 1 ; 
		else if (v_word.substr(0, 2) == L"âu") return 2 ; 

		else if (v_word.substr(0, 2) == L"ao") return 0 ; 
		else if (v_word.substr(0, 2) == L"eo") return 0 ; 
		else if (v_word.substr(0, 2) == L"au") return 0 ; 
		else if (v_word.substr(0, 2) == L"êu") return 0 ; 
		else if (v_word.substr(0, 2) == L"ưu") return 1 ; 
		else if (v_word.substr(0, 2) == L"iu") return 0 ; 
	}
	if (v_word.substr(0, 1) == L"a") return 2 ; 
	else if (v_word.substr(0, 1) == L"e") return 2 ; 
	else if (v_word.substr(0, 1) == L"ơ") return 2 ; 
	else if (v_word.substr(0, 1) == L"ê") return 2 ; 
	else if (v_word.substr(0, 1) == L"i") return 2 ; 
	else if (v_word.substr(0, 1) == L"ư") return 1 ; 
	else if (v_word.substr(0, 1) == L"o") return 1 ; 
	else if (v_word.substr(0, 1) == L"u") return 1 ; 
	else if (v_word.substr(0, 1) == L"ô") return 0 ; 
	return 0; 
}
int VietVoice::Concatenator::v_isSensitiveConsonant(std::wstring v_word) // VT
{	
	if (v_word == L"" || v_word == L"_silence_") return 0 ;
	if (v_word.length()>=2) {
		if (v_word.substr(0, 2) == L"kh") return 0 ; // Ha Noi 1
		if (v_word.substr(0, 2) == L"tr") return 1 ; //
		if (v_word.substr(0, 2) == L"ch") return 1 ; //
		if (v_word.substr(0, 2) == L"qu") return 1 ; //
	}
	if (v_word.substr(0, 1) == L"b") return 0 ; // 1
	else if (v_word.substr(0, 1) == L"c") return 1 ; //
	else if (v_word.substr(0, 1) == L"d") return 1 ; //
	//else if (v_word.substr(0, 1) == L"đ") return 2 ; //
	else if (v_word.substr(0, 1) == L"k") return 1 ; //
	else if (v_word.substr(0, 1) == L"p") return 2 ; 
	else if (v_word.substr(0, 1) == L"t") return 1 ; // Ha Noi lay lai
	else if (v_word.substr(0, 1) == L"n") return 0 ; //
	else if (v_word.substr(0, 1) == L"y") return 0 ; //
	else if (v_word.substr(0, 1) == L"i") return 0 ; //
	else if (v_word.substr(0, 1) == L"s") return 0 ; // Ha Noi
	return 0; 
}
void VietVoice::Concatenator::v_echo(vector<long> & m_word, unsigned long v_dots, int mType, int mPer) // VT
{	
	if (v_dots == 0) return ;
	::std::vector <long> mword = m_word;
	if (mType==1) {
		int msum = 0;
		for (unsigned int i = 0; i < m_word.size(); i++) {
			if (i >= v_dots && i <= m_word.size() - v_dots + 0) { // + 1
				msum=0 ;
				for (unsigned int j = i - v_dots; j < i + v_dots ; j++) {
					if (j != i) {
						if (m_word[j] >= 0) 
							msum += m_word[j] ; 
						else
							msum += m_word[j] - 0 ; 
					}
				}
				msum /= v_dots * 2 + 0; // + 1
				mword[i] = msum ;
			}
		}
		m_word = mword ;
		mword.clear();
	}

	else if (mType==2) {
		if (mPer == 0) {
			float v_hs = (float) ((-32767) / (v_dots + 1) * (v_dots + 1)) / (-32767);
			for (unsigned int i = 0; i < m_word.size(); i++) {
				m_word[i] = long (::floorl(mword[i] / (v_dots + 1)) * (v_dots +1) / v_hs) ; 
			}
		} else {
			float v_hs = (float) ((-32767) / (v_dots + 1) * (v_dots + 1)) / (-32767);
			for (unsigned int i = m_word.size() * mPer / 100; i < m_word.size(); i++) {
				m_word[i] = long (::floorl(mword[i] / (v_dots + 1)) * (v_dots +1) / v_hs) ; 
			}
		}
		mword.clear();
	}
}

void VietVoice::Concatenator::Get_Dots (long v_dots)
{	
	vdots = v_dots;
}
void VietVoice::Concatenator::Get_Not_Echo (long v_notEcho)
{	
	vnotEcho = v_notEcho;
}
void VietVoice::Concatenator::v_FadeIn (vector<long> & m_word, long v_Fpercent)
{	//return ;
// 2018**************************************
if (v_Fpercent==0) return ;
int v_point = m_word.size() * v_Fpercent / 100 ;
//int v_max = 0 ;
//for (int i = v_point - 600; i < v_point; i++)
//if (m_word[i] <= 32767 && v_max < m_word[i]) v_max = m_word[i];
for (int i = 0; i < v_point; i++)
////		if (m_word[i] <= 32767)
// m_word[i] = m_word[i] * sqrt((double) i) / sqrt((double) v_point) ; // para
m_word[i] = long (m_word[i] * sqrt(1 - ((v_point - (double) i) / v_point) * ((v_point - (double) i)/ v_point)))   ; // para
//			mWord_2(I) = mWord_0(I) * Sqrt(1 - ((mEnd - I - 1) / mEnd) ^ 2) ' Ellipse // VB
////		else
// m_word[i] = (m_word[i] -65535) * sqrt((double) i) / sqrt((double) v_point) + 65535; // para
////			m_word[i] = (m_word[i] -65535) * sqrt(1 - ((v_point - (double) i) / v_point) * ((v_point - (double) i)/ v_point)) + 65535; // para
//			mWord_2(I) = (mWord_0(I) - 65535) * Sqrt(1 - ((mEnd - I - 1) / mEnd) ^ 2) + 65535 ' Ellipse // VB
}
void VietVoice::Concatenator::v_FadeOut (vector<long> & m_word, long v_Fpercent)
{	//return ;
// 2018*************************************************
if (v_Fpercent==0) return;
int v_point = m_word.size() * (100 - v_Fpercent) / 100 ;
//int v_max = 0 ;

//for (int i = v_point + 600; i < v_point; i++)
//if (m_word[i] <= 32767 && v_max < m_word[i]) v_max = m_word[i];
for (unsigned long i = v_point; i < m_word.size(); i++)
////		if (m_word[i] <= 32767)
// m_word[i] = m_word[i] * sqrt(m_word.size() - (double) i) / sqrt(m_word.size() - (double) v_point) ; // para
m_word[i] = long (m_word[i] * sqrt(1 - (((double) i - v_point) / (m_word.size() - (double) v_point)) * (((double) i - v_point) / (m_word.size() - (double) v_point)))) ; // ellipse
//			mWord_2(I) = mWord_0(I) * Sqrt(1 - ((I - mBegin) / (TS_Byte / 2 - mBegin)) ^ 2) ' Ellipse // VB
////		else
// m_word[i] = (m_word[i] -65535) * sqrt(m_word.size() - (double) i) / sqrt(m_word.size() - (double) v_point) + 65535; // para
////			m_word[i] = (m_word[i] -65535) * sqrt(1 - (((double) i - v_point) / (m_word.size() - (double) v_point)) * (((double) i - v_point) / (m_word.size() - (double) v_point))) + 65535; // ellipse
//			mWord_2(I) = ((mWord_0(I) - 65535) * Sqrt(1 - ((I - mBegin) / (TS_Byte / 2 - mBegin)) ^ 2)) + 65535 ' Ellipse // VB
}
void VietVoice::Concatenator::Get_Fade (long v_fade)
{	
	m_fade = v_fade;
}
void VietVoice::Concatenator::Get_Sample (long v_samples)
{	
	m_samples = v_samples;
}
void VietVoice::Concatenator::Get_Per_Pitch (int v_p_p)
{	
	v_per_pitch = v_p_p;
}
void VietVoice::Concatenator::Get_Pitch0 (int v_p0)
{	
	v_pitch0 = v_p0;
}
void VietVoice::Concatenator::Get_Pitch1 (int v_p1)
{	
	v_pitch1 = v_p1;
}
void VietVoice::Concatenator::Get_Pitch2 (int v_p2)
{	
	v_pitch2 = v_p2;
}
void VietVoice::Concatenator::Get_Pitch3 (int v_p3)
{	
	v_pitch3 = v_p3;
}
void VietVoice::Concatenator::Get_Pitch4 (int v_p4)
{	
	v_pitch4 = v_p4;
}
void VietVoice::Concatenator::Get_Pitch5 (int v_p5)
{	
	v_pitch5 = v_p5;
}
void VietVoice::Concatenator::Get_Com0 (int v_c0)
{	
	v_compress0 = v_c0;
}
void VietVoice::Concatenator::Get_Com1 (int v_c1)
{	
	v_compress1 = v_c1;
}
void VietVoice::Concatenator::Get_Com2 (int v_c2)
{	
	v_compress2 = v_c2;
}
void VietVoice::Concatenator::Get_Com3 (int v_c3)
{	
	v_compress3 = v_c3;
}
void VietVoice::Concatenator::Get_Com4 (int v_c4)
{	
	v_compress4 = v_c4;
}
void VietVoice::Concatenator::Get_Com5 (int v_c5)
{	
	v_compress5 = v_c5;
}


void VietVoice::Concatenator::CanBangAmPho (vector<long> &m_word)
{	
	long max=0, min=0;
	for(int i=0;i<m_word.size();i++)
	{
		if(m_word[i]>max)
		{
			max=m_word[i];
		}
		if(m_word[i]<min)
		{
			min=m_word[i];
		}
	}
	long avg=(max+min)/2;
	for(int i=0;i<m_word.size();i++)
	{
		m_word[i]=m_word[i]-avg;
	}
}



void VietVoice::Concatenator::CanBangAmPho2 (vector<long> &m_word)
{	
	long max=0, min=0;
	for(int i=0;i<m_word.size();i++)
	{
		if(m_word[i]>max)
		{
			max=m_word[i];
		}
		if(m_word[i]<min)
		{
			min=m_word[i];
		}
	}
	
	if(abs(max)>abs(min))
	{
		for(int i=0;i<m_word.size();i++)
		{
			if(m_word[i]<0)
			{
				m_word[i]=m_word[i]*abs(max)/abs(min);
			}
		}
	}
	else
	{
		if(abs(max)<abs(min))
		{
			for(int i=0;i<m_word.size();i++)
			{
				if(m_word[i]>0)
				{
					m_word[i]=m_word[i]*abs(min)/abs(max);
				}
			}
		}
	}
}

void VietVoice::Concatenator::CanDoiAmLuongGhepTu (vector<long> &m_word, int SoSamplePU1,float percentVolumePU1,float percentVolumePU2)
{
	int DiemMaxPU1=0,DiemMaxPU2=0;
	long AmLuongPhanTren=0;
	long AmLuongPhanDuoi=0;
	for(int i=0;i<SoSamplePU1;i++)
	{
		m_word[i]=m_word[i]*percentVolumePU1/100;
	}
	
	for(int i=SoSamplePU1+1;i<m_word.size();i++)
	{
		m_word[i]=m_word[i]*percentVolumePU2/100;
	}
}



void VietVoice::Concatenator::CanBangAmPho3 (vector<long> &m_word, int VTBD, int VTKT)
{	
	long max=0, min=0;
	int VTMin=0,VTMax=0;
	for(int i=VTBD;i<=VTKT;i++)
	{
		if(m_word[i]>max)
		{
			max=m_word[i];
			VTMax=i;
		}
		if(m_word[i]<min)
		{
			min=m_word[i];
			VTMin=i;
		}
	}
	int BD=-1,KT=-1;

	if(VTMin<VTMax)
	{
		BD=VTMin;
		KT=VTMax;
	}
	else
	{
		BD=VTMax;
		KT=VTMin;
	}

	if(min<0 && max>0)
	{
		if(abs(max)>abs(min))
		{
			for(int i=BD;i<=KT;i++)
			{
				if(m_word[i]<0)
				{
					m_word[i]=m_word[i]*abs(max)/abs(min);
				}
			}
		}
		else
		{
			if(abs(max)<abs(min))
			{
				for(int i=BD;i<=KT;i++)
				{
					if(m_word[i]>0)
					{
						m_word[i]=m_word[i]*abs(min)/abs(max);
					}
				}
			}
		}
	}

	if(BD>0 && VTBD<BD-1)
	{
		CanBangAmPho3(m_word,VTBD,BD-1);
	}

	if(KT>0 && VTKT>KT+1)
	{
		CanBangAmPho3(m_word,KT+1,VTKT);
	}
}





long VietVoice::Concatenator::Get_Volume_Average (vector<long> &m_word)
{	
	vector<long> mang;
	long Tong=0;
	long SoLuong=0;
	for(int i=0;i<m_word.size();i++)
	{
		if(i==0)
		{
			mang.push_back(0);
		}
		else
		{
			if(m_word[i]>m_word[i-1])
			{
				mang.push_back(1);
			}
			else
			{
				if(m_word[i]<m_word[i-1])
				{
					mang.push_back(-1);
				}
				else
				{
					mang.push_back(mang[i-1]);
				}
			}
		}
	}

	for(int i=1;i<m_word.size()-1;i++)
	{
		if((mang[i]==1 && mang[i+1]==-1 && m_word[i]>0) || (mang[i]==-1 && mang[i+1]==1 && m_word[i]<0) )
		{
			Tong=Tong+m_word[i];
			SoLuong=SoLuong+1;
		}
	}

	return Tong/SoLuong;
}

long VietVoice::Concatenator::Get_Volume (vector<long> &m_word, int v_percent, int v_type)
{	
	// if (v_volume==100) return 0;
	int start_w = 0 ;
	int end_w = 0 ;
	int v_max = 0 ;
	float a, b ;

	if (v_type == 1 || v_type == 5) {
		start_w = 0 ;
		end_w = m_word.size() * v_percent / 100 - 0 ; // -1
	} else if (v_type==2 || v_type == 4) {
		start_w = m_word.size() * v_percent / 100 - 0 ; // -1
		end_w = m_word.size() * (100 - v_percent) / 100 ;
	} else if (v_type == 3 || v_type == 6) {
		start_w = m_word.size() * (100 - v_percent) / 100 ;
		end_w = m_word.size() ;
	}
	if (v_type == 1) {
		v_max = 0 ;
		for (int i = start_w; i < end_w; i++) 
			if (v_max < m_word[i]) v_max = m_word[i] ; 
		///::MessageBox (NULL, StringHelper::ToString (v_max).c_str (), L"Begin", MB_OK) ;
		float r_end = 1.0f ;
		float r_begin = 2.4f ; // v_max 4000 - 8000
		a = float (r_end - r_begin) / (end_w - start_w) ;
		b = float (r_begin) - start_w * float (a) ;
		for (int i = start_w; i < end_w; i++) {
			m_word[i] *= long (float (a) * i + b) ; 
		}
	} else if (v_type == 3) {
		v_max = 0 ;
		for (int i = start_w; i < end_w; i++) 
			if (v_max < m_word[i]) v_max = m_word[i] ; 
		float r_end = 2.3f ;
		float r_begin = 1.0f ;
		a = float (r_end - r_begin) / (end_w - start_w) ;
		b = float (r_begin) - start_w * float (a) ;
		for (int i = start_w; i < end_w; i++) {
			m_word[i] *= long (float (a) * i + b) ; 
		}
	} else if (v_type == 4) {
		for (int i = start_w; i < end_w; i++) 
			if (m_word[i] < 0) v_max -= m_word[i] ;
			else v_max += m_word[i] ;
			v_max /= (end_w - start_w) ;
	} else if (v_type == 5 || v_type == 6) {
		v_max = 0 ;
		for (int i = start_w; i < end_w; i++) 
			if (v_max < abs(m_word[i])) v_max = abs(m_word[i]) ;
	}
	return v_max ;
}
unsigned long VietVoice::Concatenator::Word_Cut (vector<long> &m_word, int v_percent, int v_type)
{
	if (v_percent == 0) return 0;
	std::vector<long> v_word ;
	int v_first ; int v_last ;
	if (v_type == 1) {
		v_first = m_word.size() * (v_percent) / 100 ;
		v_last = m_word.size() ;
		while (m_word[v_first] > 500 || m_word[v_first] < -500) v_first++ ;
	} else { // 3
		v_first = 0 ;
		v_last = m_word.size() * (100 - v_percent) / 100 ;
		while (m_word[v_last] > 500 || m_word[v_last] < -500) v_last-- ;
	}

	for (int i = v_first; i < v_last; i++) v_word.push_back(m_word[i]);
	m_word = v_word ;
	v_word.clear() ;
	return 1;
}


unsigned long VietVoice::Concatenator::Word_Cut_Thuan(vector<long> &m_word, int v_percent)
{
	if (v_percent == 0) return 0;
	int word_size=m_word.size();
	int cut_size=(int)word_size*v_percent/100;
	int Pos_Cut=(int)word_size/cut_size;

	std::vector<long> v_word ;

	for (int i = 0; i < word_size; i++)
	{
		if(i%Pos_Cut!=0)
			v_word.push_back(m_word[i]);
	}
	m_word = v_word ;
	v_word.clear() ;
	return 1;
}

void VietVoice::Concatenator::Get_cut_begin (long v_cb)
{	
	v_cut_begin = v_cb ;
}
void VietVoice::Concatenator::Get_cut_end (long v_ce)
{	
	v_cut_end = v_ce ;
}
int VietVoice::Concatenator::v_wordLength(std::wstring v_word) // VT
{	
	return 3500; // HCM 1
	if (v_word == L"") return 100 ;

	StringHelper::ReplaceAll(v_word,L'á',L'a') ;
	StringHelper::ReplaceAll(v_word,L'ạ',L'a') ;
	StringHelper::ReplaceAll(v_word,L'ắ',L'ă') ;
	StringHelper::ReplaceAll(v_word,L'ặ',L'ă') ;
	StringHelper::ReplaceAll(v_word,L'ấ',L'â') ;
	StringHelper::ReplaceAll(v_word,L'ậ',L'â') ;
	StringHelper::ReplaceAll(v_word,L'é',L'e') ;
	StringHelper::ReplaceAll(v_word,L'ẹ',L'e') ;
	StringHelper::ReplaceAll(v_word,L'ế',L'ê') ;
	StringHelper::ReplaceAll(v_word,L'ệ',L'ê') ;
	StringHelper::ReplaceAll(v_word,L'í',L'i') ;
	StringHelper::ReplaceAll(v_word,L'ị',L'i') ;
	StringHelper::ReplaceAll(v_word,L'ú',L'u') ;
	StringHelper::ReplaceAll(v_word,L'ụ',L'u') ;
	StringHelper::ReplaceAll(v_word,L'ứ',L'ư') ;
	StringHelper::ReplaceAll(v_word,L'ự',L'ư') ;
	StringHelper::ReplaceAll(v_word,L'ó',L'o') ;
	StringHelper::ReplaceAll(v_word,L'ọ',L'o') ;
	StringHelper::ReplaceAll(v_word,L'ố',L'ô') ;
	StringHelper::ReplaceAll(v_word,L'ộ',L'ô') ;
	StringHelper::ReplaceAll(v_word,L'ớ',L'ơ') ;
	StringHelper::ReplaceAll(v_word,L'ợ',L'ơ') ;
	// ::MessageBox (NULL, StringHelper::ToString (v_word).c_str (), L"End", MB_OK) ;

	std::wstring v_w = v_word ; 
	int v_w1 = 0 ; int v_w2 = 0 ; int v_w3 = 0 ;
	double v_l = 0 ; 
	double v_p = 39.0f ; // 55 59.0 55 39
	double v_q = 11.5f ; // 1.65f

	bool v_passed=false ;
	if (v_w.length() >= 3) {
		if (v_w.substr(0,3) == L"ngh") {
			v_w = v_w.substr(3, v_w.length()-3); v_l += 72.0f ; v_w1 = 1 ; 
			v_passed=true ;
		}
	}
	if (v_passed==false) {
		if (v_w.length() >= 2) {
			if      (v_w.substr(0,2) == L"ch") {v_w = v_w.substr(2, v_w.length()-2); v_l += 97.0f ; v_w1 = 1; v_passed=true ;} // 91 * 0.9
			else if (v_w.substr(0,2) == L"gi") {v_w = v_w.substr(2, v_w.length()-2); v_l += 73.0f ; v_w1 = 1 ; v_passed=true ;} // 153 / 2
			else if (v_w.substr(0,2) == L"gh") {v_w = v_w.substr(2, v_w.length()-2); v_l += 70.0f ; v_w1 = 1 ; v_passed=true ;} // 175 / 2.5
			else if (v_w.substr(0,2) == L"kh") {v_w = v_w.substr(2, v_w.length()-2); v_l += 86.0f ; v_w1 = 1 ; v_passed=true ;} // 147 / 2 // 76
			else if (v_w.substr(0,2) == L"ng") {v_w = v_w.substr(2, v_w.length()-2); v_l += 86.0f ; v_w1 = 1 ; v_passed=true ;} // 107 / 1.4
			else if (v_w.substr(0,2) == L"nh") {v_w = v_w.substr(2, v_w.length()-2); v_l += 86.0f ; v_w1 = 1 ; v_passed=true ;} // 92 / 1.5
			else if (v_w.substr(0,2) == L"ph") {v_w = v_w.substr(2, v_w.length()-2); v_l += 73.0f ; v_w1 = 1 ; v_passed=true ;} // 132 / 1.7
			else if (v_w.substr(0,2) == L"qu") {v_w = v_w.substr(2, v_w.length()-2); v_l += 62.0f ; v_w1 = 1 ; v_passed=true ;} // 125 / 2.5
			else if (v_w.substr(0,2) == L"tr") {v_w = v_w.substr(2, v_w.length()-2); v_l += 85.0f ; v_w1 = 1 ; v_passed=true ;} // [61]
			else if (v_w.substr(0,2) == L"th") {v_w = v_w.substr(2, v_w.length()-2); v_l += 79.0f ; v_w1 = 1 ; v_passed=true ;} // 168 / 3
		}
	}
	if (v_passed==false) {
		if      (v_w.substr(0,1) == L"b") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 64.0f ;  v_w1 = 1 ; } // 76 / 1.1
		else if (v_w.substr(0,1) == L"c") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 62.0f ; v_w1 = 1 ; } // 22 * 2
		else if (v_w.substr(0,1) == L"d") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 61.0f ; v_w1 = 1 ; } // 115 / 2 // 97 / 2 // 71
		else if (v_w.substr(0,1) == L"đ") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 92.0f ; v_w1 = 1 ; } // 77 / 2 // 97 / 2 // 72
		else if (v_w.substr(0,1) == L"g") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 70.0f ; v_w1 = 1 ; } // 150 / 2
		else if (v_w.substr(0,1) == L"h") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 80.0f ; v_w1 = 1 ; } // [74]
		else if (v_w.substr(0,1) == L"j") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 74.0f ; v_w1 = 1 ; } // 
		else if (v_w.substr(0,1) == L"k") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 62.0f ; v_w1 = 1 ; } // 26 * 2.5
		else if (v_w.substr(0,1) == L"l") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 79.0f ; v_w1 = 1 ; } // 105 / 2.3
		else if (v_w.substr(0,1) == L"m") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 69.0f ; v_w1 = 1 ; } // [64]
		else if (v_w.substr(0,1) == L"n") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 80.0f ; v_w1 = 1 ; } // 105 / 2 // 71
		else if (v_w.substr(0,1) == L"p") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 52.0f ; v_w1 = 1 ; } // 26 * 2
		else if (v_w.substr(0,1) == L"r") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 77.0f ; v_w1 = 1 ; } // 130 / 1.8
		else if (v_w.substr(0,1) == L"s") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 97.0f ; v_w1 = 1 ; } // 150 / 1.8
		else if (v_w.substr(0,1) == L"t") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 58.0f ; v_w1 = 1 ; } // [54]
		else if (v_w.substr(0,1) == L"v") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 91.0f ; v_w1 = 1 ; } // 112 / 1.9
		else if (v_w.substr(0,1) == L"w") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 99.0f ; v_w1 = 1 ; } // [105.0]
		else if (v_w.substr(0,1) == L"x") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 95.0f ; v_w1 = 1 ; } // 158 / 2
		else if (v_w.substr(0,1) == L"z") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 74.0f ; v_w1 = 1 ; } 
		// ::MessageBox (NULL, StringHelper::ToString (v_w1).c_str (), StringHelper::ToString (v_word).c_str (), MB_OK) ;
	}

	v_passed=false ;

	if (v_w.length() >=3) {
		if      (v_w.substr(0,3) == L"iêu") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 2.13f ; v_w2 = 1 ; v_passed=true ;} //// [1.64]
		else if (v_w.substr(0,3) == L"yêu") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 2.13f  ; v_w2 = 1 ; v_passed=true ;} // 2.3 // 1.8
		else if (v_w.substr(0,3) == L"oai") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.77f  ; v_w2 = 1 ; v_passed=true ;} // 2.1 - 1.9 * 1.8
		else if (v_w.substr(0,3) == L"oao") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.7f   ; v_w2 = 1 ; v_passed=true ;}
		else if (v_w.substr(0,3) == L"oay") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.7f   ; v_w2 = 1 ; v_passed=true ;}
		else if (v_w.substr(0,3) == L"oeo") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.77f   ; v_w2 = 1 ; v_passed=true ;}
		else if (v_w.substr(0,3) == L"uây") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.77f   ; v_w2 = 1 ; v_passed=true ;}
		else if (v_w.substr(0,3) == L"uya") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.87f  ; v_w2 = 1 ; v_passed=true ;} // 2.5
		else if (v_w.substr(0,3) == L"uyê") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.83f  ; v_w2 = 1 ; v_passed=true ;} // [1.81]
		else if (v_w.substr(0,3) == L"uyu") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.56f   ; v_w2 = 1 ; v_passed=true ;} // [2.1]
		else if (v_w.substr(0,3) == L"ươi") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.15f  ; v_w2 = 1 ; v_passed=true ;} // [1.65]
		else if (v_w.substr(0,3) == L"uôi") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.75f  ; v_w2 = 1 ; v_passed=true ;} // [1.5]
		else if (v_w.substr(0,3) == L"ươu") {v_w = v_w.substr(3, v_w.length()-3) ; v_l += v_p * 1.56f   ; v_w2 = 1 ; v_passed=true ;} // [2.3]
	}

	if (v_passed==false) {
		if (v_w.length() >=2) {
			if      (v_w.substr(0,2) == L"ai") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.65f ; v_w2 = 1 ; v_passed=true ; } //// [1.59//1.68]
			else if (v_w.substr(0,2) == L"ao") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.61f ; v_w2 = 1 ; v_passed=true ; } // [1.61]
			else if (v_w.substr(0,2) == L"au") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.61f ; v_w2 = 1 ; v_passed=true ; } // [1.5]
			else if (v_w.substr(0,2) == L"ay") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.62f ; v_w2 = 1 ; v_passed=true ; } // [1.55]
			else if (v_w.substr(0,2) == L"âu") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.69f ; v_w2 = 1 ; v_passed=true ; } // [1.5]
			else if (v_w.substr(0,2) == L"ây") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.59f ; v_w2 = 1 ; v_passed=true ; } // [1.5/1.55/1.59]
			else if (v_w.substr(0,2) == L"eo") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.79f ; v_w2 = 1 ; v_passed=true ; } // * 1.6
			else if (v_w.substr(0,2) == L"êu") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.5f  ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"ia") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 0.51f ; v_w2 = 1 ; v_passed=true ; } //// 1.8 * 1.7
			else if (v_w.substr(0,2) == L"ya") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 0.51f ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"iê") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.72f ; v_w2 = 1 ; v_passed=true ; } // [1.47] 62
			else if (v_w.substr(0,2) == L"yê") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.72f ; v_w2 = 1 ; v_passed=true ; } // 62
			else if (v_w.substr(0,2) == L"iu") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.5f  ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"oa") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.63f ; v_w2 = 1 ; v_passed=true ; } // [1.65]
			else if (v_w.substr(0,2) == L"oă") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.6f  ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"oe") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.7f  ; v_w2 = 1 ; v_passed=true ; } // 2.3 //// 2.1
			else if (v_w.substr(0,2) == L"oi") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.67f ; v_w2 = 1 ; v_passed=true ; } // [1.55/1.65/1.71]
			else if (v_w.substr(0,2) == L"oo") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.6f  ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"ôi") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.15f ; v_w2 = 1 ; v_passed=true ; } // [1.62]
			else if (v_w.substr(0,2) == L"ơi") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.77f ; v_w2 = 1 ; v_passed=true ; } // [1.43]
			else if (v_w.substr(0,2) == L"ua") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.16f ; v_w2 = 1 ; v_passed=true ; } // 2
			else if (v_w.substr(0,2) == L"uâ") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.65f ; v_w2 = 1 ; v_passed=true ; } // 1.2
			else if (v_w.substr(0,2) == L"uê") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.6f  ; v_w2 = 1 ; v_passed=true ; }  // 2.0
			else if (v_w.substr(0,2) == L"ui") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.5f  ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"uô") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.6f  ; v_w2 = 1 ; v_passed=true ; } //// 2.3
			else if (v_w.substr(0,2) == L"uơ") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.65f ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"uy") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.59f ; v_w2 = 1 ; v_passed=true ; } // [1.65]
			else if (v_w.substr(0,2) == L"ưa") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.61f ; v_w2 = 1 ; v_passed=true ; } // [2.1]
			else if (v_w.substr(0,2) == L"ưi") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.56f ; v_w2 = 1 ; v_passed=true ; }
			else if (v_w.substr(0,2) == L"ươ") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.69f ; v_w2 = 1 ; v_passed=true ; } // 1.5 // 1.8
			else if (v_w.substr(0,2) == L"ưu") {v_w = v_w.substr(2, v_w.length()-2) ; v_l +=  v_p * 1.54f  ; v_w2 = 1 ; v_passed=true ; } // 2.0
		}
	}

	if (v_passed==false) {


		if      (v_w.substr(0,1) == L"a") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 2.25f ; v_w2 = 1 ; } // [1.55/1.65/1.72]
		else if (v_w.substr(0,1) == L"ă") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.61f ; v_w2 = 1 ; v_passed=true ;  } // [1.59]
		else if (v_w.substr(0,1) == L"â") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.91f ; v_w2 = 1 ; v_passed=true ;  } // [1.52]
		else if (v_w.substr(0,1) == L"e") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.61f ; v_w2 = 1 ; v_passed=true ;  } // [1.65]
		else if (v_w.substr(0,1) == L"ê") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.37f ; v_w2 = 1 ; v_passed=true ;  } // [1.47]
		else if (v_w.substr(0,1) == L"i") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.5f ; v_w2 = 1 ; v_passed=true ;  } // [1.48]
		else if (v_w.substr(0,1) == L"o") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.65f ; v_w2 = 1 ; v_passed=true ;  } // [1.55]
		else if (v_w.substr(0,1) == L"ô") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.51f ; v_w2 = 1 ; v_passed=true ;  } // [1.55]
		else if (v_w.substr(0,1) == L"ơ") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.65f ; v_w2 = 1 ; v_passed=true ;  } // [1.59]
		else if (v_w.substr(0,1) == L"u") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 0.91f  ; v_w2 = 1 ; v_passed=true ;  } // 1.15
		else if (v_w.substr(0,1) == L"ư") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.95f ; v_w2 = 1 ; v_passed=true ;  } // [1.65]
		else if (v_w.substr(0,1) == L"y") {v_w = v_w.substr(1, v_w.length()-1) ; v_l +=  v_p * 1.52f ; v_w2 = 1 ; v_passed=true ;  } // [1.47]
	}

	v_passed=false ;
	if (v_w.length() >= 2) {
		if      (v_w.substr(0,2) == L"ch") {v_w = v_w.substr(2, v_w.length()-2) ; v_l += 15.0f * v_q ; v_w3 = 1 ; v_passed=true ; } // [39]
		else if (v_w.substr(0,2) == L"ng") {v_w = v_w.substr(2, v_w.length()-2) ; v_l += 105.0f * v_q ; v_w3 = 1 ; v_passed=true ; } // [71] 95
		else if (v_w.substr(0,2) == L"nh") {v_w = v_w.substr(2, v_w.length()-2) ; v_l += 70.0f * v_q ; v_w3 = 1 ; v_passed=true ; 
		//::MessageBox (NULL, StringHelper::ToString (v_word).c_str (), L"Vowel", MB_OK) ;
		} // [42]
	}
	if (v_passed==false) {
		if      (v_w.substr(0,1) == L"c") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 39.0f * v_q ; v_w3 = 1 ; } // [29.9]
		else if (v_w.substr(0,1) == L"t") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 39.0f * v_q ; v_w3 = 1 ; } // [52]
		else if (v_w.substr(0,1) == L"m") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 95.0f * v_q ; v_w3 = 1 ; } // [38]
		else if (v_w.substr(0,1) == L"n") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 95.0f * v_q ; v_w3 = 1 ; } // [54]
		else if (v_w.substr(0,1) == L"p") {v_w = v_w.substr(1, v_w.length()-1) ; v_l += 11.0f * v_q ; v_w3 = 1 ; } // [51]
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




	if (v_w1 == 0 && v_w2 == 0 || v_w1 == 0 && v_w3 == 0 || v_w2 == 0 && v_w3 == 0) { // Vowel or Consonant only
		v_l += 60; /// [75] 84 89 81
	}
	else if (v_w1 == 0 || v_w2 == 0 || v_w3 == 0) { // Vowel + Consonant or Consonant + Vowel
		v_l += (1 - v_w1) * 55 + (1 - v_w3) * 42 ; // 34  25
	}
	else { // Consonant + Vowel + Consonant
		v_l += 0 ; // [+7] 29
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	return int (v_l) ;
}
void VietVoice::Concatenator::Get_sc_comp (int v_sc)
{	
	v_sc_comp = v_sc;
}

void VietVoice::Concatenator::v_getLinkingSilence (int v_value)
{	
	v_linking_sl = v_value;
}

void VietVoice::Concatenator::Compress_Word (vector<long> & tmp_word, int v_percent, int v_accent)
{	
	if (v_percent == 0) return ;
	/////////////////////////::MessageBox(NULL, StringHelper::ToString(tmp_word.size()).c_str(), L"Tmp Truoc", MB_OK);
	////////////////////////::MessageBox(NULL, StringHelper::ToString(v_percent).c_str(), L"Percent", MB_OK) ;
	int v_99 = tmp_word.size();
	int mi = 0 ;
	unsigned int v_num_of_cycles = 0; // Số nút có sự lập lại
	unsigned int v_cycle_max = 8 ; // [6] //// 10 // * 6
	if (v_accent == 0 || v_accent == 1 || v_accent == 2) v_cycle_max = 4; //// Important // * 4






	float v_ratio = 1.1f ; // 0.05 //// 1.06 //// 0.17 // [0.1] ////// 0.2f // 2016 0.5f // * 0.7 // Ha Noi 1.2
	vector<unsigned long> v_sum ;
	vector<unsigned int> v_avg ;
	vector<unsigned int> v_loc ;
	vector<unsigned int> v_dis ;
	vector<unsigned int> v_seg ;
	vector<bool> v_cycle ;

	v_sum.push_back (0) ;
	v_avg.push_back (0) ;
	v_loc.push_back (0) ;
	v_dis.push_back (0) ;
	v_seg.push_back (0) ;
	v_cycle.push_back (false) ;
	unsigned int v_num_of_breaks = 0;
	for (unsigned long i = 0; i < (tmp_word.size() - 1); i++)
	{
		if ((tmp_word[i] * tmp_word[i+1] <= 0) && (i - mi  >= 20))
		{
			v_sum.push_back (abs(tmp_word[i])) ;
			v_avg.push_back (0) ;
			v_loc.push_back (i) ;
			v_dis.push_back (i - mi) ;
			v_seg.push_back (0) ;
			v_cycle.push_back (false) ;
			v_num_of_breaks ++;
			mi = i ;
		}
		else
		{
		}
		v_sum[v_num_of_breaks] += tmp_word[i] ;
	}
	for (unsigned int i = 0; i < v_num_of_breaks - 0; i++) v_avg[i] = v_sum[i] / (v_loc[i+1] - v_loc[i]) ;
	/////////////////::MessageBox(NULL, StringHelper::ToString(v_num_of_breaks).c_str(), L"Diem cat", MB_OK) ;

	if (v_num_of_breaks == 0) return;

	unsigned int k = 0 ;
	unsigned int j = 0 ;
	float m_ratio_sum ;
	//	float m_ratio_loc ;
	float m_ratio_dis ;

	unsigned int v_cut_cycle = 0 ;
	int v_add_cycle = 0 ;
	unsigned int vi = 0 ;
	int v_comp ;
	do {
		k = 2 ; // 2
		do {
			bool v_found = true ;
			j = 0 ;
			if (v_dis[vi + k] > v_dis[vi]) v_comp = 2 ;
			else if (v_dis[vi + k] < v_dis[vi]) v_comp = -1 ;
			else v_comp = 1 ;
			do {
				m_ratio_sum = float (v_avg[vi + k + j]) / v_avg[vi + j] ; // them + j
				m_ratio_sum = abs(1 - m_ratio_sum) + 1;
				m_ratio_dis = float (v_dis[vi + k + j]) / v_dis[vi + j] ; // them + j
				m_ratio_dis = abs(1 - m_ratio_dis) + 1;
				/////////////////////////				if ((m_ratio_sum > v_ratio) || (m_ratio_dis > v_ratio) || (v_comp == 2 && v_dis[vi + k + j] < v_dis[vi + j]) || (v_comp == -1 && v_dis[vi + k + j] > v_dis[vi + j]))
				if ((m_ratio_sum > v_ratio) || (m_ratio_dis > v_ratio))
					v_found = false ;
				j++ ;
			} while (j <= k-1) ;
			if (v_found) {
				v_seg[vi] = k ;
				v_num_of_cycles ++ ;
				vi += k - 1 ;
				k = v_cycle_max;
			}
			k++ ; //////			k++ ; // So chu ky phai chan
		} while (k <= v_cycle_max) ;
		vi ++ ;
	} while (vi < v_num_of_breaks - v_cycle_max * 2 + 1) ;


	/////////////////////////////::MessageBox(NULL, StringHelper::ToString(v_num_of_cycles).c_str(), L"Cycles", MB_OK) ;


	if (v_num_of_cycles == 0) return ;
	int v_num_of_cycle_bytes = 0 ; // Số byte sẽ lặp lại
	for (unsigned int i = 0; i < v_num_of_breaks; i++) {
		if (v_seg[i] > 0) {
			v_num_of_cycle_bytes += v_loc[i + v_seg[i]] - v_loc[i] ;
		}
	}
	float v_avg_cycle = float (v_num_of_cycle_bytes / v_num_of_cycles) ; // Số byte lập lại trung bình cho mỗi segment

	vector<long> v_tmp ;
	v_cut_cycle = unsigned int ((tmp_word.size() * abs(v_percent) / 100) / v_avg_cycle) ; // Số segment sẽ lập lại căn cứ vào tỷ lệ phần trăm
	if (v_num_of_cycles <= v_cut_cycle) {
		for (unsigned int i = 0; i < v_num_of_breaks; i++) {
			if (v_seg[i] > 0) {
				v_cycle[i] = true ;
				for (unsigned long j = 0; j < v_seg[i] - 1; j++)
					v_cycle[i + j + 1] = true ;
			}
		}
	} else {
		vector<int> m_cut;
		for (unsigned int i=0; i< v_cut_cycle; i++) {
			m_cut.push_back(int(i *  (float (v_num_of_cycles)/ v_cut_cycle))) ;
		}
		int v_count = -1 ; unsigned int j = 0 ;
		for (unsigned long i = 0; i< m_cut.size(); i++) {
			do {
				if (v_seg[j] > 0) {
					v_count ++ ;
					if (v_count == m_cut[i] ) {
						v_cycle[j] = true ;
						for (unsigned long k = 0; k < v_seg[j] - 1; k++)
							v_cycle[j + k + 1] = true ;
						j ++ ;
						break ;
					} 
					else {
					}
				}
				j++ ;
			} while (j < v_num_of_breaks && v_count < v_num_of_cycles) ;
		}
		m_cut.clear();
	}
	int v_compressed = 0;
	int v_added = 0 ;
	for (unsigned long i = 0; i < v_loc[0]; i++) { // Lay doan dau
		v_tmp.push_back(tmp_word[i]) ;
	}
	unsigned int i = 0 ;
	do {
		if (v_cycle[i] == false) {
			for (unsigned long j = v_loc[i]; j < v_loc[i + 1]; j++) {// Lay doan thu i
				v_tmp.push_back(tmp_word[j]);
			}
		}
		else {
			if (v_seg[i] > 0) v_compressed += v_loc[i + v_seg[i]] - v_loc[i] ;

			if (v_percent < 0) {


				if (v_seg[i] > 0) {
					std::vector<long> v_repeat ;
					for (unsigned long j = v_loc[i]; j < v_loc[i + 1]; j++) // Lay doan thu i
						v_repeat.push_back(tmp_word[j]);
					unsigned long v_seg_tmp = v_seg[i] ; 
					for (unsigned long k = 0; k < v_seg_tmp - 1; k ++) { // Lay so ck - 1 segment tiep theo
						i ++ ;
						for (unsigned long j = v_loc[i]; j < v_loc[i + 1]; j++) // Lay doan thu i
							v_repeat.push_back(tmp_word[j]);
					}
					for (unsigned long j = 0; j < v_repeat.size(); j ++)
						v_tmp.push_back(v_repeat[j]) ;
					for (unsigned long j = 0; j < v_repeat.size(); j ++)
						v_tmp.push_back(v_repeat[j]) ;
					v_added += v_repeat.size() ;
					v_repeat.clear() ;
				} else {


				}


			}
		}
		i++ ;
	} while (i < v_num_of_breaks - 0) ; // - 1

	for (unsigned long i = v_loc[v_num_of_breaks - 0]; i < tmp_word.size(); i++) // Lay doan cuoi
		v_tmp.push_back(tmp_word[i]) ;

	tmp_word.clear() ;
	tmp_word = v_tmp ;
	///////////////////////////::MessageBox(NULL, StringHelper::ToString(tmp_word.size()).c_str(), StringHelper::ToString(v_99).c_str(), MB_OK);
	v_tmp.clear () ;
	v_sum.clear () ;
	v_avg.clear () ;
	v_loc.clear () ;
	v_seg.clear () ;
	v_cycle.clear () ;
}

void VietVoice::Concatenator::V_Compress_Word (vector<long> & temp_word, int v_percent, int v_accent) {
	//return;



	int v_temp99 = temp_word.size() ;
	vector <long> v_temp_merge ;
	unsigned int v_num_of_segments = v_temp99 / 3000; // Ha Noi 2000 // 1000 khong chay
	//if (v_accent == 1 || v_accent == 5) v_num_of_segments = 3;

	//unsigned long v_vt_dau [v_num_of_segments + 2] ;
	unsigned long v_vt_dau [100] ; // Ha Noi 20

	for (unsigned int i = 1; i <= v_num_of_segments; i++) {
		v_vt_dau [i] = (temp_word.size() / v_num_of_segments) * (i - 1) ;
	}
	v_vt_dau [v_num_of_segments + 1] = temp_word.size() ;


	for (unsigned int i = 1; i <= v_num_of_segments - 0; i++) {
		while (v_vt_dau [i] > 0 && temp_word [v_vt_dau [i]] * temp_word [v_vt_dau [i] + 1] > 0) v_vt_dau [i]-- ;
	}
	vector <long> v_temp ;
	v_temp_merge.clear() ;
	for (unsigned int i = 1; i <= v_num_of_segments; i++) {
		if (v_temp.size() > 0) v_temp.clear() ;
		for (unsigned long j = v_vt_dau [i]; j <= v_vt_dau [i + 1] - 1; j++) {
			if (j > temp_word.size() - 1) {
				continue ;
			}
			else v_temp.push_back(temp_word [j]) ;
		}
		////////////////////////////::MessageBox(NULL, StringHelper::ToString(v_temp.size()).c_str(), L"B", MB_OK) ;
		Compress_Word(v_temp, v_percent, v_accent) ;
		////////////////////////////::MessageBox(NULL, StringHelper::ToString(v_temp.size()).c_str(), L"A", MB_OK) ;
		for (unsigned long k = 0; k < v_temp.size(); k++) v_temp_merge.push_back(v_temp [k]) ;
	}
	temp_word = v_temp_merge ;
	v_temp_merge.clear() ;
	v_temp.clear() ;
	//if (v_temp99 * (1 + v_percent / 100) != temp_word.size())
	//	::MessageBox(NULL, StringHelper::ToString(v_temp99 * (1 + v_percent / 100)).c_str(), StringHelper::ToString(temp_word.size()).c_str(), MB_OK) ;
}
void VietVoice::Concatenator::V_WSOLA_Word (WAV_INFO *in_info, WAV_INFO *out_info, float beta,int nsamp) {
	vector <long> temp_word ;
	temp_word.clear();
	for (int k = 0; k < in_info -> Nsamples-1; k++) {
		temp_word.push_back(long (in_info -> data[k]));
	}

	// vector <long> v_temp_merge ;
	vector <float> v_temp_merge ;
	//unsigned int v_num_of_segments = 15;

	int v_num_of_segments = (in_info -> Nsamples - 1)/ 1000;
	//::MessageBox(NULL, StringHelper::ToString(v_num_of_segments).c_str(), StringHelper::ToString(temp_word.size()).c_str(), MB_OK) ;
	//unsigned long v_vt_dau [v_num_of_segments + 2] ;
	unsigned long v_vt_dau [20] ;

	for (unsigned int i = 1; i <= v_num_of_segments; i++) {
		v_vt_dau [i] = (temp_word.size() / v_num_of_segments) * (i - 1) ;
	}
	v_vt_dau [v_num_of_segments + 1] = temp_word.size() ;

	for (unsigned int i = 1; i <= v_num_of_segments - 0; i++) {
		while (v_vt_dau [i] > 0 && temp_word [v_vt_dau [i]] * temp_word [v_vt_dau [i] + 1] > 0) v_vt_dau [i]-- ;
		//		::MessageBox(NULL, StringHelper::ToString(v_vt_dau [i]).c_str(), L"Vi tri dau", MB_OK) ;
	}
	vector <long> v_temp ;
	v_temp_merge.clear() ;
	for (unsigned int i = 1; i <= v_num_of_segments; i++) {
		// if (v_temp.size() > 0) v_temp.clear() ;
		v_temp.clear() ;
		for (unsigned long j = v_vt_dau [i]; j <= v_vt_dau [i + 1] - 1; j++) {
			if (j > temp_word.size() - 1) {
				continue ;
			}
			else v_temp.push_back(temp_word [j]) ;
		}

		WAV_INFO in_info1 ;
		in_info1.data = (float *) malloc((v_temp.size()) * sizeof(float));
		in_info1.Fs = 22050; in_info1.Nsamples = v_temp.size(); in_info->Nchan = 1 ;
		for (unsigned long k = 0; k < v_temp.size(); k++) {
			in_info1.data[k] = float (v_temp[k]);
		}
		//		::MessageBox(NULL, StringHelper::ToString(in_info1.Nsamples).c_str(), L"In_info1 size", MB_OK) ;

		WAV_INFO out_info1;
		long buflen = (long)(1.5f + 1.0f / beta);
		out_info1.data = (float *) malloc(buflen * (v_temp.size()) * sizeof(float));
		memset(out_info1.data, 0, buflen*(v_temp.size()) * sizeof(float));
		// out_info1.Fs = 44100; out_info1.Nsamples = buflen * v_temp.size();


		GET_FINALE_DATA( &in_info1, &out_info1, beta, nsamp);
		//		::MessageBox(NULL, StringHelper::ToString(out_info1.Nsamples).c_str(), L"Out_info1 size", MB_OK) ;

		//// v_temp.clear();
		for (int k = 0; k < out_info1.Nsamples - 1; k++) {
			//// v_temp.push_back(out_info1.data [k]);
			v_temp_merge.push_back(out_info1.data [k]);
		}
		////	::MessageBox(NULL, StringHelper::ToString(v_temp_merge.size()).c_str(), L"v_temp_merge size", MB_OK) ;

		//// for (unsigned long k = 0; k < v_temp.size(); k++) v_temp_merge.push_back(v_temp [k]) ;
	}
	out_info -> Nsamples = v_temp_merge.size() ;
	for (int k = 0; k < out_info -> Nsamples - 1; k++) {
		out_info -> data [k] = v_temp_merge [k];
	}
	temp_word.clear() ;
	v_temp_merge.clear() ;
	v_temp.clear() ;
}

int VietVoice::Concatenator::v_find_content(vector <wstring> v_dic, wstring v_exp) // VT
{
	if (find(v_dic.begin(), v_dic.end(), v_exp) != v_dic.end())
		return ::VietVoice::SilenceAdder::v_count_words(v_exp);
	else
		return 0;
}
