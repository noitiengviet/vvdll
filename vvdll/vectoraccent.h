#if !defined (VECTORACCENT_H)

#define VECTORACCENT_H

#include <map>
#include "accent.h"
#include "fileio.h"
#include "stringhelper.h"
#include "vietnameseTTSException.h"

namespace VietVoice
{
	/**
	 * Class Name:         Accent.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 14 2004
	 *
	 * Description:        Class used to cotain the data about the accent of a word
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */

	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * october 19 2005:  Converted the class from java to c++.
	 * novembre 29 2005:  Implemented the copy constructor and the = operator
	 * February 4 2006:  Modified the class to use unicode file names.
	 * March 6 2006:  Seperated implementation in source and header
	 * June 10 2006: Change the methods name so they start by a capital letter.
	 */
	class VectorAccent
	{ 
	public:

		/**
		* Constructor
		*/
		VectorAccent ();

		/**
		*
		* Constructor Create the map (accent map) from a file
		* @param name of the file
		*/
		VectorAccent (std::wstring  name);

		~VectorAccent ();

		VectorAccent  (const VectorAccent & vectorAccent);

		void operator = (const VectorAccent & vectorAccent);

		/**
		 *
		 * Create the map (accent map) from a file
		 * @param name of the file
		 */
		void Load (std::wstring filename);

		/**
		 * Determines if there is an entry in the Map object for a key in particular
		 * @param key The key for witch we wish to know if there is an entry in the Map
		 */
		bool EntryExist (std::wstring key);

		/**
		 * Obtain a numerical value representing the accent of a character
		 * @param key The character
		 * @return A numerical value representing the accent.
		 */
		int GetAccent (std::wstring key);

		/**
		 * Obtain the information on the accent of a character in particular
		 * @param key The character
		 * @return The information about the accent
		 */
		Accent GetAccentInfo (std::wstring key);

		/**
		 * Takes a character with an accent and return the "same" character without an accent.
		 * @param key Le caract�re vietnamien
		 * @return The character without an accent.
		 */
		std::wstring GetReplacement(std::wstring key);

	private:
		
		std::map<std::wstring, Accent> _accentMap;
	};
}

#endif