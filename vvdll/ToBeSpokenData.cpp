
#include "ToBeSpokenData.h"
#include "stringhelper.h"
/**
* Copy constructor
*
* Parameters:
*
* const ToBeSpokenData & data:  The object to be copied.
*/
VietVoice::ToBeSynthetiseData::ToBeSynthetiseData  (const ToBeSynthetiseData & data)
: _wordData (NULL)
{
	if (this != &data) // Make sure we are not copying the object into itself
	{
		_word = data._word;
		m_percentageAdjust = data.m_percentageAdjust;
		m_volumeAdjust = data.m_volumeAdjust;
		_originalWord = data._originalWord;
		_prepunc = data._prepunc;
		_postpunc = data._postpunc;
		_accent = data._accent;
		_index = data._index;
		_nbRepeats = data._nbRepeats;
		_wordData = NULL;
		_silenceLength = data._silenceLength;
		_tyLeTangVolumeTu=data._tyLeTangVolumeTu;
		_wordCut = data._wordCut;
		m_wordType = data.m_wordType;

		if (data._wordData != NULL)
		{
			_wordData = new Word (*data._wordData);
		}
	}
}

/**
* Assignement operator used to avoid memory leaks
*/
void VietVoice::ToBeSynthetiseData::operator = (const ToBeSynthetiseData & data)
{
	if (this != &data) // Make sure we are not copying the object into itself
	{
		_word = data._word;
		_word = data._originalWord;
		m_percentageAdjust = data.m_percentageAdjust;
		m_volumeAdjust = data.m_volumeAdjust;
		_prepunc = data._prepunc;
		_postpunc = data._postpunc;
		_accent = data._accent;
		_index = data._index;
		_nbRepeats = data._nbRepeats;
		_silenceLength = data._silenceLength;
		_tyLeTangVolumeTu=data._tyLeTangVolumeTu;
		_wordCut = data._wordCut;
		_wordData = NULL;
		m_wordType = data.m_wordType;
		
		if (data._wordData != NULL)
		{
			_wordData = new Word (*data._wordData);
		}
		else
		{
			_wordData = NULL;
		}
	}
}

/**
* Constructor
*/
VietVoice::ToBeSynthetiseData::ToBeSynthetiseData (WORD_TYPE data)
{
	_word = L""; 
	m_percentageAdjust = 0;
	m_volumeAdjust = 100;
	_accent = 0;
	_index = 0;
	_nbRepeats = 1;
	_wordData = NULL;
	_prepunc = L"";
	_postpunc = L"";
	_silenceLength = 0;
	_silenceLength = 100;
	_wordCut = 0;
	m_wordType = data;
}
        
/**
* Constructior
*
* Parameters:
*
* std::wstring word The word this data is related to
*/
VietVoice::ToBeSynthetiseData::ToBeSynthetiseData (std::wstring word, WORD_TYPE data)
{
	_word = word;
	_originalWord = word;
	m_percentageAdjust = 0;
	m_volumeAdjust = 100;
	_accent = 0;
	_index = 0;
	_nbRepeats = 1;
	_wordData = NULL;
	_prepunc = L"";
	_postpunc = L"";
	_silenceLength = 0;
	_tyLeTangVolumeTu = 100;
	_wordCut = 0;
	m_wordType = data;
}

/**
* Constructor
*
* Parameters:
*
* std::wstring word The word this data is related to
* std::wstring prepunc The prepunc The punctiation before the word
* std::wstring postpunc The punctuation after the word
*/
VietVoice::ToBeSynthetiseData::ToBeSynthetiseData (std::wstring word, std::wstring prepunc, std::wstring postpunc, WORD_TYPE data)
{
	_wordData = NULL;
	_word = word;
	m_percentageAdjust = 0;
	m_volumeAdjust = 100;
	_originalWord = word;
	_accent = 0;
	_index = 0;
	_nbRepeats = 1;
	_prepunc = prepunc;
	_postpunc = postpunc;
	_silenceLength = 0;
	_tyLeTangVolumeTu = 100;
	_wordCut = 0;
	m_wordType = data;
}

/**
*Destructor
*/
VietVoice::ToBeSynthetiseData::~ToBeSynthetiseData ()
{
	if (_wordData)
	{
		delete _wordData; //// [[]]
		_wordData = NULL;
	}
}

/**
* Changes the name of the word
*
* Parameters:
*
* std::wstring word The new word
*/
void VietVoice::ToBeSynthetiseData::SetWord (std::wstring word)
{
	_word = word;
}

/**
* Obtains the name of the word
*
* return value: The name of the word
*/
std::wstring VietVoice::ToBeSynthetiseData::GetWord ()
{
	return _word;
}

/**
* Changes the accent of the word
*
* Parameters:
*
* int accent The new accent
*/
void VietVoice::ToBeSynthetiseData::SetAccent (int accent)
{
	_accent = accent;
}

/**
* Obtains the accent of the word
*
* return value: The accent
*/
int VietVoice::ToBeSynthetiseData::GetAccent() 
{
	return _accent;
}

/**
* Changes the index of the word
*
* Parameters:
*
* int index The new index
*/
void VietVoice::ToBeSynthetiseData::SetIndex(int index) 
{
	_index = index;
}

/**
*Obtains the index of the words
*
* return value: The index
*/
int VietVoice::ToBeSynthetiseData::GetIndex()
{
	return _index;
}

/**
* Changes the number of time the word must be repeated
*
* Parameters:
*
* int nbRepeats The new number of time the function is repeated
*/
void VietVoice::ToBeSynthetiseData::SetNbRepeats(int nbRepeats)
{
	_nbRepeats = nbRepeats;
}

/**
* Obtains the number of times the word must be repeated
*
* return value: The number of times the word must be repeaded
*/
int VietVoice::ToBeSynthetiseData::GetNbRepeats()
{
	return _nbRepeats;
}

/**
* Changes the sound data
*
* Parameters:
*
* Word * word data: The new sound data
*/
void VietVoice::ToBeSynthetiseData::SetWordData (Word * wordData)
{		
	_wordData = wordData;
}

/**
* Obtains the sound data
*
* return value:  The sound data
*/
VietVoice::Word * VietVoice::ToBeSynthetiseData::GetWordData ()
{
	return _wordData;
}

/**
* Sets the pre punctuation
*
* Parameters:
*
* std::wstring punc The new pre punctuation
*/
void VietVoice::ToBeSynthetiseData::SetPrePunctuation (std::wstring punc)
{
	_prepunc = punc;
}

/**
* Gets the pre punctuation
*
* return value:  The pre punctuation
*/
std::wstring VietVoice::ToBeSynthetiseData::GetPrePunctuation ()
{
	return _prepunc;
}

/**
* Gets the post punctuation
*
* return value:  The post punctuation
*/
std::wstring VietVoice::ToBeSynthetiseData::GetPostPunctuation ()
{
	return _postpunc;
}

/**
* Sets the post punctuation
*
* Parameters:
*
* std::wstring punc The new post punctuation
*/
void VietVoice::ToBeSynthetiseData::SetPostPunctuation (std::wstring punc)
{
	_postpunc = punc;

}

/**
* Determine if the object does not contain a token
*
* return value: true if the object is empty, else false
**/
bool VietVoice::ToBeSynthetiseData::IsEmpty ()
{
	return _word.compare (L"") == 0;
}

/**
* Determine if the current token is followed by a
* punctuation mark
*
* return value:  true if the object is followed by a punctuation mark, else false
**/
bool VietVoice::ToBeSynthetiseData::HasPostPunc ()
{
	return _postpunc.compare (L"") != 0;
}

/**
 * Changes the name of the original word
 *
 * Parameters:
 *
 * std::wstring word The new word
 */
void VietVoice::ToBeSynthetiseData::SetOriginalWord (std::wstring word)
{
	_originalWord = word;
}




/**
 * Obtaih the name of the original word
 *
 * Return value: THe name of the original word
 */
std::wstring VietVoice::ToBeSynthetiseData::GetOriginalWord ()
{
	return _originalWord;
}

void VietVoice::ToBeSynthetiseData::SetSilenceLength (long silenceLength)
{
	_silenceLength = silenceLength;
}

void VietVoice::ToBeSynthetiseData::SetTyLeTangVolumeTu (long tyLeTangVolumeTu)
{
	_tyLeTangVolumeTu = tyLeTangVolumeTu;
}



long VietVoice::ToBeSynthetiseData::GetSilenceLength ()
{
	return _silenceLength;
}

long VietVoice::ToBeSynthetiseData::GetTyLeTangVolumeTu ()
{
	return _tyLeTangVolumeTu;
}



void VietVoice::ToBeSynthetiseData::SetWordCut (double wordCut)
{
	_wordCut = wordCut;
}

double VietVoice::ToBeSynthetiseData::GetWordCut ()
{
	return _wordCut;
}