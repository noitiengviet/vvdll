
#include <string>
#include "worddatabase.h"

	const int VietVoice::WordDatabase::MAGIC = 0xCAFEDA;
	const int VietVoice::WordDatabase::VERSION = 1;
	const int VietVoice::WordDatabase::SIZE_HEADER = 107;
//	const int VietVoice::WordDatabase::INDEX = 0xCADAFE;

	VietVoice::WordDatabase::WordDatabase (): INDEX (0xCADAFE)
	{
	
	}

VietVoice::WordDatabase::WordDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring extra)
	: INDEX (0xCADAFE)
{
	m_percentageAdjust = 0;
	SetDatabase (index6Accents, index2Accents, indexExtra, databaseMain, databaseExtra, extra);
}


VietVoice::WordDatabase::WordDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring silence,
									   std::wstring index6Accents2, std::wstring index2Accents2, std::wstring indexExtra2, std::wstring databaseMain2, std::wstring databaseExtra2,std::wstring silence2,
									   std::wstring extra,
									   	std::wstring indexVeryShortWord, std::wstring databaseVeryShortWord,std::wstring veryShortWord,std::wstring silenceVeryShort)
	: INDEX (0xCADAFE)
{
	m_percentageAdjust = 0;
	SetDatabase (index6Accents, index2Accents, indexExtra, databaseMain, databaseExtra, extra,silence,1);
	SetDatabase (index6Accents2, index2Accents2, indexExtra2, databaseMain2, databaseExtra2, extra,silence2,2);
	SetDatabaseVeryShortWord(indexVeryShortWord,databaseVeryShortWord,veryShortWord,silenceVeryShort);
}

VietVoice::WordDatabase::~WordDatabase ()
{}

VietVoice::WordDatabase::WordDatabase  (const WordDatabase & wb)
	: INDEX (0xCADAFE)
{
	if (this != &wb)
	{
		_index6Accents = wb._index6Accents ;
		_index2Accents = wb._index2Accents;
		_indexExtra = wb._indexExtra;
		m_percentageAdjust = wb.m_percentageAdjust;
		_extraWords = wb._extraWords;

		_databaseMain = wb._databaseMain;
		_databaseExtra    = wb._databaseExtra;
	}

}

void VietVoice::WordDatabase::operator = (const WordDatabase & wb)
{
	if (this != &wb)
	{
		_index6Accents = wb._index6Accents ;
		_index2Accents = wb._index2Accents;
		_indexExtra = wb._indexExtra;
		m_percentageAdjust = wb.m_percentageAdjust;
		_extraWords = wb._extraWords;

		_databaseMain = wb._databaseMain;
		_databaseExtra    = wb._databaseExtra;
	}		
}

/**
 * Obrains the size of the index for the base with 6 accents
 * @return The size of the index
 */
int VietVoice::WordDatabase::GetIndex6AccentsSize ()
{
	return _index6Accents.size ();
}

		

/**
 * Obtains a word from the database of words with 6 accents.
 * @param i The index of the word
 * @return The data of the desired word encapsulated in a Word object
 */
VietVoice::Word * VietVoice::WordDatabase::GetWord6Accents(int i,u_int type)
{
	std::vector <IndexElements> index6Accents;
	std::wstring databaseMain;
	if(type==1)
	{
		index6Accents=_index6Accents;
		databaseMain=_databaseMain;
	}
	else
	{
		index6Accents=_index6Accents2;
		databaseMain=_databaseMain2;
	}
	if (index6Accents[i]._posFirstWord == -1)
		return NULL;


	//::MessageBox(NULL, StringHelper::ToString().c_str(), L"Volume | Volume too high...", MB_OK); // VT

	VietVoice::Word * word = NULL;
	VietVoice::Word part1 = NULL; // [them *]
	VietVoice::Word part2 = NULL; // [them *]
	try
	{		
		{
			File::Reader reader (databaseMain);
			reader.SkipBytes(index6Accents[i]._posFirstWord); // Use the index to skip the data before the desired word			
			part1.Load (reader); // [part1]			
		}
		{
			File::Reader reader (databaseMain);
			reader.SkipBytes(index6Accents[i]._posSecondWord); // Use the index to skip the data before the desired word
			part2.Load (reader); // [part2]
		}	
		float percent1 = index6Accents[i]._percent1 ; // VS
		float percent2 = index6Accents[i]._percent2 ; // VS
		if(percent1 <= 0) percent1 = 1;
		if(percent2 <= 0) percent2 = 1;

		float percentVolumePU1 = index6Accents[i]._percentVolume1 ; // VS
		float percentVolumePU2 = index6Accents[i]._percentVolume2 ; // VS

		word = new Word (part1, percent1, part2, percent2,percentVolumePU1,percentVolumePU2,i,6,index6Accents[i]._posFirstWord,index6Accents[i]._posSecondWord);
		part1 = NULL ; // []
		part2 = NULL ; // []
	}
	catch (File::Exception ex)
	{
		if (word != NULL)
		{
			delete word; //// [[]]
			word = NULL;
		}
		throw  VietnameseTTException(L"Error while trying to get a word from the database.");
	}
	return word ;
}

/**
 * Obtains a word from the database of words with 2 accents.
 * @param i The index of the word
 * @return The data of the desired word encapsulated in a Word object
 */
VietVoice::Word * VietVoice::WordDatabase::GetWord2Accents(int i,u_int type) 
{
	std::vector <IndexElements> index2Accents;
	std::wstring databaseMain;
	if(type==1)
	{
		index2Accents=_index2Accents;
		databaseMain=_databaseMain;
	}
	else
	{
		index2Accents=_index2Accents2;
		databaseMain=_databaseMain2;
	}



	if (index2Accents[i]._posFirstWord == -1)
		return NULL;
	VietVoice::Word * word = NULL;
	VietVoice::Word part1;
	VietVoice::Word part2;
  try 
  {
		{
			File::Reader reader (databaseMain);
			reader.SkipBytes(index2Accents[i]._posFirstWord); // Use the index to skip the data before the desired word
			part1.Load (reader);
		}

		{
			File::Reader reader (databaseMain);
			reader.SkipBytes(index2Accents[i]._posSecondWord); // Use the index to skip the data before the desired word
			part2.Load (reader);
		}


//		float percent1 = _index2Accents[i]._percent1 - m_percentageAdjust; // VX
//		float percent2 = _index2Accents[i]._percent2 - m_percentageAdjust; // VX
		float percent1 = index2Accents[i]._percent1 ; // VS
		float percent2 = index2Accents[i]._percent2 ; // VS
		if(percent1 <= 0) percent1 = 1;
		if(percent2 <= 0) percent2 = 1;


		float percentVolumePU1 = index2Accents[i]._percentVolume1 ; // VS
		float percentVolumePU2 = index2Accents[i]._percentVolume2 ; // VS


		word = new Word (part1, percent1, part2, percent2,percentVolumePU1,percentVolumePU2,i,2,index2Accents[i]._posFirstWord,index2Accents[i]._posSecondWord);
		

	}
	catch (File::Exception ex) 
	{
		if (word != NULL)
		{
			delete word; //// [[]]
			word = NULL;
		}
		throw  VietnameseTTException(L"Error while trying to get a word from the database.");
	}

	return word;
}


/**
* Obtains a word from the database for extra words.
* @param name the name of the word
* @return The data of the desired word encapsulated in a Word object
*/
VietVoice::Word * VietVoice::WordDatabase::GetExtraWord (std::wstring name,u_int type)
{


	std::map <std::wstring, int> extraWords;
	std::wstring databaseExtra;
	std::vector <int> indexExtra;
	if(type==1)
	{
		extraWords=_extraWords;
		databaseExtra=_databaseExtra;
		indexExtra=_indexExtra;
	}
	else
	{
		extraWords=_extraWords;
		databaseExtra=_databaseExtra2;
		indexExtra=_indexExtra2;
	}

	

	if (extraWords.find (name) == extraWords.end ())
	{
		return NULL;
	}
	else
	{
		///////Thuan add 14-02-2020////////////
		if(name.compare(L"web")==0)
		{
			return NULL;
		}
	}

	int i = extraWords [name] ; //  Gets the position of the word in the base
	
	VietVoice::Word * word = NULL;

	try
	{
			//::MessageBox(NULL, StringHelper::ToString(databaseExtra).c_str(), L"Before Speaking file", MB_OK); // VT
		File::Reader reader (databaseExtra);
		reader.SkipBytes(indexExtra[i]); // Use the index to skip the data before the desired word
		word = new Word();
		word->Load (reader);
	}
	catch (File::Exception ex) 
	{
		if (word != NULL)
		{
			delete word; //// [[]]
			word = NULL;
		}
		throw VietnameseTTException(L"Error while trying to get a word from the database.");
	}

	return word;
}


////////////// Thuan add 12-02-2020/////////////////////////
VietVoice::Word * VietVoice::WordDatabase::GetVeryShortWord (std::wstring name)
{
	
	//::MessageBox(NULL, StringHelper::ToString(name).c_str(), L"Volume | Volume too high...", MB_OK);
	if (_veryShortWord.find (name) == _veryShortWord.end ())
		return NULL;
	
		

	int i = _veryShortWord [name] ; //  Gets the position of the word in the base
	
	VietVoice::Word * word = NULL;

	try
	{
		File::Reader reader (_databaseVeryShortWord);
		reader.SkipBytes(_indexVeryShortWord[i]); // Use the index to skip the data before the desired word
		word = new Word();
		word->Load (reader);
	}
	catch (File::Exception ex) 
	{
		if (word != NULL)
		{
			delete word; //// [[]]
			word = NULL;
		}
		throw VietnameseTTException(L"Error while trying to get a word from the database.");
	}

	return word;
}
///////////////////END Thuan add///////////////


/**
* Changes the database used by the program.
*
* Parameters:
	The various paths to the various parts of the database.
*/
void VietVoice::WordDatabase::SetDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, std::wstring extra)
{
	
	_databaseMain = databaseMain;
	_databaseExtra = databaseExtra;
	
	LoadIndex6Accents(index6Accents);
	LoadIndex2Accents (index2Accents);
	PrepareExtraWords (extra);
	LoadIndexExtra (indexExtra);
}


//Thuan add 12-02-2020///////////////
void VietVoice::WordDatabase::SetDatabase (std::wstring index6Accents, std::wstring index2Accents, std::wstring indexExtra, std::wstring databaseMain, std::wstring databaseExtra, 
										   std::wstring extra,std::wstring silence,u_int type)
{
	if(type==1)
	{
		_databaseMain = databaseMain;
		_databaseExtra = databaseExtra;
	}
	else
	{
		_databaseMain2 = databaseMain;
		_databaseExtra2 = databaseExtra;
	}
	
	LoadIndex6Accents(index6Accents,type);
	LoadIndex2Accents (index2Accents,type);
	PrepareExtraWords (extra);
	LoadIndexExtra (indexExtra,type);

	std::vector<std::wstring> _tmp;
	//File::Reader::ReadTextFileLines( _tmp, silence, L"\t\r\n" );

	 if(type==1)
	 {
		_pathSilenceNomalWord=silence;
	 }
	 else
	 {
		_pathSilenceShortWord=silence;
	 }


	//for(int i=0;i<_tmp.size();i++)
	//{
	//	std::vector<std::wstring> token;
	//	StringHelper::Tokenize(token,_tmp[i],L"_");
	//	std::size_t pos = 0 ;
 //       int number = _wtoi( token[1].c_str() ) ;


	//	//if(token[0]==L"thành")
	//	//{
	//	//	::MessageBox(NULL, StringHelper::ToString(number).c_str(), L"Pre-Processing save file", MB_OK); // VT
	//	//}

	//	 std::pair< std::wstring, int > tmpPair (token[0],number);
	//	 if(type==1)
	//	 {
	//		_listSilence.insert(tmpPair) ;
	//	 }
	//	 else
	//	 {
	//		_listSilence2.insert(tmpPair) ;
	//	 }
	//}

}
//////////////////End Thuan add//////////////////

//Thuan add 12-02-2020///////////////
void VietVoice::WordDatabase::SetDatabaseVeryShortWord (std::wstring index, std::wstring database,std::wstring pathFileVeryShortWord,std::wstring silence)
{
	_databaseVeryShortWord=database;	
	std::vector<std::wstring> _tmp;
	File::Reader::ReadTextFileLines( _tmp, pathFileVeryShortWord, L"\t\r\n" );
	
	for(int i=0;i<_tmp.size();i++)
	{
		std::vector<std::wstring> token;
		StringHelper::Tokenize(token,_tmp[i],L" ");	
		 std::pair< std::wstring, int > tmpPair (token[0], i);
		 _veryShortWord.insert(tmpPair) ;
	}
	LoadIndexVeryShortWord(index);
	_pathSilenceVeryShortWord=silence;
	//std::vector<std::wstring> _tmp1;
	//File::Reader::ReadTextFileLines( _tmp1, silence, L"\t\r\n" );
	/*_listSilenceVeryShort.clear();
	for(int i=0;i<_tmp1.size();i++)
	{
		std::vector<std::wstring> token;
		StringHelper::Tokenize(token,_tmp1[i],L"_");
		std::size_t pos = 0 ;
        int number = _wtoi( token[1].c_str() ) ;

		 std::pair< std::wstring, int > tmpPair (token[0],number);
		
			_listSilenceVeryShort.insert(tmpPair) ;
		 
	}*/
}
//////////////////End Thuan add//////////////////

/**
* Loads the binary index for the database with 6 accents.
* @param  the location  of the binary index file
*/
void VietVoice::WordDatabase::LoadIndex6Accents(std::wstring filename) 
{unsigned int i = 0;
unsigned int size = 0;
//::MessageBox(NULL, StringHelper::ToString(filename).c_str(), L"Vietnamien Voice", MB_OK); // VT
	try
	{
////////FILE *tmp_fptr=NULL;
////////tmp_fptr=fopen("6Accents.txt","wb");

		File::Reader reader (filename);


		if (reader.ReadLongJava() != INDEX) //  Check the "magic number
		{			
	
			throw VietnameseTTException(L"Wrong magic number in index 1");
		}

	
		size = reader.ReadLongJava ();
//::MessageBox(NULL, StringHelper::ToString(size).c_str(), L"Word Database", MB_OK); // VT
			
		_index6Accents.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());
		
		
		for (i = 0; i < size ; i++)
		{
			
			IndexElements elem;
			
			elem._posFirstWord = reader.ReadLongJava() - 5;
			elem._posSecondWord = reader.ReadLongJava () - 5;
//			elem._percent1 = reader.ReadByte () - 5; // VX
//			elem._percent2 = reader.ReadByte () - 5; // VX
// VT
			elem._percent1 = float (reader.ReadFloatJava () - 500) / 100 ;
			elem._percent2 = float (reader.ReadFloatJava () - 500) / 100 ;
			elem._percentVolume1=float (reader.ReadShortJava ()) ;
			elem._percentVolume2=float (reader.ReadShortJava ()) ;

			//elem._percentVolume1=100;
			//elem._percentVolume2=100;

//			if (i==size-1) {
//::MessageBox(NULL, StringHelper::ToString(elem._percent1).c_str(), L"Word Database", MB_OK); // VT
//::MessageBox(NULL, StringHelper::ToString(elem._percent2).c_str(), L"Word Database", MB_OK); // VT
//			}
// End VT
////////fprintf(tmp_fptr,"\n%d %d %2f %2f",elem._posFirstWord, elem._posSecondWord, elem._percent1, elem._percent2); // VT

			_index6Accents.push_back (elem);


		}

		
	}
	catch (File::Exception ioe)
	{
		throw VietnameseTTException(ioe.GetMsg());//L"Error, could not load the index 1 ");
	}
}


///////////Thuan add 12-02-2020//////////////
void VietVoice::WordDatabase::LoadIndex6Accents(std::wstring filename,u_int type) 
{unsigned int i = 0;
unsigned int size = 0;
	try
	{
		File::Reader reader (filename);
		if (reader.ReadLongJava() != INDEX) //  Check the "magic number
		{	
			throw VietnameseTTException(L"Wrong magic number in index 1");
		}	
		size = reader.ReadLongJava ();
		
		if(type==1)
			_index6Accents.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());
		else
			_index6Accents2.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());	
		
		for (i = 0; i < size ; i++)
		{
			IndexElements elem;			
			elem._posFirstWord = reader.ReadLongJava() - 5;
			elem._posSecondWord = reader.ReadLongJava () - 5;
			elem._percent1 = float (reader.ReadFloatJava () - 500) / 100 ;
			elem._percent2 = float (reader.ReadFloatJava () - 500) / 100 ;
			elem._percentVolume1=float (reader.ReadShortJava ()) ;
			elem._percentVolume2=float (reader.ReadShortJava ()) ;
		
			//elem._percentVolume1=100;
			//elem._percentVolume2=100;


			if(type==1)
				_index6Accents.push_back (elem);
			else
				_index6Accents2.push_back (elem);
		}		
	}
	catch (File::Exception ioe)
	{
		throw VietnameseTTException(ioe.GetMsg());//L"Error, could not load the index 1 ");
	}
}
/////////////End Thuan add////////////////////////////////////////////


/**
* Loads the binary index for the database with 2 accents.
* @param  the location  of the binary index file
*/
void VietVoice::WordDatabase::LoadIndex2Accents(std::wstring filename)
{
//::MessageBox(NULL, StringHelper::ToString(filename).c_str(), L"Vietnamien Voice", MB_OK); // VT
	try
	{
		////////FILE *tmp_fptr=NULL; // 25-1-2013
		////////tmp_fptr=fopen("2Accents.txt","wb");

		
/*
//for(int k=0;k<v_out_info.size();k++)
//{	
//	fprintf(tmp_fptr,"%f\n",v_out_info[k]);
//}
*/

		File::Reader reader (filename);

		if (reader.ReadLongJava() != INDEX) //  CHeck the "magic number
		{
			throw VietnameseTTException(L"Wrong magic number in index 1");
		}
		
		int size = reader.ReadLongJava();
		
		_index2Accents.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());
		for (int i = 0; i < size; i++)
		{

			IndexElements elem;

			elem._posFirstWord = reader.ReadLongJava() - 5;
			elem._posSecondWord = reader.ReadLongJava () - 5;
//			elem._percent1 = reader.ReadByte () - 5; // VX
//			elem._percent2 = reader.ReadByte () - 5; // VX
			elem._percent1 = float (reader.ReadFloatJava () - 500) / 100 ; // VS
			elem._percent2 = float (reader.ReadFloatJava () - 500) / 100 ; // VS
			
			elem._percentVolume1=float (reader.ReadShortJava ()) ;
			elem._percentVolume2=float (reader.ReadShortJava ()) ;

			//elem._percentVolume1=100;
			//elem._percentVolume2=100;

			////////fprintf(tmp_fptr,"\n%d %d %2f %2f",elem._posFirstWord, elem._posSecondWord, elem._percent1, elem._percent2); // VT
			

			
//			if (i==size-1) {
//::MessageBox(NULL, StringHelper::ToString(elem._percent1).c_str(), L"Vietnamien Voice", MB_OK); // VT
//::MessageBox(NULL, StringHelper::ToString(elem._percent2).c_str(), L"Vietnamien Voice", MB_OK); // VT
//			}

			//if (i == 260)
			_index2Accents.push_back (elem);		
		}
	}
	catch (File::Exception ioe)
	{
		throw VietnameseTTException(ioe.GetMsg());//L"Error, could not load the index 1 ");
	}
}

///////////Thuan add 12-02-2020//////////////////////////////
void VietVoice::WordDatabase::LoadIndex2Accents(std::wstring filename,u_int type)
{
	try
	{
		File::Reader reader (filename);

		if (reader.ReadLongJava() != INDEX) //  CHeck the "magic number
		{
			throw VietnameseTTException(L"Wrong magic number in index 1");
		}		
		int size = reader.ReadLongJava();
		if(type==1)
			_index2Accents.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());
		else
			_index2Accents2.resize (0);//.erase (_index6Accents.begin (), _index2Accents.end ());
		for (int i = 0; i < size; i++)
		{
			IndexElements elem;
			elem._posFirstWord = reader.ReadLongJava() - 5;
			elem._posSecondWord = reader.ReadLongJava () - 5;
			elem._percent1 = float (reader.ReadFloatJava () - 500) / 100 ; // VS
			elem._percent2 = float (reader.ReadFloatJava () - 500) / 100 ; // VS

			elem._percentVolume1=float (reader.ReadShortJava ()) ;
			elem._percentVolume2=float (reader.ReadShortJava ()) ;
			
			//elem._percentVolume1=100;
			//elem._percentVolume2=100;

			if(type==1)
				_index2Accents.push_back (elem);
			else
				_index2Accents2.push_back (elem);
		}
	}
	catch (File::Exception ioe)
	{
		throw VietnameseTTException(ioe.GetMsg());//L"Error, could not load the index 1 ");
	}
}
/////////// End Thuan add ///////////////////////////////////////////////



/**
* Loads the binary index for the database with extra words.
* @param  the location  of the binary index file
*/
void VietVoice::WordDatabase::LoadIndexExtra(std::wstring filename)
{	
//::MessageBox(NULL, StringHelper::ToString(filename).c_str(), L"Vietnamien Voice", MB_OK); // VT
	try
	{
		File::Reader  reader (filename);

		if (reader.ReadLongJava() != INDEX) // Check the "magic number
		{
			throw VietnameseTTException (L"Wrong magic number in index extra");

		}

		int size = reader.ReadLongJava();

		_indexExtra.resize (0);//.erase (_indexExtra.begin (), _indexExtra.end ());
	

		for (int i = 0; i < size ; i++)
		{
			_indexExtra.push_back (reader.ReadLongJava());
		}
	}
	catch (File::Exception ioe)
	{
		 throw  VietnameseTTException(L"Error, could not load the index extra");
	}
}

/////////////// Thuan add 12-02-2020///////////////////////////////
void VietVoice::WordDatabase::LoadIndexExtra(std::wstring filename,u_int type)
{	
	try
	{
		File::Reader  reader (filename);
		if (reader.ReadLongJava() != INDEX) // Check the "magic number
		{
			throw VietnameseTTException (L"Wrong magic number in index extra");
		}		
		int size = reader.ReadLongJava();
		if(type==1)
			_indexExtra.resize (0);//.erase (_indexExtra.begin (), _indexExtra.end ());
		else
			_indexExtra2.resize (0);//.erase (_indexExtra.begin (), _indexExtra.end ());
	

		for (int i = 0; i < size ; i++)
		{
			if(type==1)
				_indexExtra.push_back (reader.ReadLongJava());
			else
				_indexExtra2.push_back (reader.ReadLongJava());
		}
	}
	catch (File::Exception ioe)
	{
		 throw  VietnameseTTException(L"Error, could not load the index extra");
	}
}


void VietVoice::WordDatabase::LoadIndexVeryShortWord(std::wstring filename)
{	
	try
	{
		File::Reader  reader (filename);
		if (reader.ReadLongJava() != INDEX) // Check the "magic number
		{
			throw VietnameseTTException (L"Wrong magic number in index veryshortword");
		}
		int size = reader.ReadLongJava();

		

		_indexVeryShortWord.resize (0);
		for (int i = 0; i < size ; i++)
		{
			_indexVeryShortWord.push_back (reader.ReadLongJava());
		}
	}
	catch (File::Exception ioe)
	{
		 throw  VietnameseTTException(L"Error, could not load the index veryshortword");
	}
}


//////////End Thuan add//////////////////////


/**
* Read all extra words from a file
* @param filename The name of the file
*/
void VietVoice::WordDatabase::PrepareExtraWords (std::wstring filename)
{
	std::wstring name = L"";
	std::wstring line = L"";

	_extraWords.clear ();

	try 
	{
		File::Reader reader (filename);
		long count = reader.ReadLong (); // Read the number of lines in the file

		for (int i = 0; i < count; i++)  // For each line
		{
			line = reader.ReadWString();
			StringHelper::RemoveSpace (line);

			if (line != L"")
			{
				std::vector <std::wstring> tokens;
				StringHelper::Tokenize (tokens, line, L" \t\n");

				for (std::vector<std::wstring>::iterator iter = tokens.begin (); iter != tokens.end(); iter++)
				{
					 name = *iter; //*ptrName;
					 std::pair< std::wstring, int > tmpPair (name, i);
					 _extraWords.insert(tmpPair) ;
				}
			}
		}
	}
	catch (File::Exception ex) // Something wrong happened
	{
		throw VietnameseTTException(L"Something went wrong while reading the abbreviation file in TokenToWords" + ex.GetMsg ());
	} 
  
}


std::vector<long> VietVoice::WordDatabase::GetSilence (std::wstring word,u_int type)
{
	//::MessageBox(NULL, StringHelper::ToString(SoSample).c_str(), L"Pre-Processing save file", MB_OK); // VT 

	std::vector<long> kq;
	std::wstring path;
	if(type==1)
	{
		path=_pathSilenceNomalWord;

		/*if (_listSilence.find (word) != _listSilence.end ())
			SoSample=_listSilence[word];*/
	}
	else
	{
		if(type==2)
		{
			path=_pathSilenceShortWord;
			/*if (_listSilence2.find (word) != _listSilence2.end ())
				SoSample=_listSilence2[word];*/
		}
		else
		{
			path=_pathSilenceVeryShortWord;
			/*if (_listSilenceVeryShort.find (word) != _listSilenceVeryShort.end ())
				SoSample=_listSilenceVeryShort[word];*/
			
		}
	}

	//::MessageBox(NULL, StringHelper::ToString(SoSample).c_str(), L"Pre-Processing save file", MB_OK); // VT

	std::wstring tu=StringHelper::ChuyenTu(word);
	//::MessageBox(NULL, StringHelper::ToString(tu).c_str(), L"Pre-Processing save file", MB_OK); // VT 
	try
	{
		
		File::Reader reader (path+tu);
		long SoSample = reader.ReadLong ();
		kq.push_back(SoSample*2);
		long TyLeVoLume= reader.ReadLong ();
		kq.push_back(TyLeVoLume);		
	}
	catch (File::Exception ex) // Something wrong happened
	{
		if(word.length()==1)
		{
			long SoSample=400;
			if(type==2 || type==3)
			{
				SoSample=240;
			}
			kq.clear();
			kq.push_back(SoSample);
			kq.push_back(100);
		}
		else
		{
			kq.clear();
			kq.push_back(0);
			kq.push_back(100);
		}
		//throw VietnameseTTException(L"Lỗi load silence từ: " + word);
	} 
  

	
	return kq;
  
}

