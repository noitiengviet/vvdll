#if !defined (CONCATENATOR_H)
#define CONCATENATOR_H
#define PI   3.14159265358979f // VT
#define halfwin_sec 5 // VT //// 5 MyWsola // Ha noi 1.5
#define deta_sec	5 // VT //// 5 MyWsola // Ha noi 1.5
#define	level_0 0 
#define	level_1_pos 5  //  2 7  10
#define	level_1_neg -5   // -2 7  10
#define	level_2_pos 8  //  3 11 15
#define	level_2_neg -8  // -4 11 15


// #define v_num_of_segments 10 // VT // 3 // 6 ////////// 4 -> Good
#pragma warning( disable : 4786 ) 

#include <string.h>
#include "silenceAdder.h"
#include <fstream> // 13-8-2012
#include "useunicode.h"
#include "worddatabase.h"
#include "stringhelper.h"
#include "treatment.h"
#include "directsound.h"
#include <stdio.h> // 13-8-2012
#include <time.h> // 13-8-2012
#include <algorithm>
namespace VietVoice
{
	/**
	 * CODE MODIFICATIONS AND BUG CORRECTIONS
	 * --------------------------------------
	 * November 5 2005:  Converted most of the treat method.
	 * November 8 2005:  Converted MP3 class.  Converted saveSpokenData method.
	 * November 25 2005:  Added private copy constructor and = operator for protection
	 * March 4 2006:  Seperated implementation in source and header
	 * June 9 2006: Change the methods name so they start by a capital letter.
	 * June 14 2006:  Upgraded the comments and spacing in concatenator.cpp file.
	 * September 11 2006:  Made modification to allow saving an mp3 file wherever we want!
	 */

	/**
	 * Class Name:         Concatenator.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 15 2004
	 *
	 * Description:        Class use to play an mp3 file
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */
    class WavePlayer
    {
	public:

		/**
		* Constructor
		*/
		WavePlayer ();

		/**
		* Play a wave represented by the given data.
		*
		* Parameters:
		*
		* vector<unsigned char> spokenData:  the wave data
		*/
		void Play (std::vector<unsigned char> & spokenData);

		/**
		* Stop playing the current wave
		*/
		void Stop ();

	private:

		WavePlayer  (const WavePlayer & concatenator);
		void operator = (const WavePlayer & concatenator);

	private:

		HWAVEOUT _hWave;
	};

	/**
	 * Class Name:         Concatenator.java
	 *
	 * Author(s):          Benoit Lanteigne
	 *
	 * Creation Date:      December 15 2004
	 *
	 * Description:        Concatenates the various sound data and plays it
	 *
	 * Copyright:          All rights reserved Moncton University
	 *                     Tang-Ho L� Ph. D. - Computer Science Department
	 *
	 */
	class Concatenator : public Treatement
	{
		public:



typedef struct
{
	//char *Fname;

	//char Riff[5];
	unsigned long Rifflen;
	//char Wav[5];
	//char Fmt[5];
	//unsigned long Fmtlen;
	//short Format;
	short Nchan;
	unsigned long Fs;
	//unsigned long	bytesPerSec;
	//short	blockAlign;
	//short  BitsPerSample;
	//char  Data[5];			// "DATA"
	unsigned long DataLen;	// "DATA"
	
	long Nsamples;
	//short Width;
	float *data;

}WAV_INFO;

//FILE* GET_WAV_INFO(char * filename,WAV_INFO * wav_info);


			/**
			* Constructor
			*/
			Concatenator();

			/**
			* Destructor
			*/
			virtual ~Concatenator ();

			/**
			* Concategnate the sound data and play it
			*
			* Return value: The toSpoke data after the treatement
			*
			* Parameters:
			*
			* @param ToBeSpoken & toSpoke: Reference to the Data representing the words to be spoken
			*/
			void Treat (ToBeSynthetise & toBeSynthetise);


			/**
			* Stop playing and mp3 file
			*/
			void Stop ();
			void Get_BS1 (long v_bs); // VT
			void Get_BS2 (long v_bs); // VT
			void VietVoice::Concatenator::Get_VL (long v_vl); // VT
			void VietVoice::Concatenator::Get_MV (long v_mv); // VT
void VietVoice::Concatenator::Get_SV1 (long v_sv1); // VT
void VietVoice::Concatenator::Get_SV21 (long v_sv21); // VT
void VietVoice::Concatenator::Get_SV22 (long v_sv22); // VT
void VietVoice::Concatenator::Get_SV3 (long v_sv3); // VT
			void VietVoice::Concatenator::Get_SP (long v_sp); // VT
			void VietVoice::Concatenator::Get_Pitch (long v_ph); //// VT
			void VietVoice::Concatenator::Get_CP (long v_cp); //// VT
			void VietVoice::Concatenator::Get_WS (long v_ws); //// VT
			void VietVoice::Concatenator::Get_WS2 (long v_ws); //// VT
			void VietVoice::Concatenator::Get_light (std::wstring v_light) ; // VT
			void VietVoice::Concatenator::Get_adjective (std::wstring v_adj); // VT
			long VietVoice::Concatenator::ByteToWord (long v_byte) ; // VT
			void VietVoice::Concatenator::Change_Volume (vector<long> & m_word, int v_volume) ;
			void VietVoice::Concatenator::Change_Volume_CW (vector<long> & m_word, int v_volume, int v_percent, int v_type) ;
			void VietVoice::Concatenator::Change_Pitch (vector<long> & m_word, int v_pitch, int v_percent, int v_type) ;
			int VietVoice::Concatenator::v_isStopConsonant(std::wstring v_word, int v_accent) ;// VT

bool VietVoice::Concatenator::v_isNoun(std::wstring v_word) ;
int VietVoice::Concatenator::v_isSameAsSC(std::wstring v_word, int v_accent) ;// VT
void VietVoice::Concatenator::v_getCW1 (long v_cw1) ; // VT
			void VietVoice::Concatenator::v_getCW2 (long v_cw2) ; // VT
			void VietVoice::Concatenator::v_getStopSilence (long v_ss) ; // VT
void VietVoice::Concatenator::GET_FINALE_DATA(WAV_INFO *in_info,WAV_INFO *out_info,float beta,int nsamp);
void VietVoice::Concatenator::ENVELOP_CAL(float *data,long datalen,float **fdata,int nsamp);
long VietVoice::Concatenator::GET_SIMILE_POS_FAST(float * fx1,float * x1, long n1,float * fx2,float * x2, long n2,int nsamp);
int VietVoice::Concatenator::v_isShortStartWord(std::wstring v_word) ; // VT
int VietVoice::Concatenator::v_isSilenceBefore(std::wstring v_word) ; // VT
int VietVoice::Concatenator::v_isSonantVowel(std::wstring v_word) ; // VT

int VietVoice::Concatenator::KiemTraAmVang(std::wstring v_word,u_int type) ; // TT

int VietVoice::Concatenator::v_isBegunWithSonantVowel(std::wstring v_word) ; // VT

int VietVoice::Concatenator::v_isSensitiveConsonant(std::wstring v_word) ; // VT
void VietVoice::Concatenator::v_echo(vector<long> & m_word, unsigned long v_dots, int mType, int mPer) ; // VT
void VietVoice::Concatenator::Get_Dots (long v_dots) ; // VT
void VietVoice::Concatenator::Get_Not_Echo (long v_notEcho) ; // VT
void VietVoice::Concatenator::v_FadeIn (vector<long> & m_word, long v_Fpercent) ; // VT
void VietVoice::Concatenator::v_FadeOut (vector<long> & m_word, long v_Fpercent) ; // VT
void VietVoice::Concatenator::Get_Fade (long v_fade) ; // VT
void VietVoice::Concatenator::Get_Sample (long v_samples) ; // VT
// void VietVoice::Concatenator::_v_FadeWord (vector<long> & m_word, float v_ratio) ; // VT

void VietVoice::Concatenator::Get_Per_Pitch (int v_p_p) ; // VT
void VietVoice::Concatenator::Get_Pitch0 (int v_p0) ; // VT
void VietVoice::Concatenator::Get_Pitch1 (int v_p1) ; // VT
void VietVoice::Concatenator::Get_Pitch2 (int v_p2) ; // VT
void VietVoice::Concatenator::Get_Pitch3 (int v_p3) ; // VT
void VietVoice::Concatenator::Get_Pitch4 (int v_p4) ; // VT
void VietVoice::Concatenator::Get_Pitch5 (int v_p5) ; // VT

void VietVoice::Concatenator::Get_Com0 (int v_c0) ; // VT
void VietVoice::Concatenator::Get_Com1 (int v_c1) ; // VT
void VietVoice::Concatenator::Get_Com2 (int v_c2) ; // VT
void VietVoice::Concatenator::Get_Com3 (int v_c3) ; // VT
void VietVoice::Concatenator::Get_Com4 (int v_c4) ; // VT
void VietVoice::Concatenator::Get_Com5 (int v_c5) ; // VT
long VietVoice::Concatenator::Get_Volume (vector<long> &m_word, int v_percent, int v_type) ; // VT
long VietVoice::Concatenator::Get_Volume_Average (vector<long> &m_word) ; // Thuan them 08/04/2020
void VietVoice::Concatenator::CanBangAmPho (vector<long> &m_word) ; // Thuan them 17/04/2020
void VietVoice::Concatenator::CanBangAmPho2 (vector<long> &m_word) ; // Thuan them 18/04/2020
void VietVoice::Concatenator::CanBangAmPho3 (vector<long> &m_word,int VTBD,int VTKT) ; // Thuan them 22/04/2020

void VietVoice::Concatenator::CanDoiAmLuongGhepTu (vector<long> &m_word, int SoSamplePU1,float percentVoLumePU1,float percentVoLumePU2);// Thuan them 02/05/2020

unsigned long VietVoice::Concatenator::Word_Cut (vector<long> &m_word, int v_percent, int v_type) ; // VT
unsigned long VietVoice::Concatenator::Word_Cut_Thuan(vector<long> &m_word, int v_percent) ; // Thuan them


void VietVoice::Concatenator::Get_cut_begin (long v_cb)  ; // VT
void VietVoice::Concatenator::Get_cut_end (long v_ce) ; // VT
int VietVoice::Concatenator::v_wordLength(std::wstring v_word) ; // VT
void VietVoice::Concatenator::Get_sc_comp (int v_sc) ; // VT
void VietVoice::Concatenator::Compress_Word (vector<long> & tmp_word, int v_percent, int v_accent) ;
void VietVoice::Concatenator::V_Compress_Word (vector<long> & temp_word, int v_percent, int v_accent) ;
void VietVoice::Concatenator::V_WSOLA_Word (WAV_INFO *in_info, WAV_INFO *out_info, float beta,int nsamp) ;
int VietVoice::Concatenator::v_find_content(vector <wstring> v_dic, wstring v_exp); // VT

void VietVoice::Concatenator::v_getLinkingSilence (int v_value);



	private:

			Concatenator  (const Concatenator & concatenator);
			void operator = (const Concatenator & concatenator);

		private:
			WavePlayer _wavePlayer;
			long v_break_silence1; // VT
			long v_break_silence2; // VT
			long v_volume; // VT
			long v_main_volume; // VT
			long v_soft_volume1; // VT
			long v_soft_volume21; // VT
			long v_soft_volume22; // VT
			long v_soft_volume3; // VT
			long v_speed; // VT
			long v_pitch; //// VT
			long vdots; //// VT
			long vnotEcho ; // VT
			long m_fade ; //// VT
			long m_samples ; //// VT
			long v_compress; //// VT
			unsigned long v_wordsize; //// VT
			long v_wordsize2; //// VT
			long v_compound1; //// VT
			long v_compound2; //// VT
			long v_StopSilence ; //// VT
			std::wstring v_lightWords ; // VT
			std::wstring v_adjectiveWords ; // VT
			int v_per_pitch ; // VT
			int v_pitch0 ; // VT
			int v_pitch1 ; // VT
			int v_pitch2 ; // VT
			int v_pitch3 ; // VT
			int v_pitch4 ; // VT
			int v_pitch5 ; // VT

			int v_compress0 ; // VT
			int v_compress1 ; // VT
			int v_compress2 ; // VT
			int v_compress3 ; // VT
			int v_compress4 ; // VT
			int v_compress5 ; // VT
			int v_cut_begin ; // VT
			int v_cut_end ; // VT
			int v_sc_comp ; // VT
			long v_linking_sl;
protected:
		std::vector<std::wstring> v_quantityDictionary;
		std::vector<std::wstring> v_articleDictionary;
		std::vector<std::wstring> v_nounDictionary;
		std::vector<std::wstring> v_verbDictionary;
		std::vector<std::wstring> v_adverbDictionary;

    };
}

#endif CONCATENATOR_H