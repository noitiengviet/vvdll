Thứ Sáu, 06/01/2012, 07:59 (GMT+7) 
Các chính quyền địa phương của Trung Quốc:

Đua nhau vay nợ và lạm dụng 84 tỉ USD

TT - Cơ quan Kiểm toán quốc gia Trung Quốc (NAO) báo động chính quyền các địa phương đã đua nhau vay nợ và sử dụng sai mục đích tới 84 tỉ USD nợ công.


 
Một khu dân cư “ma” ở thành phố Trịnh Châu xây xong không có người ở - Ảnh: china.cn 


Tân Hoa xã đưa tin NAO thông báo đến tháng 6-2011, chính quyền các địa phương Trung Quốc nợ tới 10.700 tỉ NDT (1.700 tỉ USD), tương đương 1/4 GDP của Trung Quốc. Trong đó, các chính quyền sử dụng sai mục đích khoảng 531 tỉ NDT (84 tỉ USD). Thống kê cho thấy các địa phương đã chi 35,1 tỉ NDT mua cổ phiếu, bất động sản. Khoảng 14 tỉ NDT bị biển thủ.

Các địa phương lý giải đã vay hàng trăm tỉ NDT để chi trả cho các dự án nằm trong gói kích thích kinh tế mà Chính phủ Trung Quốc đưa ra hồi năm 2008. Nhưng sau đó các địa phương không nhận được đồng nào. Chính quyền các địa phương còn khẳng định đã đổ nhiều tiền vào các dự án xây dựng trường học, bệnh viện, các chương trình xã hội... nhưng không được chính quyền trung ương chi tiền.

Báo The Standard (Hong Kong) cho biết ngay sau khi chính quyền Trung Quốc công bố gói kích thích kinh tế, hàng ngàn địa phương trên cả nước đã đua nhau nộp hồ sơ vay nợ từ các ngân hàng nhà nước để cung cấp vốn cho các dự án. Các ngân hàng dễ dàng thông qua các khoản vay, nhưng không ai kiểm soát việc số tiền khổng lồ này đi đâu về đâu.

NAO cáo buộc quan chức các địa phương đã dùng đủ mọi chiêu trò để lạm dụng số tiền khổng lồ này. Trên giấy tờ, số tiền đó đi vào các dự án cơ sở hạ tầng khổng lồ, song suốt ba năm qua, các dự án đường cao tốc chưa hề được khởi công, không bao giờ có xe chạy. Các thị trấn “ma” với những khu dân cư đông đúc chỉ tồn tại trên giấy. Những tòa chung cư đã được xây dựng hầu như không có người mua.

Thậm chí các địa phương còn làm hồ sơ giả mạo, lập các công ty “ma” để hợp thức hóa việc tiêu “tiền chùa”. NAO phát hiện khoảng 1.033 công ty do các địa phương lập ra có nhiều vấn đề như sổ sách tài chính giả, không có vốn điều lệ, các hoạt động chi tiêu hỗn loạn, không có sự kiểm soát. Riêng các công ty này đã vung vít khoảng 244,15 tỉ NDT.

“Thậm chí nếu không còn tham nhũng thì giờ đây chính quyền các địa phương cũng rất khó trả số nợ này” - báo The Standard dẫn lời nhà kinh tế Hong Kong Diêu Vĩ thuộc Ngân hàng Societe Generale nhận định. Theo Ủy ban ban hành nghị định ngân hàng Trung Quốc, chính quyền các địa phương đang nắm giữ 80% số nợ vay của các ngân hàng ở Trung Quốc. Hồi tháng 10-2011, Chính phủ Trung Quốc đã cho phép bốn địa phương là Thượng Hải, Chiết Giang, Quảng Đông và Thâm Quyến phát hành trái phiếu với hi vọng sẽ huy động được vốn để trả nợ.

Giới đầu tư lo ngại số nợ khổng lồ mà các địa phương đang ôm có thể làm rung chuyển hệ thống ngân hàng Trung Quốc. “Nền kinh tế Trung Quốc đang đứng trước ngưỡng cửa phá sản và mỗi địa phương đều là một Hi Lạp” - CNN dẫn lời giáo sư tài chính Larry Lang thuộc Đại học Trung Quốc ở Hong Kong cảnh báo.

Theo NAO, hiện chính quyền trung ương đang lập kế hoạch giải quyết đống nợ khổng lồ của các địa phương để hạn chế các nguy cơ tài chính bùng nổ. Giáo sư Zhao Xijun thuộc Đại học Nhân dân Trung Quốc ở Bắc Kinh cho rằng chính quyền Trung Quốc có đủ nguồn lực để ngăn chặn nguy cơ hàng loạt địa phương vỡ nợ.

Tân Hoa xã cho biết sau các cuộc kiểm toán quốc gia năm 2010 đã có 139 trường hợp quan chức bị kết tội, 699 người khác bị kỷ luật và 81 người đã bị bỏ tù vì lạm dụng tiền nhà nước.

MỸ LOAN

