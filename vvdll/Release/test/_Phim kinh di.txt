Cuộc đối đầu không cân sức giữa 8 hành khách và những hồn ma

Rợn gáy với phim kinh dị 3D 
Thứ Ba, 19/06/2012, 04:36 AM (GMT+7)
Sự kiện: Phim chiếu rạp 2012
(Phim) - Bộ phim là hành trình đáng sợ của một nữ tiếp viên và 8 hành khách trên chuyến bay bị nguyền rủa bởi hồn ma người đã chết.
Phim 24H cập nhật nhanh nhất các tin tức điện ảnh, chuyện hậu trường và thông tin nóng hổi về các Ngôi sao

Nhân vật chính của bộ phim Chuyến bay định mệnh là New (Marsha Watanapanich), một nữ tiếp viên hàng không trẻ và xinh đẹp.

10 năm trước, chuyến bay của New không may gặp nạn khiến tất cả hành khách đều thiệt mạng, chỉ duy nhất mình cô sống sót. May mắn thoát khỏi lưỡi hái tử thần nhưng lúc nào New cũng bị ám ảnh rằng cô sẽ phải chịu đựng một sự trả thù rất khủng khiếp cho tai nạn thảm khốc này. Trước đó, New đã cố gắng cảnh báo nhưng không một ai tin những gì cô nói bởi họ cho rằng cô bị chấn động tâm lý.

Háo hức trở lại với công việc yêu thích, New không biết được những tai họa khủng khiếp đang rình rập cô ở phía trước

Sau những đợt điều trị kéo dài hàng năm trời, New quyết định trở lại với công việc yêu thích. Tuy nhiên, cô không ngờ rằng chuyến bay đầu tiên của mình trên chiếc Boeing 407 cũng là chuyến bay mang nỗi ám ảnh từ quá khứ. Khi New nhận ra đây chính là chiếc máy bay gặp nạn 10 năm trước đã được sơn sửa lại thì cũng là lúc cô phát hiện sự có mặt của những hành khách “không mời mà đến”, những hồn ma của người chết đang trở về báo thù.

Linh cảm của New đã trở thành sự thật và mọi cố gắng đều trở nên quá muộn bởi chuyến bay đã cất cánh. Ở độ cao 30.000 feet so với mặt đất, lưỡi hái định mệnh đang chờ New và 8 hành khách vô tội. Họ không còn cách nào khác là phải chiến đấu với những hồn ma để dành giật sự sống.

Những hồn ma của người chết đang trở về báo thù

Chuyến bay định mệnh (Dark Flight) là tác phẩm thứ ba của đạo diễn Isara Nadee và biên kịch  Kongkiat Komesiri, hai thành viên trong nhóm Ronin Team - nhóm các đạo diễn tài năng đã có công lao không nhỏ trong việc quảng bá phim kinh dị “made in Thailand” đến khán giả thế giới. 

Phim được Isara Nadee và Kongkiat Komesiri lấy tư liệu từ những vụ tai nạn của ngành Hàng không Thái Lan trong nhiều năm qua.

Nỗi hoảng loạn bao trùm lên những hành khách trên chuyến bay

Phim đã được công chiếu tại Thái Lan và thu về hơn 1,1 triệu USD. Bản quyền phát hành của bộ phim cũng đã được bán cho nhiều nước trên thế giới như: Anh, Hồng Kông, Nhật Bản, Malaysia, Indonesia, Singapore, Đài Loan, Campuchia...

Bộ phim sẽ được công chiếu tại Việt Nam từ ngày 22/6.

Trailer phim Chuyến bay định mệnh
